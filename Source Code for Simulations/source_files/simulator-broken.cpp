/* 
 * Evorobot* - A software for running evolutionary robotics experiments
 * Copyright (C) 2005 
 * LARAL, Institute of Cognitive Science and Technologies, CNR, Roma, Italy 
 * http://laral.istc.cnr.it

 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "public.h"
#include "robot-env.h"

double  P90,P180,P360;      // Pgreco constants

extern char robotwstatus[]; // stat message within rend_robot widget


/*
 * just set few costants
 */
void
SETP ()

{

  P90 =    asin(1.0);
  P180=2.0*asin(1.0);
  P360=4.0*asin(1.0);
}

/*
 * trasform radiants to degrees
 */
double
RADtoDGR(double r)

{
   double x;

   SETP();
   if (r < 0.0)
    x=360.0-(r*(-1.0)/P90)*90;
     else
    x=(r/P90)*90;
   return(x);
 }


/*
 * transform degrees to radiants
 */
double
DGRtoRAD(double d)

{
    double   x;

    x=(d/90)*asin(1.0);
    return(x);
}

/*
 * compute the deltax given an angle and a distance
 */
float
SetX(float x, float d, double angle)


{
    float   res;

    res =  (float) (x + d * cos(DGRtoRAD(angle)) + 0.000001);
    return(res);
}

/*
 * compute the deltay given an angle and a distance
 */
float
SetY(float y, float d, double angle)

{
    float   res;

    res = (float) (y + d * sin(DGRtoRAD(angle)) + 0.000001);
    return(res);

}


/*
 * return the distance between a point and a line
 */
double
linedist(float px,float py,float ax,float ay,float bx,float by)

{
     double d1,d2,d3;

     d1 = ((px-ax)*(px-ax)) + ((py-ay)*(py-ay));
     d2 = ((px-bx)*(px-bx)) + ((py-by)*(py-by));
     d3 = ((ax-bx)*(ax-bx)) + ((ay-by)*(ay-by));

     return(sqrt(d1 - (((d2 - d3 - d1) * (d2 - d3 - d1)) / (4 * d3))));

}



/*
 * normalize and angle between 0 and 360
 */
double
normang(double a)

{

   int v;

   v = 0;
   while((a < 0.0 || a > 360.0) && v < 10)
    {
      if (a < 0.0)
	a += 360.0;
      if (a > 360.0)
	a -= 360.0;
      v++;
    }
   if (v == 10)
	display_warning("normang: angle out of range");
   return(a);
}


/*
 * return the angle between two points with a given distance
 */

double
mang(int x, int y, int x1, int y1, double dist)

 {

   double    angolo;
   int       dxang,dyang;
   double    buffer;
   double    alfa,pi2;
   double    temp;

   if  (x==x1 && y==y1)
      angolo=0.0;
     else
      {
       dxang=(x1-x);
       dyang=(y1-y);
       buffer=abs(dyang);
       pi2 =asin(1.0);
       temp = buffer/dist;
       if (temp>1.0) temp=1.0;
       alfa=asin(temp);
       alfa=(alfa/pi2)*90;
       if (dxang>0)
	     {
              if (dyang>0);
	      if (dyang<0)
                 alfa=360-alfa;
	     }
        if (dxang<0)
	     {
              if (dyang>0) alfa=180-alfa;
	      if (dyang<0) alfa=alfa+180;
	      if (dyang==0)alfa=180;
	      }
	 if (dxang==0)
	     {
		 if (dyang>0) ;
		 if (dyang<0) alfa=270;
		 if (dyang==0)alfa=0;
	     }
	  angolo=alfa;
       }
     return(angolo);
}




/*
 * it calculate the relative angle
 */
double
mangrel(double absolute,double kepera)



{

    double  relative;


    relative = absolute - kepera;

    if (relative > 359.0)
        relative -= 360.0;
    if (relative < 0.0)
        relative += 360.0;

    return(relative);

}



/*
 * it calculate the relative angle
 */
double
mangrel2(double absolute,double kepera)

{

    double  relative;


    relative = 360 - (kepera - absolute);

    if (relative > 359.0)
        relative -= 360.0;
    if (relative < 0.0)
        relative += 360.0;

    return(relative);

}


/*
 *  return the id number of the target area in which
 *  the robot is located (0 if it is located outside target areas) 
 */
int
read_ground(struct individual *pind)
{

	double distzone;
	int z;
	float p1;
	float p2;
        struct envobject *obj;

	//calculate location of floor sensor = in front of robot body, as in ePuck
	p1 = SetX(pind->pos[0], 37.0, pind->direction);
	p2 = SetY(pind->pos[1], 37.0, pind->direction);
	if (envnobjs[3] > 0)
	{
	  for(z=0, obj = *(envobjs + 3); z < envnobjs[3]; z++, obj++)
		{
		  if (obj->subtype == 0)
		   {
		    distzone = ((p1 - obj->x) * (p1 - obj->x)) + ((p2 - obj->y) * (p2 - obj->y));
		    distzone = sqrt(distzone);
		    if (distzone < obj->r)
			return(z+1);
		   }
		  else
		   {
		    if ( (p1 >= obj->x) && (p1 < (obj->x + obj->X)) &&
                         (p2 >= obj->y) && (p2 < (obj->y + obj->Y)) )
			return(z+1);
		   }

		}

	}
	return(0);
}


/*
 *  return the color of the ground (0.0 out target areas)
 */
float
read_ground_color(struct individual *pind)
{

	double distzone;
	int z;
	float p1;
	float p2;
        struct envobject *obj;


	//calculate location of floor sensor = in front of robot body, as in ePuck
	p1 = SetX(pind->pos[0], 37.0, pind->direction);
	p2 = SetY(pind->pos[1], 37.0, pind->direction);
	if (envnobjs[3] > 0)
	{
	  for(z=0, obj = *(envobjs + 3); z < envnobjs[3]; z++, obj++)
		{
		  if (obj->subtype == 0)
		   {
		    distzone = ((p1 - obj->x) * (p1 - obj->x))+((p2 - obj->y) * (p2 - obj->y));
		    distzone = sqrt(distzone);
		    if (distzone < obj->r)
			return(obj->c[0]);
		   }
		  else
		   {
		    if ( (p1 >= obj->x) && (p1 < (obj->x + obj->X)) &&
                         (p2 >= obj->y) && (p2 < (obj->y + obj->Y)) )
			return(obj->c[0]);
		   }
		}
	}
	return(0.0);
}

/*
 *  return the color of the ground (0.0 out target areas)
 */
float
read_ground_colornew(struct individual *pind)
{

	double distzone;
	int z;
	float p1;
	float p2;
	struct envobject *obj;


	//calculate location of floor sensor = in front of robot body, as in ePuck
	p1 = SetX(pind->pos[0], 37.0, pind->direction);
	p2 = SetY(pind->pos[1], 37.0, pind->direction);
	if (envnobjs[3] > 0)
	{
	  for(z=0, obj = *(envobjs + 3); z < envnobjs[3]; z++, obj++)
		{
		  distzone = (p1 - obj->x)*(p1 - obj->x) + (p2 - obj->y)*(p2 - obj->y);
		  distzone = sqrt(distzone);
		  if (distzone < obj->r)
		     return(obj->c[0]);
		}
	}
	return(0.0);
}

/*
 * checks whether a crash occurred between a robot and an obstacle
 */
int
check_crash(float p1,float p2)

{
  int i;
  float   x,y;
  double  dist;
  double  mindist;
  struct individual *pind;
  struct envobject *obj;
  
  mindist = 9999.0;
  for(i=0, obj = *(envobjs + 0);i<envnobjs[0];i++, obj++)
   {
    if (p1 >= obj->x && p1 <= obj->X)
       x = p1;
     else
       x = obj->x;
    if (p2 >= obj->y && p2 <= obj->Y)
       y = p2;
     else
       y = obj->y;
    dist = mdist(p1,p2,x,y);
    if (dist < mindist)
      mindist = dist;
   }
  for(i=0,obj = *(envobjs + 1);i<envnobjs[1];i++,obj++)
   {
    dist = mdist(p1,p2,obj->x,obj->y)-obj->r;
    if (dist < mindist)
      mindist = dist;
   }
  for(i=0,obj = *(envobjs + 2);i<envnobjs[2];i++,obj++)
   {
    dist = mdist(p1,p2,obj->x,obj->y)-obj->r;
    if (dist < mindist)
      mindist = dist;
   }

   // check if a crash occurred with another individuals
   // by ignoring individuals that have the same idential positions - theirself
   for (pind=ind,i=0; i < nteam; i++, pind++)
	  {
	    if (p1 != pind->pos[0] || p2 != pind->pos[1])
		{
		  dist = mdist(p1,p2,pind->pos[0],pind->pos[1])-robotradius;
		  if (dist < mindist)
			{
			  mindist = dist;
			  if (dist < robotradius)
			    individual_crash = 1;
			}
		}
	  }

   if (mindist < robotradius)
     return(1);
   else
     return(0);
}

/*
 * we replace the robot in the previous position
 */
void
reset_pos_from_crash(struct individual *pind)

{

    pind->direction = pind->oldirection;
    pind->pos[0]    = pind->oldpos[0];
    pind->pos[1]    = pind->oldpos[1];

    pind->speed[0]=0.0f;
    pind->speed[1]=0.0f;
}

/*
 * update the state of the infrared sensors 
 */
void
update_infrared_s(struct individual *pind)

{

	float  p1;
	float  p2;
	double direction;
	int    i,ii;
	float  x,y;
	double dist;
	double  angle;
	int     relang;
	int     reldist;
	int     *w;
	int     *o;
	double  ang;
	struct individual *cind;
	int    t;
        struct envobject *obj;



	p1        = pind->pos[0];
	p2        = pind->pos[1];
	direction = pind->direction;

	for(i=0;i<8;i++)
	  pind->cifsensors[i] = 0.0f;

	// walls
	for(i=0, obj = *(envobjs + 0);i < envnobjs[0];i++, obj++)
	{
	  if (p1 >= obj->x && p1 <= obj->X)
	     x = p1;
	   else
	     x = obj->x;
	  if (p2 >= obj->y && p2 <= obj->Y)
	     y = p2;
	   else
	     y = obj->y;
	   dist = mdist((float)p1,(float)p2,(float)x,(float)y);
	   if (dist < (wallcf[3] + robotradius + (wallcf[2] * wallcf[4])))     // min.dist + khepera rad. + max. dist
		{                     // 5 + robotradius + 40 = 70.5
		  if (x == p1)
		  {
		    if (y < p2)
			angle = 270.0;
		       else
			angle = 90.0;
		  }
		  else
		  {
		   if (x < p1)
		      angle = 180.0;
			else
		      angle = 0.0;
		  }
		  relang  = (int) (mangrel(angle,direction) / 2.0);
		  dist -= robotradius;          // khepera rad. = robotradius
		  if (dist < wallcf[3])        // min. dist. = 5.0
		    dist = wallcf[3];
		  reldist = (int) (dist - wallcf[3]);  /* 5 for addressing */
		  reldist = (int) reldist / wallcf[4]; /* for addressing each 2 millimeters */

		  w = *(wall + reldist);
		  w = (w + (relang * 8));
		  for(ii=0;ii<8;ii++,w++)
		   pind->cifsensors[ii] += (*w / (float) 1024.0);
		}
	}

	// round obstacles
	for(i=0,obj = *(envobjs + 2);i < envnobjs[2];i++, obj++)
	{
		dist = mdist((float)p1,(float)p2,(float)obj->x,(float)obj->y) - obj->r;
//		ang = mang((int)p1,(int)p2,(int)obj->x,(int)obj->y,(double)dist+obj->r);
//		relang = (int) mangrel2((double)ang,(double)direction) / (int) 2.0;
		if (dist < (obstcf[3] + robotradius + (obstcf[2] * obstcf[4])))       // min.dist + khepera rad. + (dists*inter)
		{                     // 72.5 = 5 + robotradius + 40 = 72.5
			ang = mang((int)p1,(int)p2,(int)obj->x,(int)obj->y,(double)dist+obj->r);
			relang = (int) mangrel2((double)ang,(double)direction) / (int) 2.0;
			dist -= robotradius;          // khepera rad.
			if (dist < obstcf[3])  // min. dist. = 5.0
				dist = obstcf[3];
			reldist = (int) (dist - obstcf[3]);  // 5 for addressing
			reldist = (int) reldist / obstcf[4]; // for addressing each 2 millimeters

			o = *(obst + reldist);
			o = (o + (relang * 8));
			for(ii=0;ii<8;ii++,o++)
				pind->cifsensors[ii] += (*o / (float) 1024.0);
		}
	}


	// small round obstacles
	for(i=0, obj = *(envobjs + 1);i < envnobjs[1]; i++, obj++)
	{
			dist = mdist((float)p1,(float)p2,(float)obj->x,(float)obj->y) - obj->r;
			if (dist < (sobstcf[3] + robotradius + (sobstcf[2] * sobstcf[4])))       // min.dist + khepera rad. + max. dist
			{                     // 5 + robotradius + 40 = 72.5

				ang = mang((int)p1,(int)p2,(int)obj->x,(int)obj->y,(double)dist+obj->r);
				relang = (int) mangrel2((double)ang,(double)direction) / (int) 2.0;
				dist -= robotradius;          // khepera rad. = robotradius
				if (dist < sobstcf[3])        // min. dist. = 5.0
					dist = sobstcf[3];
				reldist = (int) (dist - sobstcf[3]);
				reldist = (int) reldist / (int) sobstcf[4];

				o = *(sobst + reldist);
				o = (o + (relang * 8));
				for(ii=0;ii<8;ii++,o++)
					pind->cifsensors[ii] += (*o / (float) 1024.0);
			}
	}

	// other robots based on sround
	// we used the sample of round obstacles assuming they are similar to robots
	for (t=0,cind=ind; t<nteam; t++, cind++)
	{
	 if ((p1 != cind->pos[0] || p2 != cind->pos[1]))
	 {
	   dist = mdist((float)p1,(float)p2,(float) cind->pos[0],(float)cind->pos[1]) - robotradius;
	   if (dist < (obstcf[3] + robotradius + (obstcf[2] * obstcf[4])))
		{
		  ang = mang((int)p1,(int)p2,(int)cind->pos[0],(int)cind->pos[1],(double)dist+robotradius);
		  relang = (int) mangrel2((double)ang,(double)direction) / (int) 2.0;
		  dist -= robotradius;
		  if (dist < obstcf[3])
		     dist = obstcf[3];
		  reldist = (int) (dist - obstcf[3]);
		  reldist = (int) reldist / (int) obstcf[4];

		  o = *(obst + reldist);
		  o = (o + (relang * 8));
		  for(ii=0;ii<8;ii++,o++)
		    pind->cifsensors[ii] += (*o / (float) 1024.0);
		}
	 }
	}

	// value over limits are truncated
	for(ii = 0; ii < 8; ii++)
	{
	  if (pind->cifsensors[ii] > 1.0)
	   pind->cifsensors[ii] = 1.0f;
	}

	// add noise to infra-red sensors if requested
	if (sensornoise > 0.0)
	  for(i=0; i < 8; i++)
	  {
	   pind->cifsensors[i] += rans(sensornoise);
	   if (pind->cifsensors[i] < 0.0) pind->cifsensors[i] = (float) 0.0;
	   if (pind->cifsensors[i] > 1.0) pind->cifsensors[i] = (float) 1.0;
          }


}

/*
 * update the state of the light sensors 
 */
void
update_lights_s(struct individual *pind)

{

     float  p1;
     float  p2;
     double direction;
     int    i;
     int    ii;
     double ang;
     int    relang;
     double dist;
     int    reldist;
     int    *o;
     struct envobject *obj;

     p1        = pind->pos[0];
     p2        = pind->pos[1];
     direction = pind->direction;

     for(i=0;i<8;i++)
	  pind->clightsensors[i] = 0.0f;

     for(i=0, obj = *(envobjs + 4);i < envnobjs[4];i++, obj++)
	   {
	      dist = mdist((float)p1,(float)p2,(float) obj->x,(float)obj->y);
	      ang = mang((int)p1,(int)p2,(int)obj->x,(int)obj->y,(double)dist);
	      relang = (int) mangrel2((double)ang,(double)direction) / (int) 2.0;
	      if (dist <= (lightcf[3] + robotradius + (lightcf[2] * lightcf[4])))    // 560 = 13 + 27 + 520 (26*20)
		{
		   dist  -= robotradius;                         // khepera rad
		   if (dist < lightcf[3])
		     dist = lightcf[3];
		   reldist = (int) (dist - lightcf[3]); // 13 for adressing
		   reldist = (int) (dist / lightcf[4]); // for addressing each 20mm       o = *(light + reldist);
		   o = *(light + reldist);
		   o = (o + (relang * 8));
		   for(ii=0;ii<8;ii++,o++)
		     pind->clightsensors[ii] += (*o / (float) 1024.0);
		}
	   }
      // we normalize between limits
      for(i = 0; i < 8; i++)
	  {
	     if (pind->clightsensors[i] > 1.0)
	       pind->clightsensors[i] = 1.0f;
	  }
      // noise
      if (sensornoise > 0.0)
	  for(i=0; i < 8; i++)
	    {
	      pind->clightsensors[i] += rans(sensornoise);
	      if (pind->clightsensors[i] < 0.0) 
		 pind->clightsensors[i] = (float) 0.0;
	      if (pind->clightsensors[i] > 1.0) 
		 pind->clightsensors[i] = (float) 1.0;
	    }

}

/*
 * update the state of signal sensors 
 */
void
update_signal_s(struct individual *pind, struct  iparameters *pipar)

{

    float p1;
    float p2;
    double direction;
    int i;
    int t;
    int s;
    int f;
    float fr;
    double dist;
    int relang;
    double ang;
    struct individual *cind;
    int nsensor;

    p1        = pind->pos[0];
    p2        = pind->pos[1];
    direction = pind->direction;

    for(i=0;i< pipar->signalss;i++)
	 {
	   pind->detected_signal[i][0] = 0.0f;
	   pind->detected_signal[i][1] = 999999.0f;
	 }
    for (t=0, cind=ind; t<nteam; t++, cind++)
	if (pind->n_team != cind->n_team)
	 {
	   dist = mdist((float)pind->pos[0],(float)pind->pos[1],(float) cind->pos[0],(float)cind->pos[1]) - robotradius;
           if (dist < (double) max_dist_attivazione)
		   {
		     if (pipar->signalss == 1)
		     // non-directional signals
		     {
		       if (dist < pind->detected_signal[0][1])
		        for(s=0;s < pipar->signalso; s++)
			{
                         pind->detected_signal[s][0] =  cind->signal_motor[s];  
                         pind->detected_signal[s][1] = (float) dist;
			}
		     }
		     else
		     // directional signals
		     {
		       ang = mang((int)pind->pos[0],(int)pind->pos[1],(int)cind->pos[0],cind->pos[1],dist);
		       relang = (int) mangrel2((double)ang,(double)direction);
		       if (relang > 315 || relang <= 45)
		       {
		        if (dist < pind->detected_signal[0][1])
		         for(s=0;s < pipar->signalso; s++)
			 {
                          pind->detected_signal[s*pipar->signalss+0][0] = cind->signal_motor[s]; 
                          pind->detected_signal[s*pipar->signalss+0][1] = (float) dist;
			 }
		       }
		       if (relang > 45 && relang <= 135)
		       {
		        if (dist < pind->detected_signal[1][1])
		         for(s=0;s < pipar->signalso; s++)
			 {
                          pind->detected_signal[s*pipar->signalss+1][0] = cind->signal_motor[s]; 
                          pind->detected_signal[s*pipar->signalss+1][1] = (float) dist;
			 }
		       }
		       if (relang > 135 && relang <= 225)
		       {
		        if (dist < pind->detected_signal[2][1])
		         for(s=0;s < pipar->signalso; s++)
			 {
                          pind->detected_signal[s*pipar->signalss+2][0] = cind->signal_motor[s]; 
                          pind->detected_signal[s*pipar->signalss+2][1] = (float) dist;
			 }
		       }
		       if (relang > 225 && relang <= 315)
		       {
		        if (dist < pind->detected_signal[3][1])
		         for(s=0;s < pipar->signalso; s++)
			 {
                          pind->detected_signal[s*pipar->signalss+3][0] = cind->signal_motor[s]; 
                          pind->detected_signal[s*pipar->signalss+3][1] = dist;
			 }
		       }
		     }

		   }

		}
	for(s=0,nsensor=0;s < pipar->signalss*pipar->signalso; s++, nsensor++)
		{
                 pind->csignals[nsensor] = pind->detected_signal[s][0];
		 if (pipar->signalssb == 1)
                  if (pind->csignals[nsensor] > 0.5)
                    pind->csignals[nsensor] = 1.0;
		  else
                    pind->csignals[nsensor] = 0.0;
		 for (f=0,fr = 0.0;f < pipar->signalssf; f++, fr += (1.0 / (float) pipar->signalssf))
		   if (pind->csignals[nsensor] >= fr && pind->csignals[nsensor] < (fr + (1.0 / (float) pipar->signalssf)))
                     pind->csignals[nsensor+1+f] = 1.0;
		   else
                     pind->csignals[nsensor+1+f] = 0.0;
		}

}


/*
 * update the camera characterized by
 *  a given cameraviewangle (e.g. +/- 18 degrees)
 *  a given number of camerareceptors for each cameracolor (1,2, or 3)
 *   with non-overlapping recepitive field
 *  a sensitivity up to a given cameradistance
 */
void
update_camera(struct individual *pind, struct  iparameters *pipar)

{

    int i;
    int ii;
    int f;
    int ff;
    int c;
    int r;
    float x_cam;
    float y_cam;
    float dir_cam;
    float objxa, objya;
    float objxb, objyb;
    double dist;
    double dista;
    double distb;
    double ang;
    double anga;
    double angb;
    double ang1, ang2;
    double angs;
    double angr;
    double a;
    double da;
    float  av[3];
    struct envobject *obj;
    struct envobject **cenvobjs;
    float  objax, objay;
    float  objbx, objby;
    float  cameradist[360];
    char message[256];

    //initialize 1-degree photoreceptors state
    for(c=0; c < pipar->cameracolors; c++)
     for(f=0; f < pipar->cameraviewangle; f++)
     {
      pind->camera[c][f] = 0.0;
      cameradist[f] = 99999.0;
     }

    // camera located on the barycentre of the robot
    x_cam    = pind->pos[0];
    y_cam    = pind->pos[1];
    dir_cam  = pind->direction;

    cenvobjs = envobjs;
// debug from 0 to 5
    for (i=1; i < 3; i++)       
     {
      for (ii=0, obj = *(cenvobjs + i); ii < envnobjs[i]; ii++, obj++)
       {
         // compute the distance between the camera and the object
         if (obj->type == 0)
          {
	   if (x_cam >= obj->x && x_cam <= obj->X)
	     dist = abs(y_cam - obj->y);
	   else
	     dist = abs(x_cam - obj->x);
          }
	 else
          {
	   dist = mdist(x_cam, y_cam, obj->x, obj->y);
          }
	 if (dist < pipar->cameradistance)
	  {
          // linear surface
	  if (obj->type == 0)
	   {
	    objxa = obj->x; 
            objya = obj->y; 
	    objxb = obj->X; 
            objyb = obj->Y;
           }
          // cylindrical surface
	  if (obj->type == 1 || obj->type == 2)
	   {
            ang = mang((int) x_cam,(int) y_cam, (int) obj->x, (int) obj->y, dist);
	    objxa = SetX(obj->x, obj->r, (double) ang + 90.0);
            objya = SetY(obj->y, obj->r, (double) ang + 90.0);
	    dista = mdist(x_cam,y_cam,objxa,objya);  
	    anga  = mang((int)x_cam,(int)y_cam,(int)objxa,(int)objya,dista);
	    angs = fabs(ang-anga);
	    angr = ang + pind->direction;
	    if (angr > 360)
	     angr -= 360;
	    sprintf(robotwstatus, "%.0f %.0f span %.0f rel %.0f",ang, anga, angs, angr);
           }

	  for(f=0,a = dir_cam - (pipar->cameraviewangle / 2); f < pipar->cameraviewangle; f++, a += 1.0)
	   {
            if (a < 0) a += 360;
	    if (a > 360) a -= 360;
	    if (a >= (angr-angs) && a <= (angr+angs) && dist < cameradist[f])
	      {
	       cameradist[f] = dist;
	       for(c=0; c < pipar->cameracolors; c++)
		pind->camera[c][f] = obj->c[c];
              }
	    }


          } // dist < cameradistance
       }
     }
    // compress and average information for photoreceptors that encode more than one degree of view
    for(f=0; f < pipar->camerareceptors; f++)
     {
       for(c=0; c < pipar->cameracolors; c++)
         av[c] = 0.0;
       for(i=0; i < (pipar->cameraviewangle / pipar->camerareceptors); i++)
       {
        for(c=0; c < pipar->cameracolors; c++)
          av[c] += pind->camera[c][(f*i)+i];
       }
       for(c=0; c < pipar->cameracolors; c++)
         pind->camera[c][f] = av[c] / (float) (pipar->cameraviewangle / pipar->camerareceptors);
     }

}


/*
 * update the simple camera 
 * the simple camera detect the other robot only
 */
void
update_simplecamera(struct individual *pind, struct  iparameters *pipar)

{

    int    i;
    float  p1;
    float  p2;
    double direction;
    int    t;
    double dist;
    double ang;
    int    relang;
    struct individual *cind;

    p1        = pind->pos[0];
    p2        = pind->pos[1];
    direction = pind->direction;

    for (i=0; i < 5; i++)
      pind->camera[0][i] = 0.0;


    if (pipar->simplevision == 1)
	{
	 for (t=0,cind=ind; t<nteam; t++, cind++)
	 {
	  if (pind->n_team != cind->n_team)
	  {
	   dist = mdist((float)p1,(float)p2,(float) cind->pos[0],(float)cind->pos[1]);
	   ang = mang((int)p1,(int)p2,(int)cind->pos[0],(int)cind->pos[1],(double)dist);
	   relang = (int) mangrel2((double)ang,(double)direction);
	   if (relang >= 342 && relang <= 351)
              pind->camera[0][0] = 1.0;
	   if (relang > 351 && relang <= 360)
              pind->camera[0][1] = 1.0;
	   if (relang >= 0 && relang <= 9)
              pind->camera[0][2] = 1.0;
	   if (relang > 9 && relang <= 18)
              pind->camera[0][3] = 1.0;
	   if (relang > 18 && relang < 342)
              pind->camera[0][4] = 1.0;
	  }
	 }
	}
	// epuck camera is simulated through computing the relevant angle for a left and right input
	// if no other epuck is perceived, both inputs are 0
	// max angle of epuck camera is defined by pipar->camera
	// activation of neurons is 'relative angle_left*(100/(degrees/2))' and 'relative angle_right*(100/18)',
	// if other robot is in front, activation for both neurons is 100
	//
	if (pipar->simplevision == 2)
	{
		for (t=0,cind=ind; t<nteam; t++, cind++)
		{
			if ((p1 != cind->pos[0] || p2 != cind->pos[1]))
			{
				float input_left;
				float input_right;
				float input_not_visible;
				float degrees = 18;

				//compute distance and angle
				dist = mdist((float)p1,(float)p2,(float) cind->pos[0],(float)cind->pos[1]) - 27.5;
				ang = mang((int)p1,(int)p2,(int)cind->pos[0],(int)cind->pos[1],(double)dist+27.5);
				relang  = mangrel2(ang,direction);

				//other epuck is in view
				if (relang < degrees || relang > (360-degrees))
				{
						if ( relang < degrees )
						{
							input_left = (float) ((degrees-relang)*(100/degrees))/100;
							input_right = (float) 0.0f;
							input_not_visible = 0.0f;
						}
						else
						{
							input_left = (float) 0.0;
							input_right = (float) ((relang-(360-degrees))*(100/degrees))/100;
							input_not_visible = 0.0f;
						}
					}

				//no other epuck is visible
				else
				{
					input_left = 0.0;
					input_right = 0.0;
					input_not_visible = 1.0f;
				}

				if (input_left > 1.0)
					input_left = 1.0;
				if (input_left < 0.0)
					input_left = 0.0;
				if (input_right > 1.0)
					input_right = 1.0;
				if (input_right < 0.0)
					input_right = 0.0;

				pind->camera[0][0] = input_left;
				pind->camera[0][1] = input_right;
				pind->camera[0][2] = input_not_visible;
			}
		}
	}
}


/*
 * update the ground sensors
 */
void
update_groundsensors(struct individual *pind, struct  iparameters *pipar)

{

    int g;
    int i;
    float c;
    float fi;
    float v;

    c = read_ground_color(pind);
    fi = 1.0 / (float) (pipar->groundsensor + 1);
    for (i=0, v=fi; i < pipar->groundsensor; i++, v += fi)
      if (c > v && c <= (v + fi))
         pind->ground[i] = (float) 1.0;
      else
         pind->ground[i] = (float) 0.0;

}

/*
 * update the ground sensors encoding the last visited area
 */
void
update_lastground(struct individual *pind, struct  iparameters *pipar)

{

    int g;
    int i;
    float c;
    float fi;
    float v;

    c = read_ground_color(pind);
    fi = 1.0 / (float) (pipar->a_groundsensor2 + 1);
    if (c > fi)
     {
      for (i=0, v=fi; i < pipar->a_groundsensor2; i++, v += fi)
       if (c > v && c <= (v + fi))
         pind->lastground[i] = (float) 1.0;
        else
         pind->lastground[i] = (float) 0.0;
     }
	
}

/*
 * update the leaky-integrator ground sensors 
 */
void
update_deltaground(struct individual *pind, struct  iparameters *pipar)

{

    int g;
    int i;
    float c;
    float fi;
    float v;

    c = read_ground_color(pind);
    fi = 1.0 / (float) (pipar->a_groundsensor + 1);
    for (i=0, v=fi; i < pipar->a_groundsensor; i++, v += fi)
      if (c > v && c <= (v + fi))
         pind->ground[i] = pind->ground[i] * 0.99 + 0.01;
      else
         pind->ground[i] = pind->ground[i] * 0.99;

}




/*
 * move the robot on the basis of sampled motion
 */
void move_robot(struct individual *pind)
 {

     int    v1,v2;
     float  **mo;
     float  *m;
     float  ang, dist;
     extern float mod_speed;
     extern int   selected_ind_mod_speeed;


	 pind->oldpos[0] = pind->pos[0];
	 pind->oldpos[1] = pind->pos[1];
	 pind->oldirection = pind->direction;

	 v1 = (int) (pind->wheel_motor[0] * 20.0);
	 v2 = (int) (pind->wheel_motor[1] * 20.0);
	 // approximate to the closest integer value
	 if ( ((pind->wheel_motor[0] * 200) - (v1 * 20)) > 5)
		 v1++;
	 if ( ((pind->wheel_motor[1] * 200) - (v2 * 20)) > 5)
	 	 v2++;

	 v1 -= 10;
	 v2 -= 10;

	 if (pind->wheel_motor[2] < 1.0f)
	 {
	  v1 = (int) ((float) v1 * pind->wheel_motor[1]);
	  v2 = (int) ((float) v2 * pind->wheel_motor[1]);
	 }

	 pind->speed[0] = (float) v1;
	 pind->speed[1] = (float) v2;

	 mo = motor;
	 m = *(mo + (v1 + 12));
	 m = (m + ((v2 + 12) * 2));
	 ang = *m;
	 m++;
	 dist = *m * (float) 10.0;

	 pind->pos[0] = SetX(pind->pos[0],dist,pind->direction);
	 pind->pos[1] = SetY(pind->pos[1],dist,pind->direction);


	 pind->direction += ang;
	 if (pind->direction >= 360.0)
		 pind->direction -= 360;
	 if (pind->direction < 0.0)
		 pind->direction += 360;
 }

 

/*
 * it load worlds description from file evorobot.env
 */
void 
load_world(char *filename)

{


     FILE   *fp;
     char   st[1024];
     char   message[512];
     char   word[256];
     char   *w;
     int    n;
     int    type;
     char   c;
     int    i;
     int    color;
     int    selected;
     struct envobject **cenvobjs;
     struct envobject *obj;

     cenvobjs = envobjs;
     for(i=0; i < 5; i++)
       envnobjs[i] = 0;
     if ((fp=fopen(filename, "r")) != NULL)
	{
	 n = 0;
	 while (fgets(st,1024,fp) != NULL && n < 1000)
	 {
	  selected = 0;
	  if (strlen(st) > 2)
	   {
	   getword(word,st,' ');
	   if (strcmp(word, "swall") == 0 && envnobjs[0] < nobjects)
	    {
	     selected = 1;
	     obj = *cenvobjs;
	     obj = (obj + envnobjs[0]);
	     obj->id = envnobjs[0];
             envnobjs[0] += 1;
	     obj->type = 0;
	    }
	   if (strcmp(word, "sround") == 0 && envnobjs[1] < nobjects)
	    {
	     selected = 1;
	     obj = *(cenvobjs + 1);
	     obj = (obj + envnobjs[1]);
	     obj->id = envnobjs[1];
             envnobjs[1] += 1;
	     obj->type = 1;
	     obj->r = 12.5;
	    }
	   if (strcmp(word, "round") == 0 && envnobjs[2] < nobjects)
	    {
	     selected = 1;
	     obj = *(cenvobjs + 2);
	     obj = (obj + envnobjs[2]);
	     obj->id = envnobjs[2];
             envnobjs[2] += 1;
	     obj->type = 2;
	     obj->r = 27.0;
	    }
	   if (strcmp(word, "ground") == 0 && envnobjs[3] < nobjects)
	    {
	     selected = 1;
	     obj = *(cenvobjs + 3);
	     obj = (obj + envnobjs[3]);
	     obj->id = envnobjs[3];
             envnobjs[3] += 1;
	     obj->type = 3;
	     obj->subtype = 3;
	    }
	   if (strcmp(word, "light") == 0 && envnobjs[4] < nobjects)
	    {
	     selected = 1;
	     obj = *(cenvobjs + 4);
	     obj = (obj + envnobjs[4]);
	     obj->id = envnobjs[4];
             envnobjs[4] += 1;
	     obj->type = 4;
	    }
	   if (selected == 1)
            {
	      color = 0;
	      obj->subtype = 0;
	      obj->x = 100;
	      obj->y = 100;
	      obj->X = 200;
	      obj->Y = 200;
	      if (obj->type != 2 && obj->type != 3)
	        obj->r = 0;
	      obj->h = 20;
	      obj->c[0] = 0.0;
	      obj->c[1] = 0.0;
	      obj->c[2] = 0.0;
              while (strlen(st) > 1)
               {
	        getword(word,st,' ');
		c = *word;
		w = word;
		w++;
	        switch(c)
	         {
	           case 't':
		    obj->subtype = atoi(w);
	            break;
	           case 'x':
		    obj->x = atof(w);
		    if (obj->x > envdx)
		      envdx = obj->x;
	            break;
	           case 'y':
		    obj->y = atof(w);
		    if (obj->y > envdy)
		      envdy = obj->y;
	            break;
	           case 'X':
		    obj->X = atof(w);
		    if (obj->X > envdx)
		      envdx = obj->X;
	            break;
	           case 'Y':
		    obj->Y = atof(w);
		    if (obj->Y > envdy)
		      envdy = obj->Y;
	            break;
	           case 'h':
		    obj->h = atof(w);
	            break;
	           case 'r':
		    obj->r = atof(w);
	            break;
	           case 'c':
		    if (color >= 0 && color < 3)
		      obj->c[color] = atof(w);
		    color++;
	            break;
		   default:
		    sprintf(message,"unknown env. parameter %c%s in file evorobot.env",c,w);
                    display_warning(message);
		    break;
	         }
	       }
	    }
	   else
            {
	     sprintf(st,"unknown object: %s", word);
	     display_warning(st);
	    }
	   n++;
	   }
	  }
        fclose(fp);
        sprintf(message,"loaded %d objects from file: evorobot.env", n);
        display_stat_message(message);
        }
       else
       {
         sprintf(message,"Error: the file evorobot.env could not be opended");
	 display_error(message);
       }
     // calculate world size (if not specified manually)
     if (envdx == 0 || envdy == 0)
       {
        for(i=0,obj = *envobjs; i < envnobjs[0]; i++, obj++)
          {
	    if (obj->x > envdx) envdx = obj->x;
	    if (obj->y > envdy) envdy = obj->y;
	    if (obj->X > envdx) envdx = obj->X;
	    if (obj->Y > envdy) envdy = obj->Y;
          }   
      }     

}


/*
 * save the environment description into a file evorobot.env
 */
void 
save_world(char *filename)

{


     FILE   *fp;
     int    i;
     int    ii;
     struct envobject **cenvobjs;
     struct envobject *obj;

     if ((fp=fopen(filename, "w")) != NULL)
      {
       cenvobjs = envobjs;
       for (i=0; i < 5; i++)       
       {
        for (ii=0, obj = *(cenvobjs + i); ii < envnobjs[i]; ii++, obj++)
        {
          if (obj->type == 0)
	   fprintf(fp, "swall x%.1f y%1.f X%.1f Y%.1f h%.1f c%.1f c%.1f c%.1f\n", obj->x, obj->y, obj->X, obj->Y, obj->h, obj->c[0], obj->c[1], obj->c[2]); 
          if (obj->type == 1)
	   fprintf(fp, "sround x%.1f y%1.f r%.1f h%.1f c%.1f c%.1f c%.1f\n", obj->x, obj->y, obj->r, obj->h, obj->c[0], obj->c[1], obj->c[2]); 
          if (obj->type == 2)
	   fprintf(fp, "round x%.1f y%1.f r%.1f h%.1f c%.1f c%.1f c%.1f\n", obj->x, obj->y, obj->r, obj->h, obj->c[0], obj->c[1], obj->c[2]);
          if (obj->type == 3)
	   fprintf(fp, "ground t%d x%.1f y%1.f r%.1f c%.1f\n", obj->subtype, obj->x, obj->y, obj->r, obj->c[0]);
          if (obj->type == 4)
	   fprintf(fp, "light x%.1f y%1.f r%.1f h%.1f\n", obj->x, obj->y, obj->r, obj->h);
        }
       }
       display_stat_message("saved current environment in file evorobot.env");
       fclose(fp);
      }
     else
      {
       display_warning("unable to open file evorobot.env");
      }

}

/*
 * load an obstacle from a .sam file
 */
void
load_obstacle(char *filename, int **object, int  *objectcf)

{

   int  **ob;
   int  *o;
   FILE *fp;
   int  i,v,s;
   char sbuffer[128];

   if ((fp = fopen(filename,"r")) != NULL)
	  {
	  }
	  else
	  {
	    sprintf(sbuffer,"I cannot open file %s", filename);
	    display_error(sbuffer);
	  }
   /*
    * we read the object configuration data
    * 0-nsensors, 1-nangles, 2-ndistances,3-initdist,4-distinterval
    */
   for (i=0, o=objectcf; i < 5; i++, o++)
     fscanf(fp,"%d ",o);
   fscanf(fp,"\n",o);

   for (v=0, ob=object; v < objectcf[2]; v++, ob++)
    {
     fscanf(fp,"TURN %d\n",&v);
     for (i = 0, o = *ob; i < objectcf[1]; i++)
      {
       for (s=0; s < objectcf[0]; s++,o++)
        {
	   fscanf(fp,"%d ",o);
        }
       fscanf(fp,"\n");
      }
    }
   fscanf(fp, "%s\n",sbuffer);
   if (strcmp(sbuffer,"END") != 0)
     {
      sprintf(sbuffer,"loading file %s: file might be inconsistent", filename);
      display_error(sbuffer);
     }
   fflush(fp);
   fclose(fp);
}


/*
 * save an obstacle in a .sam file
 */
void
save_obstacle(char *filename, int **object, int  *objectcf)

{

   int  **ob;
   int  *o;
   FILE *fp;
   int  i,v,s;
   char sbuffer[64];

   if ((fp = fopen(filename,"w")) != NULL)
	  {
	  }
	  else
	  {
	    sprintf(sbuffer,"I cannot open file %s", filename);
	    display_error(sbuffer);
	  }
   /*
    * we write the object configuration data
    * 0-nsensors, 1-nangles, 2-ndistances,3-initdist,4-distinterval
    */
   for (i=0, o=objectcf; i < 5; i++, o++)
     fprintf(fp,"%d ",*o);
   fprintf(fp,"\n");

   for (v=0, ob=object; v < objectcf[2]; v++, ob++)
    {
     fprintf(fp,"TURN %d\n",v);
     for (i = 0, o = *ob; i < objectcf[1]; i++)
      {
       for (s=0; s < objectcf[0]; s++,o++)
        {
	   fprintf(fp,"%d ",*o);
        }
       fprintf(fp,"\n");
      }
    }
   fprintf(fp, "END\n");
   fflush(fp);
   fclose(fp);
}


/*
 * load motor samples
 * the first line should be the number of samples
 * for each sample we compute 3 simmitrical
 */
void
load_motor()

{

	extern  float **motor;
	float  **mo;
	float  *m;
	FILE   *fp;
	int    v,vv;
#ifdef EVOWINDOWS
	if ((fp = fopen("../bin/sample_files/motor.sam","r")) != NULL)
#else
	if ((fp = fopen("..\bin\sample_files\motor.sam","r")) != NULL)

#endif
  {
  }
	else
	{
		display_error("cannot open file motor.sam");
	}
	for (v=0,mo = motor; v < 25; v++,mo++)
	{
		for (vv=0,m = *mo; vv < 25; vv++,m++)
		{
			fscanf(fp,"%f\t",m);
			m++;
		}
		fscanf(fp,"\n");
	}
	for (v=0,mo = motor; v < 25; v++,mo++)
		{
			for (vv=0,m = *mo; vv < 25; vv++,m++)
			{
				m++;
				fscanf(fp,"%f\t",m);
			}
			fscanf(fp,"\n");
		}
		fclose(fp);
	}




/*
 * allocate space for sampled objects
 */
void
create_world_space()

{
    int 	**wa;
    int		**ob;
    float	**mo;
    int         *v;
    int         i;
    int         ii;

     /*
     * wall
     * allocate space for 30 distances for each sensor
     */
    wall = (int **) malloc(30 * sizeof(int *));
    wallcf = (int *) malloc(5 * sizeof(int));

    if (wall == NULL || wallcf == NULL)
	 display_error("system error: wall malloc");

    /*
     * allocate space for directions
     */
    wa = wall;
    for (i=0; i<30; i++,wa++)
	  {
	   *wa = (int *)malloc((8 * 180) * sizeof(int));
	   if (*wa == NULL)
	      display_error("system error: wa malloc");
	   for (ii=0,v=*wa; ii < (8 *180); ii++,v++)
		*v = 0;
	  }

    /*
     * round obstacles
     * allocate space for 30 distances for each sensor
     */
     obst = (int **) malloc(30 * sizeof(int *));
     obstcf = (int *) malloc(5 * sizeof(int));

     if (obst == NULL || obstcf == NULL)
	  display_error("system error: obst malloc");
      /*
       * allocate space for directions
       */
      ob = obst;
      for (i=0; i<30; i++,ob++)
	  {
	   *ob = (int *)malloc((8 * 180) * sizeof(int));
	   if (*ob == NULL)
	      display_error("system error: ob malloc");
	   for (ii=0,v=*ob; ii < (8 *180); ii++,v++)
		*v = 0;
	  }
    /*
     * small round obstacles
     * allocate space for 30 distances for each sensor
     */
     sobst = (int **) malloc(30 * sizeof(int *));
     sobstcf = (int *) malloc(5 * sizeof(int));

     if (sobst == NULL || sobstcf == NULL)
	 display_error("system error: sobst malloc");
      /*
       * allocate space for directions
       */
      ob = sobst;
      for (i=0; i<30; i++,ob++)
	  {
	   *ob = (int *)malloc((8 * 180) * sizeof(int));
	   if (*ob == NULL)
	     display_error("system error: ob malloc");
	   for (ii=0,v=*ob; ii < (8 *180); ii++,v++)
		*v = 0;
	  }
    /*
     * light
     * allocate space for 30 distances for each sensor
     */
    light = (int **) malloc(30 * sizeof(int *));
    lightcf = (int *) malloc(5 * sizeof(int));

    if (light == NULL || lightcf == NULL)
	 display_error("system error: light malloc");
      /*
       * allocate space for directions
       */
    ob = light;
    for (i=0; i<30; i++,ob++)
	  {
	   *ob = (int *)malloc((8 * 180) * sizeof(int));
	   if (*ob == NULL)
	    display_error("system error: light malloc");
	   for (ii=0,v=*ob; ii < (8 *180); ii++,v++)
		*v = 0;
	  }
    /*
     * allocate space for 25(v1) different speeds
     */
    motor = (float **) malloc(25 * sizeof(float *));
    if (motor == NULL)
	    display_error("system error: motor malloc");
      /*
       * allocate space for 25(v1) different speeds (ang & dist)
       */
    mo = motor;
    for (i=0; i<25; i++,mo++)
	  {
	   *mo = (float *)malloc((25 * 2) * sizeof(float));
	   if (*mo == NULL)
	    display_error("system error: mo malloc");
	  }


}





