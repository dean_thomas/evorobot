/* 
 * Evorobot* - rend_robot3d.cpp
 * Copyright (C) 2009, Onofrio Gigliotta
 * LARAL, Institute of Cognitive Science and Technologies, CNR, Roma, Italy 
 * http://laral.istc.cnr.it

 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "public.h"
#define EVO3DGRAPHICS
#define EVOGRAPHICS

#ifdef EVOGRAPHICS
#ifdef EVO3DGRAPHICS
#include "rend_robot3d.h"
//#include "simpleViewer.h"
#include "robot-env.h"
#include <gl/glu.h>
#include "vec.h"


Viewer *view;

// Draws a parallepiped
void Viewer::drawWall(float x, float y, float w, float h, float thin, float thinup)
{

  glEnable(GL_TEXTURE_2D);
  glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
  this->bindTexture(*floorText);
	
  glBegin(GL_QUADS);
  //front	
  glNormal3f(0.0, 0.0, 1.0);
  glTexCoord2f(0.0, 0.0); glVertex3f(x,  y,  thin);
  glTexCoord2f(1.0, 0.0); glVertex3f(x,  y+h,thin);
  glTexCoord2f(1.0, 1.0); glVertex3f(x+w,y+h,thin);
  glTexCoord2f(0.0, 1.0); glVertex3f(x+w,y,  thin);

  // top   
  glNormal3f(0.0, 1.0, 0.0);
  glTexCoord2f(0.0, 0.0); glVertex3f(x+w, y+h,  thin);
  glTexCoord2f(1.0, 0.0); glVertex3f(x+w, y+h,  thinup);
  glTexCoord2f(1.0, 1.0); glVertex3f(x,   y+h,  thinup);
  glTexCoord2f(0.0, 1.0); glVertex3f(x,   y+h,  thin);

  // back
  glNormal3f(0.0, 0.0, -1.0);
  glTexCoord2f(0.0, 0.0); glVertex3f(x,  y,  thin);
  glTexCoord2f(1.0, 0.0); glVertex3f(x,  y+h,thin);
  glTexCoord2f(1.0, 1.0); glVertex3f(x+w,y+h,thin);
  glTexCoord2f(0.0, 1.0); glVertex3f(x+w,y,  thin);

  // bottom
  glNormal3f(0.0, -1.0, 0.0);
  glTexCoord2f(0.0, 0.0); glVertex3f(x,  y, thinup);
  glTexCoord2f(1.0, 0.0); glVertex3f(x,  y, thin);
  glTexCoord2f(1.0, 1.0); glVertex3f(x+w,y, thin);
  glTexCoord2f(0.0, 1.0); glVertex3f(x+w,y, thinup);
  // left
  glNormal3f(-1.0, 0.0, 0.0);
  glTexCoord2f(0.0, 0.0); glVertex3f(x, y,  thinup);
  glTexCoord2f(1.0, 0.0); glVertex3f(x, y+h,thinup);
  glTexCoord2f(1.0, 1.0); glVertex3f(x, y+h,thin);
  glTexCoord2f(0.0, 1.0); glVertex3f(x, y,  thin);  
  // right
  glNormal3f(1.0, 0.0, 0.0);
  glTexCoord2f(0.0, 0.0); glVertex3f(x+w, y,   thinup);
  glTexCoord2f(1.0, 0.0); glVertex3f(x+w, y+h, thinup);
  glTexCoord2f(1.0, 1.0); glVertex3f(x+w, y+h, thin);
  glTexCoord2f(0.0, 1.0); glVertex3f(x+w, y,   thin);

  glEnd();
  glDisable(GL_TEXTURE_2D);

}

void Viewer::draw()
{
  const float nbSteps = 200.0;
  float  dx,dy,dz;
  int    dd;
  int    ww, hh;
  struct individual *pind;
  int    team;
  struct envobject *obj;
  float  scolor;
  int robottype=2; //0 khepera, 1 Epuck, 2 Lego

  //to adapt coordinate system
  // 3dX=x; 3dY=-y;  3dDirection=-dir
  //I added a rotation to synchronize textures 

  dx=dz=dy=2;
  glClearColor(1.0,1.0,1.0,0.0),
  glClear(GL_COLOR_BUFFER_BIT);
  GLUquadricObj *qobj;
  qobj=gluNewQuadric();
  gluQuadricNormals(qobj, GLU_SMOOTH);
  
  //drawing the floor
  glColor3f(1.0,1.0,1.0);
  drawWall(0,0,envdx,-envdx,-30,-40); 

  //drawing wall
  for(dd=0, obj = *(envobjs + 0);dd < envnobjs[0];dd++, obj++)
  {
    ww=(obj->X - obj->x);
    hh=-(obj->Y - obj->y);
    glColor3f(obj->c[0],obj->c[1],obj->c[2]); //coloring walls
    if (ww==0) ww = 10;
    if (hh==0) hh = 10;
    drawWall(obj->x, -obj->y, ww, hh, obj->h, -30); //baco corretto mancava il meno a obj->y
  }
  //drawing small round obstacles
  for(dd=0, obj = *(envobjs + 1);dd < envnobjs[1]; dd++, obj++)
  {
      glColor3f(obj->c[0],obj->c[1],obj->c[2]);
      glPushMatrix();
      glEnable(GL_TEXTURE_2D);
      glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);		
      glTranslatef(obj->x, - obj->y, -30); 
      gluQuadricTexture(qobj, true); 
      this->bindTexture(*floorText);
      gluCylinder(qobj,obj->r,obj->r,obj->h,40,40); 
      glTranslatef(0,0,obj->h);
      glRotatef(180,0.0,0.0,1.0);
      gluDisk(qobj,0,obj->r,40,4); 
      glDisable(GL_TEXTURE_2D);
      glPopMatrix();
  }
  //drawing target areas 
  for(dd=0, obj = *(envobjs + 3); dd < envnobjs[3]; dd++, obj++)
  {
     scolor = (1.0 - obj->c[0]) * 0.6 + 0.4;
     glColor3f(0.0,scolor,scolor);
     glPushMatrix();
     glEnable(GL_TEXTURE_2D);
     glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);		
     glTranslatef(obj->x,- obj->y, -28);		
     gluQuadricTexture(qobj, true); 
     this->bindTexture(*floorText);
     gluDisk(qobj,0,obj->r,40,4);		
     glDisable(GL_TEXTURE_2D);
     glPopMatrix();
  }
 //drawing lights
 glColor3f(2.5,2.5,0.3);
 for(dd=0, obj = *(envobjs + 4);dd < envnobjs[4];dd++, obj++)
  {
     glPushMatrix();
     glEnable(GL_TEXTURE_2D);
     glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);		
     glTranslatef(obj->x,- obj->y,28);		
     gluQuadricTexture(qobj, true); 
     this->bindTexture(*floorText);		
     gluSphere(qobj,15,40,40);
     glDisable(GL_TEXTURE_2D);
     glPopMatrix();
  }
  //drawing robot
  glColor3f(1.5,1.5,1.5);
  for(team=0,pind=ind;team<nteam;team++,pind++)
   {
      glPushMatrix();
      glEnable(GL_TEXTURE_2D);
      glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);	
	  switch(robottype)
	  {
	  case 0:
		  break;
	  case 1:
      glTranslatef(pind->pos[0],-pind->pos[1],-30);
      glRotatef(-pind->direction+90,0.0,0.0,1.0);
      gluQuadricTexture(qobj, true); 
      this->bindTexture(*epuckText);
	  gluCylinder(qobj,35,35,48,40,40);      
	  glTranslatef(0,0,47);
      this->bindTexture(*headText);
      glRotatef(180,0.0,0.0,1.0);
      gluDisk(qobj,0,35,40,4);
	  break;
	  case 2:
	glTranslatef(pind->pos[0],-pind->pos[1],-30);
      glRotatef(-pind->direction+90,0.0,0.0,1.0);
      gluQuadricTexture(qobj, true); 
      this->bindTexture(*epuckText);
	  gluCylinder(qobj,100,100,200,40,40);      
	  glTranslatef(0,0,200);
      this->bindTexture(*headText);
      glRotatef(180,0.0,0.0,1.0);
      gluDisk(qobj,0,100,40,4);
	  
	 break;

	  }


      glDisable(GL_TEXTURE_2D);
      glPopMatrix();	
    }  	
}

void Viewer::init()
{
  setWindowTitle(tr("3D Robot/Environment"));
  restoreStateFromFile();
 floorText= new QImage(":/images/txfabric.jpg");

 #if ROBOTTYPE=EPUCK
  epuckText= new QImage(":/images/epuck3d2.png");
  
  headText = new QImage(":/images/epuckHead.png");
#endif

   #if ROBOTTYPE=LEGO
  epuckText= new QImage(":/images/legorostro.png");
  
  headText = new QImage(":/images/legohead.png");
#endif


  
  
  qglviewer::Vec minv(-1000.0f,-1000.0f,-1000.0f);
  qglviewer::Vec maxv(1000.0f,1000.0f,1000.0f);
  setSceneBoundingBox(minv,maxv);
  //camera()->centerScene();
  
  //showEntireScene();
  //Opens help window
  //help();
}

QString Viewer::helpString() const
{
  QString text("<h2>S i m p l e V i e w e r</h2>");
  /*
  text += "Use the mouse to move the camera around the object. ";
  text += "You can respectively revolve around, zoom and translate with the three mouse buttons. ";
  text += "Left and middle buttons pressed together rotate around the camera view direction axis<br><br>";
  text += "Pressing <b>Alt</b> and one of the function keys (<b>F1</b>..<b>F12</b>) defines a camera keyFrame. ";
  text += "Simply press the function key again to restore it. Several keyFrames define a ";
  text += "camera path. Paths are saved when you quit the application and restored at next start.<br><br>";
  text += "Press <b>F</b> to display the frame rate, <b>A</b> for the world axis, ";
  text += "<b>Alt+Return</b> for full screen mode and <b>Control+S</b> to save a snapshot. ";
  text += "See the <b>Keyboard</b> tab in this window for a complete shortcut list.<br><br>";
  text += "Double clicks automates single click actions: A left button double click aligns the closer axis with the camera (if close enough). ";
  text += "A middle button double click fits the zoom of the camera and the right button re-centers the scene.<br><br>";
  text += "A left button double click while holding right button pressed defines the camera <i>Revolve Around Point</i>. ";
  text += "See the <b>Mouse</b> tab and the documentation web pages for details.<br><br>";
  text += "Press <b>Escape</b> to exit the viewer.";
  */
  return text;
}


void Viewer::update3d()
{
  update(0,0,1000,1000);
}


void
update_rend3drobot()
{
   if (view!= NULL)
	view->update3d();
}

void 
init_rend3drobot()

{

  if (view != NULL) 
      view->show();

  if (view == NULL)
     {
	 view=new Viewer();
	 view->show();
     }
}

#endif
#endif

