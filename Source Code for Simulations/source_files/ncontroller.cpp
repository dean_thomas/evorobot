/* 
 * Evorobot* - ncontroller.cpp
 * Copyright (C) 2009, Stefano Nolfi
 * LARAL, Institute of Cognitive Science and Technologies, CNR, Roma, Italy 
 * http://laral.istc.cnr.it

 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/*
 * network.c
 *
 * There are some improvements to make:
 * The variable nhidden should be allocated here. To do that, however,
 * we have to divide the vector containing the label of the neurons in different groups 
 * (sensory, internal, and motors)
 * We have to allocate the vector for neurons state and the vector for free
 * parameters here dynamically
 * We should include the block description into the individual structure
 * so to allow the use of non-homogeneous individuals
 */

#include "public.h"
#include "ncontroller.h"
#include "robot-env.h" //modificato per fare slavare la rete totale

int ninputs;                // n. input units                          
int nhiddens;               // n. hidden units                    
int noutputs;               // n. output units
int nneurons;               // total n. of neurons
int net_nblocks;            // network number of connection blocks
int net_block[MAXN][5];     // blocks: 0=type (w,b,d), from 1 per 2 receive from 3 per 4
int neuronbias[MAXN];       // whether neurons have bias
int neurondelta[MAXN];      // whether neurons have time constants
int neuronxy[MAXN][2];      // neurons xy position in display space
int neurondisplay[MAXN];    // whether neurons should be displayed or not
float *neuronlesion;        // lesioned neurons are frozen on fixed value 
int neuronlesions=0;        // whether we have at least one neuron leasioned
int dynrechid=0;            // recurrent connections on the hiddens
int dynrecout=0;            // recurrent connections on the outputs
int dynioconn=0;            // input output connections
int dyndeltai=0;            // delta on the input units
int dyndeltah=0;            // delta on the hidden units
int dyndeltao=0;            // delta on the output units
int drawnstyle=0;           // style of draws (1=bars, 2=curves)
int drawn_alltogether=0;    // draw individuals' value on the same graph  
int drawneuronsdx=500;      // width of windows area for displaying neurons state  
float wrange=5.0;           // weights and biases range                 
float *phenotype;			// phenotype's fixed parameters  
int pheno_loaded=0;         // whether phenotype parameters have been loaded


extern float wrange;        // weights and biases range
extern int n_variable_evolution_is_running;
extern int nindividuals;
extern int color_into_genotype;
extern void modify_individual_color(struct individual *pind);


struct ncontroller *ncontrollers=NULL;          // neural controllers                


/*
 * create network parameters
 */
void
create_network_par()

{
   create_iparameter("rec_hiddens", "network",&dynrechid, 0, 1, "whether internal neurons have recurrent connections or not");
   create_iparameter("rec_outputs", "network",&dynrecout, 0, 1, "whether motor neurons have recurrent connections or not");
   create_iparameter("input_output_c", "network",&dynioconn, 0, 1, "whether sensory neurons are connected to motor neurons or not");
   create_iparameter("delta_inputs", "network",&dyndeltai, 0, 1, "whether sensory neurons are leaky neurons or not");
   create_iparameter("delta_hiddens", "network",&dyndeltah, 0, 1, "whether internal neurons are leaky neurons or not");
   create_iparameter("delta_outputs", "network",&dyndeltao, 0, 1, "whether motor neurons are leaky neurons or not");
   create_iparameter("drawnstyle", "network",&drawnstyle, 0, 1, "draw neurons state by using bars or curves");
   create_iparameter("drawn_allt", "network",&drawn_alltogether, 0, 1, "draw the state of the neurons of different individuals on the same graph");
   create_iparameter("drawneuronsdx", "network",&drawneuronsdx, 0, 1000, "width of drawing window");
   create_fparameter("wrange","network",&wrange, 0, 0, "range of weights and biases parameters");
}



/*
 * initialize ncontrollers
 */
void
init_network(int first_ind_idx)

{
    float *nl;
    int   i;
    struct ncontroller *cncontroller, *tmp_ncontrollers;
    int nf;
    float *phe;

	// we create the neural controllers structure
	if (ncontrollers == NULL) { 		// se al momento della chiamata il vettore delle reti di controllo non � allocato,
										// allora vuol dire che � alla prima chiamata 
		ncontrollers = (struct ncontroller *) malloc((nteam) * sizeof(struct ncontroller));
		if (ncontrollers == NULL)
		{
			display_error("controller malloc error");
		}
	}
	else {					// altrimenti lo rialloca i fenotipi rispetto ai nuovi genotipi creati
		tmp_ncontrollers = (struct ncontroller *) realloc(ncontrollers,(nteam) * sizeof(struct ncontroller));
		if (tmp_ncontrollers == NULL)
		{
			display_error("controller reallocation error");
		}
		ncontrollers=tmp_ncontrollers;
	}

	for(i=first_ind_idx, cncontroller = ncontrollers+first_ind_idx; i < nteam; i++, cncontroller++)
     {
       read_robot_units(i, &cncontroller->ninputs, &cncontroller->nhiddens, &cncontroller->noutputs);
       cncontroller->nneurons = cncontroller->ninputs + cncontroller->nhiddens + cncontroller->noutputs;        
       cncontroller->activation = (float *)malloc(cncontroller->nneurons * sizeof(float));	                    
     }

    // create the net blocks
	if ((load_net_blocks("evorobot.net") == 0) && (first_ind_idx == 0)) // esegue solo la prima volta
      create_net_block(ncontrollers);
    // compute the number of free parameters
    nf = compute_nfreep(ncontrollers->nneurons);

    // allocate space for free parameters
    for(i=first_ind_idx, cncontroller = ncontrollers+first_ind_idx; i < nteam; i++, cncontroller++)
    {
       cncontroller->nfreep   = nf;
       cncontroller->freep = (float *)malloc(nf * sizeof(float));	                    
    }

	if (first_ind_idx == 0) {											// esegue solo la prima volta
		phenotype = (float *)malloc(ncontrollers->nfreep * sizeof(float));
		for (i=0,phe=phenotype; i < ncontrollers->nfreep; i++, phe++)
			*phe = -999.0;

	    neuronlesion = (float *)malloc(ncontrollers->nneurons * sizeof(float));
		for (i=0, nl=neuronlesion; i < ncontrollers->nneurons; i++, nl++)
			*nl = -9.9;

	    load_phenotype_data("robot.phe", ncontrollers->freep, ncontrollers->nfreep);
	}
}

/*
 * return the number of free parameters
 */
void
ncontroller_read_nfreep(int *numfreep)

{
   *numfreep = ncontrollers->nfreep;
}


/*
 * perform the genotype to phenotype mapping for controller i 
 */
void
genotype_to_phenotype(int i, int *geno, int nbits, int homogeneous)

{
	int     j;	
    float   *w;
    int     *gi;
    float   value;
    struct  ncontroller *cncontroller;
    float   *phe;
	struct  individual  *pind;

	cncontroller = (ncontrollers + i);	// seleziona il controller dell' individuo i-esimo
    if (homogeneous == 0)
		gi = (geno + (cncontroller->nfreep * i));
    else
		gi = geno;
  
    for (j=0, w = cncontroller->freep; j < cncontroller->nfreep; j++, w++, gi++)
    {
		value = (float) (*gi + 1) / (float) 256.0;		// normalizza tra 0 e 1 il parametro
		*w = wrange - (value * (wrange * (float) 2.0));	// normalizza tra -5 e 5 il parametro
    }
    
    if (pheno_loaded == 1) 
		for (j=0, w = cncontroller->freep, phe = phenotype; j < cncontroller->nfreep; j++, w++, phe++)
		{
			if (*phe != -999.0)
				*w = *phe;
		}

	// Se il colore viene codificato nel genotipo ed evoluto
	if (color_into_genotype) {
		pind = (ind + i);						// seleziona l'individuo di cui caricare l'RGB
		gi=(geno + cncontroller->nfreep + 20);	// seleziona gli ultimi 3 byte del genotipo per caricarli nell'RGB dell'individuo	
		pind->RColor=*gi;						// componente RED
		gi++;
		pind->GColor=*gi;						// componente GREEN
		gi++;
		pind->BColor=*gi;						// componente BUE
		gi++;
	
		modify_individual_color(ind+i);		// assegna il nuovo colore originario all'individuo resuscitato
		pind->ROrigColor=pind->RColor;		// Imposta come colore originario del robot quello impostato
		pind->GOrigColor=pind->GColor;
		pind->BOrigColor=pind->BColor;

	}
}


/*
 * create the block structure for a standard architecture
 * create x-y coordinate for network architecture display
 * store in local variable the number of neurons
 */

void
create_net_block(struct ncontroller *cncontroller) 
{

    int n;
    int i;
    int ng;
    int startx;
    int dx;

    ninputs = cncontroller->ninputs;
    noutputs = cncontroller->noutputs;
    nneurons = cncontroller->nneurons;
    nhiddens = nneurons - (ninputs + noutputs);

    // time constants 
    for(i=0; i < ninputs; i++)
      if (dyndeltai > 0)
	neurondelta[i]= 1;
       else
	neurondelta[i]= 0;
    for(i=ninputs; i < (nneurons - noutputs); i++)
      if (dyndeltah > 0)
	neurondelta[i]= 1;
       else
	neurondelta[i]= 0;
    for(i = (nneurons - noutputs); i < nneurons; i++)
      if (dyndeltao > 0)
	neurondelta[i]= 1;
       else
	neurondelta[i]= 0;

    // biases
    for(i=0; i < nneurons; i++)
      if (i < ninputs)
	neuronbias[i]= 0;
       else
	neuronbias[i]= 1;

    net_nblocks = 0;
    // input update block

    net_block[net_nblocks][0] = 1;
    net_block[net_nblocks][1] = 0;
    net_block[net_nblocks][2] = ninputs;
    net_block[net_nblocks][3] = 0;
    net_block[net_nblocks][4] = 0;
    net_nblocks++;

    // input-hidden connections
    if (nhiddens > 0)
      {
	net_block[net_nblocks][0] = 0;
	net_block[net_nblocks][1] = ninputs;
	net_block[net_nblocks][2] = nhiddens;
	net_block[net_nblocks][3] = 0;
	net_block[net_nblocks][4] = ninputs;
	net_nblocks++;
      }

    // hidden-hidden connections
    if (dynrechid > 0)
      {
	net_block[net_nblocks][0] = 0;
	net_block[net_nblocks][1] = ninputs;
	net_block[net_nblocks][2] = nhiddens;
	net_block[net_nblocks][3] = ninputs;
	net_block[net_nblocks][4] = nhiddens;
	net_nblocks++;
      }

    // hidden update block
    if (nhiddens > 0)
      {
       net_block[net_nblocks][0] = 1;
       net_block[net_nblocks][1] = ninputs;
       net_block[net_nblocks][2] = nhiddens;
       net_block[net_nblocks][3] = 0;
       net_block[net_nblocks][4] = 0;
       net_nblocks++;
      }

    // input-output connections
    if (nhiddens == 0 || dynioconn > 0)
      {
	net_block[net_nblocks][0] = 0;
	net_block[net_nblocks][1] = ninputs + nhiddens;
	net_block[net_nblocks][2] = noutputs;
	net_block[net_nblocks][3] = 0;
	net_block[net_nblocks][4] = ninputs;
	net_nblocks++;
      }

    // hidden-output connections
    if (nhiddens > 0)
      {
	net_block[net_nblocks][0] = 0;
	net_block[net_nblocks][1] = ninputs + nhiddens;
	net_block[net_nblocks][2] = noutputs;
	net_block[net_nblocks][3] = ninputs;
	net_block[net_nblocks][4] = nhiddens;
	net_nblocks++;
      }

    // output-output connections
    if (dynrecout > 0)
      {
	net_block[net_nblocks][0] = 0;
	net_block[net_nblocks][1] = ninputs + nhiddens;
	net_block[net_nblocks][2] = noutputs;
	net_block[net_nblocks][3] = ninputs + nhiddens;
	net_block[net_nblocks][4] = noutputs;
	net_nblocks++;
      }

    // output update block
    net_block[net_nblocks][0] = 1;
    net_block[net_nblocks][1] = ninputs + nhiddens;
    net_block[net_nblocks][2] = noutputs;
    net_block[net_nblocks][3] = 0;
    net_block[net_nblocks][4] = 0;
    net_nblocks++;

   // set neuron xy coordinate for display 
   dx = 25;

   n = 0;
   for(i=0; i < ninputs; i++, n++)
   {
     neuronxy[n][0] = (i * dx) + dx;
     neuronxy[n][1] = 400;
   }

   if (ninputs > (nneurons - (ninputs + noutputs)))
     startx = ninputs * dx;

   for(i=0; i < (nneurons - (ninputs + noutputs)); i++, n++)
   {
     neuronxy[n][0] = startx + (i * dx) + dx;
     neuronxy[n][1] = 225;
   }

   if (ninputs > noutputs)
     startx = ((ninputs - noutputs) / 2) * dx;
   for(i=0; i < noutputs; i++, n++)
   {
     neuronxy[n][0] = startx + (i * dx) + dx;
     neuronxy[n][1] = 50;
   }

   // set neurons whose activation should be displayed
   for(i=0; i < nneurons; i++)
     neurondisplay[i] = 1;
}


/*
 * Update the neural controller
 * and return a pointer to the activation vector 
 */
float *
update_ncontroller(int id, float *input)

{
	int i;
	int t;
	int b;
	float *p;
	float delta;
	float netinput[MAXN];
	float *nl;
	float *act;
	struct ncontroller *cncontroller;

	cncontroller = (ncontrollers + id);

	p   = cncontroller->freep;
	act = cncontroller->activation;
	nl  = neuronlesion; 

	// biases
	for(i=0;i < cncontroller->nneurons;i++)
	{
		if (neuronbias[i] == 1) {
			netinput[i] = *p;
			p++;
		} else {
			netinput[i] = 0.0f;
		}
	}
    // blocks
    for (b=0; b < net_nblocks; b++)
	{
		// connection block
		if (net_block[b][0] == 0) {
			for(t=net_block[b][1]; t < net_block[b][1] + net_block[b][2];t++)
				for(i=net_block[b][3]; i < net_block[b][3] + net_block[b][4];i++)
				{
					netinput[t] += act[i] * *p;
			        p++;
				}
	    } 
		// update block
		if (net_block[b][0] == 1) {
			for(t=net_block[b][1]; t < (net_block[b][1] + net_block[b][2]); t++)
			{
				if (t < cncontroller->ninputs) {
					if (neurondelta[t] == 0) {
					    act[t] = input[t];
					} else {
						delta = (float) (fabs((double) *p) / wrange);
					    p++;
					    act[t] = (act[t] * delta)  + (input[t] * (1.0f - delta));
					}
				} else {
					if (neurondelta[t] == 0) {
						act[t] = logistic(netinput[t]);
					    delta = 0;
					} else {
					    delta = (float) (fabs((double) *p) / wrange);
						p++;
						act[t] = (act[t] * delta)  + (logistic(netinput[t]) * (1.0f - delta));
					}
				}

				if (neuronlesions == 1) {
					if (*nl >= 0.0 && *nl <= 1.0)
						act[t] = *nl;
					nl++;
				}
			}
		}
	}
	
	return(act);
}



/*
 * set to 0.0 the activation value of all units
 */
void
reset_controller(int id)
{

  int    i;
  struct ncontroller *cncontroller;

  cncontroller = (ncontrollers + id);

  for(i=0; i < cncontroller->nneurons; i++)
	cncontroller->activation[i] = 0.0f;		//inizializza tutte le attivazioni 
}


/*
 * This function compute and return the number of free parameters
 * on the basis of the network architectures
 */
int
compute_nfreep(int nneurons)

{
    int i;
    int t;
    int b;
    float delta;
    int   updated[40]; 
    int   ng;
    int   warning;
    int   mode;
    char  warnst[256];

    // if mode == 0, free parameters has not been created yet
    // therefore we do not read them
    //mode = ddelta + dweight + dbias;

    ng  = 0;
    warning = 0;
    for(i=0;i < nneurons;i++)
      updated[i] = 0;
    // biases
    for(i=0;i < nneurons;i++)
	 {
	   if (neuronbias[i] == 1)
	     ng++;
	 }

     // blocks
     for (b=0; b < net_nblocks; b++)
	 {	  
	   // connection block
	   if (net_block[b][0] == 0)
	   {
	     for(t=net_block[b][1]; t < net_block[b][1] + net_block[b][2];t++)
	      for(i=net_block[b][3]; i < net_block[b][3] + net_block[b][4];i++)
	      {
		ng++;
	      }
	     
	   } 
	   // update block
	   if (net_block[b][0] == 1)
	   {
	     for(t=net_block[b][1]; t < (net_block[b][1] + net_block[b][2]); t++)
	      {
                  updated[t] +=1;
		  if (neurondelta[t] == 0)
		   {
		    ;
		   }
		   else
		   {
		    ng++;
		   }
	      }
	   }
	 }

        for(i=0;i < nneurons;i++)
	{
          if (updated[i] < 1 && warning == 0)
	  {
            sprintf(warnst,"neuron %d will never be activated according to the current architecture", i);
	    display_warning(warnst);
	    warning++;
	  }
          if (updated[i] > 1 && warning == 0)
	  {
            sprintf(warnst,"neuron %d will be activated more than once according to the current architecture", i);
	    display_warning(warnst);
	    warning++;
	  }
	}
return(ng);

}

//assembling and compiling
void compile_evonxt(float *p, int np)
{
	//caricare il file evoj1 
	int i,r;
	FILE *fpevo; //
	FILE *fpcmp; //file da assemblare
	char mystring[300];
	fpcmp=fopen("EvoNetNXT.java","w");
	fpevo=fopen("EvoNetNXTS.dat","r");
	

	if ( (fpevo!= NULL) && (fpcmp!=NULL))
	{
	while(fgets(mystring,300,fpevo)!=NULL)
	{
	
		fprintf(fpcmp,"%s",mystring);

	}

		//cominciamo a scrivere
			fprintf(fpcmp,"ninputs	=	%d;\n",ninputs);	 //nsensors
			fprintf(fpcmp,"nhiddens=	%d;\n",nhiddens);	 //da mettere
			fprintf(fpcmp,"noutputs=	%d;\n",noutputs);
			fprintf(fpcmp,"nneurons=	%d;\n",nneurons);
			fprintf(fpcmp,"net_nblocks= %d;\n",net_nblocks);	//nblocks
			fprintf(fpcmp,"wrange	=	%f;\n",wrange);
			fprintf(fpcmp,"ndata	=	%d;\n",np);
			//parte per i sensori attivi
			fprintf(fpcmp,"bumpsensors	=	%d;\n",ipar->bumpsensors);
			fprintf(fpcmp,"bpsensors_startang_sect1	=	%d;\n",ipar->bpsensors_startang_sect1);
			fprintf(fpcmp,"bpsensors_endang_sect1	=	%d;\n",ipar->bpsensors_endang_sect1);
			fprintf(fpcmp,"bpsensors_startang_sect2	=	%d;\n",ipar->bpsensors_startang_sect2);
			fprintf(fpcmp,"bpsensors_endang_sect2	=	%d;\n",ipar->bpsensors_endang_sect2);
			fprintf(fpcmp,"bpsensors_startang_sect3	=	%d;\n",ipar->bpsensors_startang_sect3);
			fprintf(fpcmp,"bpsensors_endang_sect3	=	%d;\n",ipar->bpsensors_endang_sect3);
			fprintf(fpcmp,"bpsensors_startang_sect4	=	%d;\n",ipar->bpsensors_startang_sect4);
			fprintf(fpcmp,"bpsensors_endang_sect4	=	%d;\n",ipar->bpsensors_endang_sect4);
			fprintf(fpcmp,"bumpsensors_display	=	%d;\n",ipar->bumpsensors_display);
			fprintf(fpcmp,"nifsensors	=	%d;\n",ipar->nifsensors);
			fprintf(fpcmp,"nxtcamsensor	=	%d;\n",ipar->nxtcam);
			fprintf(fpcmp,"RGBColorRetina	=	%d;\n",ipar->rgbcolorretina);
			fprintf(fpcmp,"RGBColorRetinaViewAngle	=	%d;\n",ipar->rgbcolorretinaviewangle);
			fprintf(fpcmp,"RGBColorRetinaGrayScale	=	%d;\n",ipar->rgbcolorretinagrayscale);
			fprintf(fpcmp,"RGBColorRetinaManualGrayScale	=	%d;\n",ipar->rgbcolorretinamanualgrayscale);
			fprintf(fpcmp,"RGBColorRetinaMaxDistance	=	%d;\n",ipar->rgbcolorretinamaxdistance);
			fprintf(fpcmp,"RGBColorRetinaBlueDisabledRace	=	%d;\n",ipar->rgbcolorretinabluedisabledrace);
			fprintf(fpcmp,"OrientationCam	=	%d;\n",ipar->orientationcam);
			fprintf(fpcmp,"eyes	=	%d;\n",ipar->eyes);
			fprintf(fpcmp,"motorload	=	%d;\n",ipar->motorload);
			//fine parte sensori


			fprintf(fpcmp,"neuronbias=new int[nneurons];\n");
			fprintf(fpcmp,"neurondelta=new int[nneurons];\n");
			fprintf(fpcmp,"data=new double[ndata];\n");
			fprintf(fpcmp,"act=new double[nneurons];\n");
			fprintf(fpcmp,"input=new double[ninputs];\n");
			fprintf(fpcmp,"netinput=new double[nneurons];\n");
			fprintf(fpcmp,"net_block=new int[net_nblocks][5];\n");

			fprintf(fpcmp,"for(i=0;i<nneurons;i++) act[i]=0;\n");
			
			//adesso scriviamoi dati bias e delta
			for(i=0;i<nneurons;i++)
			{
				fprintf(fpcmp,"neuronbias[%d]=%d;\n",i,neuronbias[i]);
				fprintf(fpcmp,"neurondelta[%d]=%d;\n",i,neurondelta[i]);


			}
			fprintf(fpcmp,"\n");
			//adesso scriviamo
			for(i=0;i<np;i++)
			{
			fprintf(fpcmp,"data[%d] = %f;\n",i,*p);
			p++;
			}
			//Mettiamo i netblock
			for(i=0;i<net_nblocks;i++)
				for(r=0;r<5;r++)
					fprintf(fpcmp,"net_block[%d][%d]=%d;\n",i,r,net_block[i][r]);
				
		
		fprintf(fpcmp,"}\n");
		fprintf(fpcmp,"}\n");
	
		


	}else
	{
	//errore
	}
	fclose(fpevo);
	fclose(fpcmp);
	system("nxjc EvoNetNXT.java"); //compiling
	system("nxj  EvoNetNXT");		//downloading


}

/*
 * save the description of a network architecture in blocks
 */
void
save_net_blocks(char *filename)


{

   FILE	   *fp;
   int     b;
   int     n;
   char  warnst[256];


   if ((fp = fopen(filename,"w")) != NULL)
	{
         fprintf(fp,"nneurons %d\n", nneurons);
         fprintf(fp,"nsensors %d\n", ninputs);
         fprintf(fp,"nmotors %d\n", noutputs);
         fprintf(fp,"nblocks %d\n", net_nblocks);
	 for (b=0; b < net_nblocks; b++)
	 {
           fprintf(fp,"%d  %d %d %d %d", net_block[b][0],net_block[b][1],net_block[b][2],net_block[b][3],net_block[b][4]);
           if (net_block[b][0] == 0)
              fprintf(fp," // connections block\n");
           if (net_block[b][0] == 1)
              fprintf(fp," // block to be updated\n");
	 }

        fprintf(fp,"neurons xy coordinate and display mode\n");
	for(n=0; n < nneurons; n++)
           fprintf(fp,"%d %d %d %d %d\n", neuronbias[n], neurondelta[n], neuronxy[n][0], neuronxy[n][1], neurondisplay[n]);

	sprintf(warnst, "network architecture data saved on file %s", filename);
	display_stat_message(warnst);
	}
      else
       {
	  display_warning("unable to create the file evorobot.net");
       }
   fclose(fp);
}


/*
 * load the description of a network architecture in blocks
 */
int
load_net_blocks(char *filename)


{

   FILE	   *fp;
   int     b;
   int     n;
   char    warnst[256];

   if ((fp = fopen(filename,"r")) != NULL)
	{
         fscanf(fp,"nneurons %d\n", &nneurons);
         fscanf(fp,"nsensors %d\n", &ninputs);
         fscanf(fp,"nmotors %d\n", &noutputs);
         nhiddens = nneurons - (ninputs + noutputs);
         fscanf(fp,"nblocks %d\n", &net_nblocks);
	 for (b=0; b < net_nblocks; b++)
	 {
           fscanf(fp,"%d  %d %d %d %d", &net_block[b][0],&net_block[b][1],&net_block[b][2],&net_block[b][3],&net_block[b][4]);
           if (net_block[b][0] == 0)
              fscanf(fp," // connections block\n");
           if (net_block[b][0] == 1)
              fscanf(fp," // block to be updated\n");
	 }
        fscanf(fp,"neurons xy coordinate and display mode\n");
	for(n=0; n < nneurons; n++)
           fscanf(fp,"%d %d %d %d %d\n", &neuronbias[n], &neurondelta[n], &neuronxy[n][0], &neuronxy[n][1], &neurondisplay[n]);
	fclose(fp);

	sprintf(warnst,"loaded file %s", filename);
	display_stat_message(warnst);
	update_rendnetwork(0);
	return(1);
	}
      else
       {
	return(0);
       }
}

void save_nn_data(char *filen, float *p, int np)
{
//saving all neural net data in a single file
int i,t,b,n;
char  errorm[64];
FILE *fp;

    if ((fp=fopen(filen, "w+")) == NULL) 
    {
	sprintf(errorm,"cannot open %s file",filen);
	display_error(errorm);
    }
	//NDEF neuron definition
	fprintf(fp,"robot lego\n");
	fprintf(fp,"neurondefs\n");
	if (ipar->bumpsensors >0) {
		 fprintf(fp,"bumpsensors %d\n",ipar->bumpsensors);
		 fprintf(fp,"bpsensors_startang_sect1 %d\n",ipar->bpsensors_startang_sect1);
		 fprintf(fp,"bpsensors_endang_sect1 %d\n",ipar->bpsensors_endang_sect1);
		 fprintf(fp,"bpsensors_startang_sect2 %d\n",ipar->bpsensors_startang_sect2);
		 fprintf(fp,"bpsensors_endang_sect2 %d\n",ipar->bpsensors_endang_sect2);
		 fprintf(fp,"bpsensors_startang_sect3 %d\n",ipar->bpsensors_startang_sect3);
		 fprintf(fp,"bpsensors_endang_sect3 %d\n",ipar->bpsensors_endang_sect3);
		 fprintf(fp,"bpsensors_startang_sect4 %d\n",ipar->bpsensors_startang_sect4);
		 fprintf(fp,"bpsensors_endang_sect4 %d\n",ipar->bpsensors_endang_sect4);
		 fprintf(fp,"bumpsensors_display %d\n",ipar->bumpsensors_display);
	}
	if (ipar->nifsensors >0)
		 fprintf(fp,"nifsensors %d\n",ipar->nifsensors);
	if (ipar->ifang>0)
		 fprintf(fp,"ifang %d\n",ipar->ifang);
	if (ipar->ifdang>0)
		 fprintf(fp,"ifdang %d\n",ipar->ifdang);
	if (ipar->ifrange>0)
		 fprintf(fp,"ifrange %d\n",ipar->ifrange);
	if (ipar->motmemory>0)
		 fprintf(fp,"motmemory %d\n",ipar->motmemory);
	
	if (ipar->groundsensor >0)
		 fprintf(fp,"groundsensor %d\n",ipar->groundsensor);
	
	//	Sensors relating to food
	if (ipar->foodSensor > 0)
		fprintf(fp, "foodSensor %d\n",ipar->foodSensor);
	if (ipar->hungerSensor > 0)
		fprintf(fp, "hungerSensor %d\n",ipar->hungerSensor);
	
	//	Sensors relating to water
	if (ipar->waterSensor > 0)
		fprintf(fp, "waterSensor %d\n",ipar->waterSensor);
	if (ipar->thirstSensor > 0)
		fprintf(fp, "thirstSensor %d\n",ipar->thirstSensor);

	if (ipar->a_groundsensor >0)
		 fprintf(fp,"a_groundsensor %d\n",ipar->a_groundsensor);
	if (ipar->a_groundsensor2 >0)
		 fprintf(fp,"a_groundsensor2 %d\n",ipar->a_groundsensor2);
	if (ipar->lightsensors >0)
		 fprintf(fp,"lightsensors %d\n",ipar->lightsensors);
	if (ipar->oppsensor >0)
		 fprintf(fp,"oppsensor %d\n",ipar->oppsensor);
	if (ipar->camerareceptors >0)
		 fprintf(fp,"camerareceptors %d\n",ipar->camerareceptors);
	if (ipar->cameraviewangle >0)
		 fprintf(fp,"cameraviewangle %d\n",ipar->cameraviewangle);
	if (ipar->cameradistance >0)
		 fprintf(fp,"cameradistance %d\n",ipar->cameradistance);
	if (ipar->cameracolors >0)
		 fprintf(fp,"cameracolors %d\n",ipar->cameracolors);
	if (ipar->input_outputs >0)
		 fprintf(fp,"input_outputs %d\n",ipar->input_outputs);
	if (ipar->signal_sensors >0)
		 fprintf(fp,"signal_sensors %d\n",ipar->signal_sensors);
	if (ipar->signalss >0)
		 fprintf(fp,"signalss %d\n",ipar->signalss);
	if (ipar->signalso >0)
		 fprintf(fp,"signalso %d\n",ipar->signalso);
	if (ipar->signalssf >0)
		 fprintf(fp,"signalssf %d\n",ipar->signalssf);
	if (ipar->signalssb >0)
		 fprintf(fp,"signalssb %d\n",ipar->signalssb);
	if (ipar->signaltype >0)
		 fprintf(fp,"signaltype %d\n",ipar->signaltype);
	if (ipar->wheelmotors >0)
		 fprintf(fp,"wheelmotors %d\n",ipar->wheelmotors);
	if (ipar->compass >0)
		 fprintf(fp,"compass %d\n",ipar->compass);
	if (ipar->simplevision >0)
		 fprintf(fp,"simplevision %d\n",ipar->simplevision);
	if (ipar->nutrients >0)
		 fprintf(fp,"nutrients %d\n",ipar->nutrients);
	if (ipar->energy >0)
		 fprintf(fp,"energy %d\n",ipar->energy);
	if (ipar->nxtcam >0)
		 fprintf(fp,"nxtcam %d\n",ipar->nxtcam);
	if (ipar->orientationcam >0)
		 fprintf(fp,"OrientationCam %d\n",ipar->orientationcam);
	if (ipar->eyes >0)
		 fprintf(fp,"eyes %d\n",ipar->eyes);
	if (ipar->leadersfollowerspercepsystem >0)
		 fprintf(fp,"leadersfollowerspercepsystem %d\n",ipar->leadersfollowerspercepsystem);

	//	Food
	if (ipar->foodperceptionsystem >0)
		 fprintf(fp,"foodperceptionsystem %d\n",ipar->foodperceptionsystem);
	
	//	Water
	if (ipar->waterperceptionsystem >0)
		 fprintf(fp,"waterperceptionsystem %d\n",ipar->waterperceptionsystem);
	
	//	Day / Night cycles
	if (ipar->switchDayNightEvery >0)
		fprintf(fp,"switchDayNightEvery %d\n",ipar->switchDayNightEvery);
	if (ipar->dayNightSensor > 0)
		fprintf(fp, "dayNightSensor %d\n",ipar->dayNightSensor);

	//	Age sensor
	if (ipar->ageSensor > 0)
		fprintf(fp, "ageSensor %d\n",ipar->ageSensor);

	if (ipar->nperceptualindividuals >0)
		 fprintf(fp,"nperceptualindividuals %d\n",ipar->nperceptualindividuals);
	if (ipar->perceptualrace >0)
		 fprintf(fp,"perceptualrace %d\n",ipar->perceptualrace);
	if (ipar->motionlessrace >0)
		 fprintf(fp,"motionlessrace %d\n",ipar->motionlessrace);
	if (ipar->motionlessgroup_but_nind >0)
		 fprintf(fp,"motionlessgroup_but_nind %d\n",ipar->motionlessgroup_but_nind);
	if (ipar->motionlessgroup_but_nind2 >0)
		 fprintf(fp,"motionlessgroup_but_nind2 %d\n",ipar->motionlessgroup_but_nind2);
	if (ipar->motionlessind >0)
		 fprintf(fp,"motionlessind %d\n",ipar->motionlessind);
	if (ipar->Xmotionlessind >0)
		 fprintf(fp,"Xmotionlessind %d\n",ipar->Xmotionlessind);
	if (ipar->Ymotionlessind >0)
		 fprintf(fp,"Ymotionlessind %d\n",ipar->Ymotionlessind);
	if (ipar->motionlessind >0)
		 fprintf(fp,"FDmotionlessind %d\n",ipar->FDmotionlessind);
	if (ipar->lab_test_all_seed >0)
		 fprintf(fp,"lab_test_all_seed %d\n",ipar->lab_test_all_seed);
	if (ipar->lab_test_number_seed >0)
		 fprintf(fp,"lab_test_number_seed %d\n",ipar->lab_test_number_seed);
	if (ipar->lab_test_all_gen >0)
		 fprintf(fp,"lab_test_all_gen %d\n",ipar->lab_test_all_gen);
	if (ipar->lab_test_gen_step >0)
		 fprintf(fp,"lab_test_gen_step %d\n",ipar->lab_test_gen_step);
	if (ipar->lab_test_number_gen >0)
		 fprintf(fp,"lab_test_number_gen %d\n",ipar->lab_test_number_gen);
	if (ipar->lab_test_team1 >0)
		 fprintf(fp,"lab_test_team1 %d\n",ipar->lab_test_team1);
	if (ipar->lab_test_team1_all_ind_race >0)
		 fprintf(fp,"lab_test_team1_all_ind_race %d\n",ipar->lab_test_team1_all_ind_race);
	if (ipar->lab_test_team2 >0)
		 fprintf(fp,"lab_test_team2 %d\n",ipar->lab_test_team2);
	if (ipar->lab_test_team2 >0)
		 fprintf(fp,"lab_test_team2_number_seed %d\n",ipar->lab_test_team2_number_seed);
	if (ipar->lab_test_team2 >0)
		 fprintf(fp,"lab_test_team2_number_gen %d\n",ipar->lab_test_team2_number_gen);
	if (ipar->lab_test_team2_all_ind_race >0)
		 fprintf(fp,"lab_test_team2_all_ind_race %d\n",ipar->lab_test_team2_all_ind_race);
	if (ipar->lab_test_all_pairs >0)
		 fprintf(fp,"lab_test_all_pairs %d\n",ipar->lab_test_all_pairs);
	if (ipar->lab_test_team3 >0)
		 fprintf(fp,"lab_test_team3 %d\n",ipar->lab_test_team3);
	if (ipar->lab_test_team4 >0)
		 fprintf(fp,"lab_test_team3 %d\n",ipar->lab_test_team4);
	if (ipar->lab_ntestingteam >0)
		 fprintf(fp,"lab_ntestingteam %d\n",ipar->lab_ntestingteam);
	if (ipar->lab_test_best_team1 >0)
		 fprintf(fp,"lab_test_best_team1 %d\n",ipar->lab_test_best_team1);
	if (ipar->lab_test_best_team2 >0)
		 fprintf(fp,"lab_test_best_team2 %d\n",ipar->lab_test_best_team2);
	if (ipar->lab_test_best_team3 >0)
		 fprintf(fp,"lab_test_best_team3 %d\n",ipar->lab_test_best_team3);
	if (ipar->lab_test_best_team4 >0)
		 fprintf(fp,"lab_test_best_team4 %d\n",ipar->lab_test_best_team4);
	if (ipar->lab_test_ghost_team1  >0)
		 fprintf(fp,"lab_test_ghost_team1  %d\n",ipar->lab_test_ghost_team1 );
	if (ipar->lab_test_ghost_team2  >0)
		 fprintf(fp,"lab_test_ghost_team2  %d\n",ipar->lab_test_ghost_team2 );
	if (ipar->lab_test_ghost_team3  >0)
		 fprintf(fp,"lab_test_ghost_team3  %d\n",ipar->lab_test_ghost_team3 );
	if (ipar->lab_test_ghost_team4  >0)
		 fprintf(fp,"lab_test_ghost_team4  %d\n",ipar->lab_test_ghost_team4 );
	if (ipar->lab_show_number >0)
		 fprintf(fp,"lab_show_number %d\n",ipar->lab_show_number);
	if (ipar->percepindwithdiffcolor >0)
		 fprintf(fp,"percepindwithdiffcolor %d\n",ipar->percepindwithdiffcolor);
	if (ipar->percepindcannotseeother >0)
		 fprintf(fp,"percepindcannotseeother %d\n",ipar->percepindcannotseeother);
	if (ipar->percepindcannothavefitness  >0)
		 fprintf(fp,"percepindcannothavefitness  %d\n",ipar->percepindcannothavefitness );
	if (ipar->probpercepindinheritfromfather >0)
		 fprintf(fp,"probpercepindinheritfromfather %d\n",ipar->probpercepindinheritfromfather);
	if (ipar->nxtcolors >0)
		 fprintf(fp,"nxtcolors %d\n",ipar->nxtcolors);
	if (ipar->wrange >0)
		 fprintf(fp,"wrange %f\n",ipar->wrange);
	
		 fprintf(fp,"blocks \n");
	     fprintf(fp,"nneurons %d\n", nneurons);
         fprintf(fp,"nsensors %d\n", ninputs);
         fprintf(fp,"nmotors %d\n", noutputs);
         fprintf(fp,"nblocks %d\n", net_nblocks);
	 for (b=0; b < net_nblocks; b++)
	 {
           fprintf(fp,"%d  %d %d %d %d", net_block[b][0],net_block[b][1],net_block[b][2],net_block[b][3],net_block[b][4]);
           if (net_block[b][0] == 0)
              fprintf(fp," // connections block\n");
           if (net_block[b][0] == 1)
              fprintf(fp," // block to be updated\n");
	 }

        fprintf(fp,"neuronsxy\n");
	for(n=0; n < nneurons; n++)
           fprintf(fp,"%d %d %d %d %d\n", neuronbias[n], neurondelta[n], neuronxy[n][0], neuronxy[n][1], neurondisplay[n]);

	//
	fprintf(fp,"data\n");
	for(i=0;i<np;i++)
	{
	fprintf(fp,"%f\n",*p);
	p++;
	}






	 fprintf(fp,"END\n");
     fflush(fp);	       
     fclose(fp);


//end
}

/*
 * Save the free parameters *p into a file .phe
 */
void
save_phenotype_data(char *filen, float *p, int np)

{
    int i;
    int t;
    int b;
    char  errorm[64];
    FILE *fp;

    if ((fp=fopen(filen, "w+")) == NULL) 
    {
	sprintf(errorm,"cannot open %s file",filen);
	display_error(errorm);
    }

    // biases
    for(i=0;i < nneurons;i++)
	 {
	   if (neuronbias[i] == 1)
	   {
	     fprintf(fp,"%f \tbias %s\n",*p, neuronl[i]);
	     p++;
	   }
	 }
     // blocks
     for (b=0; b < net_nblocks; b++)
	 {
	   if (net_block[b][0] == 0)
	   {
	     for(t=net_block[b][1]; t < net_block[b][1] + net_block[b][2];t++)
	      {
	       for(i=net_block[b][3]; i < net_block[b][3] + net_block[b][4];i++)
	       {
		fprintf(fp,"%f \tweight %s from %s\n",*p, neuronl[t], neuronl[i]);
	        p++;
	       }
	      }
	   }
	   if (net_block[b][0] == 1)
	   {
	     for(t=net_block[b][1]; t < (net_block[b][1] + net_block[b][2]); t++)
	      {
		if (neurondelta[t] == 1)
		   {
                    fprintf(fp,"%f \ttimeconstant %s (%.2f)\n", *p, neuronl[t],(fabs((double) *p) / wrange));
		    p++;
		   }  
	     }
	   }	   
	 } 
	 
     fprintf(fp,"END\n");
     fflush(fp);	       
     fclose(fp);
}

/*
 * Load the free parameters from a .phe file
 */
int
load_phenotype_data(char *filen, float *p, int np)

{
    int i;
    int t;
    int b;
    int iv;
    char  cbuffer[128];
    char  cbufferb[128];
    float *phe;
    FILE *fp;

    if ((fp=fopen(filen, "r")) == NULL) 
    {
	if (strcmp(filen, "robot.phe") != 0)
         {
	  sprintf(cbuffer,"cannot open %s file",filen);
	  display_error(cbuffer);
         }
	return(0);
    }

   i = 0;
   phe = phenotype;
   while (fgets(cbuffer,128,fp) != NULL && i < np)
     {
	  if (*cbuffer == '*')
	    *p = -999.0;
	   else
	    sscanf(cbuffer,"%f",p);
	  *phe = *p;
	  i++;
	  p++;
	  phe++;
     }
   pheno_loaded = 1;

   if (i < np)
      display_warning("the file contain an insufficient number of data");
   //fgets(cbuffer,128,fp);
   //if (*cbuffer != 'E')
   //   display_warning("the file does not contain the expected number of values");

   fclose(fp);
   return(1);

}




//-----------------------------------------------------------------
//------------ Private Functions ----------------------------------
//-----------------------------------------------------------------


/*
 * standard logistic function
 */
float
logistic(float f)

{
    
    return((float) (1.0 / (1.0 + exp(0.0 - f))));
 
}


