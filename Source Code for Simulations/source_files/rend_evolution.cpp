/* 
 * Evorobot* - rend_evolution.cpp
 * Copyright (C) 2009, Stefano Nolfi
 * LARAL, Institute of Cognitive Science and Technologies, CNR, Roma, Italy 
 * http://laral.istc.cnr.it

 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "public.h"

#ifdef EVOGRAPHICS  

#include <QtGui>        // Widget
#include <QKeyEvent>    // Dialog
#include <QFileDialog>  // Dialog
#include <QMessageBox>  // Dialog
#include "rend_evolution.h"
#include "evolution.h"


RendEvolution *rendEvolution;      // The evolution rendering widget
EvolutionDialog *evolutionDialog;  // The evolution Dialog

extern int widgetdx;			        // x-size of the widget
extern int widgetdy;					// y-size of the widget

int wevolution = 0;              // record whether the widget and/or the dialog has been created
int evwidgetdx = 400;			 // x-size of the widget
int evwidgetdy = 400;            // y-size of the widget

int rendevolutionmode;           // whether we plot best or best and average performace or the best of all replications

//----------------------------------------------------------------------
//-------------------  PUBLIC FUNCTIONS  -------------------------------
//----------------------------------------------------------------------

void
init_rendevolution(int mode)
{

    if (mode == 1 && wevolution == 0)
      {
       rendEvolution = new RendEvolution;
       wevolution = 1;
      }

    if (mode == 2 && wevolution == 1)
    {
     evolutionDialog = new EvolutionDialog(NULL);
     evolutionDialog->setWindowTitle("Evolution");
     evolutionDialog->setWidget(rendEvolution, 0, 0);
     evolutionDialog->show();
     update_rendevolution(1);
     wevolution = 2;
    }
    if (mode == 2 && wevolution == 2)
    {
     evolutionDialog->show();
     update_rendevolution(1);
    }
}

void
hide_rendevolution()
{
    if (wevolution == 2)
      evolutionDialog->hide();
}

void
update_rendevolution(int mode)


{

     rendevolutionmode = mode;
     rendEvolution->repaint();
     QCoreApplication::processEvents();

}


void
add_evolution_widget(QGridLayout *mainLayout, int row, int column)
{
  mainLayout->addWidget(rendEvolution, row, column);
}

//----------------------------------------------------------------------
//-------------------  PRIVATE FUNCTIONS  -------------------------------
//----------------------------------------------------------------------

// -----------------------------------------------------
// Dialog and Toolbar
// -----------------------------------------------------

void EvolutionDialog::createToolBars()
{
    QToolBar *evolutiontoolbar;

    evolutiontoolbar = toolBar();

    evolutiontoolbar->addAction(openAct);
    evolutiontoolbar->addAction(displaystatAct);
    evolutiontoolbar->addAction(displayallstatAct);
    evolutiontoolbar->addAction(createlineageAct);

}

void EvolutionDialog::createActions()
{

    openAct = new QAction(QIcon(":/images/open.png"), tr("&Open a file"), this);
    openAct->setShortcut(tr("Ctrl+N"));
    openAct->setStatusTip(tr("Open a file"));
    connect(openAct, SIGNAL(triggered()), this, SLOT(open()));

    displaystatAct = new QAction(QIcon(":/images/fitness.png"), tr("&show fitness"), this);
    displaystatAct->setShortcut(tr("Ctrl+F"));
    displaystatAct->setStatusTip(tr("show fitness statistics"));
    connect(displaystatAct, SIGNAL(triggered()), this, SLOT(displaystat()));

    displayallstatAct = new QAction(QIcon(":/images/fitnessall.png"), tr("&show all fitness"), this);
    displayallstatAct->setShortcut(tr("Ctrl+F"));
    displayallstatAct->setStatusTip(tr("show fitness statistics for all replications"));
    connect(displayallstatAct, SIGNAL(triggered()), this, SLOT(displayallstat()));

    createlineageAct = new QAction(QIcon(":/images/lineage.png"), tr("&create lineage"), this);
    createlineageAct->setShortcut(tr("Ctrl+L"));
    createlineageAct->setStatusTip(tr("create the lineage file"));
    connect(createlineageAct, SIGNAL(triggered()), this, SLOT(createlineage()));
}

int substsubstr(char *strdest, char *strorig, int pos) {

	int i;

	for (i=0; i<strlen(strorig); i++)
		strdest[pos+i]=strorig[i];

	return 1;
}


/*
 * open a .gen .fit or .phe file
 */
void EvolutionDialog::open()
{

	char *f;
    QByteArray filen;
    char filename[256];
	char *substring, *substring2;
	extern int  nraces;									   // number of races
	int race;
	int pos;

    QString fileName = QFileDialog::getOpenFileName(this,"Choose a filename to save under",
						"","*.gen *.fit world?.env *.phe");

	if (fileName.endsWith("gen"))		// capire perch� va in crash dopo questa lettura
	{
		filen = fileName.toAscii();
        f = filen.data();
		strcpy(filename, f);
		
		substring = strstr(filename, "R0.gen");
		if ((substring != NULL) && (nraces>1)) {							// se ci sono pi� razze gestisce la lettura dei best dei genotipi di tutte le razze
			pos=(int)(substring - filename);
			loadallg(-1, filename, 0, 1);

			for (race=1;race<nraces;race++) {
				sprintf(substring,"R%d.gen",race);
				//substsubstr(filename,substring,pos);
				loadallg(-1, filename, 0, race+1);							// fa partire le razze da 1 e non da 0
			}
		} else 
			loadallg(-1, filename, 0, 0);
	}
	else
	if (fileName.endsWith("fit"))
	{
		filen = fileName.toAscii();
		f = filen.data();
		strcpy(filename, f);
		loadstatistics(filename, 1);
	}
}

/*
 * display the fitness statistics of all replications
 */
void EvolutionDialog::displaystat()
{
       update_rendevolution(1);
}

/*
 * display the fitness statistics of all replications
 */
void EvolutionDialog::displayallstat()
{
  display_all_stat();
}


/*
 * create the lineage file
 */
void EvolutionDialog::createlineage()
{
 create_lineage();
}


// -----------------------------------------------------
// Widget
// -----------------------------------------------------


RendEvolution::RendEvolution(QWidget *parent)
    : QWidget(parent)
{
    shape = Polygon;
    antialiased = false;
    pixmap.load(":/images/qt-logo.png");

    setBackgroundRole(QPalette::Base);
}

QSize RendEvolution::minimumSizeHint() const
{
    return QSize(evwidgetdx, evwidgetdy);
}

QSize RendEvolution::sizeHint() const
{
    return QSize(evwidgetdx, evwidgetdy);
}

void RendEvolution::setShape(Shape shape)
{
    this->shape = shape;
    update();
}

void RendEvolution::setPen(const QPen &pen)
{
    this->pen = pen;
    update();
}

void RendEvolution::setBrush(const QBrush &brush)
{
    this->brush = brush;
    update();
}

void RendEvolution::setAntialiased(bool antialiased)
{
    this->antialiased = antialiased;
    update();
}

void RendEvolution::setTransformed(bool transformed)
{
    this->transformed = transformed;
    update();
}


/*
 * we update the widget when the user click on the window

void 
RendEvolution::mousePressEvent(QMouseEvent *event)
{
	
  update_rendevolution(1);

} */

void RendEvolution::paintEvent(QPaintEvent *)
{
 

   int    team;
   int    i;
   int    u;
   int    c;
   int    t,b;
   int    counter;
   int    n_start, n_max;
   int    h1,h2;
   int    sx,sy,dx,dy;
   float  headx, heady;
   int    g,n;
   int    offx,offy;
   int    maxx;
   float  x_scale, y_scale;
   double statfit_int;
   char   label[128];
   char   filen[128];
   char   *chelp;

   int    **ob;
   int    *o;
   int    *ocf;
   int    a,d,s,obj;
   int    a1;
   int    stx, sty;
   int    heig;
   int    scolor;
   
   QRect labelxy(0,0,30,20);          // neuron labels 
   QPoint pxy;

   QPainter painter(this);
   painter.setPen(pen);
   QPen pen(Qt::black, 1);                 // black solid line, 1 pixels wide
   painter.setPen(pen);
   dx = evwidgetdx - 60;
   dy = evwidgetdy - 60;

   // draw statistics
   if (statfit_n > 0)				   // per il momento visualizza la curva di fitness della prima razza
	{
	  offx = 40;
	  offy = 40;
	  // graph dimension 500X500 + offx & offy
	  statfit_int = statfit_max - statfit_min;
	  if (statfit_n > 1 && statfit_n < 500)
	    x_scale = (dx - offx) / (double)(statfit_n - 1); //baco visualizzazione fitness per + di 100 gen
	   else
	    x_scale = 1.0;
	  maxx = dx;
	  if (statfit_n > dx)
		  maxx = statfit_n; 

	  if (statfit_int > 0)
	    y_scale = (dy - offy) / statfit_int;
	   else
        y_scale = 1.0;

      painter.drawLine(0 + offx, dy + offy, maxx + offx, dy + offy);
      painter.drawLine(0 + offx, 0 + offy, 0 + offx, dy + offy);
      sprintf(label,"%d",statfit_n);
      painter.drawText(maxx+40,dy+40, label);
      sprintf(label,"%.1f",statfit_min);
      painter.drawText(0, dy+40, label);
      sprintf(label,"%.1f",statfit_max);
      painter.drawText(0, 40, label);
      if (rendevolutionmode == 1)
	   {
	    if (statfit_n == 1)
               painter.drawEllipse(0 * x_scale + offx - 5, (statfit_int - statfit[0][1]) * y_scale + offy - 5, 10, 10);

	    for(n=0;n < 2; n++)
	    {
	     for(g=0;g < (statfit_n - 1); g++)
	     {
	       painter.drawLine(g * x_scale + offx, (statfit_int - statfit[g][n]) * y_scale + offy, (g + 1) * x_scale + offx, (statfit_int - statfit[g+1][n]) * y_scale + offy);
	     }
	    }
	  }  
	  if (rendevolutionmode == 2)
	   {
	    for(n=0;n < nreplications; n++)
	    {
             sprintf(filen,"statS%d.fit",seed + n);
             loadstatistics(filen,0);
	     for(g=0;g < (statfit_n - 1); g++)
	     {
	       painter.drawLine(g * x_scale + offx, (statfit_int - statfit[g][0]) * y_scale + offy, (g + 1) * x_scale + offx, (statfit_int - statfit[g+1][0]) * y_scale + offy);
	     }
	    }
	  }   
	}   
}

// -----------------------------------------------------
// Dialog
// -----------------------------------------------------

EvolutionDialog::EvolutionDialog(QWidget *parent) :
	QDialog(parent),
	m_mainLayout(NULL),
	m_layout(NULL),
	m_toolBar(NULL)
{
	// Creating the main layout. Direction is BottomToTop so that m_layout (added first)
	// is at bottom, while the toolbar (added last) is on top
	m_mainLayout = new QBoxLayout(QBoxLayout::BottomToTop, this);
	//m_mainLayout->setContentsMargins(0, 0, 0, 0);

	// Creating the layout for external widgets
	m_layout = new QGridLayout();
	//m_layout->setContentsMargins(0, 0, 0, 0);
	m_mainLayout->addLayout(m_layout);

        createActions();
	createToolBars();
}

EvolutionDialog::~EvolutionDialog()
{
}

void EvolutionDialog::setWidget(QWidget *w, int r, int c)
{
	m_layout->addWidget(w, r, c);
}

QToolBar* EvolutionDialog::toolBar()
{
	if (m_toolBar == NULL) {
		// Creating toolbar and adding it to the main layout
		m_toolBar = new QToolBar(this);
		m_mainLayout->addWidget(m_toolBar);
	}

	return m_toolBar;
}

void EvolutionDialog::closeEvent(QCloseEvent *)
{
	emit dialogClosed();
}

void EvolutionDialog::keyReleaseEvent(QKeyEvent* event)
{
    /*
	if (event->matches(QKeySequence::Print)) {
		// Taking a screenshow of the widget
		shotWidget();
	} else {
		// Calling parent function
		QDialog::keyReleaseEvent(event);
	}*/
}

void EvolutionDialog::shotWidget()
{
    /*
	// Taking a screenshot of this widget
	QPixmap shot(size());
	render(&shot);

	// Asking the user where to save the shot
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save Shot"), "./widget.png", tr("Images (*.png *.xpm *.jpg)"));
	if (!fileName.isEmpty()) {
		shot.save(fileName);

		QMessageBox::information(this, QString("File Saved"), QString("The widget shot has been saved"));
	}*/
}


#endif
