

//    Quick Links
//
//    Food Perception System: code:#FoodPerceptionSystem
//    Water Perception System: code:#WaterPerceptionSystem

/*
 * Evorobot* - robot-env.cpp
 * Copyright (C) 2009, Stefano Nolfi
 * LARAL, Institute of Cognitive Science and Technologies, CNR, Roma, Italy
 * http://laral.istc.cnr.it

 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/*
 * robot-env.cpp
 */


#include "public.h"
#include "robot-env.h"
#include "evolution.h"
#define MAXDELAY 100
#define MAXDELAYEDSENSORS 20
#include <math.h>


#ifdef EVOWINDOWS
#include "time.h"
clock_t sTime;
clock_t eTime;
#endif

// type of robot
float robotradius=101;	    //the radius of the robot's body (KHEPERA RADIUS: 27.5   EPUCK RADIUS: 37.5  LEGO: -wheels 57.5 - body 101)
float wheelr=57.5;			//distance from wheel to the center of the axes to be used in updating robot position

// Parametri di regolazione dei nuovi setup da corvids9 in poi
long int T1_max=2000;		// Tempo massimo azzerato il quale avviene il blocco della sbarra dell'individuo n.2
long int T2_max=2000;		// Tempo massimo azzerato il quale avviene il blocco della sbarra dell'individuo n.2
int		T_dec=5;			// Valori di decremento del tempo di blocco della sbarra
int     T_inc=1;			// Valori di incremento del tempo di vita del robot 
int		differtime=200;		// Tempo differenziale tra l'arrivo di un lato della sbarra e l'altra estremit�. 
							// Utilizzato nelle fitness corvids8, corvids12, ecc...	
double  Critical_dist=500;	// Distanza critica all'interno della quale muta le regole di
							// decremento dei tempi T1 e T2

// general 
int		subst_array[MAXSUBSTITUTIONS]={1,3,2,4,1,2,3,4,4,3,1,3,3,3,3,2,4,2,3,4,4,3,3,3,2,2,4,3,4,3};	// array of substitutions of individuals
int     real=0;             // whether we use real or simulated robots
char    neuronl[40][8];     // neurons labels
char    neuroncl[40][8];    // neurons labels
int     drawtrace=0;        // draw robots' trajectory trace for n. cycles 
int	outputFitnessAtEndOfTrial=0;
int		test_statistics=0;	// test statistics on trials 
int		test_output_type=0;	// statistic test output type : 0 - Excel, 1 - R Analysis
int		save_ind_fitness=0;	// save fitness of each individual
int		test_nrace=0;		// test of only this number of race
int		test_measure_leadership=0;						// measure of leadership in laboratory on/off, 1=everywhere, 2=only outside the foodzone
int		test_measure_leadership_eco=0;					// measure of leadership in ecology on/off, 1=everywhere, 2=only outside the foodzone
int		test_distance_average=0;						// test distance average
int		test_distance_average_per_cycle=0;				// test distance average per cycle
int		test_distance_average_per_cycle_all_gen=0;		// test distance average per cycle all gen
int	    test_all_ind_positions=0;						// test all individuals positions
int		test_distance_with_other_robots_average_per_cycle=0;	// test distance average with all other robots per cycle
int		test_distance_from_fz=0;						// Distance from food zones
int		test_nindintoneighborhood=0;	// test nindintoneighborhood
int		test_exploratory=0;				// test exploratory abilities
int		test_exploratory_eco=0;				// test exploratory abilities
int		test_exploratory_all_gen=0;		// test exploratory abilities in all gen
int		test_exploratory_all_gen_eco=0;	// test exploratory abilities in all gen
int		test_mobility_all_gen_eco=0;	// test mobility in all gen
int		test_barycentre=0;				// test barycentre in one gen
int		test_barycentre_distances_all_gen=0;	// test barycentre in all gen
int		test_barycentre_distances=0;	// test barycentre distances in one gen
int		test_hamming_distance=0;		// measure of hamming distance between genotypes on/off - 1=poulations, 2=bests comparisons
int		xcell=0;			// cell size x
int		ycell=0;			// cell size y
int     explorindnumber=0;	// exploration test on individual of number X
int     test_times=0;		// test times
int     test_times_all_gen=0;		// test times
int     test_times_all_gen_eco=0;	// test times into the ecology
int     test_fitness_lab=0;			// test fitness into laboratory 
int     test_fitness_lab_all_gen=0;	// test fitness into laboratory all gen
int     test_times_ntestpop=0;		// population percentade for testing
int		display_delay=0;	// display delay
int		display_under_evolution=0;				// display robots under evolution	
int		n_ind_to_display_under_evolution=-1;	// individual to display under avolution, -1 all
float   max_dist_attivazione = 0.0f;  //range di attivazione del sensore other
long int sweeps = 0;        // number of step for each trial  
long int nreproducingcycles = 0;	 // number of cycles after which individual reproduce by itself
long int ngraphicsrefreshcycles = 1; // number of cycles after which refresh graphics, default each cycle
long int run = 0;           // current cycle 
long int trace_run = 0;		// current cycle for tracing of robots
int     cepoch=0;           // current trial
int     cgen=0;			    //current generation
int     fitmode=0;          // 1=fitness/lifecycles 2=fitness/epochs
char    ffitness_descp[255];// description of the current fitness function
int     individual_crash;   // a crash occurred between two individuals
int     manual_start=0;     // whether robots' position has been manually set by the user
int     stop;               // stop the trial (when 1)
int		movedbar=0;			// switch to 1 when the bar is moved at the first time
int		sincrobots=0;		// syncronization flag : 1 if both the robot emit pushin units signal
int		noverticalbar=0;	// switch to 1 when the bar is moved from the vertical position
int		memlifecycle1=0;	// memory of the registered lifecycle of the first robot in food zone 1
int		memlifecycle2=0;	// memory of the registered lifecycle of the second robot in food zone 2
long int T1=T1_max;			// Tempo prima del blocco della sbarra dell'individuo n.1
long int T2=T2_max;			// Tempo prima del blocco della sbarra dell'individuo n.2
long int success_counter=0;	// Contatore dei successi nel conseguimento del compito
long int first_success_counter=0;	// Contatore dei successi nel conseguimento del compito
long int second_success_counter=0;	// Contatore dei successi nel conseguimento del compito
int    prefixpos=0;			// posizione di partenza prefissata random

int    ncrashedind=-3;					// numero dell'individuo con cui � avvenuto il crash
int	   RColorPreyDeadInd=255;			// componente rossa del colore dell'individuo preda morto
int	   GColorPreyDeadInd=25;			// componente verde del colore dell'individuo preda morto	
int	   BColorPreyDeadInd=25;			// componente blu del colore dell'individuo	preda morto
int	   RColorPredatorDeadInd=200;		// componente rossa del colore dell'individuo predatore morto
int	   GColorPredatorDeadInd=0;			// componente verde del colore dell'individuo predatore morto	
int	   BColorPredatorDeadInd=0;			// componente blu del colore dell'individuo	predatore morto

int	   RColor_fake=0;					// Red Color component of substituted individual
int	   GColor_fake=0;					// Green Color component of substituted individual
int	   BColor_fake=0;					// Blue Color component of substituted individual

int	   RColor_new=0;					// Red Color new component of substituted individual
int	   GColor_new=0;					// Green Color new component of substituted individual
int	   BColor_new=0;					// Blue Color new component of substituted individual

int	   movem=0;							// Selection of the movements of a mobile foodzone


// lifetime
int   amoebaepochs=0;       // number of epochs                        
int   timelaps=1;           // timelaps x 100 milliseconds             
int   timeadjust=0;         // used to slowdown the update on the robots or the rendering in simulation       
int   testsamep=0;          // test, start from the same position       
int   nobreak=0;            // no artificial break on epochs          
int   stop_when_crash=1;    // we stop lifetime if a crash occurred  
int   ind_cannot_crash=0;   // individuals cannot crash  
int   restart_from_n_pos_before=0;  // if 1 robot collides with wall and with each other in the same way of the bar
int   restart_around=0;		// if 1 robot restarts around after crash
int   bar_without_crash=0;	// if 1 reset robot position to the 4 step old position when crash with bar 
int   railsbar=0;			// if 1 bar must move on rails obly if both robots push it
int	  norotationalbar=0;	// if 1 bar cannot rotate but only translate
int   max_dist_rebound_after_crash=30;	// Max positions of rebound after crash
int   startfarfrom=0;       // the robot start far from target areas and round obstacles
int	  starting_distance_from_fz=0;	// starting area distance from other food zones
int	  start_in_a_little_area=0;	// robots all start in a little food zone far from others food zones
int	  starting_area_radius=0;	// starting area radius
int	  starting_area_distance_from_fz=0;	// starting area distance from other food zones
float starting_area_x=0.0;
float starting_area_y=0.0;
//sensors & actuators
struct iparameters *ipar=NULL; // individual parameters 
float  sensornoise=0.0;     // noise on sensors

//delay line data
double delayLine[MAXDELAY][MAXDELAYEDSENSORS];
int delayIndex=0;
double dswap[MAXDELAYEDSENSORS];

// the environments
int   nworlds=1;            // number of different environments     
int   random_groundareas=0; // randomize foodzone positions
int   mobile_tareas=0;		// mobile target areas
int	  tareas_speed=10;		// mobile target areas speed
int   random_sround=0;      // randomize sround positions
int   random_round=0;       // randomize round positions   
float **motor;              // motor sampling, 12 rows each with 12*2(ang and dist) columnsi
int   **wall;               // infrared wall sampling, (180 degrees and 38 distances)
int   **obst;               // infrared round obstacle sampling, (180 d. and 38 distances
int   **sobst;              // infrared small round obstacle sampling (180 d. and 20 distances
int   **light;              // light sampling (180 degrees and 27 distances)
int   *wallcf;              /* wall sampling configuration                          */
int   *obstcf;              /* obst sampling configuration                          */
int   *sobstcf;             /* sobst sampling configuration                          */
int   *lightcf;             /* light sampling configuration                          */
int   grid=0;               // the step of the grid for the render of the environment

struct envobject **envobjs;    // environment, pointer to object lists
struct envobject **membar;	   // memorizza la posizione originaria delle sbarre
int    envnobjs[NOBJS];           // current number of objects for each category
int    nobjects = 50;          // max number of objects for each categorgy
int    envdx=0;                // environment dx
int    envdy=0;                // environment dy

int    *startx;				   // Dichiara l'ascissa di start di tutti i robot
int    *starty;				   // Dichiara l'ordinata di start di tutti i robot

int    *perceptualinds;		   // Contiene gli indici degli individui che possono percepire la food zone

long int delay=0;			   // Inizializza la variabile di ritardo dei cicli

struct  individual *ind=NULL;  // individual phenotype                

float ffitness_obstacle_avoidance(struct individual *pind);

float ffitness_near_red(struct individual *pind);
float ffitness_near_red2(struct individual *pind);     //usa la distanza dal primo ostacolo rosso
float ffitness_exploration(struct individual *pind);   //esplorazione su una matrice di 100x100 celle
float ffitness_wallfollowing(struct individual *pind); //segue il muro

float ffitness_discrim(struct  individual  *pind);
float ffitness_discrim_race(struct  individual  *pind);
float ffitness_discrim_predator_eats_prey(struct  individual  *pind);
float ffitness_groupdiscrim(struct  individual  *pind);		   // only collective component	
float ffitness_groupdiscrim_1(struct  individual  *pind);	   // individual and collective component
float ffitness_groupdiscrim_2(struct  individual  *pind);	   // individual and collective component
float ffitness_predator_prey1(struct  individual  *pind);	   // fitness nello studio della predazione per la formazione di gruppi, le prede sono premiate se mangiano cibo
float ffitness_predator_prey2(struct  individual  *pind);	   // fitness nello studio della predazione per la formazione di gruppi, le prede sono premiate se vivono pi� a lungo
float ffitness_corvids14(struct  individual  *pind);		   // fitness per l'esperimento corvidi
float ffitness_corvids15(struct  individual  *pind);		   // fitness per l'esperimento corvidi
float ffitness_leadersfollowers(struct  individual  *pind);    // fitness per l'esperimento leaders / followers
float ffitness_leadersfollowers2(struct  individual  *pind);   // fitness per l'esperimento leaders / followers
void initialize_world_discrim();

//	New fitness
float ffitness_distributedLeadership(struct  individual  *pind);		   // only collective component	
float ffitness_distributedLeadership_Simplified(struct  individual  *pind);		   // only collective component	
float ffitness_distributedLeadershipUnnormalised(struct  individual  *pind);

float ffitness_stay2_on_areas(struct  individual  *pind);
void initialize_robot_position_stay2();
void initialize_one_robot_position_and_color(struct individual *pind, int team);

float ffitness_stay_on_different_areas(struct  individual  *pind);

float ffitness_tswitch(struct individual *pind);
void initialize_world_tswitch();

float ffitness_arbitrate(struct individual *pind);
void initialize_robot_position_arbitrate();
void initialize_world_arbitrate();
void assign_individual_color(struct individual *pind);

int check_perceptualinds(int team);

extern int dtime;              //time to accomplish sense-act cycle in real robot

float (*pffitness)(struct  individual  *pind) = NULL;  // pointer to fitness function()
void  (*pinit_robot_pos)() = NULL;                     // pointer to initialize_robot_position()
void  (*pinit_world)() = NULL;                         // pointer to initialize_world()

extern int	seed;									   // current random seed                              
extern int  test_seed;								   // test_seed
extern int  nraces;									   // number of races
extern int  n_variable_evolution_is_running;		   // n-variable evolutionary process is running	
extern int  n_fixed_evolution_social_is_running;	   // social n-fixed evolution is currently running
extern int  testpopulation_is_running;				   // test population
extern int  nindividuals;
extern struct ncontroller *ncontrollers;			   // pointer to neural controllers                
extern int	**genome;								   // genome of the population
extern int color_into_genotype;						   // save RGB colors component into genotype	
extern int nkids;						               // number of kids for father                
extern int bestfathers;					               // number of fathers that make kids         
extern float   mem_bar_orig[10][4];					   // memorizza la posizione originaria della sbarra
void update_bump_sensors(struct individual *pind, float xbump, float ybump);	   // aggiorna i bump sensors	
extern int nindinspecies0;		  		   			   // number of individuals in species 0
extern int nindinspecies1;							   // number of individuals in species 1
extern int **explorationarray;			   	  		   // exploration array
//extern int ***explorationarray_eco;		   	  		   // exploration array in ecology
extern void move_individual_color(struct individual *pind);
extern int  labtest_is_running;						   // laboratory testing is currently tested
extern int	evaluate_different_inds;				   // evaluate different individuals in a n-fixed evolution with more races. 1=different inds without repetition, 2=different inds with repetition  
extern int	**selected_ind_array;					   // array to track selected individuals in evaluate_different_inds mode
extern int  races_with_diffcolors;					   // different races with different colors
extern int  team_with_diffcolors;				  	   // different team members have different colors
extern int  test_barycentre;
extern int  test_barycentre_distances_all_gen;
extern int	**barycentrearray;						   // barycentre array
extern int	**barycentredistancesarray;				   // barycentre distances array
extern float randomize(float min, float max);
extern int	**distances_from_fz_array;				   // Average distances from food zones of individuals
extern int RColor_fake;
extern int GColor_fake;
extern int BColor_fake;
extern int RColor_new;
extern int GColor_new;
extern int BColor_new;

//	From simulator.cpp
extern float read_food_color(struct individual *pind);
extern float read_water_color(struct individual *pind);

// statistics
int	    *successruntime;			/* run time needful for successes of individuals	 */
int		*successfirstsideruntime;	/* run time needful for successes of first side bar  */
int		*successsecondsideruntime;	/* run time needful for successes of second side bar */
double	*distanceaverage;			/* media delle distanze individuo - individuo pi� vicino */
double	*distancestdev;				/* deviazione standard delle distanze individuo - individuo pi� vicino */
double  *firsthalflifedistave;		/* distanza media della prima parte della vita relativa all'epoca k */
double  *secondhalflifedistave;		/* distanza media della seconda parte della vita relativa all'epoca k */
double  *entirelifedistave;			/* distanza media dell'intera vita relativa all'epoca k */

double	*distanceaverage_ave_epoch;/* media delle distanze individuo - individuo pi� vicino per cyclo e per epoca*/

double  *nindintoneighborhood;						/* media degli individui vicini entro la distanza di visione */
double  *firsthalflifenindintoneighborhoodave;		/* media degli individui vicini nella prima parte della vita */
double  *secondhalflifenindintoneighborhoodave;		/* media degli individui vicini nella seconda parte della vita */
double  *entirelifenindintoneighborhoodave;			/* media degli individui vicini nell'intera vita */

//	group Accumulators used for 'distributed leadership' fitness function 
double foodAccumulator;
double waterAccumulator;

double hungerAccumulator;
double thirstAccumulator;

//	Flags to tell us how the thirst or hunger
//	accumulators should be updated this cycle
//	if true the accumulator will be increased
//	else it will decrease
bool incrementHunger;
bool incrementThirst;

bool isDaytime;	//	True if it is daytime in the environment false if else

void initHungerArray(struct  individual  *pind)
{
	//	Zero all hunger sensor inputs
	for (int i = 0; i < 10; i++)
	{
		pind->hunger[i] = 0.0f;
	}
}

void initThirstArray(struct  individual  *pind)
{
	//	Zero all thirst sensor inputs
	for (int i = 0; i < 10; i++)
	{
		pind->thirst[i] = 0.0f;
	}
}

/*
 * create robot and environmental parameters
 */
void create_robot_env_par()
{
	int    i,ii,iii;
	int    w;

	// allocate space for only one individual's parameter
	ipar = (struct iparameters *) malloc(1 * sizeof(struct iparameters));
	sprintf(ipar->ffitness,"NULL");
	ipar->wheelmotors     = 0;
	ipar->bumpsensors     = 0;
	ipar->bpsensors_startang_sect1	= 0;
	ipar->bpsensors_endang_sect1	= 0;
	ipar->bpsensors_startang_sect2	= 0;
	ipar->bpsensors_endang_sect2	= 0;
	ipar->bpsensors_startang_sect3	= 0;
	ipar->bpsensors_endang_sect3	= 0;
	ipar->bpsensors_startang_sect4	= 0;
	ipar->bpsensors_endang_sect4	= 0;
	ipar->bumpsensors_display	= 0;
	ipar->nifsensors      = 0;
	ipar->ifsensorstype   = 0;
	ipar->ifang           = -90;
	ipar->ifdang          = 36;
	ipar->ifrange         = 300;
	ipar->lightsensors    = 0;
	ipar->camerareceptors = 0;
	ipar->cameraviewangle = 18;
	ipar->cameradistance  = 6000;
	ipar->cameracolors    = 1;
	ipar->motmemory       = 0;
	ipar->oppsensor       = 0;

	//	Sensors activated when over
	//	the specific parts of the environment
	ipar->groundsensor    = 0;
	ipar->foodSensor      = 0;
	ipar->hungerSensor    = 0;
	
	ipar->hungerThreshold = 0;
	
	ipar->hungerIncreaseRate = 0.0f;
	ipar->hungerDecreaseRate = 0.0f;

	ipar->hungerIncreaseRate0 = 0.0f;
	ipar->hungerDecreaseRate0 = 0.0f;

	ipar->hungerIncreaseRate1 = 0.0f;
	ipar->hungerDecreaseRate1 = 0.0f;
	
	ipar->hungerIncreaseRate2 = 0.0f;
	ipar->hungerDecreaseRate2 = 0.0f;
	
	ipar->hungerIncreaseRate3 = 0.0f;
	ipar->hungerDecreaseRate3 = 0.0f;

	ipar->waterSensor     = 0;
	ipar->thirstSensor	  = 0;
	
	ipar->thirstThreshold = 0;

	ipar->thirstIncreaseRate = 0.0f;
	ipar->thirstDecreaseRate = 0.0f;

	ipar->a_groundsensor  = 0;
	ipar->a_groundsensor2 = 0;
	ipar->input_outputs   = 0;
	ipar->signal_sensors  = 0;
	ipar->signalss        = 0;
	ipar->signalso        = 0;
	ipar->corvidsstart    = 0;
	ipar->communications  = 0;
	ipar->dist_com		 = 0;
	ipar->lfpercepsystemdist = 0;
	ipar->onlyfewcanperceive = 0;
	ipar->nperceptualindividuals = 0;
	ipar->perceptualrace = 0;
	ipar->motionlessrace = 0;
	ipar->motionlessgroup_but_nind = 0;
	ipar->motionlessgroup_but_nind2 = 0;
	ipar->motionlessind = 0;
	ipar->Xmotionlessind = 0;
	ipar->Ymotionlessind = 0;
	ipar->FDmotionlessind = 0;
	ipar->lab_test_all_seed = 0;
	ipar->lab_test_startseed = 1;
	ipar->lab_test_number_seed = 0;
	ipar->lab_test_all_gen = 0;
	ipar->lab_test_gen_step = 0;
	ipar->lab_test_startgen = 0;
	ipar->lab_test_number_gen = 0;
	ipar->lab_test_team1 = 0;
	ipar->lab_test_team1_all_ind_race =0;	
	ipar->lab_test_team2 = 0;
	ipar->lab_test_team2_number_seed = 0;
	ipar->lab_test_team2_number_gen = 0;
	ipar->lab_test_subst_with_array=0;
	ipar->lab_test_subst_ind_orig=0;
	ipar->lab_test_subst_ind_dest=0;
	ipar->lab_test_subst_num=0;
	ipar->lab_test_subst_number_seed = 0;
	ipar->lab_test_subst_startseed = 0;  
	ipar->lab_test_subst_number_gen = 0;
	ipar->lab_test_subst_color = 0;
	ipar->lab_test_team2_all_ind_race = 0;	
	ipar->lab_test_all_pairs = 0;
	ipar->lab_test_team3 = 0;
	ipar->lab_test_alternate_team2=0;
	ipar->lab_ntestingteam = 0;
	ipar->lab_test_best_team1 = 0;
	ipar->lab_test_best_team2 = 0;
	ipar->lab_test_best_team3 = 0;
	ipar->lab_test_best_team4 = 0;
	ipar->lab_test_ghost_team1 = 0;
	ipar->lab_test_ghost_team2 = 0;
	ipar->lab_test_ghost_team3 = 0;
	ipar->lab_test_ghost_team4 = 0;
	ipar->lab_show_number = 0;
	ipar->color_race0 = 0;
	ipar->color_race1 = 0;
	ipar->color_race2 = 0;
	ipar->color_race3 = 0;
	ipar->test_ind1_with_diff_color=0;
	ipar->test_num_ind1=0;			
	ipar->test_color_ind1=0;		
	ipar->test_ind2_with_diff_color=0;		
	ipar->test_num_ind2=0;					
	ipar->test_color_ind2=0;					
	ipar->percepindwithdiffcolor = 0;
	ipar->percepindcannotseeother = 0;
	ipar->percepindcannothavefitness = 0;	
	ipar->probpercepindinheritfromfather = 0;
	ipar->save_leader_followers_percent = 0;
	ipar->leader_cannot_become_follower = 0;
	ipar->pushingunits    = 0;
	ipar->categorization  = 0;
	ipar->binarycom       = 0;
	ipar->binarycat       = 0;
	ipar->visible_individual = 0;
	ipar->visible_died_individual = 0;
	ipar->resurrect_after_die = 0;
	ipar->signalssf       = 0;
	ipar->signalssb       = 0;
	ipar->signaltype      = 0;
	ipar->nhiddens        = 0;
	ipar->dynrechid       = 0;
	ipar->dynrecout       = 0;
	ipar->dynioconn       = 0;
	ipar->dyndeltai       = 0;
	ipar->dyndeltah       = 0;
	ipar->dyndeltao       = 0;
	ipar->simplevision    = 0;
	ipar->nutrients       = 0;
	ipar->compass         = 0;
	ipar->energy          = 0;
	ipar->nxtcam			 = 0;
	ipar->rgbcolorretina	 = 0;
	ipar->rgbcolorretinaviewangle	 = 0;
	ipar->rgbcolorretinagrayscale	 = 0;
	ipar->rgbcolorretinamanualgrayscale	 = 0;
	ipar->rgbcolorretinamaxdistance	 = 8000;
	ipar->rgbcolorretinabluedisabledrace = 0;
	ipar->orientationcam	 = 0;
	ipar->orientationcammaxdist = 0;
	ipar->eyes			 = 0;
	ipar->leadersfollowerspercepsystem = 0;
	
	//	Food
	ipar->foodperceptionsystem = 0;
	ipar->foodpercepdist = 0;
	ipar->foodpercepdist0 = 0;
	ipar->foodpercepdist1 = 0;
	ipar->foodpercepdist2 = 0;
	ipar->foodpercepdist3 = 0;
	
	//	Water
	ipar->waterperceptionsystem = 0;
	ipar->waterpercepdist = 0;
	ipar->waterpercepdist0 = 0;
	ipar->waterpercepdist1 = 0;
	ipar->waterpercepdist2 = 0;
	ipar->waterpercepdist3 = 0;
	
	//	Night / day cycles
	ipar->switchDayNightEvery = 0;
	ipar->dayNightSensor = 0;

	//	Age Sensor relative to lifetime
	ipar->ageSensor = 0;

	ipar->drawifsensorray = 0;
	ipar->nxtcolors		 = 0;
	ipar->clock1			 = 0;
	ipar->clock2			 = 0;
	ipar->clock3			 = 0;
	ipar->brownoise		 = 0;
	ipar->whitenoise		 = 0;
	ipar->motorload		 = 0;
	ipar->gatenoise		 = 1;
	ipar->sensorsDelay    = 0;
	//  da impostare la lista dei colori

	create_cparameter("ffitness","individual",&ipar->ffitness, 0, 0, "fitness function");
	create_iparameter("nhiddens","individual",&ipar->nhiddens, 0, 40, "number of internal neurons");
	create_iparameter("motors","individual",&ipar->wheelmotors, 0, 3, "motor neurons controlling the two wheels");
	create_iparameter("motmemory","individual",&ipar->motmemory, 0, 1, "efferent copy of motor neurons");
	create_iparameter("bumpsensors","individual",&ipar->bumpsensors, 0, 1, "bump sensors");
	create_iparameter("bpsensors_startang_sect1","individual",&ipar->bpsensors_startang_sect1, 0, 360, "bpsensors_startang_sect1");
	create_iparameter("bpsensors_endang_sect1","individual",&ipar->bpsensors_endang_sect1, 0, 360, "bpsensors_endang_sect1");
	create_iparameter("bpsensors_startang_sect2","individual",&ipar->bpsensors_startang_sect2, 0, 360, "bpsensors_startang_sect2");
	create_iparameter("bpsensors_endang_sect2","individual",&ipar->bpsensors_endang_sect2, 0, 360, "bpsensors_endang_sect2");
	create_iparameter("bpsensors_startang_sect3","individual",&ipar->bpsensors_startang_sect3, 0, 360, "bpsensors_startang_sect3");
	create_iparameter("bpsensors_endang_sect3","individual",&ipar->bpsensors_endang_sect3, 0, 360, "bpsensors_endang_sect3");
	create_iparameter("bpsensors_startang_sect4","individual",&ipar->bpsensors_startang_sect4, 0, 360, "bpsensors_startang_sect4");
	create_iparameter("bpsensors_endang_sect4","individual",&ipar->bpsensors_endang_sect4, 0, 360, "bpsensors_endang_sect4");
	create_iparameter("bumpsensors_display","individual",&ipar->bumpsensors_display, 0, 5, "bumpsensors_display");
	create_iparameter("nifsensors","individual",&ipar->nifsensors, 0, 8, "n. of infrared sensors");
	create_iparameter("ifang","individual",&ipar->ifang, 0, 0, "relative angle of the first infrared sensor");
	create_iparameter("ifdang","individual",&ipar->ifdang, 0, 0, "delta angle between sensors");
	create_iparameter("ifrange","individual",&ipar->ifrange, 0, 0, "infrared sensors (geometrical) range (mm)");
	create_iparameter("ifsensorstype","individual",&ipar->ifsensorstype, 0, 1, "0=sampled 1=geometrical infrared sensors");
	create_iparameter("nlightsensors","individual",&ipar->lightsensors, 0, 8, "n. of light sensors");
	create_iparameter("camerareceptors","individual",&ipar->camerareceptors, 0, 360, "camera: n. of photoreceptors");
	create_iparameter("cameraviewangle","individual",&ipar->cameraviewangle, 0, 180, "camera: view angle (+/- n. degrees)");
	create_iparameter("cameradistance","individual",&ipar->cameradistance, 0, 12000, "cameradistance: +/- 12000");
	create_iparameter("cameracolors","individual",&ipar->cameracolors, 1, 3, "camera: n. colors discriminated by the camera");
	create_iparameter("simplevision","individual",&ipar->simplevision, 0, 2, "angle of visually detected objects");
	create_iparameter("nxtcam","individual",&ipar->nxtcam, 0, 2, "enable nxt camera"); 
	create_iparameter("RGBColorRetina","individual",&ipar->rgbcolorretina, 0, 100, "enable RGB Color Retina"); 
	create_iparameter("RGBColorRetinaViewAngle","individual",&ipar->rgbcolorretinaviewangle, 0, 180, "RGB Color Retina View Angle"); 
	create_iparameter("RGBColorRetinaGrayScale","individual",&ipar->rgbcolorretinagrayscale, 0, 1, "RGB Color Retina Gray Scale"); 
	create_iparameter("RGBColorRetinaManualGrayScale","individual",&ipar->rgbcolorretinamanualgrayscale, 0, 1, "RGB Color Retina Manual Gray Scale"); 
	create_iparameter("RGBColorRetinaMaxDistance","individual",&ipar->rgbcolorretinamaxdistance, 0, 15000, "RGB Color Retina Max Distance"); 
	create_iparameter("RGBColorRetinaBlueDisabledRace","individual",&ipar->rgbcolorretinabluedisabledrace, 0, MAXRACES, "Number of species which has RGB Color Retina disabled"); 
	create_iparameter("OrientationCam","individual",&ipar->orientationcam, 0, 1, "enable Orientation Cam"); 
	create_iparameter("OrientationCamMaxDist","individual",&ipar->orientationcammaxdist, 0, 50000, "Orientation Cam Max Distance");
	create_iparameter("eyes","individual",&ipar->eyes, 0, 2, "enable eyes"); 
	create_iparameter("leadersfollowerspercepsystem","individual",&ipar->leadersfollowerspercepsystem, 0, 4, "enable perceptual system of leaders and followers"); 

	//	Create parameters for the perception of food in the environment
	create_iparameter("foodperceptionsystem","individual",&ipar->foodperceptionsystem, 0, 1, "food perception system"); 
	create_iparameter("foodpercepdist","individual",&ipar->foodpercepdist, 0, 0, "food perception system distance for all agents"); 
	create_iparameter("foodpercepdist0","individual",&ipar->foodpercepdist0, 0, 0, "food perception system distance for race 0 (green)"); 
	create_iparameter("foodpercepdist1","individual",&ipar->foodpercepdist1, 0, 0, "food perception system distance for race 1 (dark blue)"); 
	create_iparameter("foodpercepdist2","individual",&ipar->foodpercepdist2, 0, 0, "food perception system distance for race 2 (cyan)"); 
	create_iparameter("foodpercepdist3","individual",&ipar->foodpercepdist3, 0, 0, "food perception system distance for race 3 (yellow)"); 

	//	Create parameters for the perception of water in the environment
	create_iparameter("waterperceptionsystem","individual",&ipar->waterperceptionsystem, 0, 1, "water perception system"); 
	create_iparameter("waterpercepdist","individual",&ipar->waterpercepdist, 0, 0, "water perception system distance"); 
	create_iparameter("waterpercepdist0","individual",&ipar->waterpercepdist0, 0, 0, "water perception system distance for race 0 (green)"); 
	create_iparameter("waterpercepdist1","individual",&ipar->waterpercepdist1, 0, 0, "water perception system distance for race 0 (dark blue)"); 
	create_iparameter("waterpercepdist2","individual",&ipar->waterpercepdist2, 0, 0, "water perception system distance for race 0 (cyan)"); 
	create_iparameter("waterpercepdist3","individual",&ipar->waterpercepdist3, 0, 0, "water perception system distance for race 0 (yellow)"); 

	//	Create parameters for the perception of night and day cycles
	create_iparameter("switchDayNightEvery","individual",&ipar->switchDayNightEvery, 0, 0, "day / night will switch every n cycles"); 
	create_iparameter("dayNightSensor","individual",&ipar->dayNightSensor, 0, 0, "n. of day / night sensors");
	
	//	Create parameters for an tracking of age relative to life time
	create_iparameter("ageSensor","individual",&ipar->ageSensor, 0, 0, "robots keep track of time");
	
	create_iparameter("drawifsensorray","individual",&ipar->drawifsensorray, 0, 1, "draw infrared sensors ray"); 
	create_fparameter("clock1","individual",&ipar->clock1,0,1,"Clock 1 dt");
	create_fparameter("clock2","individual",&ipar->clock2,0,1,"Clock 2 dt");
	create_fparameter("clock3","individual",&ipar->clock3,0,1,"Clock 3 dt");
	create_iparameter("brownoise","individual",&ipar->brownoise,0,5,"Brownian noise units");
	create_iparameter("whitenoise","individual",&ipar->whitenoise,0,5,"White noise units");
	create_iparameter("motorload","individual",&ipar->motorload,0,1,"Motorload units");
	create_fparameter("gatenoise","individual",&ipar->gatenoise,0,1,"Gate noise switch off sensories with prob. p");
	create_iparameter("sensorsDelay","individual",&ipar->sensorsDelay,0,50,"Inputs delay");

	create_iparameter("motsensors","individual",&ipar->motmemory, 0, 1, "efferent copies of motor states");
	create_iparameter("input_outputs","individual",&ipar->input_outputs, 0, 0, "additional outputs with efferent copies");
	
	//	Sensors active when over a specific environment feature
	create_iparameter("groundsensor","individual",&ipar->groundsensor, 0, 0, "n. of ground sensors");
	create_iparameter("waterSensor","individual",&ipar->waterSensor, 0, 0, "n. of water sensors");
	create_iparameter("thirstSensor","individual",&ipar->thirstSensor, 0, 0, "type of thirst sensors");
	create_iparameter("foodSensor","individual",&ipar->foodSensor, 0, 0, "n. of food sensors");
	create_iparameter("hungerSensor","individual",&ipar->hungerSensor, 0, 0, "type of hunger sensors");
	
	//	Threshold values for hunger and thirst for all robots
	create_iparameter("hungerThreshold","individual",&ipar->hungerThreshold, 0, 0, "Threshold for when robot is no longer considered hungry");
	create_iparameter("thirstThreshold","individual",&ipar->thirstThreshold, 0, 0, "Threshold for when robot is no longer considered thirsty");

	//	Values which control the incrementing and decrementing of the hunger accumulator
	create_fparameter("hungerIncreaseRate","individual",&ipar->hungerIncreaseRate, 0, 0, "Rate at which the hunger accumulator increases for all robots");
	create_fparameter("hungerDecreaseRate","individual",&ipar->hungerDecreaseRate, 0, 0, "Rate at which the hunger accumulator decreases for all robots");
	
	//	Values which control the incrementing and decrementing of the hunger accumulator for race 0
	create_fparameter("hungerIncreaseRate0","individual",&ipar->hungerIncreaseRate0, 0, 0, "Rate at which the hunger accumulator increases for race 0");
	create_fparameter("hungerDecreaseRate0","individual",&ipar->hungerDecreaseRate0, 0, 0, "Rate at which the hunger accumulator decreases for race 0");

	//	Values which control the incrementing and decrementing of the hunger accumulator for race 1
	create_fparameter("hungerIncreaseRate1","individual",&ipar->hungerIncreaseRate1, 0, 0, "Rate at which the hunger accumulator increases for race 1");
	create_fparameter("hungerDecreaseRate1","individual",&ipar->hungerDecreaseRate1, 0, 0, "Rate at which the hunger accumulator decreases for race 1");

	//	Values which control the incrementing and decrementing of the hunger accumulator for race 2
	create_fparameter("hungerIncreaseRate2","individual",&ipar->hungerIncreaseRate2, 0, 0, "Rate at which the hunger accumulator increases for race 2");
	create_fparameter("hungerDecreaseRate2","individual",&ipar->hungerDecreaseRate2, 0, 0, "Rate at which the hunger accumulator decreases for race 2");

	//	Values which control the incrementing and decrementing of the hunger accumulator for race 3
	create_fparameter("hungerIncreaseRate3","individual",&ipar->hungerIncreaseRate3, 0, 0, "Rate at which the hunger accumulator increases for race 3");
	create_fparameter("hungerDecreaseRate3","individual",&ipar->hungerDecreaseRate3, 0, 0, "Rate at which the hunger accumulator decreases for race 3");

	//	Values which control the incrementing and decrementing of the thirst accumulator
	create_fparameter("thirstIncreaseRate","individual",&ipar->thirstIncreaseRate, 0, 0, "Rate at which the thirst accumulator increases for all robots");
	create_fparameter("thirstDecreaseRate","individual",&ipar->thirstDecreaseRate, 0, 0, "Rate at which the thirst accumulator decreases for all robots");

	//	Values which control the incrementing and decrementing of the thirst accumulator for race 0
	create_fparameter("thirstIncreaseRate0","individual",&ipar->thirstIncreaseRate0, 0, 0, "Rate at which the thirst accumulator increases for race 0");
	create_fparameter("thirstDecreaseRate0","individual",&ipar->thirstDecreaseRate0, 0, 0, "Rate at which the thirst accumulator decreases for race 0");

	//	Values which control the incrementing and decrementing of the thirst accumulator for race 1
	create_fparameter("thirstIncreaseRate1","individual",&ipar->thirstIncreaseRate1, 0, 0, "Rate at which the thirst accumulator increases for race 1");
	create_fparameter("thirstDecreaseRate1","individual",&ipar->thirstDecreaseRate1, 0, 0, "Rate at which the thirst accumulator decreases for race 1");

	//	Values which control the incrementing and decrementing of the thirst accumulator for race 2
	create_fparameter("thirstIncreaseRate2","individual",&ipar->thirstIncreaseRate2, 0, 0, "Rate at which the thirst accumulator increases for race 2");
	create_fparameter("thirstDecreaseRate2","individual",&ipar->thirstDecreaseRate2, 0, 0, "Rate at which the thirst accumulator decreases for race 2");

	//	Values which control the incrementing and decrementing of the thirst accumulator for race 3
	create_fparameter("thirstIncreaseRate3","individual",&ipar->thirstIncreaseRate3, 0, 0, "Rate at which the thirst accumulator increases for race 3");
	create_fparameter("thirstDecreaseRate3","individual",&ipar->thirstDecreaseRate3, 0, 0, "Rate at which the thirst accumulator decreases for race 3");


	create_iparameter("a_groundsensor","individual",&ipar->a_groundsensor, 0, 0, "additional ground sensors which encode the previous state of ground sensors");
	create_iparameter("a_groundsensor2","individual",&ipar->a_groundsensor2, 0, 0, "additional ground sensors which encode the previous state of ground sensors");
	create_iparameter("signalss","individual",&ipar->signalss, 0, 4, "n. of sensors detecting signals");
	create_iparameter("signalso","individual",&ipar->signalso, 0, 4, "n. of outputs producing signals");
	create_iparameter("signalssb","individual",&ipar->signalssb, 0, 1, "binary signals");
	create_iparameter("signaltype","individual",&ipar->signaltype, 0, 1, "type of signal 0=sound 1=light");
	create_iparameter("compass","individual",&ipar->compass, 0, 1, "compass sensor");
	create_iparameter("energysensor","individual",&ipar->energy, 0, 1, "energy sensor");
	create_iparameter("corvidsstart","individual",&ipar->corvidsstart, 0, 3, "if >0, corvids start from prearranged positions");
	create_iparameter("communication","individual",&ipar->communications, 0, 4, "n. of communication outputs");
	create_iparameter("dist_com","individual",&ipar->dist_com, 0, 12000, "max communication distance");
	create_iparameter("lfpercepsystemdist","individual",&ipar->lfpercepsystemdist, 0, 50000, "max leaders' and followers' perception system distance");
	create_iparameter("onlyfewcanperceive","individual",&ipar->onlyfewcanperceive, 0, 1, "Only few individuals can perceive something binded to this option");
	create_iparameter("nperceptualindividuals","individual",&ipar->nperceptualindividuals, 0, 0, "Number of individuals which can perceive the food zone");
	create_iparameter("perceptualrace","individual",&ipar->perceptualrace, 0, MAXRACES, "Number of perceptual race, 0=all races");
	create_iparameter("motionlessrace","individual",&ipar->motionlessrace, 0, MAXRACES, "Number race motion less, 0=no races");
	create_iparameter("motionlessgroup_but_nind","individual",&ipar->motionlessgroup_but_nind, 0, MAXINDIVIDUALS, "Number of mobile individual, motion less group, 0=no individuals");
	create_iparameter("motionlessgroup_but_nind2","individual",&ipar->motionlessgroup_but_nind2, 0, MAXINDIVIDUALS, "Number of second mobile individual, motion less group, 0=no individuals");
	create_iparameter("motionlessind","individual",&ipar->motionlessind, 0, MAXINDIVIDUALS, "Number individual motion less, 0=no individuals");
	create_iparameter("Xmotionlessind","individual",&ipar->Xmotionlessind, 0, envdx, "X of motion less ind");
	create_iparameter("Ymotionlessind","individual",&ipar->Ymotionlessind, 0, envdy, "Y of motion less ind");
	create_iparameter("FDmotionlessind","individual",&ipar->FDmotionlessind, 0, 360, "Face direction of motion less ind");
	create_iparameter("lab_test_all_seed","individual",&ipar->lab_test_all_seed, 0, 1, "Laboratory Testing of individuals in all seed");
	create_iparameter("lab_test_startseed","individual",&ipar->lab_test_startseed, 0, 0, "Laboratory Testing of individuals in all seed");
	create_iparameter("lab_test_startgen","individual",&ipar->lab_test_startgen, 0, 0, "Laboratory Testing of individuals in all generations");
	create_iparameter("lab_test_number_seed","individual",&ipar->lab_test_number_seed, 0, MAXGENERATIONS, "Laboratory Testing of individual of a seed X");
	create_iparameter("lab_test_all_gen","individual",&ipar->lab_test_all_gen, 0, 1, "Laboratory Testing of individuals in all generations");
	create_iparameter("lab_test_gen_step","individual",&ipar->lab_test_gen_step, 0, MAXGENERATIONS, "Laboratory Testing step of individuals in all generations");
	create_iparameter("lab_test_number_gen","individual",&ipar->lab_test_number_gen, 0, 0, "Laboratory Testing of individual of a generation X");
	create_iparameter("lab_test_all_ind","individual",&ipar->lab_test_all_ind, 0, 1, "Laboratory Testing of all individuals in all generations");
	create_iparameter("lab_test_team1","individual",&ipar->lab_test_team1, 0, MAXINDIVIDUALS, "Laboratory Testing on individual number 1");
	create_iparameter("lab_test_team1_all_ind_race","individual",&ipar->lab_test_team1_all_ind_race, 0, MAXRACES, "Laboratory Testing Indivuals of Race X into team 1");
	create_iparameter("lab_test_team2","individual",&ipar->lab_test_team2, 0, MAXINDIVIDUALS, "Laboratory Testing on individual number 2");
	create_iparameter("lab_test_team2_number_seed","individual",&ipar->lab_test_team2_number_seed, 0, 0, "Laboratory Testing on individual number 2, number of seed");
	create_iparameter("lab_test_team2_number_gen","individual",&ipar->lab_test_team2_number_gen, 0, 0, "Laboratory Testing on individual number 2, number of gen");
	create_iparameter("lab_test_subst_with_array","individual",&ipar->lab_test_subst_with_array, 0, 1, "Search in the proper array the number of individual in the team to be substituted");
	create_iparameter("lab_test_subst_ind_orig","individual",&ipar->lab_test_subst_ind_orig, 0, 0, "Number of source individual in the team to be substituted");
	create_iparameter("lab_test_subst_ind_dest","individual",&ipar->lab_test_subst_ind_dest, 0, 0, "Number of destination individual in the team to be substituted");
	create_iparameter("lab_test_subst_num","individual",&ipar->lab_test_subst_num, 0, 0, "Number of individual in the population to insert in the team");
	create_iparameter("lab_test_subst_number_seed","individual",&ipar->lab_test_subst_number_seed, 0, 0, "Number of seed of the individual to insert in the team");
	create_iparameter("lab_test_subst_startseed","individual",&ipar->lab_test_subst_startseed, 0, 0, "Number of generation from which to start");
	create_iparameter("lab_test_subst_number_gen","individual",&ipar->lab_test_subst_number_gen, 0, 0, "Number of generation of the individual to insert in the team");
	create_iparameter("lab_test_subst_color","individual",&ipar->lab_test_subst_color, 0, 1, "Substitution of color with a fake color to be perceived by the substituted individual");
	create_iparameter("lab_test_team1_flag","individual",&ipar->lab_test_team1_flag, 0, 0, "Laboratory Testing on individual number 1, new flag");
	create_iparameter("lab_test_team2_flag","individual",&ipar->lab_test_team2_flag, 0, 0, "Laboratory Testing on individual number 2, new flag");
	create_iparameter("lab_test_team3_flag","individual",&ipar->lab_test_team3_flag, 0, 0, "Laboratory Testing on individual number 3, new flag");
	create_iparameter("lab_test_team4_flag","individual",&ipar->lab_test_team4_flag, 0, 0, "Laboratory Testing on individual number 4, new flag");
	create_iparameter("lab_test_alternate_team2","individual",&ipar->lab_test_alternate_team2, 0, 0, "Alternate team2 visualization");
	create_iparameter("lab_test_team2_all_ind_race","individual",&ipar->lab_test_team2_all_ind_race, 0, MAXRACES, "Laboratory Testing Indivuals of Race X into team 2");
	create_iparameter("lab_test_all_pairs","individual",&ipar->lab_test_all_pairs, 0, 1, "Laboratory Testing All Individual Pairs");
	create_iparameter("lab_test_team3","individual",&ipar->lab_test_team3, 0, MAXINDIVIDUALS, "Laboratory Testing on individual number 3");
	create_iparameter("lab_test_team4","individual",&ipar->lab_test_team4, 0, MAXINDIVIDUALS, "Laboratory Testing on individual number 4");
	create_iparameter("lab_ntestingteam","individual",&ipar->lab_ntestingteam, 0, 4, "Number of testing individuals in Laboratory");
	create_iparameter("lab_test_best_team1","individual",&ipar->lab_test_best_team1, 0, 1, "Laboratory Testing of Best Individuals on Team 1");
	create_iparameter("lab_test_best_team2","individual",&ipar->lab_test_best_team2, 0, 1, "Laboratory Testing of Best Individuals on Team 2");
	create_iparameter("lab_test_best_team3","individual",&ipar->lab_test_best_team3, 0, 1, "Laboratory Testing of Best Individuals on Team 3");
	create_iparameter("lab_test_best_team4","individual",&ipar->lab_test_best_team4, 0, 1, "Laboratory Testing of Best Individuals on Team 4");
	create_iparameter("lab_test_ghost_team1","individual",&ipar->lab_test_ghost_team1, 0, 1, "Lab test Ind 1 ghost");
	create_iparameter("lab_test_ghost_team2","individual",&ipar->lab_test_ghost_team2, 0, 1, "Lab test Ind 2 ghost");
	create_iparameter("lab_test_ghost_team3","individual",&ipar->lab_test_ghost_team3, 0, 1, "Lab test Ind 3 ghost");
	create_iparameter("lab_test_ghost_team4","individual",&ipar->lab_test_ghost_team4, 0, 1, "Lab test Ind 4 ghost");
	create_iparameter("lab_show_number","individual",&ipar->lab_show_number, 0, 1, "Show individual number into Laboratory");
	create_iparameter("color_race0","individual",&ipar->color_race0, 0, 15, "Color of Species 0");
	create_iparameter("color_race1","individual",&ipar->color_race1, 0, 15, "Color of Species 1");
	create_iparameter("color_race2","individual",&ipar->color_race2, 0, 15, "Color of Species 2");
	create_iparameter("color_race3","individual",&ipar->color_race3, 0, 15, "Color of Species 4");
	create_iparameter("test_ind1_with_diff_color","individual",&ipar->test_ind1_with_diff_color, 0, MAXINDIVIDUALS, "test individual 1 displaying it with a different color");
	create_iparameter("test_num_ind1","individual",&ipar->test_num_ind1, 0, MAXINDIVIDUALS, "test from individual 1 to num individuals displaying them with a different color");
	create_iparameter("test_color_ind1","individual",&ipar->test_color_ind1, 0, 15, "color with which to display from individual 1");
	create_iparameter("test_ind2_with_diff_color","individual",&ipar->test_ind2_with_diff_color, 0, MAXINDIVIDUALS, "test individual 2 displaying it with a different color");
	create_iparameter("test_num_ind2","individual",&ipar->test_num_ind2, 0, MAXINDIVIDUALS, "test from individual 2 to num individuals displaying them with a different color");
	create_iparameter("test_color_ind2","individual",&ipar->test_color_ind2, 0, 15, "color with which to display from individual 2");
	create_iparameter("percepindwithdiffcolor","individual",&ipar->percepindwithdiffcolor, 0, 1, "Perceptual individuals with different colors");
	create_iparameter("percepindcannotseeother","individual",&ipar->percepindcannotseeother, 0, 1, "Perceptual individuals cannot see others");
	create_iparameter("percepindcannothavefitness","individual",&ipar->percepindcannothavefitness, 0, 1, "Perceptual individuals cannot obtain fitness"); 
	create_iparameter("probpercepindinheritfromfather","individual",&ipar->probpercepindinheritfromfather, 0, 100, "Perceptual individuals cannot obtain fitness"); 
	create_iparameter("save_leader_followers_percent","individual",&ipar->save_leader_followers_percent, 0, 1, "save leaders followers percentades");
	create_iparameter("leader_cannot_become_follower","individual",&ipar->leader_cannot_become_follower, 0, 1, "leader cannot become follower everywhere");
	create_iparameter("pushingunits","individual",&ipar->pushingunits, 0, 4, "n. of pushing units");
	create_iparameter("categorization","individual",&ipar->categorization, 0, 4, "n. of categorization outputs");
	create_iparameter("binarycom","individual",&ipar->binarycom, 0, 1, "binary comunication and pushing unit outputs");
	create_iparameter("binarycat","individual",&ipar->binarycat, 0, 1, "binary categorization outputs");
	create_iparameter("visible_individual","individual",&ipar->visible_individual, 0, 1, "1 if each individual is visible by other individuals");
	create_iparameter("visible_died_individual","individual",&ipar->visible_died_individual, 0, 1, "1 if each dead individual is visible by researcher");
	create_iparameter("resurrect_after_die","individual",&ipar->resurrect_after_die, 0, 1, "1 if each dead individual resurrect after die");

	create_iparameter("nworlds","lifetime",&nworlds, 1, 5, "number of environments");
	create_iparameter("ntrials","lifetime",&amoebaepochs, 0, 0, "number of trials");
	create_iparameter("ncycles","lifetime",&sweeps, 0, 0, "number of step for each trial");
	create_iparameter("nreproducingcycles","lifetime",&nreproducingcycles, 0, 0, "number of cycles after which individual reproduce by itself");
	create_iparameter("ngraphicsrefreshcycles","lifetime",&ngraphicsrefreshcycles, 0, 0, "number of cycles after which refresh graphics");
	create_iparameter("timelaps","lifetime",&timelaps, 0, 0, "duration of a cycle in hundreds of milliseconds");
	create_iparameter("timeincrease","lifetime",&T_inc,0,10, "Time increase for fitnesses which require it");
	create_iparameter("differtime","lifetime",&differtime,0,1000, "Differential time between first bar side and second bar side arrivals");
	create_iparameter("pause","lifetime",&timeadjust, 0, 0, "length of the pause in each step (in hardware)");
	create_fparameter("sensornoise","lifetime",&sensornoise, 0, 0, "% of noise on infrared and light sensors");
	create_fparameter("signal_mdist","lifetime",&max_dist_attivazione, 0, 0, "range of communication sensors");
	create_iparameter("stop_on_crash","lifetime",&stop_when_crash, 0, 1, "trials end when one robot collide");
	create_iparameter("ind_cannot_crash","lifetime",&ind_cannot_crash, 0, 1, "individuals cannot crash");
	create_iparameter("restart_from_n_pos_before","lifetime",&restart_from_n_pos_before, 0, 2, "if stop_when_crash 0 robot collide like bar");
	create_iparameter("restart_around","lifetime",&restart_around, 0, 1, "robot restart around after colliding");
	create_iparameter("bar_without_crash","lifetime",&bar_without_crash, 0, 1, "reset robot position when robot collide with bar");
	create_iparameter("railsbar","lifetime",&railsbar, 0, 1, "if 1 bar moves when touched by both robots");
	create_iparameter("norotationalbar","lifetime",&norotationalbar, 0, 1, "if 1 bar cannot rotate when touched, but only translate");
	create_iparameter("max_dist_rebound_after_crash","lifetime",&max_dist_rebound_after_crash, 1, 100, "max distance of rebound after crash");
	create_iparameter("random_tareas","lifetime",&random_groundareas, 0, 1, "randomize foodzone positions");
	create_iparameter("mobile_tareas","lifetime",&mobile_tareas, 0, 1, "Mobile food zones");
	create_iparameter("tareas_speed","lifetime",&tareas_speed, 0, 100, "Mobile food zones speed");
	create_iparameter("random_round","lifetime",&random_round, 0, 1, "randomize small round");
	create_iparameter("startfarfrom","lifetime",&startfarfrom, 0, 1, "start far from targets and obstacles");
	create_iparameter("starting_distance_from_fz","lifetime",&starting_distance_from_fz, 0, envdx, "distance between each robot and other food zones");
	create_iparameter("start_in_a_little_area","lifetime",&start_in_a_little_area, 0, 1, "robots all start in a little food zone far from others food zones");
	create_iparameter("starting_area_radius","lifetime",&starting_area_radius, 0, 0, "robots all start in a little food zone far from others food zones");
	create_iparameter("starting_area_distance_from_fz","lifetime",&starting_area_distance_from_fz, 0, envdx, "distance between staring area and other food zones");
	create_iparameter("drawtrace","lifetime",&drawtrace, 0, 0, "draw robot trace for n. cycles");
	create_iparameter("output_fitness_at_end_of_trial","lifetime",&outputFitnessAtEndOfTrial, 0, 0, "can output the fitness of a population at the end of each trial");
	create_iparameter("test_statistics","lifetime",&test_statistics, 0, 1, "save the number of success trials and time");
	create_iparameter("test_output_type","lifetime",&test_output_type, 0, 1, "Statistic test output type");
	create_iparameter("save_ind_fitness","lifetime",&save_ind_fitness, 0, 1, "save fitness of each individual under evolution");
	create_iparameter("test_measure_leadership","lifetime",&test_measure_leadership , 0, 2, "Measure of leadership on/off, 1=everywhere, 2=only outside the foodzone");
	create_iparameter("test_measure_leadership_eco","lifetime",&test_measure_leadership_eco , 0, 2, "Measure of leadership in ecology on/off, 1=everywhere, 2=only outside the foodzone");
	create_iparameter("test_distance_average","lifetime",&test_distance_average , 0, 1, "Measure of distance average on/off");
	create_iparameter("test_distance_average_per_cycle","lifetime",&test_distance_average_per_cycle , 0, 1, "Measure of distance average per cycle on/off");
	create_iparameter("test_distance_average_per_cycle_all_gen","lifetime",&test_distance_average_per_cycle_all_gen , 0, 1, "Measure of distance average per cycle all gen on/off");
	create_iparameter("test_all_ind_positions","lifetime",&test_all_ind_positions , 0, 1, "Measure of All Individuals Positions on/off");
	create_iparameter("test_distance_with_other_robots_average_per_cycle","lifetime",&test_distance_with_other_robots_average_per_cycle , 0, 1, "Test distance average with all other robots per cycle on/off");
	create_iparameter("test_distance_from_fz","lifetime",&test_distance_from_fz , 0, 1, "Measure of distance of each individual from each food zone on/off");
	create_iparameter("test_nindintoneighborhood","lifetime",&test_nindintoneighborhood , 0, 1, "Measure of nindintoneighborhood on/off");
	create_iparameter("test_nrace","lifetime",&test_nrace, 0, MAXRACES, "test of only this number of race");
	create_iparameter("test_exploratory","lifetime",&test_exploratory , 0, 1, "Measure of exploratory ability on/off");
	create_iparameter("test_exploratory_eco","lifetime",&test_exploratory_eco , 0, 1, "Measure of exploratory ability in ecology on/off");
	create_iparameter("test_exploratory_all_gen","lifetime",&test_exploratory_all_gen , 0, 1, "Measure of exploratory ability in all gen on/off");
	create_iparameter("test_exploratory_all_gen_eco","lifetime",&test_exploratory_all_gen_eco , 0, 2, "Measure of exploratory ability in all gen into ecology on/off");
	create_iparameter("test_mobility_all_gen_eco","lifetime",&test_mobility_all_gen_eco , 0, 2, "Measure of mobility in all gen into ecology on/off");
	create_iparameter("test_barycentre","lifetime",&test_barycentre , 0, 1, "Measure of group barycentre into laboratory in one gen on/off");
	create_iparameter("test_barycentre_distances_all_gen","lifetime",&test_barycentre_distances_all_gen , 0, 1, "Measure of group barycentre into laboratory on/off");
	create_iparameter("test_barycentre_distances","lifetime",&test_barycentre_distances , 0, 2, "Measure of Euclidean distances between each individual and all the group in one gen into laboratory on/off");
	create_iparameter("test_hamming_distance","lifetime",&test_hamming_distance , 0, 2, "Measure of Hamming Distance between Genotypes on/off - 1=poulations, 2=bests comparisons");
	create_iparameter("xcell","lifetime",&xcell, 0, 0, "exploration cell - x size");
	create_iparameter("ycell","lifetime",&ycell, 0, 0, "exploration cell - y size");
	create_iparameter("explorindnumber","lifetime",&explorindnumber, 0, 0, "exploration individual number");
	create_iparameter("test_times","lifetime",&test_times , 0, 1, "Measure of times on/off");
	create_iparameter("test_times_all_gen","lifetime",&test_times_all_gen , 0, 1, "Measure of times into all generations on/off");
	create_iparameter("test_times_ntestpop","lifetime",&test_times_ntestpop , 0, 100, "Percentade of eaten preies population");
	create_iparameter("test_times_all_gen_eco","lifetime",&test_times_all_gen_eco , 0, 1, "Test Times Population Percentade Testing on/off");
	create_iparameter("test_fitness_lab","lifetime",&test_fitness_lab , 0, 1, "Test Fitness into Laboratory on/off");
	create_iparameter("test_fitness_lab_all_gen","lifetime",&test_fitness_lab_all_gen , 0, 1, "Test Fitness into Laboratory all gen on/off");
	create_iparameter("grid","lifetime",&grid, 0, 0, "grid step affecting the world editing interface");
	create_iparameter("envdx","lifetime",&envdx, 0, 0, "dx of the environment");
	create_iparameter("envdy","lifetime",&envdy, 0, 0, "dy of the environment");
	create_iparameter("display_delay","lifetime",&display_delay, 1, 1000000000, "display delay");
	create_iparameter("display_under_evolution","lifetime",&display_under_evolution, 0, 1, "display robots under evolution");
	create_iparameter("n_ind_to_display_under_evolution","lifetime",&n_ind_to_display_under_evolution, 0, 1000, "n individual to display under evolution");

	//  Creo i parametri per la partenza dei due robot
	//  Create the parameters for the departure of the robots
	create_iparameter("startx_1","individual",&startx[0],0,envdx,"x start for the first robot");
	create_iparameter("starty_1","individual",&starty[0],0,envdy,"y start for the first robot");
	create_iparameter("startx_2","individual",&startx[1],0,envdx,"x start for the second robot");
	create_iparameter("starty_2","individual",&starty[1],0,envdy,"y start for the second robot");
	create_iparameter("startx_3","individual",&startx[2],0,envdx,"x start for the third robot");
	create_iparameter("starty_3","individual",&starty[2],0,envdy,"y start for the third robot");
}


// Assegna un cilindro-colore ad ogni individuo
void assign_individual_color(struct individual *pind)
{
	struct envobject *obj,**eobj,*eobj_tmp;		// struttura temporanea per il controllo cilindri, ed altri elementi
	int identifier;
	
	// Espediente dei cilindri per permettere ai robot di vedersi tra loro. 
	// Da modificare e migliorare in seguito.
    eobj = envobjs + SROUND;				// Punta alla lista dei cilindri
    envnobjs[SROUND] += 1;					// Incrementa il numero di cilindri
	identifier = envnobjs[SROUND]-1;		// Imposta l'identificatore e selettore

    int nobjs=envnobjs[SROUND];

	// Rialloca la lista dei cilindri per inserire il nuovo elemento   
	eobj_tmp = (struct envobject *) realloc(*eobj,envnobjs[SROUND] * sizeof(struct envobject));
	*eobj=eobj_tmp;

    obj = *eobj;							// Punta alla lista dei cilindri
	obj = (obj + identifier);				// Punta al nuovo elemento aggiunto	
	obj->id = identifier;					// Inserisce come id il numero di cilindri presenti nell'ambiente
	obj->type = SROUND;
	obj->r = robotradius;
	obj->nind = pind->number;				// Assegna il cilindro all'individuo
	obj->subtype = 10;						// cilindro posizionato sull'individuo
	obj->x=pind->pos[0];					// Posiziona il cilindro dov'� l'individuo
	obj->y=pind->pos[1];					// Posiziona il cilindro dov'� l'individuo
	obj->c[0] = pind->RColor;				// Assegna la componente rossa del colore
	obj->c[1] = pind->GColor;				// Assegna la componente verde del colore
	obj->c[2] = pind->BColor;				// Assegna la componente blue del colore
}

// Modifica il colore di un cilindro associato ad un individuo
void modify_individual_color(struct individual *pind)
{
	struct envobject *obj;					// struttura temporanea per il controllo cilindri, ed altri elementi
	int i;
	
	for(i=0,obj = *(envobjs + SROUND);i<envnobjs[SROUND];i++,obj++)
		if ((obj->subtype==10) && (obj->nind==pind->number)) {
			// Assegna al cilindro il nuovo colore assegnato al robot
			obj->c[0] = pind->RColor;				// Assegna la componente rossa del colore
			obj->c[1] = pind->GColor;				// Assegna la componente verde del colore
			obj->c[2] = pind->BColor;				// Assegna la componente blue del colore
			break;
		}
}

int check_perceptualinds(int team) {
	int match=0;

	for (int i=0; (i<ipar->nperceptualindividuals) && !match; i++) 
		match=*(perceptualinds+i)==team;

	return (match);
}

/*
 * Initializes the robots from a start point : index of first robot to initialize
 */
void
init_robot(int first_ind_idx)
{
    struct  individual  *pind, *tmp_ind;
    struct  iparameters *pipar;
 
    int     team;
    int     cneuron;
    int     i,ii;
    int     a;	
	int     percept_candidate;

	int		nindperrace;		// Numero di individui per razza
	int		race;

	int *sx, *sy;
	float *px, *py, *ds;	

	int	    **ea;				// cursore dell'exploration array
	int		***e1, **e2;	   // cursore aggiuntivo
	int x_size, y_size;

	// Test delle distanze degli individui dalle food zone
	if ((test_distance_from_fz) && (envnobjs[GROUND] > 0)) {
		x_size=nteam;															// Numero di individui
		y_size=envnobjs[GROUND];												// Numero di food zones all'interno dell'ambiente
																		
		distances_from_fz_array = (int **) malloc(x_size * sizeof(int *));		// Vettore delle distanze medie di ogni individuo dalle rispettive food zone, all'interno di vari trial
	
		if (distances_from_fz_array == NULL) 
		    display_error("System error: distances_from_fz_array array malloc");

		ea=distances_from_fz_array;							// cursore
		for (i=0; i < x_size; i++,ea++)						// scorre la matrice
															// della popolazione corrente
		{
			*ea = (int *)malloc(y_size * sizeof(int));		// alloca ogni riga della matrice

			if (*ea == NULL)
				display_error("System error: ea malloc");	
		}
	}

	// Misura statistica n.16 - 17 (test del barycentro)
	// attenzione test da fare solo su un epoca altrimenti non ha senso !!
	if ((test_barycentre) || (test_barycentre_distances_all_gen) || (test_barycentre_distances) /*&& (test_statistics)*/) {		// Allocazione eventuale dell'barycentre array
		x_size=sweeps;												// Numero di elementi pari al numero di cicli
		y_size=nteam*2+2+nteam*2;										// Numero di colonne pari al numero di posizioni (*2 ascissa e ordinata) degli individui + posizione del baricentro
																		// + posizioni del baricentro del gruppi composti da individui : 1,2,3 - 0,2,3 - 0,1,3 - 0,1,2			
		barycentrearray = (int **) malloc(x_size * sizeof(int *));
	
		if (barycentrearray == NULL) 
		    display_error("System error: barycentre array malloc");

		ea=barycentrearray;								// cursore
		for (i=0; i < x_size; i++,ea++)						// scorre la matrice
															// della popolazione corrente
		{
			*ea = (int *)malloc(y_size * sizeof(int));		// alloca ogni riga della matrice

			if (*ea == NULL)
				display_error("System error: ea malloc");	
		}
	}

	// Misura statistica n.18 (test delle distanze euclidee dal baricentro)
	// questo test � possibile farlo su pi� epoche, verranno mediate le distanze ciclo per ciclo su tutte le epoche !!
	// ovviamente � possibile farlo solo su una sola epoca. 
	if ((test_barycentre_distances) || (test_barycentre_distances_all_gen) /*&& (test_statistics)*/) {								// Allocazione eventuale del' barycentre distances array
		x_size=sweeps;												// Numero di elementi pari al numero di cicli
		y_size=nteam;												// Numero di colonne pari al numero di individui : per ognuno una distanza rispetto al baricentro
		barycentredistancesarray = (int **) malloc(x_size * sizeof(int *));
	
		if (barycentredistancesarray == NULL) 
		    display_error("System error: barycentre distances array malloc");

		ea=barycentredistancesarray;						// cursore
		for (i=0; i < x_size; i++,ea++)						// scorre la matrice
															// della popolazione corrente
		{
			*ea = (int *)malloc(y_size * sizeof(int));		// alloca ogni riga della matrice

			if (*ea == NULL)
				display_error("System error: ea malloc");	
		}
	}

	// Misura statistica n.8 (test di esplorativit� degli individui) 
	if ((test_exploratory) || (test_exploratory_all_gen)) {		// Allocazione eventuale dell'exploration array
		x_size=envdx/xcell;
		y_size=envdy/ycell;
		explorationarray = (int **) malloc(x_size * sizeof(int *));
	
		if (explorationarray == NULL) 
		    display_error("System error: exploration array malloc");

		ea=explorationarray;								// cursore
		for (i=0; i < x_size; i++,ea++)						// scorre la matrice di esplorazione
															// della popolazione corrente
		{
			*ea = (int *)malloc(y_size * sizeof(int));		// alloca ogni riga della matrice

			if (*ea == NULL)
				display_error("System error: ea malloc");	
		}
	}

	// Inizializza il vettore che memorizza gli individui estratti casualmente nell'evoluzione con pi� razze (popolazioni) 
	// Solo nel caso in cui non ci sono ripetizioni nell'estrazione degli individui
	if ((n_fixed_evolution_is_running == 1) && (evaluate_different_inds) && (nraces>0)) {		
		nindperrace=nindividuals/nraces;
		selected_ind_array = (int **) malloc(nindperrace * sizeof(int *));
	
		if (selected_ind_array == NULL) 
		    display_error("System error: exploration array malloc");

		ea=selected_ind_array;								// cursore
		for (i=0; i < nindperrace; i++,ea++)				// scorre la matrice di tracciamento
															// della popolazione corrente
		{
			*ea = (int *)malloc(nraces * sizeof(int));		// alloca ogni riga della matrice di nraces righe

			if (*ea == NULL)
				display_error("System error: ea malloc");	
		}
	}

	if (ind == NULL) { 		// se al momento della chiamata di init_robot il vettore dei fenotipi non � allocato, allora vuol
							// dire che � alla prima chiamata 
		ind = (struct individual *) malloc((nteam) * sizeof(struct individual));
		if (ind == NULL)
		{
			display_error("individual malloc error");
		}
	}
	else {					// altrimenti lo rialloca i fenotipi rispetto ai nuovi genotipi creati
		if ((n_fixed_evolution_is_running == 0) || ((n_fixed_evolution_is_running == 1) && (nraces>1))) { // in caso di evoluzione singola e di assenza di specie, non rialloca nulla
			tmp_ind = (struct individual *) realloc(ind,(nteam) * sizeof(struct individual));
			if (tmp_ind == NULL)
			{
				display_error("individual reallocation error");
			}
			ind=tmp_ind;
		}
	}

	if (ipar->onlyfewcanperceive) {	// solo se ci sono individui percettivi li seleziona a caso
		// Alloca e Inizializza il vettore degli individui che percepiscono la food zone
		if (perceptualinds == NULL) {			// ha bisogno di allocare il vettore una volta sola
			perceptualinds = (int *) malloc(ipar->nperceptualindividuals * sizeof(int));	

			if (perceptualinds == NULL) {
				display_error("perceptualinds malloc error");
			}
		}

		// Inizializza casualmente il vettore degli indici degli individui che percepiscono la food zone
		// NOTA ! - per evoluzioni sociale nteam=1 altrimenti di seguito va in loop infinito (sistemare in seguito) 
		// Probabilmente lo esegue due volte in tutti i casi che nteam viene settato due volte
		if (nteam>1) {															// Solo per team maggiori di 1 effettua il caricamehnto
			for (team=0; team < ipar->nperceptualindividuals; team++) { 
				do {
					percept_candidate=mrand(nteam);								// genera un numero compreso tra 0 ed nteam-1 e verifica che non sia stato gi� trovato (senza ripetizioni)
					// controlla che non sia stato gi� generato
					for (int j=0; j<team;j++)  
						if (*(perceptualinds+j)==percept_candidate) {
							percept_candidate=-1;	
							break;
						}
				} while (percept_candidate==-1);
			
				*(perceptualinds+team)=percept_candidate;
			}
		}
	}

	// Effettua la costruzione della rete a partire dall'individuo con indice first_ind_idx
    for (team=first_ind_idx, pind=ind+first_ind_idx, pipar = ipar; team < nteam; team++, pind++)
	{
		pind->population = team;
		pind->n_team = team;
		pind->controlmode = 0;
		pind->dead = 0;
		pind->died_individual = 0;
		pind->ghost_individual = 0;
		pind->ninputs  = 0;
		pind->noutputs = 0;
		pind->clk1 = 0;
		pind->clk2 = 0;
		pind->clk3 = 1;
		cneuron = 0;
		pind->fitness   = 0.0;
		pind->virtualfitness = 0.0;
		pind->oldfitness= 0.0;
		pind->totcycles = 0;
		pind->number = team;
		pind->orig_number = team;
		pind->can_see_fake_colors = 0;
		
		initHungerArray(pind);
		
		initThirstArray(pind);
		

		if (ipar->bumpsensors >0)
			for(i=0; i < 2; i++) 
				pind->bumpsensors[i] = 0;

		if (pipar->rgbcolorretina>0) {
	
			if (!pipar->rgbcolorretinagrayscale) { 
				pind->RColorPerceived=(int *) malloc(pipar->rgbcolorretina * sizeof(int)); 
				if (pind->RColorPerceived == NULL)
					display_error("pind->RColorPerceived malloc error");
			
				pind->GColorPerceived=(int *) malloc(pipar->rgbcolorretina * sizeof(int)); 
				if (pind->GColorPerceived == NULL)
					display_error("pind->GColorPerceived malloc error");
			
				pind->BColorPerceived=(int *) malloc(pipar->rgbcolorretina * sizeof(int)); 
				if (pind->BColorPerceived == NULL)
					display_error("pind->BColorPerceived malloc error");
		
				for (i=0; i < pipar->rgbcolorretina; i++) {
					pind->RColorPerceived[i] = 0;		// componente rossa percepita
					pind->GColorPerceived[i] = 0;		// componente verde percepita		
					pind->BColorPerceived[i] = 0;		// componente blue percepita	
				}
			} else {
				pind->GrayScaleColorPerceived=(int *) malloc(pipar->rgbcolorretina * sizeof(int)); 
				if (pind->GrayScaleColorPerceived == NULL)
					display_error("pind->GrayScaleColorPerceived malloc error");
	
				for (i=0; i < pipar->rgbcolorretina; i++) 
					pind->GrayScaleColorPerceived[i] = 0;		// componente di grigio percepita
			}		
		}

		pind->RColor=0;
		pind->GColor=255;	// colore verde
		pind->BColor=0;

		if ((pipar->onlyfewcanperceive) && (pipar->percepindwithdiffcolor) && (check_perceptualinds(team))) {
			pind->RColor=0;
			pind->GColor=0;	// colore nero
			pind->BColor=0;
		}
		
		pind->race=0;		// Tipo di razza
		
		// sensors
        if (pipar->motmemory == 1)	   
			for (i=0; i < pipar->wheelmotors; i++, cneuron++, pind->ninputs++)
				sprintf(neuronl[cneuron],"M%d",i);
        if (pipar->bumpsensors == 1)	   
			for (i=0; i < 2; i++, cneuron++, pind->ninputs++)
				sprintf(neuronl[cneuron],"Bp%d",i);
		for (i=0; i < pipar->nifsensors; i++, cneuron++, pind->ninputs++)
            sprintf(neuronl[cneuron],"I%d",i);
        for (i=0,a = pipar->ifang; i < pipar->nifsensors; i++, a+= pipar->ifdang) 
		{
			if (a > 360) a -= 360;
			if (a < 0) a += 360;
				pind->cifang[i] = a;
			if (ROBOTTYPE == KHEPERA && i == 6)
				pind->cifang[i] = 144;
			if (ROBOTTYPE == KHEPERA && i == 7)
				pind->cifang[i] = 216;
				pind->cifsensors[i] = 0.0;
		}
        for (i=0; i < pipar->lightsensors; i++, cneuron++, pind->ninputs++)
            sprintf(neuronl[cneuron],"L%d",i);		   
        for (i=0; i < (pipar->signalss * pipar->signalso); i++, cneuron++, pind->ninputs++)
            sprintf(neuronl[cneuron],"s%d",i);
		for (i=0; i < pipar->signalso; i++, cneuron++, pind->ninputs++)
            sprintf(neuronl[cneuron],"S%d",i);	
		for (i=0; i < pipar->camerareceptors * pipar->cameracolors; i++, cneuron++, pind->ninputs++)
            sprintf(neuronl[cneuron],"V%d",i); 
	    if (pipar->simplevision == 1)  
			for (i=0; i < 5; i++, cneuron++, pind->ninputs++)
				sprintf(neuronl[cneuron],"V%d",i);
        if (pipar->simplevision == 2)  
			for (i=0; i < 3; i++, cneuron++, pind->ninputs++)
				sprintf(neuronl[cneuron],"V%d",i);
		// Da modificare pi� in la, inserendo un blocco unico con moltiplicazione
		if(pipar->nxtcam==1)
			for (i=0; i < 3; i++, cneuron++, pind->ninputs++)
				sprintf(neuronl[cneuron],"xC%d",i);
		if(pipar->nxtcam==2)
			for (i=0; i < 6; i++, cneuron++, pind->ninputs++)
				sprintf(neuronl[cneuron],"xC%d",i);
		
		if(pipar->orientationcam==1) {
			// Percettore dell'individuo pi� vicino
			for (i=0; i < 2; i++, cneuron++, pind->ninputs++)
				sprintf(neuronl[cneuron],"Or%d",i);
		}

		if (!pipar->rgbcolorretinagrayscale) {
			for (i=0;i<pipar->rgbcolorretina;i++) {
				sprintf(neuronl[cneuron],"R%d",i);
				pind->ninputs++; cneuron++;
				sprintf(neuronl[cneuron],"G%d",i);
				pind->ninputs++; cneuron++;
				sprintf(neuronl[cneuron],"B%d",i);
				pind->ninputs++; cneuron++;
			}
		} else {
			for (i=0;i<pipar->rgbcolorretina;i++) {
				sprintf(neuronl[cneuron],"R%d",i);
				pind->ninputs++; cneuron++;
			}
		}

		if(pipar->nxtcam==1)
			for (i=0; i < 3; i++, cneuron++, pind->ninputs++)
				sprintf(neuronl[cneuron],"xC%d",i);

		if(pipar->leadersfollowerspercepsystem>0) {
			// Percettore del leader pi� vicino
			if (pipar->leadersfollowerspercepsystem >= 2)
				for (i=0; i < 2; i++, cneuron++, pind->ninputs++)
					sprintf(neuronl[cneuron],"nL%d",i);
			// Percettore della food zone pi� vicina pi� vicino
			if (pipar->leadersfollowerspercepsystem >= 1)
				for (i=0; i < 2; i++, cneuron++, pind->ninputs++)
					sprintf(neuronl[cneuron],"nIs%d",i);
			// Percettore della percentuale dei leader
			if (pipar->leadersfollowerspercepsystem >= 4) {
				sprintf(neuronl[cneuron],"%%L");
				cneuron++;
				pind->ninputs++;
			}

			// Percettore della percentuale dei follower
			if (pipar->leadersfollowerspercepsystem >= 3) {
				sprintf(neuronl[cneuron],"%%F");
				cneuron++;
				pind->ninputs++;
			}
		}
		
		if(pipar->foodperceptionsystem>0) {
			// Perceptor of food
			for (i=0; i < 2; i++, cneuron++, pind->ninputs++)
				sprintf(neuronl[cneuron],"F%d",i);
		}

		if(pipar->waterperceptionsystem>0) {
			// Perceptor of food
			for (i=0; i < 2; i++, cneuron++, pind->ninputs++)
				sprintf(neuronl[cneuron],"W%d",i);
		}

		if(pipar->clock1>0)
			for (i=0; i < 1; i++, cneuron++, pind->ninputs++)
				sprintf(neuronl[cneuron],"Clk1",i);
		if(pipar->clock2>0)
			for (i=0; i < 1; i++, cneuron++, pind->ninputs++)
				sprintf(neuronl[cneuron],"Clk2",i);
		if(pipar->clock3>0)
			for (i=0; i < 1; i++, cneuron++, pind->ninputs++)
				sprintf(neuronl[cneuron],"Clk3",i);
		if(pipar->brownoise>0)
			for (i=0; i < pipar->brownoise; i++, cneuron++, pind->ninputs++)
				sprintf(neuronl[cneuron],"BN%d",i);
		if(pipar->whitenoise>0)
			for (i=0; i < pipar->whitenoise; i++, cneuron++, pind->ninputs++)
				sprintf(neuronl[cneuron],"WN%d",i);
		if(pipar->motorload>0)
			for (i=0; i < 2; i++, cneuron++, pind->ninputs++)
				sprintf(neuronl[cneuron],"Ml%d",i);
        for (i=0; i < pipar->communications; i++, cneuron++, pind->ninputs++)
            sprintf(neuronl[cneuron],"C%d",i);
		for (i=0; i < pipar->a_groundsensor2; i++, cneuron++, pind->ninputs++)
            sprintf(neuronl[cneuron],"g%d",i);

        for (i=0; i < pipar->groundsensor; i++, cneuron++, pind->ninputs++)
            sprintf(neuronl[cneuron],"G%d",i);

		for (i=0; i < pipar->foodSensor; i++, cneuron++, pind->ninputs++)
            sprintf(neuronl[cneuron],"FZ%d",i);
		for (i=0; i < pipar->waterSensor; i++, cneuron++, pind->ninputs++)
            sprintf(neuronl[cneuron],"WZ%d",i);

		// Perceptor of hunger
		if ((pipar->hungerSensor == 1) || (pipar->hungerSensor == 2))
		{
			//	Hunger sensor types 1 & 2 display only a single output
			for (i=0; i < 1; i++, cneuron++, pind->ninputs++)
				sprintf(neuronl[cneuron],"HU");
		}
		else if ((pipar->hungerSensor == 3) || (pipar->hungerSensor == 4) || (pipar->hungerSensor == 5) || (pipar->hungerSensor == 6))
		{
			//	Hunger sensor type 3, 4, 5 & 6 displays a three bit encoding of hunger
			for (i=0; i < 3; i++, cneuron++, pind->ninputs++)
				sprintf(neuronl[cneuron],"HU%d",i);
		}

		// Perceptor of thirst
		if ((pipar->thirstSensor == 1) || (pipar->thirstSensor == 2))
		{
			//	Thirst sensor types 1 & 2 display only a single output
			for (i=0; i < 1; i++, cneuron++, pind->ninputs++)
				sprintf(neuronl[cneuron],"TH");
		}
		else if ((pipar->thirstSensor == 3) || (pipar->thirstSensor == 4) || (pipar->thirstSensor == 5) || (pipar->thirstSensor == 6))
		{
			//	Thirst sensor type 3, 4, 5 displays a three bit encoding of thirst
			for (i=0; i < 3; i++, cneuron++, pind->ninputs++)
				sprintf(neuronl[cneuron],"TH%d",i);
		}

		//	Perceptor of night and day
		for (i=0; i < pipar->dayNightSensor; i++, cneuron++, pind->ninputs++)
            sprintf(neuronl[cneuron],"DN%d",i);

		if (pipar->ageSensor == 1)
		{
			//	Type 1: Tracking of age to 3 bit resolution
			for (i=0; i < 3; i++, cneuron++, pind->ninputs++)
				sprintf(neuronl[cneuron],"A%d",i);
		}
        for (i=0; i < pipar->a_groundsensor; i++, cneuron++, pind->ninputs++)
            sprintf(neuronl[cneuron],"g%d",i);
        for (i=0; i < pipar->input_outputs; i++, cneuron++, pind->ninputs++)
            sprintf(neuronl[cneuron],"O%d",i);
        if (pipar->nutrients > 0)  
			for (i=0; i < 2; i++, cneuron++, pind->ninputs++)
	            sprintf(neuronl[cneuron],"N%d",i);
	    if (pipar->compass == 1)  
	        for (i=0; i < 2; i++, cneuron++, pind->ninputs++)
	            sprintf(neuronl[cneuron],"C%d",i);
	    if (pipar->energy == 1)  
	         for (i=0; i < 1; i++, cneuron++, pind->ninputs++)
	            sprintf(neuronl[cneuron],"E");
	    // hidden 	   
        for (i=0; i < pipar->nhiddens; i++, cneuron++)
			sprintf(neuronl[cneuron],"H%d",i);
	    // motor neurons
	    if (pipar->wheelmotors > 0)
			for (i=0; i < pipar->wheelmotors; i++, cneuron++, pind->noutputs++)
				sprintf(neuronl[cneuron],"M%d",i);
		// altri segnali dui uscita
		for (i=0; i < pipar->signalso; i++, cneuron++, pind->noutputs++)
			sprintf(neuronl[cneuron],"S%d",i);
		for (i=0; i < pipar->pushingunits; i++, cneuron++, pind->noutputs++)
			sprintf(neuronl[cneuron],"PU%d",i);		// aggiunge le pushing-units
		for (i=0; i < pipar->categorization; i++, cneuron++, pind->noutputs++)
			sprintf(neuronl[cneuron],"CH%d",i);		// aggiunge gli output di categorizzazione
		for (i=0; i < pipar->communications; i++, cneuron++, pind->noutputs++)
			sprintf(neuronl[cneuron],"C%d",i);		// aggiunge gli output di comunicazione
		// segnali di input - output
	    for (i=0; i < pipar->input_outputs; i++, cneuron++, pind->noutputs++)
		    sprintf(neuronl[cneuron],"O%d",i);

		// fitness 
        sprintf(neuronl[cneuron],"F");
        cneuron++;
		// camera
		if (pipar->camerareceptors > 0)
	    {
			pind->camera_nr = pipar->camerareceptors;
			pind->camera_c = pipar->cameracolors;
			pind->camera_ang = (pipar->cameraviewangle / 2);						// Angolazione di met� campo visivo			
			pind->camera_dang = pipar->cameraviewangle / pipar->camerareceptors;	// Porzione di angolo assegnata ad ogni recettore RGB
			for(i=0; i < pipar->cameracolors; i++)
				for(ii=0; ii < pipar->cameraviewangle; ii++)
					pind->camera[i][ii] = 0.0;					// vettore camera organizzato per colore e angolazione
	    }
		// nxtcam
		if(pipar->nxtcam>0)
		{
			//initialization
		}

		pind->nhiddens = pipar->nhiddens;

		for(i=0;i < (pind->ninputs + pind->noutputs + pipar->nhiddens);i++)
			strcpy(neuroncl[i],neuronl[i]); 
	}
	
	// Se ci sono pi� specie, a seconda del criterio scelto in configurazione divide la popolazione
	// in pi� specie, ma solo durante l'inizializzazione iniziale, ovvero quando first_ind_idx = 0
	// Al momento funziona solo con evoluzione n_fissa
	if ((nraces>1) && (first_ind_idx == 0) && ((testindividual_is_running == 1)||(n_fixed_evolution_social_is_running == 1)||(testpopulation_is_running == 1))) {	// viene lanciato solo la prima volta
		// Effettua la suddivisione equa della popolazione in razze
		nindperrace=nteam/nraces;				// il numero di individui totali su cui calcolare 
												// la numerosit� di ogni razza � dal numero figli x numero padri
		/*if (n_fixed_evolution_is_running == 1)
			nindperrace=nindividuals/nraces;	// correzione forse necessaria per adattare l'evoluzione singola alle razze
		*/
		for (race=0; race < nraces; race++) 
			for (team=first_ind_idx+nindperrace*race, pind=ind+(first_ind_idx+nindperrace*race), pipar = ipar; team < nindperrace*(race+1); team++, pind++)
				pind->race=race;														// Assegna la specie all'individuo
	}

	// If there are more races in sigle evolution, generate races for all the team
	if ((nraces>1) && (first_ind_idx == 0) && (n_fixed_evolution_is_running == 1)) {	// viene lanciato solo la prima volta

		for (team=first_ind_idx, pind=ind, pipar = ipar; team < nteam; team++, pind++)
			pind->race=team;
	}

	if (nraces==1) { 
		if (team_with_diffcolors) {
			// Se ogni individuo non ha colore diverso codificato nel genotipo
			if (!color_into_genotype) {
				for (team=first_ind_idx, pind=ind+first_ind_idx; team < nteam; team++, pind++) {
					if (pind->number==0) {
						pind->RColor=0;
						pind->GColor=255;				// il primo membro del team ha colore verde di default
						pind->BColor=0;
					} else if (pind->number==1) {
						pind->RColor=0;
						pind->GColor=0;					// il secondo membro del team ha colore verde di default
						pind->BColor=255;
					} else if (pind->number==2) {
						pind->RColor=0;
						pind->GColor=255;				// il terzo membro del team ha colore verde di default
						pind->BColor=255;
					} else if (pind->number==3) {
						pind->RColor=255;
						pind->GColor=255;				// il quarto membro del team ha colore verde di default
						pind->BColor=0;
					} else {
						pind->RColor=70+pind->number*20;
						pind->GColor=0;					// per tutti gli altri membri del team diverse tonalit� di rosso
						pind->BColor=0;
					}
				}
			}
		}
	} else {
		if (races_with_diffcolors) {					// Popolazioni di differente colore
			// Se ogni individuo non ha colore diverso codificato nel genotipo e se ci sono pi� razze allora colora gli individui di razza diversa con colore diverso. 
			if (!color_into_genotype)  {
				for (team=first_ind_idx, pind=ind+first_ind_idx; team < nteam; team++, pind++) {
					if (pind->race==0) {
						if (ipar->color_race0==0) {
							pind->RColor=0;
							pind->GColor=255;			// la prima specie colore verde di default
							pind->BColor=0;
						} else {
							if (ipar->color_race0==3) {
								pind->RColor=255;
								pind->GColor=0;			
								pind->BColor=0;
							} else if (ipar->color_race0==6) {
								pind->RColor=0;
								pind->GColor=255;			
								pind->BColor=0;
							} else if (ipar->color_race0==4) {
								pind->RColor=0;
								pind->GColor=0;			
								pind->BColor=255;
							} else if (ipar->color_race0==12) {
								pind->RColor=100;
								pind->GColor=0;			
								pind->BColor=0;
							} else if (ipar->color_race0==5) {
								pind->RColor=0;
								pind->GColor=127;			
								pind->BColor=255;
							} else if (ipar->color_race0==7) {
								pind->RColor=127;
								pind->GColor=255;			
								pind->BColor=0;
							} else if (ipar->color_race0==8) {
								pind->RColor=255;
								pind->GColor=0;			
								pind->BColor=127;
							} else if (ipar->color_race0==9) {
								pind->RColor=0;
								pind->GColor=255;			
								pind->BColor=127;
							}
						}
					} else if (pind->race==1) {
						if (ipar->color_race1==0) {
							pind->RColor=0;
							pind->GColor=0;					// la seconda specie color blue di default
							pind->BColor=255;
						} else {
							if (ipar->color_race1==3) {
								pind->RColor=255;
								pind->GColor=0;			
								pind->BColor=0;
							} else if (ipar->color_race1==6) {
								pind->RColor=0;
								pind->GColor=255;			
								pind->BColor=0;
							} else if (ipar->color_race1==4) {
								pind->RColor=0;
								pind->GColor=0;			
								pind->BColor=255;
							} else if (ipar->color_race1==12) {
								pind->RColor=100;
								pind->GColor=0;			
								pind->BColor=0;
							} else if (ipar->color_race1==5) {
								pind->RColor=0;
								pind->GColor=127;			
								pind->BColor=255;
							} else if (ipar->color_race1==7) {
								pind->RColor=127;
								pind->GColor=255;			
								pind->BColor=0;
							} else if (ipar->color_race1==8) {
								pind->RColor=255;
								pind->GColor=0;			
								pind->BColor=127;
							} else if (ipar->color_race1==9) {
								pind->RColor=0;
								pind->GColor=255;			
								pind->BColor=127;
							}
						}
					} else if (pind->race==2) {
						if (ipar->color_race2==0) {
							pind->RColor=0;
							pind->GColor=255;				// la terza specie colore azzurro di default
							pind->BColor=255;
						} else {
							if (ipar->color_race2==3) {
								pind->RColor=255;
								pind->GColor=0;			
								pind->BColor=0;	
							} else if (ipar->color_race2==6) {
								pind->RColor=0;
								pind->GColor=255;			
								pind->BColor=0;
							} else if (ipar->color_race2==4) {
								pind->RColor=0;
								pind->GColor=0;			
								pind->BColor=255;
							} else if (ipar->color_race2==12) {
								pind->RColor=100;
								pind->GColor=0;			
								pind->BColor=0;
							} else if (ipar->color_race2==5) {
								pind->RColor=0;
								pind->GColor=127;			
								pind->BColor=255;
							} else if (ipar->color_race2==7) {
								pind->RColor=127;
								pind->GColor=255;			
								pind->BColor=0;
							} else if (ipar->color_race2==8) {
								pind->RColor=255;
								pind->GColor=0;			
								pind->BColor=127;
							} else if (ipar->color_race2==9) {
								pind->RColor=0;
								pind->GColor=255;			
								pind->BColor=127;
							}
						}
					} else if (pind->race==3) {
						if (ipar->color_race3==0) {
							pind->RColor=255;
							pind->GColor=255;				// la terza specie colore giallo di default
							pind->BColor=0;
						} else {
							if (ipar->color_race3==3) {
								pind->RColor=255;
								pind->GColor=0;			
								pind->BColor=0;	
							} else if (ipar->color_race3==6) {
								pind->RColor=0;
								pind->GColor=255;			
								pind->BColor=0;
							} else if (ipar->color_race3==4) {
								pind->RColor=0;
								pind->GColor=0;			
								pind->BColor=255;
							} else if (ipar->color_race3==12) {
								pind->RColor=100;
								pind->GColor=0;			
								pind->BColor=0;
							} else if (ipar->color_race3==5) {
								pind->RColor=0;
								pind->GColor=127;			
								pind->BColor=255;
							} else if (ipar->color_race3==7) {
								pind->RColor=127;
								pind->GColor=255;			
								pind->BColor=0;
							} else if (ipar->color_race3==8) {
								pind->RColor=255;
								pind->GColor=0;			
								pind->BColor=127;
							} else if (ipar->color_race3==9) {
								pind->RColor=0;
								pind->GColor=255;			
								pind->BColor=127;
							}
						}
					} else {
						pind->RColor=70+pind->race*20;
						pind->GColor=0;					// per tutte le altre specie diverse tonalit� di rosso
						pind->BColor=0;
					}
				}
			}	
		}
	}

	// assegna un eventuale cilindro-colore ad ogni individuo
	if (ipar->visible_individual == 1) { 
		for (team=first_ind_idx, pind=ind+first_ind_idx; team < nteam; team++, pind++) {
			assign_individual_color(pind);		
			pind->ROrigColor=pind->RColor;		// Imposta come colore originario del robot quello impostato
			pind->GOrigColor=pind->GColor;
			pind->BOrigColor=pind->BColor;
		}
	}	
	
	// Alloca e Inizializza i vettori che contengono le posizioni fisse dei robot
	if (startx == NULL) {
		startx = (int *) malloc(nteam * sizeof(int)); 
		if (startx == NULL)
			display_error("startx malloc error");
	
		for (team=first_ind_idx, sx=startx+first_ind_idx; team < nteam; team++, sx++)
			*sx=0;
	} else if (startx[0]==0 && starty[0]==0) {				// soltanto se non � pieno lo rialloca
		sx = (int *) realloc(startx,nteam * sizeof(int)); 
		if (startx == NULL)
			display_error("startx realloc error");
		startx=sx;

		for (team=first_ind_idx, sx=startx+first_ind_idx; team < nteam; team++, sx++)
			*sx=0;
	}

	if (starty == NULL) {
		starty = (int *) malloc(nteam * sizeof(int)); 
		if (starty == NULL)
			display_error("starty malloc error");

		for (team=first_ind_idx, sy=starty+first_ind_idx; team < nteam; team++, sy++) 
			*sy=0;
	} else if (startx[0]==0 && starty[0]==0) {				// soltanto se non � pieno lo rialloca
		sy = (int *) realloc(starty,nteam * sizeof(int)); 
		if (starty == NULL)
			display_error("starty realloc error");
		starty=sy;

		for (team=first_ind_idx, sy=starty+first_ind_idx; team < nteam; team++, sy++) 
			*sy=0;
	}
}


/*
 * Initializes the environment
 */
void init_env()
{
    struct  envobject **eobj;
    int     i;

    envobjs = (struct envobject **) malloc(NOBJS * sizeof(struct envobject));
	for (i=0, eobj = envobjs; i < NOBJS; i++, eobj++)
	{
		*eobj = (struct envobject *) malloc(nobjects * sizeof(struct envobject));
		envnobjs[i] = 0;
    }
	load_world("evorobot.env");

	create_world_space();
	load_motor();

#ifdef EVOWINDOWS
	load_obstacle("wall.sam",   wall,     wallcf);
	load_obstacle("round.sam",  obst,     obstcf);
	load_obstacle("small.sam",  sobst,    sobstcf);
	load_obstacle("light.sam",  light,    lightcf);
#endif
#ifdef EVOLINUX	
    load_obstacle("../bin/sample_files/wall.sam",   wall,     wallcf);
	load_obstacle("../bin/sample_files/round.sam",  obst,     obstcf);
	load_obstacle("../bin/sample_files/small.sam",  sobst,    sobstcf);
	load_obstacle("../bin/sample_files/light.sam",  light,    lightcf);
#endif
#ifdef EVOMAC
	load_obstacle("wall.sam",   wall,     wallcf);
	load_obstacle("round.sam",  obst,     obstcf);
	load_obstacle("small.sam",  sobst,    sobstcf);
	load_obstacle("light.sam",  light,    lightcf);
#endif
}


/*
 * Initializes the fitness function
 */
void
init_fitness()

{
    struct  individual  *pind;

	// pointer to the default function that initialize robot positions
	pinit_robot_pos = initialize_robot_position;
	// pointer to the default initialize world position
    pinit_world = initialize_world;

	sprintf(ffitness_descp,"Fitness: undefined");

	if (strcmp(ipar->ffitness,"obstacle-avoidance") == 0)
    {
		sprintf(ffitness_descp,"Fitness: ostacle-avoidance (move fast, straitght, and keep far from obstacles)");
		pffitness = ffitness_obstacle_avoidance;
		fitmode = 1;
    }

	if (strcmp(ipar->ffitness,"near-red") == 0)
    {
		sprintf(ffitness_descp,"Fitness: rewards individual able to stay close to the red obstacle");
		pffitness = ffitness_near_red;
		fitmode = 1;
    }

	if (strcmp(ipar->ffitness,"near-red2") == 0)
    {
		sprintf(ffitness_descp,"Fitness: rewards individual able to stay close to the red obstacle outer fitness");
		pffitness = ffitness_near_red2;
		fitmode = 1;
    }
	if (strcmp(ipar->ffitness,"exploration") == 0)
    {
		sprintf(ffitness_descp,"Fitness: rewards individual able to explore the environment");
		pffitness = ffitness_exploration;
		fitmode = 1;
    }
	if (strcmp(ipar->ffitness,"wallfollowing") == 0)
    {
		sprintf(ffitness_descp,"Fitness: rewards wall following behavior");
		pffitness = ffitness_wallfollowing;
		fitmode = 1;
    }
	if (strcmp(ipar->ffitness,"discrim") == 0)
    {
		sprintf(ffitness_descp,"Fitness: discrim (1.0 when the robot is inside a target area)");
		pffitness = ffitness_discrim;
		pinit_world = initialize_world_discrim; // randomizza la posizione dei ground ed sground
		fitmode = 1;
    }
	if (strcmp(ipar->ffitness,"discrim_race") == 0)
    {
		sprintf(ffitness_descp,"Fitness: discrim_race (1.0 when the robot of one race is inside a target area)");
		pffitness = ffitness_discrim_race;
		pinit_world = initialize_world_discrim; // randomizza la posizione dei ground ed sground
		fitmode = 1;
    }
	if (strcmp(ipar->ffitness,"discrim_predator_eats_prey") == 0)
    {
		sprintf(ffitness_descp,"Fitness: discrim_predator_eats_prey (1.0 when the robot of one race is inside a target area, one race can eat another race)");
		pffitness = ffitness_discrim_predator_eats_prey;
		pinit_world = initialize_world_discrim; // randomizza la posizione dei ground ed sground
		fitmode = 1;
    }
	if (strcmp(ipar->ffitness,"groupdiscrim") == 0)
    {
		sprintf(ffitness_descp,"Fitness: group discrim (1.0 when all the robots of the team are inside a target area)");
		pffitness = ffitness_groupdiscrim;
		pinit_world = initialize_world_discrim; // randomizza la posizione dei ground ed sground
		fitmode = 4;
    }
	//	Food / Water combined fitness function
	if (strcmp(ipar->ffitness,"distributedLeadership") == 0)
    {
		sprintf(ffitness_descp,"Fitness: distributed leadership fit = (Food + Water - |Food - Water|)/%i",sweeps);
		pffitness = ffitness_distributedLeadership;
		pinit_world = initialize_world_discrim; // randomizza la posizione dei ground ed sground
		fitmode = 4;
    }
	if (strcmp(ipar->ffitness,"distributedLeadership_Simplified") == 0)
    {
		sprintf(ffitness_descp,"Fitness: distributed leadership fit = (Food + Water)/%i",sweeps);
		pffitness = ffitness_distributedLeadership_Simplified;
		pinit_world = initialize_world_discrim; // randomizza la posizione dei ground ed sground
		fitmode = 4;
    }
	if (strcmp(ipar->ffitness,"distributedLeadershipUnnormalised") == 0)
    {
		sprintf(ffitness_descp,"Fitness: distributed leadership fit = Food + Water - |Food - Water|");
		pffitness = ffitness_distributedLeadershipUnnormalised;
		pinit_world = initialize_world_discrim; // randomizza la posizione dei ground ed sground
		fitmode = 4;
    }
	if (strcmp(ipar->ffitness,"groupdiscrim_1") == 0)
    {
		sprintf(ffitness_descp,"Fitness: group discrim (1.0 when all the robots of the team are inside a target area, +0.25 individual component)");
		pffitness = ffitness_groupdiscrim_1;
		pinit_world = initialize_world_discrim; // randomizza la posizione dei ground ed sground
		fitmode = 4;
    }
	if (strcmp(ipar->ffitness,"groupdiscrim_2") == 0)
    {
		sprintf(ffitness_descp,"Fitness: group discrim (1.0 when all the robots of the team are inside a target area, +0.25 individual component, 0.50 two are in food zone, etc...)");
		pffitness = ffitness_groupdiscrim_2;
		pinit_world = initialize_world_discrim; // randomizza la posizione dei ground ed sground
		fitmode = 4;
    }
	if (strcmp(ipar->ffitness,"predator_prey1") == 0)
    {
		sprintf(ffitness_descp,"Fitness: predator_prey1 (1.0 when prey robot is inside a target area) (1.0 when predator robot eat one prey)");
		pffitness = ffitness_predator_prey1;
		pinit_world = initialize_world_discrim; // randomizza la posizione dei ground ed sground
		fitmode = 2;
    }
	if (strcmp(ipar->ffitness,"predator_prey2") == 0)
    {
		sprintf(ffitness_descp,"Fitness: predator_prey2 (1.0 when prey lives for all his life");
		pffitness = ffitness_predator_prey2;
		pinit_world = initialize_world_discrim; // randomizza la posizione dei ground ed sground
		fitmode = 2;
    }
	if (strcmp(ipar->ffitness,"corvids14") == 0)
    {
		sprintf(ffitness_descp,"Fitness: corvids14 (rewards individual able to keep the bar close to the small target area, and which arrive in the same time, with continuous fitness)");
		pffitness = ffitness_corvids14;
		fitmode = 1;
    }
	if (strcmp(ipar->ffitness,"corvids15") == 0)
    {
		sprintf(ffitness_descp,"Fitness: corvids15 (rewards individual able to keep the bar close to the small target area, and which arrive in the same time, with continuous fitness)");
		pffitness = ffitness_corvids15;
		fitmode = 1;
    }
	if (strcmp(ipar->ffitness,"leadersfollowers") == 0)
    {
		sprintf(ffitness_descp,"Fitness: Leaders - Followers (rewards individual able to enter into island");
		pffitness = ffitness_leadersfollowers;
		fitmode = 1;
    }
	if (strcmp(ipar->ffitness,"leadersfollowers2") == 0)
    {
		sprintf(ffitness_descp,"Fitness: Leaders - Followers (rewards individual able to enter into island");
		pffitness = ffitness_leadersfollowers2;
		fitmode = 1;
    }
	if (strcmp(ipar->ffitness,"collective-navigation") == 0)
    {
		sprintf(ffitness_descp,"Fitness: collective-navigation (0.25 for each robot inside targets, -1.0 for each extra robot)");
		pffitness = ffitness_stay2_on_areas;
		pinit_robot_pos = initialize_robot_position_stay2; // dedicated function for initializing the robot position
		fitmode = 2;
    }
	if (strcmp(ipar->ffitness,"different-areas") == 0)
    {
		sprintf(ffitness_descp,"Fitness: different-areas (1.0 if two robots are in two different areas, -1 if they are in the same)");
		pffitness = ffitness_stay_on_different_areas;
		fitmode = 2;
    }
	if (strcmp(ipar->ffitness,"tswitch") == 0)
    {
		sprintf(ffitness_descp,"Fitness: tswitch (1.0 every time the two robots are in two different area for the first time or after a switch)");
		pffitness = ffitness_tswitch;
        pinit_world = initialize_world_tswitch;
		fitmode = 2;
    }
	if (strcmp(ipar->ffitness,"arbitrate") == 0)
    {
		sprintf(ffitness_descp,"Fitness: recharge, forage, and avoid predator");
		pffitness = ffitness_arbitrate;
		pinit_world = initialize_world_arbitrate;
		pinit_robot_pos = initialize_robot_position_arbitrate;
		ind->controlmode = 0;
		if (nteam > 1)
	    {
			pind = (ind + 1);
			pind->controlmode = 1;
        }
		fitmode = 2;
    }

    // we now call the function that initialize the robot position
	// WE SHOULD DISCOMMENT THIS AND REMOVE THE CALL FROM MAINWINDOW.CPP
    //(*pinit_robot_pos)();
}



/*
 * read and return the number of sensors, internal and motors
 */
void
read_robot_units(int i, int *nsensors, int *ninternals, int *nmotors)

{
  struct  individual  *pind;

  pind = (ind + i);

  *nsensors = pind->ninputs;
  *ninternals = pind->nhiddens;
  *nmotors = pind->noutputs;
}

/*
 * set sensor activations (i.e. it updates pind->input[])
 * #UpdateSensorsVariableDeclarations	
 */
void
update_sensors(struct individual *pind, struct  iparameters *pipar, int *num_leaders, int *num_followers)
{
	//    Quick Links
	//    code:#FoodPerceptionSystem
	//    code:#WaterPerceptionSystem
    struct  individual  *cind;		// current individual
    float   mcom_out[4];			// medie degli output di comunicazion
	float  p1;
	float  p2;
	double direction;
	int    i,ii,j;
	int    n;
	int    c;
	int    s;
	int    nsensor;
	float  arb_max;
	int    arb_ind;
	int    gtnoise;
	int     mt; //per evitare di ritardare se presenti i motori in input
	int     mte;// per evitare di ritardare se presente i compass
	double kcomp; //costante compass per netterli piano piano

	float tot_ind_around = 0;			// Totale degli individui trovati nell'intorno dell'individuo corrente
	float perc_leaders_around = 0;		// Percentuale normalizzata (0-1) dei leaders trovati nell'intorno dell'individuo corrente		
	float perc_followers_around = 0;	// Percentuale normalizzata (0-1) dei followers trovati nell'intorno dell'individuo corrente

	struct envobject *obj;
	
	double ang;							// angolo relativo alla posizione di un individuo o food zone in un intorno
	double dist,mindist;				// distanze per il calcolo della food zone di minima distanza
	float min_posx,min_posy;			// coordinate della food zone di distanza minima
	int	  cod[2];						// codifica della direzione 
	FILE  *fout;

	mt=0;
	mte=0;
	kcomp=0;
	
	
	gtnoise=0; //0: off 1:on

	p1        = pind->pos[0];
	p2        = pind->pos[1];
	direction = pind->direction;

	nsensor = 0;
	
	if(ipar->gatenoise<1)
	{
		if((rans(0.5)+0.5)< ipar->gatenoise) gtnoise=0;
		else
			gtnoise=1;
	}

	// efferent copies of motor neurons
	// acquisisce lo stato dei motori proprio
	if (pipar->motmemory > 0)
	{
		for (i=0; i < ipar->wheelmotors; i++, nsensor++)
			pind->input[nsensor] = pind->wheel_motor[i]; 
	}

	// bump sensors
	// attivazione dei neuroni di urto
	if (pipar->bumpsensors > 0)
	{
		for (i=0; i < 2; i++, nsensor++)
			pind->input[nsensor]= pind->bumpsensors[i];
	
		// Al momento sembra non servire
		// Azzera in quei casi in cui i valori dei bump sensors superano la soglia di 1
		/*if ((pind->input[nsensor-1]>1) || (pind->input[nsensor-2]>1)) {
			pind->input[nsensor-1]=0;
			pind->input[nsensor-2]=0;
		}*/
	}	

	// infrared sensors
	// attivazione dei sensori infrarossi
	if (pipar->nifsensors > 0)
	{
		if (real==0)
			if (ipar->ifsensorstype == 0)
				update_infrared_s(pind);
			else
				update_ginfrared_s(pind, pipar);
		
		switch(pipar->nifsensors)
		{
		case 3:
			for(ii=0;ii<3;ii++,nsensor++)		
				if(gtnoise==0)
					pind->input[nsensor] = pind->cifsensors[ii];
				else
					pind->input[nsensor]=0.0;
		    break;
	    case 4:
			pind->input[nsensor]   = (pind->cifsensors[0] + pind->cifsensors[1]) / 2.0f;
			pind->input[nsensor+1] = (pind->cifsensors[2] + pind->cifsensors[3]) / 2.0f;
			pind->input[nsensor+2] = (pind->cifsensors[4] + pind->cifsensors[5]) / 2.0f;
			pind->input[nsensor+3] = (pind->cifsensors[6] + pind->cifsensors[7]) / 2.0f;
			nsensor += 4;
			break;
	    case 6:
			for(ii=0;ii<6;ii++,nsensor++)
				pind->input[nsensor] = pind->cifsensors[ii];
			break;
	    case 8:
			for(ii=0;ii<8;ii++,nsensor++)
				pind->input[nsensor] = pind->cifsensors[ii];
			break;
        }
    }
	
	// light sensors
	// attivazione dei sensori di luce
	if (pipar->lightsensors > 0)
	{
		if (real==0)
			update_lights_s(pind);
		
		switch(pipar->lightsensors)
		{
	    case 2:
			pind->input[nsensor] = pind->clightsensors[2];
			nsensor++;
			pind->input[nsensor] = pind->clightsensors[6];
			nsensor++;
			break;
	    case 3:
			pind->input[nsensor] = (pind->clightsensors[0] + pind->clightsensors[1]) / 2.0f;
			nsensor++;
			pind->input[nsensor] = (pind->clightsensors[2] + pind->clightsensors[3]) / 2.0f;
			nsensor++;
			pind->input[nsensor] = (pind->clightsensors[4] + pind->clightsensors[5]) / 2.0f;
			nsensor++;
			break;
	    case 8:
			for(ii=0;ii<8;ii++,nsensor++)
				pind->input[nsensor] = pind->clightsensors[ii];
			break;
        }
    }

    // signals from nearby robots
	// segnali di comunicazione ? 
	if (pipar->signalss > 0)
	{
		update_signal_s(pind, pipar);
		for(i=0;i < pipar->signalss*pipar->signalso;i++,nsensor++)
			pind->input[nsensor] = pind->csignals[i];
	}

    // sensors encoding previously produced signals
	if (pipar->signalso > 0)
	{
		for(s=0;s < pipar->signalso; s++,nsensor++)
			pind->input[nsensor] = pind->signal_motor[s];
	}

    // linear camera
    if (pipar->camerareceptors > 0)
    {
		if (real==0)
			update_camera(pind, pipar);
	  
		for(c=0; c < pipar->cameracolors; c++)
			for(i=0; i < pipar->camerareceptors; i++, nsensor++)
				pind->input[nsensor] = pind->camera[c][i];
	}

	// simple vision
	if (pipar->simplevision > 0)
	{
		if (real==0)
			update_simplecamera(pind, pipar);
		if (pipar->simplevision == 1)
			for(i=0; i < 5; i++, nsensor++)
				pind->input[nsensor] = pind->scamera[i];
		if (pipar->simplevision == 2)
			for(i=0; i < 3; i++, nsensor++)
				pind->input[nsensor] = pind->scamera[i];
	}

	// nxtcam sensors three for each tracked color
	if(pipar->nxtcam>0)
	{
		update_nxtcam(pind, pipar);	// cam sensibile solo al primo colore (rosso)

		//to do: mettere dentro individual un vettore per il tracking di 8 blob
		//       aggiornare i tre senori, due per l'angolo uno per l'ampiezza
			
		// primo neurone associato alla posizione del blob e d� il massimo quando si trova
		// sull'ascissa 58 (0-176)
		pind->input[nsensor]=1-abs(58.0-pind->trackedBlob[0][0])/60.0;
		if ((pind->input[nsensor]<0.05) || (pind->trackedBlob[0][2]!=1)) pind->input[nsensor]=0;
		nsensor++;
			
		// secondo neurone associato alla posizione del blob e d� il massimo quando si trova
		// sull'ascissa 118 (0-176)
		pind->input[nsensor]=1-abs(118.0-pind->trackedBlob[0][0])/60.0;
		if ((pind->input[nsensor]<0.05) || (pind->trackedBlob[0][2]!=1)) pind->input[nsensor]=0;
		nsensor++;

		// neurone che acquisisce quanto � grosso il blob
		pind->input[nsensor]=pind->trackedBlob[0][1]/176.0;
		if ((pind->input[nsensor]<0) || (pind->trackedBlob[0][2]!=1)) pind->input[nsensor]=0;
		nsensor++;
			
		// Se ci sono due cam attiva anche l'altra
		if (pipar->nxtcam>1) {
			 //to do: mettere dentro individual un vettore per il tracking di 8 blob
			 //       aggiornare i tre senori, due per l'angolo uno per l'ampiezza
				
			 // primo neurone associato alla posizione del blob e d� il massimo quando si trova
			 // sull'ascissa 58 (0-176)
			 pind->input[nsensor]=1-abs(58.0-pind->trackedBlob[1][0])/60.0;
			 if ((pind->input[nsensor]<0.05) || (pind->trackedBlob[1][2]!=2)) pind->input[nsensor]=0;
			 nsensor++;
			
			 // secondo neurone associato alla posizione del blob e d� il massimo quando si trova
			 // sull'ascissa 118 (0-176)
		     pind->input[nsensor]=1-abs(118.0-pind->trackedBlob[1][0])/60.0;
			 if ((pind->input[nsensor]<0.05) || (pind->trackedBlob[1][2]!=2)) pind->input[nsensor]=0;
			 nsensor++;

        	 // neurone che acquisisce quanto � grosso il blob
			 pind->input[nsensor]=pind->trackedBlob[1][1]/176.0;
			 if ((pind->input[nsensor]<0) || (pind->trackedBlob[1][2]!=2)) pind->input[nsensor]=0;
			 nsensor++;
		}
	}

	// Orientation Camera
	if (pipar->orientationcam>0) {
		// Gli indvidui che possono percepire da lontanto (leaders), nel caso fosse espresso non possono vedere la posizione ed il colore degli altri
		if ((!pipar->onlyfewcanperceive)||(!((ipar->onlyfewcanperceive) && (check_perceptualinds(pind->number)) && (pipar->percepindcannotseeother)))) {
			// Determina la posizione dell'individuo pi� vicino 
			mindist=envdx+envdy;
			min_posx=0.0;
			min_posy=0.0;
			for(n=0, cind=ind; n < nteam; n++, cind++) {	// seleziona tutti gli altri individui 
				if (cind!=pind) {		// verifica che sia differente da pind (il corrente)
					// Determina la distanza tra l'individuo corrente e quello trovato nell'intorno
					dist=mdist(pind->pos[0],pind->pos[1],cind->pos[0],cind->pos[1]);
					if ((dist<=pipar->orientationcammaxdist) && (dist<=mindist)) {	// il raggio di percezione � definito dalla configurazione
						mindist=dist;			// la nuova distanza minima � dist
						min_posx=cind->pos[0];	// ascissa dell'individuo a distanza minima
						min_posy=cind->pos[1];	// ordinata dell'individuo a distanza minima
					}
				}
			}
	
			// se non sono entrambi uguali a zero vuol dire che � stata trovato un individuo
			if (!((min_posx==0) && (min_posy==0))) {	

				// Traslazione delle coordinate rispetto al centro del robot
				min_posx=min_posx-pind->pos[0];
				min_posy=min_posy-pind->pos[1];

				// Determina l'arcotangente 2 della posizione dell'individuo pi� vicino, in modo da determinare il quadrante dove � posizionato
				ang=RADtoDGR(atan2(min_posy,min_posx));	

				// Normalizzazione dell'angolo rispetto all'angolo di movimento del robot
				if (ang>=pind->direction) 
					ang=ang-pind->direction;
				else 
					ang=(360+ang)-pind->direction;
				
				// Determina il quadrante dove si trova 				
				if (((ang>=0) && (ang<=90)) || (ang==360)) {	// la food zone � nella destra
					cod[0]=0;
					cod[1]=1;
				} else if ((ang>90) && (ang<=270)) {		    // la food zone � dietro
					cod[0]=1;
					cod[1]=1;
				} else if ((ang>270) && (ang<360)) {			// la food zone � nella sinistra
					cod[0]=1;
					cod[1]=0;
				}	
		
				for(i=0; i < 2; i++, nsensor++) 
					pind->input[nsensor] = cod[i];
			} else {
				// Altrimenti carica 0.0 in entrambi gli input
				for(i=0; i < 2; i++, nsensor++) 
					pind->input[nsensor] = 0.0;
			}		
		} else {
			// Altrimenti carica 0.0 in entrambi gli input
			for(i=0; i < 2; i++, nsensor++) 
				pind->input[nsensor] = 0.0;
		}		
	}
	
	// RGB Color Cam sensors
/*	if ((pipar->rgbcolorretina>0) && (((startx[pind->number]!=0) && (starty[pind->number]!=0)) && 
		(!((pind->pos[0]>startx[pind->number]-2*robotradius) && (pind->pos[0]<startx[pind->number]+2*robotradius) && (pind->pos[1]>starty[pind->number]-2*robotradius) && (pind->pos[1]<starty[pind->number]+2*robotradius)))))
*/
//	if ((pipar->rgbcolorretina>0) && ((pind->lifecycle>60) && (startx[pind->number]!=0) && (starty[pind->number]!=0)))
	if (pipar->rgbcolorretina>0)
	{

		update_RGBColorRetina(pind, pipar);			// retina sensibile a tutti i colori possibili dell'individuo pi� vicino
		
		if (!pipar->rgbcolorretinagrayscale) { 

			// neurone associato al rosso normalizzato
			for (i=0; i<pipar->rgbcolorretina;i++) {
				
				if ((ipar->lab_test_subst_number_seed > 0) && (ipar->lab_test_subst_color > 0) && (pind->can_see_fake_colors > 0) && 
					(pind->RColorPerceived[i]==RColor_fake) && (pind->GColorPerceived[i]==GColor_fake) && (pind->BColorPerceived[i]==BColor_fake)) {
					
					pind->RColorPerceived[i]=RColor_new;		// Change the perceived color
					pind->GColorPerceived[i]=GColor_new;
					pind->BColorPerceived[i]=BColor_new;
				}
								
				pind->input[nsensor]=pind->RColorPerceived[i] / 256.0;
				nsensor++;
				
				// neurone associato al verde normalizzato
				pind->input[nsensor]=pind->GColorPerceived[i] / 256.0;
				nsensor++;

				// neurone associato al blue normalizzato
				pind->input[nsensor]=pind->BColorPerceived[i] / 256.0;
				nsensor++;
			}

		} else {
			// neurone associato alla scala di grigio acquisito normalizzato
			for (i=0; i<pipar->rgbcolorretina;i++) {
				pind->input[nsensor]=pind->GrayScaleColorPerceived[i] / 256.0;
				nsensor++;
			}
		}
	}
	
	// loading leaders - follower perceptual system
	if(pipar->leadersfollowerspercepsystem > 0)
	{
		// Percettore del leader pi� vicino
		// Caricamento della codifica del leader pi� vicino (10 : sinistra, 01 : destra, 11 : dietro)
		if (pipar->leadersfollowerspercepsystem >= 2) {
			
			if (pind->categorization[0]<=0.5) {	// solo i followers possono vedere i leaders
				// Determina il leader con distanza minima 
				mindist=envdx+envdy;
				min_posx=0.0;
				min_posy=0.0;
				for(n=0, cind=ind; n < nteam; n++, cind++) {	// seleziona tutti gli altri individui 
					if ((cind!=pind) && (cind->categorization[0]>0.5)) {		// verifica che sia leader e differente da pind (il corrente)
						// Determina la distanza tra l'individuo corrente e uno dei leader trovati nell'intorno
						dist=mdist(pind->pos[0],pind->pos[1],cind->pos[0],cind->pos[1]);
						if ((dist<=pipar->lfpercepsystemdist) && (dist<=mindist)) {	// il raggio di percezione � definito dalla configurazione
							mindist=dist;			// la nuova distanza minima � dist
							min_posx=cind->pos[0];	// ascissa del leader a distanza minima
							min_posy=cind->pos[1];	// ordinata del leader a distanza minima
						}
					}
				}
			}
			// se non sono entrambi uguali a zero vuol dire che � stata trovato un leader
			// e solo i followers possono vedere i leader altrimenti per i follower l'input � 
			// 0-0
			if ((!((min_posx==0) && (min_posy==0))) && (pind->categorization[0]<=0.5)) { 

				// Traslazione delle coordinate della food zone rispetto al centro del robot
				min_posx=min_posx-pind->pos[0];
				min_posy=min_posy-pind->pos[1];

				// Determina l'arcotangente 2 della posizione della food zone pi� vicina, in modo da determinare il quadrante dove � posizionata
				ang=RADtoDGR(atan2(min_posy,min_posx));	

				// Normalizzazione dell'angolo rispetto all'angolo di movimento del robot
				if (ang>=pind->direction) 
					ang=ang-pind->direction;
				else 
					ang=(360+ang)-pind->direction;
			
				// Determina il quadrante dove si trova 				
				if (((ang>=0) && (ang<=90)) || (ang==360)) {	// la food zone � nella destra
					cod[0]=0;
					cod[1]=1;
				} else if ((ang>90) && (ang<=270)) {		    // la food zone � dietro
					cod[0]=1;
					cod[1]=1;
				} else if ((ang>270) && (ang<360)) {			// la food zone � nella sinistra
					cod[0]=1;
					cod[1]=0;
				}	
		
				for(i=0; i < 2; i++, nsensor++) 
					pind->input[nsensor] = cod[i];
			} else {
				// Altrimenti carica 0.0 in entrambi gli input
				for(i=0; i < 2; i++, nsensor++) 
					pind->input[nsensor] = 0.0;
			}		
		}
	
		// Percettore della food zone pi� vicina
		// Caricamento della codifica della food zone pi� vicina (10 : sinistra, 01 : destra, 11 : dietro)
		if (pipar->leadersfollowerspercepsystem >= 1) {	
		
			if ((!pipar->onlyfewcanperceive)||((pipar->onlyfewcanperceive) && (check_perceptualinds(pind->number)) && 
				(((pipar->perceptualrace>0) && (pind->race==pipar->perceptualrace-1))||(pipar->perceptualrace == 0)))) {		// solo un certo numero di individui pu� vedere la food zone
				if ((pind->categorization[0]>0.5) || (pipar->leadersfollowerspercepsystem == 1)) {							// solo i leaders possono vedere le food zone, oppure individui soli
					// Determina la food zone con distanza minima 
					mindist=envdx+envdy;
					min_posx=0.0;
					min_posy=0.0;
					for(j=0, obj = *(envobjs + GROUND);j<envnobjs[GROUND];j++, obj++) {
						dist=mdist(pind->pos[0],pind->pos[1],obj->x,obj->y);
						// controlla che la food zone sia ad una distanza utile dall'individuo
						if ((dist<=pipar->lfpercepsystemdist) && (dist<=mindist)) {		// il raggio di percezione � definito dalla configurazione
							mindist=dist;		// la nuova distanza minima � dist
							min_posx=obj->x;	// ascissa della food zone a distanza minima
							min_posy=obj->y;	// ordinata della food zone a distanza minima
						}
					}
				}	
				// se non sono entrambi uguali a zero vuol dire che � stata trovata una food zone
				// e solo i leader possono vedere le food zone altrimenti per i leader l'input � 
				// 0-0
				if ((!((min_posx==0) && (min_posy==0))) && ((pind->categorization[0]>0.5) || (pipar->leadersfollowerspercepsystem == 1))) { 
					// Traslazione delle coordinate della food zone rispetto al centro del robot
					min_posx=min_posx-pind->pos[0];
					min_posy=min_posy-pind->pos[1];

					// Determina l'arcotangente 2 della posizione della food zone pi� vicina, in modo da determinare il quadrante dove � posizionata
					ang=RADtoDGR(atan2(min_posy,min_posx));	

					// Normalizzazione dell'angolo rispetto all'angolo di movimento del robot
					if (ang>=pind->direction) 
						ang=ang-pind->direction;
					else 
						ang=(360+ang)-pind->direction;
			
					// Determina il quadrante dove si trova 				
					if (((ang>=0) && (ang<=90)) || (ang==360)) {	// la food zone � nella destra
						cod[0]=0;
						cod[1]=1;
					} else if ((ang>90) && (ang<=270)) {		    // la food zone � dietro
						cod[0]=1;
						cod[1]=1;
					} else if ((ang>270) && (ang<360)) {			// la food zone � nella sinistra
						cod[0]=1;
						cod[1]=0;
					}		

					// Carica la codifica trovata in input alla rete
					for(i=0; i < 2; i++, nsensor++) 
						pind->input[nsensor] = cod[i];
					} else {
					// Altrimenti carica 0.0 in entrambi gli input
					for(i=0; i < 2; i++, nsensor++) 
						pind->input[nsensor] = 0.0;
				}		
			} else {
				// Altrimenti carica 0.0 in entrambi gli input
				for(i=0; i < 2; i++, nsensor++) 
					pind->input[nsensor] = 0.0;
			}		
		}

		*num_leaders=0;		// azzera il numero di leaders nell'intorno
		*num_followers=0;	// azzera il numero di followers nell'intorno
		// Individuazione e caricamento della percentuale di leader e followers all'interno di un intorno
		for(n=0, cind=ind; n < nteam; n++, cind++) {	// seleziona tutti gli altri individui 
														// tranne il corrente
			if ((cind!=pind) && (mdist(pind->pos[0],pind->pos[1],cind->pos[0],cind->pos[1])<pipar->lfpercepsystemdist)) 
			{
				tot_ind_around++;				// Incrementa il numero di individui trovati nell'intorno dell'individuo corrente

				if ((cind->categorization[0]>0.5) && (pipar->leadersfollowerspercepsystem >= 4))
					perc_leaders_around++;		// Incrementa il numero di leaders trovati nell'intorno dell'individuo corrente		

				if ((cind->categorization[0]<=0.5) && (pipar->leadersfollowerspercepsystem >= 3))	
					perc_followers_around++;	// Incrementa il numero di followers trovati nell'intorno dell'individuo corrente		
			}
		
			if ((cind->categorization[0]>0.5) && (pipar->leadersfollowerspercepsystem >= 4))
				(*num_leaders)++;		// Incrementa il numero di leaders totali		

			if ((cind->categorization[0]<=0.5) && (pipar->leadersfollowerspercepsystem >= 3))	
				(*num_followers)++;	// Incrementa il numero di followers totali		
		}

		if (tot_ind_around>0) {		// per non rischiare divisioni per 0 quando non ci sono individui nell'intorno
			// Determina la percentuale normalizzata (0-1) di leaders trovati nell'intorno dell'individuo corrente		
			if (pipar->leadersfollowerspercepsystem >= 4) 
				perc_leaders_around=perc_leaders_around/tot_ind_around;
			// Determina la percentuale normalizzata (0-1) di followers trovati nell'intorno dell'individuo corrente		
			if (pipar->leadersfollowerspercepsystem >= 3) 
				perc_followers_around=perc_followers_around/tot_ind_around;		
		} else {
			perc_leaders_around=0.0;
			perc_followers_around=0.0;		
		}

		// Effettua il salvataggio solo per un individuo, l'individuo 0
		if ((pipar->save_leader_followers_percent == 1) && (pipar->leadersfollowerspercepsystem >=4) && 
			(pind->number == 0) && ((testindividual_is_running) || (testpopulation_is_running))) {

			// Misura statistica dei leaders e followers
			fout=fopen("LeadersFollowers.txt","a");							// file di analisi statistica

			fprintf (fout, "%d : %d %d %d %d\n",pind->lifecycle, *num_leaders,*num_followers,(int) (100*(*num_leaders)/(tot_ind_around+1)),(int) (100*(*num_followers)/(tot_ind_around+1))); // +1 perch� l'individuo corrente viene sempre escluso dal computo	

			fclose (fout);
		}		

		// Carica le percentuali in input alla rete
		if (pipar->leadersfollowerspercepsystem >= 3) {
			pind->input[nsensor]=perc_leaders_around;
			nsensor++;
		}
		if (pipar->leadersfollowerspercepsystem >= 4) {
			pind->input[nsensor]=perc_followers_around;		
			nsensor++;
		}
	}

	//	Check for food near the robot
	perceptionSystem(pind,pipar,nsensor,FOOD);
	//	Need to increase the target neuron index by two
	//	to move past the food neurons
	nsensor += 2;

	//	Check for water near the robot
	perceptionSystem(pind,pipar,nsensor,WATER);
	//	Need to increase the target neuron index by two
	//	to move past the food neurons
	nsensor += 2;

	//cloks if present
	if(pipar->clock1>0)
	{
		pind->input[nsensor]=pind->clk1;
		pind->clk1+=pipar->clock1;
		if(pind->clk1>1.0) pind->clk1=0;
		nsensor++;
	}

	if(pipar->clock2>0)
	{
		pind->input[nsensor]=pind->clk2;
		pind->clk2+=pipar->clock2;
		if(pind->clk2>1.0)pind->clk2=0;
		nsensor++;

	}

	if(pipar->clock3>0)
	{
		pind->input[nsensor]=pind->clk3;
		//pind->clk3+=pipar->clock3;
		//if(pind->clk3>1.0)pind->clk3=0;
		pind->clk3-=pipar->clock3;
		if(pind->clk3<0.0)pind->clk3=1.0;
		nsensor++;

	}
	if(pipar->brownoise>0)
	{
		for(i=0;i<pipar->brownoise;i++)
		{
			pind->input[nsensor]+=rans(0.5);
			if (pind->input[nsensor]>1.0) pind->input[nsensor]=1.0;
			if (pind->input[nsensor]<0.0) pind->input[nsensor]=0.0;
			nsensor++;
		}
	}

	if(pipar->whitenoise>0)
	{
		for(i=0;i<pipar->whitenoise;i++)
		{
			pind->input[nsensor]=rans(0.5)+0.5;
			nsensor++;
		}
	}

	if(pipar->motorload>0)
	{
		for(i=0;i<2;i++)
		{
			pind->input[nsensor]+=pind->wheel_motor[i]*0.2-0.15;
			if (pind->input[nsensor]>1.0)pind->input[nsensor]=1.0;
			if (pind->input[nsensor]<0.0)pind->input[nsensor]=0.0;
			nsensor++;
		}
	}

	// Attivazione degli input di comunicazione se presenti
	// se ci sono pi� individui ogni input di comunicazione riceve
	// la media dei rispettivi output di comunicazione degli altri 
	// robot. Funziona solo con 2 o pi� robot.
	if ((pipar->communications > 0) && (nteam>1)) {
		// Inizializza il vettore delle medie degli output
		for (i=0; i<pipar->communications; i++) 
			mcom_out[i]=0.0;
		
		for(n=0, cind=ind; n < nteam; n++, cind++)	// seleziona tutti gli altri individui tranne il corrente
		{
			// acquisisce gli output di tutti gli altri individui, facendo
			// la media se ci sono pi� di due robot nella simulazione. 
			// Inoltre l'acquisizione avviene solo se l'altro robot si 
			// trova ad una distanza al di sotto di una distanza massima di
			// comunicazione specificata nel file di configurazione. 
			if ((cind!=pind) && (mdist(pind->pos[0],pind->pos[1],cind->pos[0],cind->pos[1])<pipar->dist_com)) {
				for (i=0; i<pipar->communications; i++) 
					mcom_out[i]+=cind->com_out[i];
			}
		}
		// inserisce in input la media degli output di comunicazione degli 
		// altri individui
		for (i=0; i<pipar->communications; i++, nsensor++) 
			pind->input[nsensor]=mcom_out[i]/(nteam-1); // nteam-1 perch� considera gli altri individui per la media
	}

	// sensors of last visited ground area
	if (pipar->a_groundsensor2 > 0)
	{
		for (i=0;i < pipar->a_groundsensor2; i++, nsensor++)
			pind->input[nsensor] = pind->lastground[i];
        if (real==0)
			update_lastground(pind, pipar);
	}

	// ground sensors
	if (pipar->groundsensor > 0)
	{
		if (real==0)
			update_groundsensors(pind, pipar);
		for (i=0;i < pipar->groundsensor; i++, nsensor++)
            pind->input[nsensor] = pind->ground[i];
	}

	// food zone sensors
	if (pipar->foodSensor > 0)
	{
		if (real==0)
			update_foodSensors(pind, pipar);
		for (i=0;i < pipar->foodSensor; i++, nsensor++)
            pind->input[nsensor] = pind->food[i];
	}

	// water zone sensors
	if (pipar->waterSensor > 0)
	{
		if (real==0)
			update_waterSensors(pind, pipar);
		for (i=0;i < pipar->waterSensor; i++, nsensor++)
            pind->input[nsensor] = pind->water[i];
	}

	//	Hunger sensors
	if ((pipar->hungerSensor == 1) || (pipar->hungerSensor == 2) )
	{
		//	Hunger sensors type 1, 2 only use the first value of the array
		if (real==0)
			update_hungerSensors(pind, pipar);
            pind->input[nsensor++] = pind->hunger[0];
	}
	else if ((pipar->hungerSensor == 3)	|| (pipar->hungerSensor == 4) || (pipar->hungerSensor == 5) || (pipar->hungerSensor == 6))
	{
		//	Hunger sensors type 3, 4, 5 & 6 uses three values from the hunger array
		if (real==0)
			update_hungerSensors(pind, pipar);
		for (i=0;i < 3; i++, nsensor++)
            pind->input[nsensor] = pind->hunger[i];
	}

	// thirst sensors
	if ((pipar->thirstSensor == 1) || (pipar->thirstSensor == 2))
	{
		//	Thirst sensors type 1 and 2 only use the first value of the array
		if (real==0)
			update_thirstSensors(pind, pipar);
            pind->input[nsensor++] = pind->thirst[0];
	}
	else if ((pipar->thirstSensor == 3) || (pipar->thirstSensor == 4) || (pipar->thirstSensor == 5) || (pipar->thirstSensor == 6))
	{
		//	Thirst sensors type 3, 4, 5 & 6 uses three values from the thirst array
		if (real==0)
			update_thirstSensors(pind, pipar);
		for (i=0;i < 3; i++, nsensor++)
            pind->input[nsensor] = pind->thirst[i];
	}

	// day / night sensors
	if (pipar->dayNightSensor > 0)
	{
		if (real==0)
			update_dayNightSensors(pind, pipar);
		for (i=0;i < pipar->dayNightSensor; i++, nsensor++)
			pind->input[nsensor] = pind->dayNight[i];
	}

	// Age sensors
	if (pipar->ageSensor == 1)
	{
		//	Type 1 (3 bit encoding)
		if (real==0)
			update_ageSensors(pind, pipar);
		for (i=0;i < 3; i++, nsensor++)
			pind->input[nsensor] = pind->age[i];
	}

	// additional ground sensors (delta)
	if (pipar->a_groundsensor > 0)
	{
	    if (real==0)
		    update_deltaground(pind, pipar);
		for (i=0;i < pipar->a_groundsensor; i++, nsensor++)
			pind->input[nsensor] = pind->ground[i];
	}

	// the state of additional output units at time t-1
	if (pipar->input_outputs > 0)
	{
		for (i=0;i < pipar->input_outputs; i++,nsensor++)
			pind->input[nsensor] = pind->pseudo_motor[i];
	}

	// proprioceptors of eaten nutrients
	if (pipar->nutrients > 0)
	{
         pind->input[nsensor] = pind->eatenfood[0] / 100.0f;
         pind->input[nsensor+1] = pind->eatenfood[1] / 100.0f;
         nsensor += 2;
	}

	// energy sensor
	if (pipar->energy > 0)
	{
		if (read_ground(pind) > 0)
			pind->energylevel = 1.0f;
		else
			pind->energylevel = pind->energylevel * 0.99f;
		pind->input[nsensor] = pind->energylevel;
		nsensor++;
	}
		
	// a compass sensor
	if (pipar->compass > 0)
	{
		//if(cgen<100) kcomp=0.0;
		//if(cgen>100 && cepoch <=200) kcomp=(float)(cgen%100)/100.0;
		//if(cgen>200) kcomp=1.0;
		kcomp=1.0;

        pind->input[nsensor] = pind->pos[0]/2000.0;//kcomp*((float) (sin(DGRtoRAD(pind->direction)) + 1.0) / 2.0);
		nsensor++;
        pind->input[nsensor] =pind->pos[1]/2000.0; //kcomp*((float) (cos(DGRtoRAD(pind->direction)) + 1.0) / 2.0);
		nsensor++;
	}
	
	//here we introduce delay if present
	if(ipar->sensorsDelay>0)
	{
		//resetting delay line at the first cycle
		if (run==0)
			for(i=0;i<MAXDELAY;i++)
				for(ii=0;ii<MAXDELAYEDSENSORS;ii++)
					delayLine[i][ii]=0.0;

		if(pipar->motmemory>0)
			mt=2;
		
		if(pipar->compass>0) mte=2;
		
		for (ii=mt;ii<nsensor-mte;ii++)
		{
			dswap[ii]=pind->input[ii];
			pind->input[ii]=delayLine[delayIndex][ii];
			delayLine[delayIndex][ii]=dswap[ii];
		}

		delayIndex++;
		if (delayIndex>=ipar->sensorsDelay)
			delayIndex-=ipar->sensorsDelay;
	}//fine linea di delay
}

void perceptionSystem(struct individual *pind, struct  iparameters *pipar, int nsensor, const int GOAL_TYPE)
{
	double ang;							//	angle in degree's to food zone
	double dist,mindist;				//  used to store distances to nearest zone
	float min_posx,min_posy;			//  coordinates of the nearest zone
	int	  cod[2];						//  output levels

	FILE  *fout;

	//    #FoodPerceptionSystem
	//    Perceptor of closest food zones
	//    (10 : left, 01 : right, 11 : back)
	if (((GOAL_TYPE == FOOD) && (pipar->foodperceptionsystem >= 1)) || ((GOAL_TYPE == WATER) && (pipar->waterperceptionsystem >= 1)))
	{		
		//    code:#UpdateSensorsVariableDeclarations
		int iterator;
		struct envobject *goalObject;
		int perceptionDistance;

		// Determines the food area at minimum distance
		mindist=envdx+envdy;
		min_posx=0.0;
		min_posy=0.0;

		if (GOAL_TYPE == WATER)
		{
			if ((pind->race == 0) && (pipar->waterpercepdist0 > 0))
			{
				//	Agent is from race0 (Green) and waterpercepdist is non-zero
				//	so use the specified waterpercepdist
				perceptionDistance = pipar->waterpercepdist0;
			}
			else if ((pind->race == 1) && (pipar->waterpercepdist1 > 0))
			{
				//	Agent is from race1 (DarkBlue) and waterpercepdist is non-zero
				//	so use the specified waterpercepdist
				perceptionDistance = pipar->waterpercepdist1;
			}
			else if ((pind->race == 2) && (pipar->waterpercepdist2 > 0))
			{
				//	Agent is from race2 (Cyan) and waterpercepdist is non-zero
				//	so use the specified waterpercepdist
				perceptionDistance = pipar->waterpercepdist2;
			}
			else if ((pind->race == 3) && (pipar->waterpercepdist3 > 0))
			{
				//	Agent is from race3 (Yellow) and waterpercepdist is non-zero
				//	so use the specified waterpercepdist
				perceptionDistance = pipar->waterpercepdist3;
			}
			else
			{
				//	Use the group waterpercepdist
				perceptionDistance = pipar->waterpercepdist;
			}
		}
		else if (GOAL_TYPE == FOOD)
		{
			if ((pind->race == 0) && (pipar->foodpercepdist0 > 0))
			{
				//	Agent is from race0 (Green) and foodpercepdist is non-zero
				//	so use the specified foodpercepdist
				perceptionDistance = pipar->foodpercepdist0;
			}
			else if ((pind->race == 1) && (pipar->foodpercepdist1 > 0))
			{
				//	Agent is from race1 (DarkBlue) and foodpercepdist is non-zero
				//	so use the specified foodpercepdist
				perceptionDistance = pipar->foodpercepdist1;
			}
			else if ((pind->race == 2) && (pipar->foodpercepdist2 > 0))
			{
				//	Agent is from race2 (Cyan) and foodpercepdist is non-zero
				//	so use the specified foodpercepdist
				perceptionDistance = pipar->foodpercepdist2;
			}
			else if ((pind->race == 3) && (pipar->foodpercepdist3 > 0))
			{
				//	Agent is from race3 (Yellow) and foodpercepdist is non-zero
				//	so use the specified foodpercepdist
				perceptionDistance = pipar->foodpercepdist3;
			}
			else
			{
				//	Use the group foodpercepdist
				perceptionDistance = pipar->foodpercepdist;
			}
		}
		else
		{
			printf("Unknown goal type\n");
		}

		//    iterator is used to step through each of the target objects in the environment
		//    goalObject is a pointer to the actual target
		for(iterator=0, goalObject = *(envobjs + GOAL_TYPE);
			iterator<envnobjs[GOAL_TYPE];
			iterator++, goalObject++) 
		{
			dist=mdist(pind->pos[0],pind->pos[1],goalObject->x,goalObject->y);
			
			//	check if the object is within the perception distance 
			//	from the individual and if this is closer to the object
			//	currently marked as nearest
			if ((dist<=perceptionDistance) && (dist<=mindist)) 
			{	
				mindist=dist;			// new distance
				min_posx=goalObject->x;	// position on x-axis
				min_posy=goalObject->y;	// position on y-axis
			}
		}

		if (!((min_posx == 0) || (min_posy == 0)))
		{
			// Moving of origin of coordinates to the centre of robot
			min_posx=min_posx-pind->pos[0];
			min_posy=min_posy-pind->pos[1];

			// Arctan of the closest food zone, determines the quadrant where it is related to
			ang=RADtoDGR(atan2(min_posy,min_posx));	

			// Normalization of the angle
			if (ang>=pind->direction) 
			{
				ang=ang-pind->direction;
			}
			else 
			{
				ang=(360+ang)-pind->direction;
			}

			// Quadrant where it is placed in degrees  				
			if (((ang>=0) && (ang<=90)) || (ang==360)) 
			{
				// food is on the right
				cod[0]=0;
				cod[1]=1;
			} 
			else if ((ang>90) && (ang<=270)) 
			{
				//	object is back
				cod[0]=1;
				cod[1]=1;
			} 
			else if ((ang>270) && (ang<360)) 
			{
				// food is on the left
				cod[0]=1;
				cod[1]=0;
			}		

			// Load the encoding into the input of the net
			for(int i=0; i < 2; i++, nsensor++) 
				pind->input[nsensor] = cod[i];
		} 
		else 
		{
			// Otherwise it loads 0.0 into the input of the net
			for(int i=0; i < 2; i++, nsensor++) 
				pind->input[nsensor] = 0.0;
		}		
	}
}

/*
 * update motor states 
 */
void
update_motors(struct individual *pind, struct  iparameters *pipar, float *act)

{

	int nmotor; 
	int i;
	float *a;

    if (act != NULL && pipar->wheelmotors > 0)
    {
		nmotor = pind->ninputs + pind->nhiddens;
		a = (act + nmotor);

		pind->wheel_motor[2] = 1.0;
		// acquisisce le attivazioni dei neuroni codificanti i segnali ai motori
		for (i=0; i < ipar->wheelmotors; i++, nmotor++, a++)
			pind->wheel_motor[i] = *a;
		// acquisisce le attivazioni dei neuroni codificanti i segnali verso altri 
		for (i=0; i < ipar->signalso; i++, nmotor++, a++)
			pind->signal_motor[i] = *a;
		// acquisisce le attivazioni dei neuroni codificanti i segnali ricorrenti di input-output
		for (i=0; i < ipar->input_outputs; i++, nmotor++, a++)
			pind->pseudo_motor[i] = *a;
		// acquisisce le attivazioni dei neuroni codificanti i segnali intenzionali
		for (i=0; i < ipar->pushingunits; i++, nmotor++, a++) {  
			if (ipar->binarycom)		// se � richiesto che sia binario allora lo inserisce direttamente 
				if (*a>=0.5) 
					*a = 1;				// imposta 1 in uscita dalla PU
				else 
					*a = 0;				// altrimenti imposta 0

			pind->pushing_units[i] = *a;  // carica l'output intenzionale o pushing unit
		}
		// acquisisce le attivazioni dei neuroni codificanti i segnali di categorizzazione
		for (i=0; i < ipar->categorization; i++, nmotor++, a++) {  
			if (ipar->binarycat)		// se � richiesto che sia binario allora lo inserisce direttamente 
				if (*a>=0.5) 
					*a = 1;				// imposta 1 in uscita dalla categorization unit
				else 
					*a = 0;				// altrimenti imposta 0
			
			// Nelle simulazioni leader-followers verifica che i leader siano o
			// meno in una food zone, in una food zone non possono diventare follower			
			if (pipar->leadersfollowerspercepsystem < 2) 
				pind->categorization[i] = *a;  // carica l'output di categorizzazione
			else 
				// cambia stato se � follower oppure se � leader e non � nella food zone 
				if ((pind->categorization[i]<=0.5) || ((pind->categorization[i]>0.5) && (!(read_ground(pind) > 0)) && (!(ipar->leader_cannot_become_follower == 1))))
					pind->categorization[i] = *a;  // carica l'output di categorizzazione
				else 
					*a = pind->categorization[i];  // altrimenti lascia l'uscita invariata
		}
		
		// acquisisce le attivazioni dei neuroni codificanti i segnali di comunicazione
		for (i=0; i < ipar->communications; i++, nmotor++, a++) {
			if (ipar->binarycom)		// se � richiesto che sia binario allora lo inserisce direttamente 
				if (*a>=0.5) 
					*a = 1;				// imposta 1 in uscita dalla comunicazione
				else 
					*a = 0;				// altrimenti imposta 0
			
			pind->com_out[i] = *a;		// carica l'output di comunicazione	
		}
	}
    else
	{
		// inizializza gli outputs
		pind->wheel_motor[2] = 1.0;
		for (i=0; i < ipar->wheelmotors; i++, nmotor++, a++)
			pind->wheel_motor[i] = 0.0;
		for (i=0; i < ipar->signalso; i++, nmotor++, a++)
			pind->signal_motor[i] = 0.0;
		for (i=0; i < ipar->input_outputs; i++, nmotor++, a++)
			pind->pseudo_motor[i] = 0.0;
		for (i=0; i < ipar->pushingunits; i++, nmotor++, a++)
			 pind->pushing_units[i] = 0.0;	
		for (i=0; i < ipar->categorization; i++, nmotor++, a++)
			 pind->categorization[i] = 0.0;	
		for (i=0; i < ipar->communications; i++, nmotor++, a++)
			 pind->com_out[i] = 0.0;	
    }
}

/*
 * set the initial position of the robots
 * default function (may be overwritten by experiment specific functions)
 */
void
initialize_robot_position()
{
	struct individual *pind;
	struct envobject *obj;
	int    volte;
	int    startset;
	int    team;
	int	   not_far_from_fz=0;
	int    ta;
	double start_dist;

	// Nel caso si volesse inserire l'opzione di scegliere la casualit� rispetto al tempo 
	//srand((unsigned)time(NULL));     // inizializza il seme dei numeri casuali in base al tempo
									   // in questo modo ad ogni riavvio la posizione � diversa		
									   // ma crea una simulazione non riproducibile, ATTENZIONE !!!
	
	if (start_in_a_little_area) {	   // sceglie casualmente la posizione dell'area circolare di partenza dei robot
		do {
			not_far_from_fz=0;

			starting_area_x=(float) fabs(rans((float) envdx - starting_area_radius));
			starting_area_y=(float) fabs(rans((float) envdy - starting_area_radius));
		
			// verifica che sia alla debita distanza da ogni food zone
			for (ta=0, obj = *(envobjs + 3); ta < envnobjs[3]; ta++, obj++) {
				start_dist=mdist(starting_area_x,starting_area_y, obj->x, obj->y);
				if (start_dist < starting_area_distance_from_fz)
					not_far_from_fz=1;
			}
		} while ((!((starting_area_x>starting_area_radius) && (starting_area_y>starting_area_radius))) || (not_far_from_fz==1));	// non pu� andare oltre i limiti dell'ambiente
	}

	for(team=0,pind=ind; team < nteam; team++, pind++) 
		initialize_one_robot_position_and_color(pind,team);			// Inizializza la posizione del singolo individuo
}


/*
 * set the initial position of the robots in input
 * default function (may be overwritten by experiment specific functions)
 */
void
initialize_one_robot_position_and_color(struct individual *pind, int team)
{
	struct envobject *obj;
	int    volte;
	int    startset;
	int	   found=0;
	float  xi,yi;
	int    n;
	int	   out_of_starting=0;
	int    ta;
	double start_dist;
	int    r;

	if (ipar->corvidsstart) {
		if (ipar->corvidsstart==1)	
			prefixpos=mrand(4);
		else if (ipar->corvidsstart==2)
			prefixpos=(++prefixpos)%4;		// sequenza prefissata crescente
		else if (ipar->corvidsstart>2)	
			prefixpos=(++prefixpos+2)%4;	// sequenza prefissata decrescente	
		
		if (team==0) {
			switch (prefixpos) {
				case 0: 
					startx[team]=400;
					starty[team]=400;
					break;
				case 1: 
					startx[team]=1400;
					starty[team]=400;
					break;
				case 2: 
					startx[team]=400;
					starty[team]=1400;
					break;
				case 3: 
					startx[team]=1400;
					starty[team]=1400;
					break;
				default: ;
			}
		} else {
			switch (prefixpos) {
				case 0: 
					startx[team]=400;
					starty[team]=3200;
					break;
				case 1: 
					startx[team]=1400;
					starty[team]=3200;
					break;
				case 2: 
					startx[team]=400;
					starty[team]=4200;
					break;
				case 3: 
					startx[team]=1400;
					starty[team]=4200;
					break;
				default: ;
			}
		}
	}

	if (pind->dead == 0)
	{
		startset = 0;

		// se non sono specificate delle posizioni fisse di partenza del robot, il robot
		// partir� da una posizione casuale secondo le modalit� specificate in origine
		// altrimenti partir� da una posizione fissa
		if (startx[team]==0 && starty[team]==0)  // verifica che gli startx e y siano entrambi 0
												// individuo 0 o 1 siano diverse da 0
		{
			/*
			* start out of foodzones
			*/
			if ((startfarfrom == 1 && startset == 0) && (envnobjs[RANDRECTHALLWAY] == 0))
			{
				//pind->pos[0]    = 50.0 + ( double ) mrand(envdx - 100);
				//pind->pos[1]    = 50.0 + ( double ) mrand(envdy - 100);
				//pind->direction = (double) mrand(360);
				/*volte = 0;
				while (volte < 1000 && ((check_crash(pind->pos[0],pind->pos[1],-1,&xi,&yi,&n,pind->died_individual,pind->ghost_individual) == 1) || (read_ground(pind) > 0)))
				{
					volte++;
					pind->pos[0]    = 50.0 + ( double ) mrand(envdx - 100);
					pind->pos[1]    = 50.0 + ( double ) mrand(envdy - 100);
					pind->direction = (double) mrand(360);
				}*/

				volte = 0;
				do {
					if ((start_in_a_little_area) || (starting_distance_from_fz>0)) out_of_starting=0;
					volte++;
					/*pind->pos[0]    = 50.0 + ( double ) mrand(envdx - 100);		// determina un bias pesante
					pind->pos[1]    = 50.0 + ( double ) mrand(envdy - 100);
					pind->direction = (double) mrand(360);
					*/
					/*pind->pos[0]    = randomize(robotradius, envdx-robotradius);
					pind->pos[1]    = randomize(robotradius, envdy-robotradius);
					pind->direction = randomize(0, 360);*/

					//for (r=0;r<50;r++) {		// test per vedere di rimescolare le carte
						pind->pos[0] = (float) fabs(rans((float) envdx-robotradius))+robotradius;
						pind->pos[1] = (float) fabs(rans((float) envdy-robotradius))+robotradius;
					//}
					pind->direction = fabs(rans(360.0));	// genera una direzione casuale tra 0 e 360�

					if (start_in_a_little_area) {
						if ((pind->pos[0]<starting_area_x-starting_area_radius) || (pind->pos[0]>starting_area_x+starting_area_radius) ||
						   (pind->pos[1]<starting_area_y-starting_area_radius) || (pind->pos[1]>starting_area_y+starting_area_radius)) 
						   out_of_starting=1;
					}

					if ((starting_distance_from_fz>0) && (!start_in_a_little_area)) {		// ogni individuo deve nascere molto lontano da ciascuna food zone
						// verifica che sia alla debita distanza da ogni food zone
						for (ta=0, obj = *(envobjs + 3); ta < envnobjs[3]; ta++, obj++) {
							start_dist=mdist(pind->pos[0],pind->pos[1], obj->x, obj->y);
							if (start_dist < starting_distance_from_fz) {
								out_of_starting=1;
								break;
							}
						}
					}
					

				} while ((volte < 1000 && ((check_crash(pind->pos[0],pind->pos[1],-1,&xi,&yi,&n,pind->died_individual,pind->ghost_individual) == 1) || (read_ground(pind) > 0)) || (out_of_starting==1)));

				found = 1;		// posizioni trovate
			}

			// random positions without collisions (default case)
			// verifica la partenza casuale dell'individuo evitando collisioni  
			if ((startset == 0) && (!found))	
			{
				if (envnobjs[RANDRECTHALLWAY]>0) {
					volte=0;
					obj = *(envobjs + RANDRECTHALLWAY); // acquisisce le info sull'area di partenza random
					do
					{
						volte++;
						// la posizione dell robot deve essere compresa all'interno del corridoio random
						while ((pind->pos[0] = (float) fabs(rans((float) obj->X)))<obj->x) {};
						while ((pind->pos[1] = (float) fabs(rans((float) obj->Y)))<obj->y) {};
						pind->direction = fabs(rans(360.0));	// genera una direzione casuale tra 0 e 360�
					}
					while (volte == 0 || (volte < 1000 && (check_crash(pind->pos[0],pind->pos[1],-1,&xi,&yi,&n,pind->died_individual,pind->ghost_individual) == 1) ));
				} else {	
					volte=0;
					do
					{
						volte++;
						pind->pos[0] = (float) fabs(rans((float) (envdx - 80))) + 40;
						pind->pos[1] = (float) fabs(rans((float) (envdy - 80))) + 40;
						pind->direction = fabs(rans(360.0));	// genera una direzione casuale tra 0 e 360�
					}
					while (volte == 0 || (volte < 1000 && (check_crash(pind->pos[0],pind->pos[1],-1,&xi,&yi,&n,pind->died_individual,pind->ghost_individual) == 1) ));
				}				
			}
		} else {
			if (startset == 0)
			{
				// fa partire il robot da una posizione fissa se almeno
				// almeno una tra ascissa e ordinata � diversa da 0
				pind->pos[0]=(float) startx[team];
				pind->pos[1]=(float) starty[team];
				pind->direction = fabs(rans(360.0));	// genera una direzione casuale tra 0 e 360�
			}
		}
	}
}


/*
 * re-initialize the characteristics of the environment
 */
void
initialize_world()


{

    int    i;
    int    ta,tt;
    int    tw;
    double d,mind;

    double w1, w2;
    int    volte;
    double min_dist_ob;
    struct envobject *obj;
    struct envobject *objb;

    // we randomize sround positions
    if (random_round == 1)
	{
		for (ta=0,obj = *(envobjs + SROUND); ta<envnobjs[SROUND]; ta++, obj++)
		{
			if (obj->subtype!=10) {		// se non � un cilindro posto sul robot, 
				mind = 0.0;
				volte = 0;
				min_dist_ob = 150;
				while (mind < min_dist_ob && volte < 1000)
				{	
					volte++;
					if ((volte > 199) && (volte % 10) == 0)
						min_dist_ob -= 1;
					obj->x = (float) (fabs(rans((float) envdx - (double) (120 * 2))) + 120);
					obj->y = (float) (fabs(rans((float) envdy - (double) (120 * 2))) + 120);
					for (tt=0, objb = *(envobjs + 1), mind = 9999.9; tt < ta; tt++, objb)
					{
						d = mdist(objb->x, objb->y, obj->x, obj->y);
						if (d < mind)
							mind = d;
					}
				}
			}
		}
	}

    // we randomize round positions
    if (random_round == 1)
	{
	  for (ta=0, obj = *(envobjs + ROUND); ta<envnobjs[ROUND]; ta++, obj++)
	  {
	    mind = 0.0;
	    volte = 0;
	    min_dist_ob = 150;
	    while (mind < min_dist_ob && volte < 1000)
		{
		  mind = 9999.9;
		  volte++;
		  if ((volte > 199) && (volte % 10) == 0)
		      min_dist_ob -= 1;
		  obj->x = (float) (fabs(rans((float) envdx - 275)) + 132.0);
		  obj->y = (float) (fabs(rans((float) envdy - 275)) + 132.0);
		  for (tt=0, objb = *(envobjs + 2); tt < ta; tt++, objb++)
			{
			  d = mdist(objb->x, objb->y, obj->x, obj->y);
			  if (d < mind)
			    mind = d;
			}
		  for (tt=0, objb = *(envobjs + 1); tt < envnobjs[1]; tt++, objb++)
			{
			  d = mdist(objb->x,objb->y,obj->x,obj->y);
			  if (d < mind)
			    mind = d;
			}
		}
	  }
	}

}
/* initialize some individual parameters */
void init_ind_parameters(struct  individual *pind, int team) 
{
	pind->fitness   = 0.0;
	pind->virtualfitness = 0.0;
	pind->oldfitness= 0.0;
	pind->totcycles = 0;
	pind->number = team;
	pind->lifecycle    = 0;
	pind->visited[0]   = 0;
	pind->eatenfood[0] = 0.0;
	pind->eatenfood[1] = 0.0;
	pind->energylevel = 1.0;
	pind->crashed = -1;
	pind->jcrashed = 0;
	pind->touchedbar = 0;
}

/* Restituisce la fitness dell'individuo di numero nind */
double getindfitness(int nind) {

	struct  individual *pind;

	pind=ind+nind;				// seleziona l'individuo nind-esimo

	return (pind->fitness);
}

/* Restituisce la razza dell'individuo di numero nind */
int getindrace(int nind) {

	struct  individual *pind;

	pind=ind+nind;				// seleziona l'individuo nind-esimo

	return (pind->race);
}

/*
 * it let the robots interact with the environment for one trial
 */
void
trial(int gen, int epoch, int n_ind) 
{
	struct  envobject *obj,*obj_tmp;				// oggetto nell'ambiente
	struct  individual *pind;
	struct  individual *find;
	struct  individual *otherpind;
	struct  individual *cind;
    struct  iparameters *pipar;
	int     i,j,n,k;
	int     team;
	int     oldzone, oldzoneb;
	int     prev_zone, prev_zoneb;
	int     count;
	extern  float sound;                                         
	extern  int reproduce_and_create_child(int, int);

	extern	int	explorationarray_eco[100][100][40];

	double  totmot;
	int     nturn1, nturn2;
	float   *act;
	float   ind_fitness;					// Fitness ottenuta da ciascu individuo
	char    sbuffer[256];
	int		r,c;
	int		nchildren;						// indice del nuovo figlio generato dalla riproduzione
	int     tmp_nteam=0;
	int		num_leaders=0, num_followers=0;
	double  dist,mindist;					// distanze per il calcolo della food zone di minima distanza
	float   xi,yi;							// punto d'intersezione del crash avvenuto	
	int		nind;
	int     crash=0;
	int     race;
	int		nindperrace;					// Numero di individui per razza
	int		nindividualsintospecies0, nindividualsintospecies1;
	int     explorx, explory;
	int		mem_explorx[40], mem_explory[40];	// Rendere dinamica questa parte
	int		x_size, y_size;
	int		nindnear;
	int		nindtest;
	int     ave_barycentre_x=0, ave_barycentre_y=0;
	int		nelem;
	int		ta;

	float   speedx, speedy;

	int		tareas_mov_after_n_cycle=200;

	float   old_x, old_y;

	FILE   *fout;
	char   fileout[128];	 
	double IncreaseFoodUnitsMinimumSize=200/pow(2,3.0);
	double IncreaseWaterUnitsMinimumSize=200/pow(2,3.0);


	// INITIALIZATION OF THE TRIAL
	run = 0;
	trace_run = 0;
	stop = 0;
	T1=T1_max;
	T2=T2_max;
	movedbar = 0;				// flag su sbarra spostata
	sincrobots = 0;				// flag di sincronizzazione
	memlifecycle1 = 0;
	memlifecycle2 = 0;

	noverticalbar = 0;			// flag su sbarra in posizione verticale

	individual_crash = 0;
	nturn1 = 0;
	nturn2 = 0;
	totmot = 0.0;

	oldzone = 0;
	oldzoneb = 0;
	prev_zone = 0;
	prev_zoneb = 0;
	count = 0;

	// Misura statistica n.8 (test di esplorativit� degli individui) 
	if ((test_exploratory) || (test_exploratory_all_gen)) {		// Inizializzazione eventuale dell'exploration array
		x_size=envdx/xcell;
		y_size=envdy/ycell;

		for (i=0;i<x_size;i++)
			for (j=0;j<y_size;j++)
				explorationarray[i][j] = 0;		// Inizializzazione dell'array delle esplorazioni
	}
	
	// Misura statistica n.11 (test di esplorativit� degli individui in ecologia) 
	if ((test_exploratory_eco) || (test_exploratory_all_gen_eco) || (test_mobility_all_gen_eco)) {			// Inizializzazione eventuale dell'exploration array
		x_size=envdx/xcell;
		y_size=envdy/ycell;

		for (i=0;i<x_size;i++) {
			for (j=0;j<y_size;j++)
				for (n=0;n<nindividuals;n++)
					explorationarray_eco[i][j][n] = 0;		// Inizializzazione dell'array delle esplorazioni
		}
	}

	// Misura statistica n.16 - 17 (test del baricentro) - inzializzazione dell'array
	// attenzione test da fare solo su un epoca altrimenti non ha senso !!
	if (((test_barycentre) || (test_barycentre_distances_all_gen) || (test_barycentre_distances)) /*&& (test_statistics)*/) {
		x_size=sweeps;												// Numero di elementi pari al numero di cicli
		y_size=nteam*2+2+nteam*2;										// Numero di colonne pari al numero di posizioni (*2 ascissa e ordinata) degli individui + posizione del baricentro
																		// + posizioni del baricentro del gruppi composti da individui : 1,2,3 - 0,2,3 - 0,1,3 - 0,1,2			
		for (i=0;i<x_size;i++)
			for (j=0;j<y_size;j++)
				barycentrearray[i][j] = 0;					// Inizializzazione dell'array dei baricentri
	}


	// fa resuscitare tutti gli individui morti, per l'inizio del nuovo trial
	// Questa parte deve essere qui per del riposizionamento degli individui alrimenti si crea un bug sulle
	// collisioni
	for(team=0,pind=ind;team<nteam;team++,pind++)
	{
		if (pind->died_individual) {
			pind->died_individual = 0;
			pind->RColor=pind->ROrigColor;		// Imposta il colore originario dell'individuo prima di morire	
			pind->GColor=pind->GOrigColor;		
			pind->BColor=pind->BOrigColor;		
			modify_individual_color(pind);		// assegna il nuovo colore originario all'individuo resuscitato
		}
	}	

	// Decide quale individuo visualizzare nel caso di alternamento
	if ((ipar->lab_test_alternate_team2) && (ipar->lab_ntestingteam==3)) {
		if (epoch%2==0) {		// se � epoca pari visualizza uno altrimenti l'altro
			(ind+1)->RColor=RColorPreyDeadInd;		// Indviduo mangiato ha un colore da morto	
			(ind+1)->GColor=GColorPreyDeadInd;
			(ind+1)->BColor=BColorPreyDeadInd;
			modify_individual_color(ind+1);				// assegna il nuovo colore all'individuo morto	
			(ind+1)->died_individual = 1;				// preda mangiata e morta
		} else {
			(ind+2)->RColor=RColorPreyDeadInd;		// Indviduo mangiato ha un colore da morto	
			(ind+2)->GColor=GColorPreyDeadInd;
			(ind+2)->BColor=BColorPreyDeadInd;
			modify_individual_color(ind+2);			// assegna il nuovo colore all'individuo morto	
			(ind+2)->died_individual = 1;				// preda mangiata e morta
		}
	}

	
	// In presenza di pi� specie
	// Verifica quali individui devono essere considerati morti, perch� oltre il numero massimo consentito per una specie
	if ((nraces>1) && ((testindividual_is_running == 1)||(n_fixed_evolution_social_is_running == 1)||(n_fixed_evolution_is_running == 1)||(testpopulation_is_running == 1))) {	// viene lanciato solo la prima volta
		nindperrace=nindividuals/nraces;			 // Calcola il numero di individui per razza
		if (nindinspecies0>0)						 // determina il numero di individui di cui deve essere composta la prima specie inizialmente
			nindividualsintospecies0=nindinspecies0;
		else
			nindividualsintospecies0=nteam/nraces;
	
		if (nindinspecies1>0)						 // determina il numero di individui di cui deve essere composta la seconda specie inizialmente
			nindividualsintospecies1=nindinspecies1;
		else
			nindividualsintospecies1=nteam/nraces;

		for (race=0; race < nraces; race++) 
			for (team=nindperrace*race, pind=ind+nindperrace*race, pipar = ipar; team < nindperrace*(race+1); team++, pind++) {
				if ((race==0) && ((team-nindperrace*race)>=nindividualsintospecies0)) {	// Imposta come morti gli individui che eccedono il numero massimo consentito della specie 0 
					pind->RColor=RColorPreyDeadInd;		// Indviduo mangiato ha un colore da morto	
					pind->GColor=GColorPreyDeadInd;
					pind->BColor=BColorPreyDeadInd;
					modify_individual_color(pind);		// assegna il nuovo colore all'individuo morto	
					pind->died_individual=1;	
					pind->fitness=0.0;					// la fitness di chi � morto, � sempre 0
				}
				if ((race==1) && ((team-nindperrace*race)>=nindividualsintospecies1)) {	// Imposta come morti gli individui che eccedono il numero massimo consentito della specie 1 
					pind->RColor=RColorPreyDeadInd;		// Indviduo mangiato ha un colore da morto	
					pind->GColor=GColorPreyDeadInd;
					pind->BColor=BColorPreyDeadInd;
					modify_individual_color(pind);		// assegna il nuovo colore all'individuo morto	
					pind->died_individual=1;
					pind->fitness=0.0;					// la fitness di chi � morto, � sempre 0
				}
			}
	}

	// we update the process events (i.e. the main window) every trial 
    update_events();

	if (manual_start == 0)
	{
		(*pinit_world)();		  // initialize world
		// Si esclude l'inizializzazione delle posizioni dei robot nella prima epoca dell'evolu-
		// zione n-variabile ed n_fissa con cloni per evitare all'inizio dell'epoca il cambiamento di posizione
		//if (((epoch == 0) && (n_fixed_evolution_social_is_running || testpopulation_is_running)) || (epoch > 0)) 			  
		(*pinit_robot_pos)(); // dalla schermata iniziale, dopo tanto ho capito che andava fatto sempre
							  // altrimenti non parte fuori dalla food zone, salta start_far_from
	}
	else
	{
		manual_start = 0;
	}

	// Test delle distanze di ogni individuo dalle rispettive food zone, alla partenza
	if ((test_distance_from_fz) && (envnobjs[GROUND] > 0)) {
		for(team=0,pind=ind;team<nteam;team++,pind++)
			for(i=0, obj = *(envobjs + GROUND);i<envnobjs[GROUND];i++, obj++)			// tutte le food zone
				distances_from_fz_array[team][i]+=(mdist(pind->pos[0],pind->pos[1],obj->x,obj->y)/amoebaepochs);
	}

	// Riposiziona la sbarra nella posizione originaria, cancellandola dalla 
	// posizione attuale
	if(envnobjs[BAR]>0) {
		for(i=0, obj = *(envobjs + BAR);i<envnobjs[BAR];i++, obj++)	{		// tutte le sbarre
			// controlla che non ci siano cilindri attaccati alla sbarra, in tal caso dovr� 
			// ripristinare la posizine anche di questi.
			for(j=0, obj_tmp = *(envobjs + SROUND);j<envnobjs[SROUND];j++, obj_tmp++) {
				// controlla che il cilindro appartenga ad un estremo 
				if ((obj_tmp->x==obj->x) && (obj_tmp->y==obj->y)) {
					obj_tmp->x=mem_bar_orig[i][0];	// aggiorna le coordinate del cilindro sulle nuove dell'estremo
					obj_tmp->y=mem_bar_orig[i][1];
				}
				// o all'altro della sbarra
				if ((obj_tmp->x==obj->X) && (obj_tmp->y==obj->Y)) {
					obj_tmp->x=mem_bar_orig[i][2];  // aggiorna le coordinate del cilindro sulle nuove dell'estremo
					obj_tmp->y=mem_bar_orig[i][3];
				}
			}
			// ripristina la posizione di partenza di tutte le sbarre
			obj->x=mem_bar_orig[i][0];			
			obj->y=mem_bar_orig[i][1];			
			obj->X=mem_bar_orig[i][2];			
			obj->Y=mem_bar_orig[i][3];			
		}
	}

	find=ind;
	for(team=0,pind=ind;team<nteam;team++,pind++)
	{
		for(r=0;r<100;r++)
			for(c=0;c<100;c++) pind->grid[r][c]=0;

		reset_controller(team);    // we set neurons state to 0.0

		for (i=0;i < pind->ninputs; i++)
			pind->input[i] = 0.0;

		update_motors(pind, ipar, NULL);   // inizializza gli output dei robot

		pind->lifecycle    = 0;
		pind->visited[0]   = 0;
		pind->eatenfood[0] = 0.0;
		pind->eatenfood[1] = 0.0;
		pind->energylevel = 1.0;
		pind->crashed = -1;
		pind->jcrashed = 0;
		pind->touchedbar = 0;

		// Imposta l'epoca corrente nell'individuo
		pind->current_epoch=epoch;	

		for (i=0;i < ipar->a_groundsensor; i++)
			pind->ground[i] = 0.0;
		for (i=0;i < ipar->a_groundsensor2; i++)
			pind->lastground[i] = 0.0;
		if (epoch == 0)
		{
			pind->oldcfitness = 0.0;
			pind->oldfitness = 0.0;
		}
        else
			pind->oldfitness = pind->fitness;

		if (ipar->bumpsensors >0)
			for(i=0; i < 2; i++) 
				pind->bumpsensors[i] = 0;
	}

	// Misura statistica n.

	//	Output of positional data throughout a trial
	//	Original code modified by Dean to allow the epoch number to be appended to the
	//	filenane, allowing mutliple data files to be collected for each simulation.
	if (test_all_ind_positions) 
	{
		//	Filename now includes epoch number
		sprintf(fileout,"All_Individuals_Positions_Epoch%d_S%d.txt",epoch,test_seed);

		//	Open the file with write-access
		fout=fopen(fileout,"w");								

		//	This style is not used in my version of the simulator
		if (test_output_type==0) 
		{
			fprintf (fout, "\t  Positions and Face Directions of all Individuals\n");
			fprintf (fout, "\t-----------------------------------------------------------\n\n");
			fprintf (fout, "\t  Individual X | Inidividual Y | Individual Face Direction \n");
			fprintf (fout, "\t-----------------------------------------------------------\n\n");
		} 
		else if (test_output_type==1) 
		{
			//	Output the header for the file - actual data is output later in the method...
			for (k=0; k<nteam-1; k++)
			{
				fprintf (fout, "Time_Step X_Ind_%d Y_Ind_%d FD_Ind_%d ", 
					(ind+k)->number, 
					(ind+k)->number, 
					(ind+k)->number);
			}

			fprintf (fout, "Time_Step X_Ind_%d Y_Ind_%d FD_Ind_%d\n", 
				(ind+k)->number, 
				(ind+k)->number, 
				(ind+k)->number);			
		}

		fflush(fout);
		fclose(fout);	
	}

	// BEGIN OF THE TRIAL
	while ((run < sweeps) && stop == 0)
	{

		//	run: current frame in the cycle
		//	sweeps: maximum number of frames in a lifecycle
		if ((ipar->switchDayNightEvery > 0) && ((run % ipar->switchDayNightEvery) == 0))
		{
			switchDayNight();
		}

#ifdef EVOWINDOWS
		if (real==1)
			sTime=clock();
#endif

		if (real==1)
            update_realsensors();
		
		for(team=0,pind=ind, pipar = ipar;team<nteam;team++, pind++)
		{
			pind->oldcfitness = pind->fitness;
			if (real==0)
			{
				update_sensors(pind, pipar, &num_leaders, &num_followers);
			}
	        else
			{
				set_input_real(pind,pipar);
			}
		}


		for(team=0,pind=ind, pipar=ipar;team<nteam;team++, pind++) {

			if (pind->dead == 0)
			{
				if (pind->controlmode == 0)
				{
					act = update_ncontroller(team, pind->input);// aggiorna la rete la vecchia activate_net_dyn
					update_motors(pind, pipar, act);
					if (pipar->wheelmotors >= 2.0)
					{
						if (!((pipar->motionlessrace > 0) && (pind->race==pipar->motionlessrace-1) && (run>0)))
							move_lego_robot(pind);							 // funzione che aggiorna la posizione del robot
							//move_robot(pind);
						
						/*if (run <= 300)
						{
						// Test : Ancora fissamente i robot per test, ATTENZIONE INDICIZZARLI CON IND !!!
						(ind+0)->direction=0.0;
						(ind+0)->pos[0]=8700.0-7400;
						(ind+0)->pos[1]=8700.0;

						//(ind+0)->pos[0]-=7000.0;

						(ind+1)->direction=0.0;
						(ind+1)->pos[0]=9000.0-7400;
						(ind+1)->pos[1]=8700.0;

						//(ind+1)->pos[0]-=7000.0;

						(ind+2)->direction=0.0;
						(ind+2)->pos[0]=8400.0-7400;
						(ind+2)->pos[1]=8700.0;
	
						//(ind+2)->pos[0]-=3000.0;

						(ind+3)->direction=0.0;
						(ind+3)->pos[0]=8700.0-7400;
						(ind+3)->pos[1]=9000.0;

						//(ind+3)->pos[0]-=7000.0;
						}
						else
						{
						// Test : Ancora fissamente i robot per test, ATTENZIONE INDICIZZARLI CON IND !!!
						(ind+0)->direction=0.0;
						(ind+0)->pos[0]=8700.0-7400;
						(ind+0)->pos[1]=8700.0;

						//(ind+0)->pos[0]-=7000.0;

						(ind+1)->direction=0.0;
						(ind+1)->pos[0]=9000.0-7400;
						(ind+1)->pos[1]=8700.0;

						//(ind+1)->pos[0]-=7000.0;

						(ind+2)->direction=0.0;
						(ind+2)->pos[0]=8400.0-7400;
						(ind+2)->pos[1]=8700.0;
	
						//(ind+2)->pos[0]-=3000.0;

						(ind+3)->direction=0.0;
						(ind+3)->pos[0]=8700.0-3400;
						(ind+3)->pos[1]=9000.0;

						//(ind+3)->pos[0]-=7000.0;

						}*/
						
						// Se � specificato che un individuo deve stare fermo allora lo blocca alla posizione specificata
						if (pipar->motionlessind==pind->number+1) {
							pind->pos[0]=pipar->Xmotionlessind;
							pind->pos[1]=pipar->Ymotionlessind;
							pind->direction=pipar->FDmotionlessind;
							//pind->direction++;
							//if (pind->direction==360) pind->direction=0;
						}

						// Se � specificato che tutto un gruppo deve stare fermo tranne un individuo allora blocca il gruppo nella posizione specificata
						if (((ipar->motionlessgroup_but_nind) && (pind->number+1!=pipar->motionlessgroup_but_nind)) && 
							((ipar->motionlessgroup_but_nind2) && (pind->number+1!=pipar->motionlessgroup_but_nind2))) {
							pind->pos[0]=pipar->Xmotionlessind-2.3*robotradius*pind->number;	// Trasla gli individui di una certa ascissa
							pind->pos[1]=pipar->Ymotionlessind;
							pind->direction=pipar->FDmotionlessind;
						}
					}

					// Misura statistica n.

					//	Output of positional data throughout a trial
					//	Original code modified by Dean to allow the epoch number to be appended to the
					//	filenane, allowing mutliple data files to be collected for each simulation.
					if (test_all_ind_positions) 
					{
						//	Filename now includes epoch number
						sprintf(fileout,"All_Individuals_Positions_Epoch%d_S%d.txt",epoch,test_seed);
						
						//	This time, open the file to append data to the end
						fout=fopen(fileout,"a");								

						//	Output cyclenumber, x coordinate, y coordinate and direction for each agent
						if (pind->number<nteam-1)
							fprintf (fout, "%d %d %d %d ", pind->lifecycle, 
							(int) pind->pos[0], 
							(int) pind->pos[1], 
							(int) pind->direction);			
						else	
							fprintf (fout, "%d %d %d %d\n", pind->lifecycle, 
							(int) pind->pos[0], 
							(int) pind->pos[1], 
							(int) pind->direction);			

						//	Write to and close the file
						fflush(fout);
						fclose(fout);	
					}
				}
				else
                {
					update_bcontroller(pind, pipar);
				}

				// In evoluzione aggiorna il processamento degli eventi della grafica ogni 2000 cicli
				// a partire dal ciclo 1 e non 0 perch� � stata gi� aggiornata prima
				if (((n_fixed_evolution_is_running == 1) || (n_fixed_evolution_social_is_running == 1)) &&
					 ((run % ngraphicsrefreshcycles == 0) && (pind->number==nteam-1)))	// aggiorna ogni nteam individui
					// we update the process events (i.e. the main window) every trial 
				    update_events();

				// check for collision
				pind->jcrashed = 0;
				if (crash=check_crash(pind->pos[0],pind->pos[1],-1,&xi,&yi,&nind,pind->died_individual,pind->ghost_individual)) // controlla la collisione, e restituisce il punto dove � avvenuta la collisione
				{
					pind->crashed = run;
					pind->jcrashed = 1;
					ncrashedind=nind;
					
					if (stop_when_crash == 1)
						stop = 1;
					else 
						if (restart_from_n_pos_before > 0)				// se specificato, il robot collide con i muri e 
							reset_pos_from_crash_n_pos_before(pind);	// l'altro robot ripartendo dopo la collisione da
																		// una delle posizioni precedenti e direzione casuale
						else if (restart_around) 
							reset_pos_from_crash_around(pind,0);
						else
							reset_pos_from_crash(pind);

					if (pipar->bumpsensors>0)							// Attiva i sensori di urto se sono definiti
						update_bump_sensors(pind,xi,yi);
				} else {
					if (pipar->bumpsensors>0)							// Se non � avvenuto un urtro maniene a 0 i bump sensors
						for(i=0; i < 2; i++) 
							pind->bumpsensors[i] = 0;
				}

				// se ci sono sbarre controlla la collisione con la sbarra e ne effettua l'aggiornamento della
				// posizione		
				// Imposta il flag che descrive che entrambi i segnali intenzionali sono maggiori di 0.5 
				if(envnobjs[BAR]>0) {
					if ((pffitness == ffitness_corvids15) || (pffitness == ffitness_corvids14)) {	
					
						if (pind->number==0) otherpind=(pind+1);	// seleziona e punta all'altro individuo 
						if (pind->number==1) otherpind=(pind-1);	// seleziona e punta all'altro individuo
					
						if  (((pipar->pushingunits>0) && (((!railsbar) && ((pind->pushing_units[0]>0.5) && (otherpind->pushing_units[0]>0.5)))||
							((railsbar) && ((pind->touchedbar) && (otherpind->touchedbar) && 
									(pind->pushing_units[0]>0.5) && (otherpind->pushing_units[0]>0.5))))) || 
							((pipar->pushingunits==0) && (!railsbar||(railsbar && ((pind->touchedbar) && (otherpind->touchedbar))))))

							sincrobots=1;
						else 
							sincrobots=0;	
					}

					pind->jcrashed = 0;
					pind->touchedbar=check_update_bar(pind,T1,T2,pffitness);

					// se la barra � stata toccata ed � bloccata allora genera un crash
					if (((pffitness == ffitness_corvids15) || (pffitness == ffitness_corvids14)) && (pind->touchedbar) && !(sincrobots)) {	
						pind->crashed = run;
						pind->jcrashed = 1;

						if (bar_without_crash) {
							if (restart_from_n_pos_before > 0) {			// se specificato, il robot collide con i muri e 
								pind->blocked_after_crash=1;				// barra bloccata, serve a non modificare le posizioni memorizzate					
								reset_pos_from_crash_n_pos_before(pind);	// l'altro robot ripartendo dopo la collisione da
																			// una delle posizioni precedenti e direzione casuale
							} else if (restart_around) { 
								pind->blocked_after_crash=1;				// barra bloccata, serve a non modificare le posizioni memorizzate					
								reset_pos_from_crash_around(pind,0);
							}
						} else {
							if (stop_when_crash == 1)
								stop = 1;
							else
								reset_pos_from_crash(pind);
						}
					} else {
						pind->blocked_after_crash=0;	// barra sbloccata					
					}
				}

				// Se � impostato che gli individui percettivi non possono ottenere fitness allora il contributo della 
				// loro fitness � nullo
				if ((ipar->onlyfewcanperceive) && (check_perceptualinds(pind->number)) && (pipar->percepindcannothavefitness)) 
					ind_fitness=0.0;					
				else
					ind_fitness=(*pffitness)(pind);			// Fitness ottenuta
			
				// Reset Crash
				ncrashedind=-3;								// riazzera il crash
				
				// compute fitness 1 (after each robot moved)
				if (((n_fixed_evolution_is_running == 1) || (testindividual_is_running == 1)) && (nraces==1)) { 
					if (((fitmode == 1) || (fitmode == 4)) && (*pffitness != NULL))	// Il fitmode viene usato solo nel caso dell'evoluzione con i cloni		
						ind->fitness += ind_fitness;
				} else if (((n_fixed_evolution_social_is_running == 1) || (n_fixed_evolution_is_running == 1) || (testpopulation_is_running == 1) || (testindividual_is_running == 1)) && (nraces>1)) {
					pind->fitness += ind_fitness;			// somma la fitness indistintamente
															// ad ogni individuo e non solamente
															// al primo come nel caso classico
															// per calcolarne la media alla fine
				}
			}		

			// Misura statistica n.8 (test di esplorativit� degli individui) 
			if ((test_exploratory) || (test_exploratory_all_gen)) {		// avvia il test dell'esplorativit� dell'individuo
				if (pind->number==explorindnumber) {				    // numero di individuo da testare						
					explorx = (int) (pind->pos[0] / xcell);				// ricalcolo della x della casella in cui si trova il robot
					explory = (int) (pind->pos[1] / ycell);				// ricalcolo della y della casella in cui si trova il robot

					if (!explorationarray[explorx][explory]) {
						explorationarray[explorx][explory] = 1;		// � gi� passato da questa cella
						pind->explorationability++;					// incrementa il contatore delle visite
					}
				}
			}

			// Misura statistica n.16 - 17 (test del baricentro
			// attenzione test da fare solo su un epoca altrimenti non ha senso !!
			if ((test_barycentre) || (test_barycentre_distances_all_gen) || (test_barycentre_distances) /*&& (test_statistics)*/) {	// avvia il test del baricentro del grouppo
				barycentrearray[run][pind->number*2]=(int) pind->pos[0];
				barycentrearray[run][pind->number*2+1]=(int) pind->pos[1];
			}


			// Misura dell'esplorativit� in ecologia	
			// Bisogna estendere il codice ad n individui
			// ==2 effettua il test solo fuori dalle food zone
			if ((test_exploratory_eco==1) || ((test_exploratory_eco==2) && (!(read_ground(pind)>0))) || (test_exploratory_all_gen_eco==1) || ((test_exploratory_all_gen_eco==2) && (!(read_ground(pind)>0)))) {		// avvia il test dell'esplorativit� dell'individuo
				explorx = (int) (pind->pos[0] / xcell);				// ricalcolo della x della casella in cui si trova il robot
				explory = (int) (pind->pos[1] / ycell);				// ricalcolo della y della casella in cui si trova il robot

				if (((test_nrace>0) && (pind->race==test_nrace-1)) || (test_nrace==0)) {	// test_nrace : disattivare questo parametro quando si considerano solo i best delle popolazioni
																							// per esmepio usarlo nel modello prede-predatori per testare i predatori, ma non usarlo nel modello 
																							// problema di coordinamento per leadership
					
					// Converte il numero di individuo per inserire tutto all'inizio dell'array
					if ((test_nrace>0) && (nraces>0))
						nindtest=pind->number-(nteam/nraces)*pind->race;
					else 
						nindtest=pind->number;

					if (!explorationarray_eco[explorx][explory][nindtest]) {		// In questo caso misura l'esplorazione in quanto non conta le celle gi� precedentemente visitate
						explorationarray_eco[explorx][explory][nindtest] = 1;		// � gi� passato da questa cella
						pind->explorationability++;									// incrementa il contatore delle visite
					}
				}
			}

			// Misura della mobilit� in ecologia	
			// ==2 effettua il test solo fuori dalle food zone
			if ((test_mobility_all_gen_eco==1) || ((test_mobility_all_gen_eco==2) && (!(read_ground(pind)>0)))) {			// avvia il test della mobilit� dell'individuo
				explorx = (int) (pind->pos[0] / xcell);										// ricalcolo della x della casella in cui si trova il robot
				explory = (int) (pind->pos[1] / ycell);										// ricalcolo della y della casella in cui si trova il robot

				if (((test_nrace>0) && (pind->race==test_nrace-1)) || (test_nrace==0)) {	// test_nrace : disattivare questo parametro quando si considerano solo i best delle popolazioni
																							// per esmepio usarlo nel modello prede-predatori per testare i predatori, ma non usarlo nel modello 
																							// problema di coordinamento per leadership
					
					// Converte il numero di individuo per inserire tutto all'inizio dell'array
					if ((test_nrace>0) && (nraces>0))
						nindtest=pind->number-(nteam/nraces)*pind->race;
					else 
						nindtest=pind->number;
					
					// Solo se non permane nella stessa casella, allora effettua il conteggio
					if (!((explorx==mem_explorx[pind->number]) && (explory==mem_explory[pind->number])))
						explorationarray_eco[explorx][explory][nindtest]++;						// � gi� passato da questa cella, memorizza quante volte � passato da questa cella				
				}
				mem_explorx[pind->number]=explorx;												// Memorizza la cella precedente per ogni robot
				mem_explory[pind->number]=explory;
			}
		}			// fine del loop sugli individui di un ciclo solo del trial

		//	Calculate hunger and thirst accumulators if using distributed leadeship
		if ((pffitness == ffitness_distributedLeadership) || (pffitness == ffitness_distributedLeadership_Simplified)
			|| (pffitness == ffitness_distributedLeadershipUnnormalised))
		{
			// Compute hungerAccumulator
			if (incrementHunger)
			{
				//	Check to see if 'hungerIncreaseRate' is defined
				//	this will allow us to check if we are using an
				//	old group accumulator based setup
				if (pipar->hungerIncreaseRate == 0)
				{
					//	Old group hunger accumulator
					//	Note: there was no overrun check in the original
					if (hungerAccumulator < pipar->hungerThreshold)
					{
						hungerAccumulator++;
					}
				}
				else
				{
					//	New inidividual hungerAccumulators
					for(team=0,pind=ind, pipar=ipar;team<nteam;team++, pind++)
					{
						//	Hunger accumulator is below the maximum
						if (pind->hungerAccumulator < pipar->hungerThreshold)
						{
							float hungerIncreaseRate;

							//	By default we will use the group increase rate
							hungerIncreaseRate = pipar->hungerIncreaseRate;

							if ((pind->race == 0) && (pipar->hungerIncreaseRate0 > 0))
							{
								//	Agent is from race0 (Green) and hungerIncreaseRate0 is non-zero
								//	so use the specified hungerIncreaseRate for race 0
								hungerIncreaseRate = pipar->hungerIncreaseRate0;
							}
							else if ((pind->race == 1) && (pipar->hungerIncreaseRate1 > 0))
							{
								//	Agent is from race1 (Dark Blue) and hungerIncreaseRate1 is non-zero
								//	so use the specified hungerIncreaseRate for race 1
								hungerIncreaseRate = pipar->hungerIncreaseRate1;
							}
							else if ((pind->race == 2) && (pipar->hungerIncreaseRate2 > 0))
							{
								//	Agent is from race2 (Cyan) and hungerIncreaseRate2 is non-zero
								//	so use the specified hungerIncreaseRate for race 2
								hungerIncreaseRate = pipar->hungerIncreaseRate2;
							}
							else if ((pind->race == 3) && (pipar->hungerIncreaseRate3 > 0))
							{
								//	Agent is from race3 (Yellow) and hungerIncreaseRate3 is non-zero
								//	so use the specified hungerIncreaseRate for race 3
								hungerIncreaseRate = pipar->hungerIncreaseRate3;
							}

							//	Increment the hunger accumulator by the specified value
							pind->hungerAccumulator += hungerIncreaseRate;

							//	Check for overrun
							if (pind->hungerAccumulator > pipar->hungerThreshold)
							{
								pind->hungerAccumulator = pipar->hungerThreshold;
							}
						}
					}
				}
			}
			else
			{
				//	Check to see if 'hungerDecreaseRate' is defined
				//	this will allow us to check if we are using an
				//	old group accumulator based setup
				if (pipar->hungerDecreaseRate == 0)
				{
					//	Old group hunger accumulator
					//	Note: there was no underrun check in the original
					if (hungerAccumulator > 0)
					{
						hungerAccumulator--;
					}
				}
				else
				{
					//	New inidividual hungerAccumulators
					for(team=0,pind=ind, pipar=ipar;team<nteam;team++, pind++)
					{
						//	Hunger accumulator is above the minimum
						if (pind->hungerAccumulator > 0)
						{
							float hungerDecreaseRate;

							//	By default we will use the group decrease rate
							hungerDecreaseRate = pipar->hungerDecreaseRate;

							if ((pind->race == 0) && (pipar->hungerDecreaseRate0 > 0))
							{
								//	Agent is from race0 (Green) and hungerDecreaseRate0 is non-zero
								//	so use the specified hungerDecreaseRate for race 0
								hungerDecreaseRate = pipar->hungerDecreaseRate0;
							}
							else if ((pind->race == 1) && (pipar->hungerDecreaseRate1 > 0))
							{
								//	Agent is from race1 (Dark Blue) and hungerDecreaseRate1 is non-zero
								//	so use the specified hungerDecreaseRate for race 1
								hungerDecreaseRate = pipar->hungerDecreaseRate1;
							}
							else if ((pind->race == 2) && (pipar->hungerDecreaseRate2 > 0))
							{
								//	Agent is from race2 (Cyan) and hungerDecreaseRate2 is non-zero
								//	so use the specified hungerDecreaseRate for race 2
								hungerDecreaseRate = pipar->hungerDecreaseRate2;
							}
							else if ((pind->race == 3) && (pipar->hungerDecreaseRate3 > 0))
							{
								//	Agent is from race3 (Yellow) and hungerDecreaseRate3 is non-zero
								//	so use the specified hungerDecreaseRate for race 3
								hungerDecreaseRate = pipar->hungerDecreaseRate3;
							}

							//	Decrement the hunger accumulator by the specified value
							pind->hungerAccumulator -= hungerDecreaseRate;

							//	Check for underrun
							if (pind->hungerAccumulator < 0)
							{
								pind->hungerAccumulator = 0;
							}
						}
					}
				}
			}

			// Compute thirstAccumulator
			if (incrementThirst)
			{
				//	Check to see if 'thirstIncreaseRate' is defined
				//	this will allow us to check if we are using an
				//	old group accumulator based setup
				if (pipar->thirstIncreaseRate == 0)
				{
					//	Old group hunger accumulator
					//	Note: there was no overrun check in the original
					if (thirstAccumulator < pipar->thirstThreshold)
					{
						thirstAccumulator++;
					}
				}
				else
				{
					//	New individual thirstAccumulators
					for(team=0,pind=ind, pipar=ipar;team<nteam;team++, pind++)
					{
						//	Thirst accumulator is below the maximum
						if (pind->thirstAccumulator < pipar->thirstThreshold)
						{
							float thirstIncreaseRate;

							//	By default we will use the group increase rate
							thirstIncreaseRate = pipar->thirstIncreaseRate;

							if ((pind->race == 0) && (pipar->thirstIncreaseRate0 > 0))
							{
								//	Agent is from race0 (Green) and thirstIncreaseRate0 is non-zero
								//	so use the specified thirstIncreaseRate for race 0
								thirstIncreaseRate = pipar->thirstIncreaseRate0;
							}
							else if ((pind->race == 1) && (pipar->thirstIncreaseRate1 > 0))
							{
								//	Agent is from race1 (Dark Blue) and thirstIncreaseRate1 is non-zero
								//	so use the specified thirstIncreaseRate for race 1
								thirstIncreaseRate = pipar->thirstIncreaseRate1;
							}
							else if ((pind->race == 2) && (pipar->thirstIncreaseRate2 > 0))
							{
								//	Agent is from race2 (Cyan) and thirstIncreaseRate2 is non-zero
								//	so use the specified thirstIncreaseRate for race 2
								thirstIncreaseRate = pipar->thirstIncreaseRate2;
							}
							else if ((pind->race == 3) && (pipar->thirstIncreaseRate3 > 0))
							{
								//	Agent is from race3 (Yellow) and thirstIncreaseRate3 is non-zero
								//	so use the specified thirstIncreaseRate for race 3
								thirstIncreaseRate = pipar->thirstIncreaseRate3;
							}

							//	Increment the thirst accumulator by the specified value
							pind->thirstAccumulator += thirstIncreaseRate;

							//	Check for overrun
							if (pind->thirstAccumulator > pipar->thirstThreshold)
							{
								pind->thirstAccumulator = pipar->thirstThreshold;
							}
						}
					}
				}
			}
			else
			{
				//	Check to see if 'thirstDecreaseRate' is defined
				//	this will allow us to check if we are using an
				//	old group accumulator based setup
				if (pipar->thirstDecreaseRate == 0)
				{

					//	Old group hunger accumulator
					//	Note: there was no underrun check in the original
					if (thirstAccumulator > 0)
					{
						thirstAccumulator--;
					}
				}
				else
				{
					//	New individual thirstAccumulators
					for(team=0,pind=ind, pipar=ipar;team<nteam;team++, pind++)
					{
						//	Thirst accumulator is above the minimum
						if (pind->thirstAccumulator > 0)
						{
							float thirstDecreaseRate;

							//	By default we will use the group decrease rate
							thirstDecreaseRate = pipar->thirstDecreaseRate;

							if ((pind->race == 0) && (pipar->thirstDecreaseRate0 > 0))
							{
								//	Agent is from race0 (Green) and thirstDecreaseRate0 is non-zero
								//	so use the specified thirstDecreaseRate for race 0
								thirstDecreaseRate = pipar->thirstDecreaseRate0;
							}
							else if ((pind->race == 1) && (pipar->thirstDecreaseRate1 > 0))
							{
								//	Agent is from race1 (Dark Blue) and thirstDecreaseRate1 is non-zero
								//	so use the specified thirstDecreaseRate for race 1
								thirstDecreaseRate = pipar->thirstDecreaseRate1;
							}
							else if ((pind->race == 2) && (pipar->thirstDecreaseRate2 > 0))
							{
								//	Agent is from race2 (Cyan) and thirstDecreaseRate2 is non-zero
								//	so use the specified thirstDecreaseRate for race 2
								thirstDecreaseRate = pipar->thirstDecreaseRate2;
							}
							else if ((pind->race == 3) && (pipar->thirstDecreaseRate3 > 0))
							{
								//	Agent is from race3 (Yellow) and thirstDecreaseRate3 is non-zero
								//	so use the specified thirstDecreaseRate for race 3
								thirstDecreaseRate = pipar->thirstDecreaseRate3;
							}

							//	Decrement the hunger accumulator by the specified value
							pind->thirstAccumulator -= thirstDecreaseRate;

							//	Check for underrun
							if (pind->thirstAccumulator < 0)
							{
								pind->thirstAccumulator = 0;
							}
						}
					}
				}
			}
		}

		
	
		// Misura statistica n.16 - 17 (test del baricentro, calcola le coordinate del baricentro se il test � attivo
		if ((test_barycentre) || (test_barycentre_distances_all_gen) || (test_barycentre_distances) /*&& (test_statistics)*/) {	// avvia il test del baricentro del grouppo
			// Calcolo del baricentro del gruppo intero 
			ave_barycentre_x=0;
			ave_barycentre_y=0;
			nelem=0;											
			for(n=0; n < nteam; n++) {								// seleziona tutti gli individui tranne i ghost
				if (((pipar->lab_test_ghost_team1==0)&&(pipar->lab_test_ghost_team2==0)&&(pipar->lab_test_ghost_team3==0)&&(pipar->lab_test_ghost_team4==0))||
					(((n!=0) && (pipar->lab_test_ghost_team1)) || ((n!=1) && (pipar->lab_test_ghost_team2)) || ((n!=2) && (pipar->lab_test_ghost_team3)) || ((n!=3) && (pipar->lab_test_ghost_team4)))) { // non considera gli individui ghost
					ave_barycentre_x=ave_barycentre_x+barycentrearray[run][n*2];
					ave_barycentre_y=ave_barycentre_y+barycentrearray[run][n*2+1];
					nelem++;
				}
			}
			ave_barycentre_x=ave_barycentre_x/nelem;				// Calcola la media delle x e delle y
			ave_barycentre_y=ave_barycentre_y/nelem;
			barycentrearray[run][n*2]=ave_barycentre_x;				// Carica le coordinate del baricentro calcolato nell'ultima posizione del vettore
			barycentrearray[run][n*2+1]=ave_barycentre_y;
		
			// Calcolo del baricentro di 4 sottogruppi composti escludendo prima l'individuo 0,1,2 ecc....
			for (i=0; i<nteam; i++) {
				ave_barycentre_x=0;
				ave_barycentre_y=0;
				nelem=0;
				for(n=0; n < nteam; n++) 								// seleziona tutti gli individui tranne i ghost
					if ((i!=n) && (((pipar->lab_test_ghost_team1==0)&&(pipar->lab_test_ghost_team2==0)&&(pipar->lab_test_ghost_team3==0)&&(pipar->lab_test_ghost_team4==0))||
					(((n!=0) && (pipar->lab_test_ghost_team1)) || ((n!=1) && (pipar->lab_test_ghost_team2)) || ((n!=2) && (pipar->lab_test_ghost_team3)) || ((n!=3) && (pipar->lab_test_ghost_team4))))) { // non considera gli individui ghost
						ave_barycentre_x=ave_barycentre_x+barycentrearray[run][n*2];
						ave_barycentre_y=ave_barycentre_y+barycentrearray[run][n*2+1];
						nelem++;
					}
				
				ave_barycentre_x=ave_barycentre_x/nelem;					// Calcola la media delle x e delle y ma solo su nelem membri selezionati
				ave_barycentre_y=ave_barycentre_y/nelem;
				barycentrearray[run][nteam*2+2+i*2]=ave_barycentre_x;		// Carica al posto esatto le coordinate del baricentro calcolato nell'ultima posizione del vettore
				barycentrearray[run][nteam*2+2+i*2+1]=ave_barycentre_y;
			}
		}

		// Misura statistica n.18 (test delle distanze individui - baricentro dell'intero gruppo
		if ((test_barycentre_distances) || (test_barycentre_distances_all_gen)) {							// avvia il test del baricentro del gruppo
			if ((test_barycentre_distances==1) || (test_barycentre_distances_all_gen==1)) {					// primo tipo : distanza tra ogni individuo e il baricentro dell'intero gruppo
				for(n=0; n < nteam; n++) 							// seleziona tutti gli individui 
					barycentredistancesarray[run][n]+=mdist(barycentrearray[run][n*2],barycentrearray[run][n*2+1],barycentrearray[run][nteam*2],barycentrearray[run][nteam*2+1]);	// Misura la distanza tra un individuo e la posizione del baricentro del gruppo a cui appartiene, 
																																													// e la aggiunge a quella dell'epoca precedente in modo da poterne fare la media alla fine su tutte le epoche			
			} else if ((test_barycentre_distances>1) || (test_barycentre_distances_all_gen>1)) {			// secondo tipo : distanza tra ogni individuo e il baricentro della parte restante del gruppo (escluso l'individuo stesso)
				for(n=0; n < nteam; n++) 							// seleziona tutti gli individui 
					barycentredistancesarray[run][n]+=mdist(barycentrearray[run][n*2],barycentrearray[run][n*2+1],barycentrearray[run][nteam*2+2+n*2],barycentrearray[run][nteam*2+2+n*2+1]);	// Misura la distanza tra un individuo e la posizione del baricentro del gruppo a cui appartiene, 

			}
		}

		// compute fitness after all individuals moved
		if ((n_fixed_evolution_is_running == 1) || (testindividual_is_running == 1)) 
			if (fitmode == 2 && *pffitness != NULL)
				ind->fitness += (*pffitness)(ind);
		

		// set a delay on test time
		if (n_fixed_evolution_is_running == 0 && n_variable_evolution_is_running == 0 && n_fixed_evolution_social_is_running == 0) 
			for (delay=0;delay<display_delay;delay++);

		// update the rendering of the robot/environment and network
		if ((testindividual_is_running == 1) || (testpopulation_is_running == 1) || display_under_evolution) // visualizza i robot in evoluzione nel caso � richiesto
	    {
			if(!test_statistics) {			// se deve stampare le statistiche non visualizza la grafica per velocizzare		
				update_rendnetwork(run);	// aggiorna il disegno dei segnali dei neuroni
				update_rendrobot();			// aggiorna il disegno dei robots
			}
#ifdef EVO3DGRAPHICS
//	     update_rend3drobot();
#endif
	    }

		// update world and neurons graphics
		if (testindividual_is_running == 1)
		{
			// Legge l'individuo correntemente selezionato con il mouse
			pind=ind+cindividual;

			sprintf(sbuffer,"Gen %d epoch %d cycle %d fitness %.3f tfitness %.3f ", gen, cepoch,pind->lifecycle, pind->fitness - pind->oldcfitness, pind->fitness);

			display_stat_message(sbuffer);
		} else if (testpopulation_is_running == 1)
		{
			// Legge l'individuo correntemente selezionato con il mouse
			pind=ind+cindividual;

			if (pipar->leadersfollowerspercepsystem >= 4) {
				sprintf(sbuffer,"Seed %d Gen %d epoch %d cycle %d fitness %.3f tfitness %.3f nLeader %d nFollowers %d", test_seed, gen, cepoch,pind->lifecycle, pind->fitness - pind->oldcfitness, pind->fitness, num_leaders, num_followers);
			} else {
				sprintf(sbuffer,"Seed %d Gen %d epoch %d cycle %d fitness %.3f tfitness %.3f ", test_seed, gen, cepoch,pind->lifecycle, pind->fitness - pind->oldcfitness, pind->fitness);
			}
			display_stat_message(sbuffer);
		} else if (((n_fixed_evolution_social_is_running) || (n_variable_evolution_is_running)) && (display_under_evolution == 1))
		{
			sprintf(sbuffer,"Gen %d epoch %d cycle %d ", gen, cepoch,ind->lifecycle);

			display_stat_message(sbuffer);
		} else if ((n_fixed_evolution_is_running) && (display_under_evolution == 1))
		{
			sprintf(sbuffer,"Gen %d epoch %d Ind %d cycle %d ", gen, cepoch,n_ind,ind->lifecycle);

			display_stat_message(sbuffer);
		}

		for(team=0,pind=ind; team < nteam; team++, pind++)
		{
			pind->lifecycle++;
			pind->totcycles++;
		}

		// Evoluzione n-variabile con overlap generazionale	
		// Se il numero dei cicli di vita dell'individuo � un multiplo del numero di cicli utile per riprodursi, 
		// allora l'individuo viene riprodotto e mutato secondo una percentuale definita. 
		// Per eventuale elitismo inserire 0 nell'opzione di mutazione. 
		// Va inserito subito dopo l'incremento dei cicli in modo da evitare al ciclo 0 la generazione di nuovi
		// individui addizionali. 
		if (n_variable_evolution_is_running == 1) {
			for(tmp_nteam=nteam, team=0; team < tmp_nteam; team++) {		// tmp_nteam serve a non generare un 
																			// loop infinito dato che nteam viene
																			// incrementato nel loop
				pind=ind+team;	// Questo elimina il problema che l'area di memoria del puntatore ad ind viene rilasciato di volta in volta	

				if (pind->lifecycle % nreproducingcycles == 0) {
	
					nchildren=reproduce_and_create_child(pind->number,1);	 			
					nteam=nindividuals;							// aggiorna il numero di individui nel team
					init_robot(nchildren);						// crea un numero di fenotipi nuovi aggiornato al nuovo numero 
																// della popolazione a partire da nchildren
					init_network(nchildren);					// crea un numero di reti neurali di controllo nuove aggiornate
																// al nuovo numero della popolazione a partire da nchildren
				    
					// Inizializza alcune variabili legate al nuovo individuo appena creato
					init_ind_parameters(ind+nchildren,nchildren);

					// Inizializza la posizione del singolo individuo creato
					initialize_one_robot_position_and_color(ind+nchildren,nchildren);	
				
					// Se il numero di razze � maggiore di 1 allora imposta la razza del figlio
					// pari a quella del padre
					if (nraces>1) 
						(ind+nchildren)->race=pind->race;
				}
			}
		}

		if (timeadjust > 0 && ((testindividual_is_running == 1) || (testpopulation_is_running == 1)))
		{
			pause(timeadjust);
		}

		trace_run++;		// incrementa l'indice necessario al trace 

		if (n_variable_evolution_is_running == 0) // Se � attiva l'evoluzione o il test classico 
			run=ind->lifecycle;		// usa questo metodo di incremento del run, laddove siano necessari incrementi dei cicli multipli (opzione timeincrease)
		else 
			run++;					// altrimenti lo incrementa direttamente, perch� nel nuovo tipo di evoluzione ogni individuo
									// ha un ciclo di vita differente
		
		if (mobile_tareas) {
			for (ta=0, obj = *(envobjs + 3); ta < envnobjs[3]; ta++, obj++)
			{	
				// Memorizza la posizione della food zone per poi eventualmente ripristinarla
				old_x=obj->x;
				old_y=obj->y;
				
				do {// Sceglie una direzione casuale ed una velocit� casuale della food zone
					
					if (movem==0) 
						obj->x = obj->x + fabs(rans(tareas_speed));		
					else if (movem==1) 
						obj->y = obj->y + fabs(rans(tareas_speed));
					else if (movem==2)
						obj->x = obj->x - fabs(rans(tareas_speed));		
					else if (movem==3)
						obj->y = obj->y - fabs(rans(tareas_speed));
					else if (movem==4) {
						obj->x = obj->x + fabs(rans(tareas_speed));		
						obj->y = obj->y + fabs(rans(tareas_speed));
					} else if (movem==5) {
						obj->x = obj->x - fabs(rans(tareas_speed));		
						obj->y = obj->y - fabs(rans(tareas_speed));
					} else if (movem==6) {
						obj->x = obj->x + fabs(rans(tareas_speed));		
						obj->y = obj->y - fabs(rans(tareas_speed));
					} else if (movem==7) {
						obj->x = obj->x - fabs(rans(tareas_speed));		
						obj->y = obj->y + fabs(rans(tareas_speed));
					}
				
					/*if (obj->x<obj->r)
						obj->x=envdx-obj->r;
					if (obj->y<obj->r)
						obj->y=envdy-obj->r;
					if (obj->x+obj->r>envdx)
						obj->x=obj->r;
					if (obj->y+obj->r>envdy)
						obj->y=obj->r;
					*/
							
					/*if (run % tareas_mov_after_n_cycle == 0)							
						movem=fabs(rans(7));
					*/
					if ((obj->x<obj->r) || (obj->y<obj->r) || (obj->x+obj->r>envdx) || (obj->y+obj->r>envdy)) {
						movem=fabs(rans(8));
						obj->x=old_x;			// ripristina la posizione precedente e riprova, in questo modo si evitano le condizioni di stallo
						obj->y=old_y;	
					}

				} while ((obj->x<obj->r) || (obj->y<obj->r) || (obj->x+obj->r>envdx) || (obj->y+obj->r>envdy));
			}
		}		
		
		// calcola la distanza media individuo-individuo pi� vicino, per ogni ciclo. 
		if /*(test_statistics)*/ ((test_distance_average)||(test_distance_average_per_cycle)||(test_distance_average_per_cycle_all_gen)) {
			// Misura statitica n.3
			for(team=0,pind=ind;team<nteam;team++, pind++)
			{
				if (((nraces>0) && (test_nrace>0) && (pind->race==test_nrace-1)) || 
					(test_nrace==0) || (nraces==0)) {						// seleziona eventualmente solo gli individui della razza richiesta
					// Determina l'individuo con distanza minima 
					mindist=envdx+envdy;
					for(n=0, cind=ind; n < nteam; n++, cind++) {	// seleziona tutti gli altri individui 
						if (((nraces>0) && (test_nrace>0) && (cind->race==test_nrace-1)) || 
							(test_nrace==0) || (nraces==0)){		// confronta eventualmente solo gli individui della razza richiesta
							if (cind!=pind) {		// verifica che sia differente da pind (il corrente)
								// Determina la distanza tra l'individuo corrente e uno dei trovato nell'intorno
								dist=mdist(pind->pos[0],pind->pos[1],cind->pos[0],cind->pos[1]);
								if (dist<=mindist) 
									mindist=dist;					// la nuova distanza minima � dist
							}
						}
					}
				
					*(distanceaverage+run)+=mindist;	// per ogni individuo somma la distanza minima trovata
				}
			}
			*(distanceaverage+run)=*(distanceaverage+run)/nteam;	
			// calcola la media per ogni ciclo	

			// Se deve valutare la distanza su una specie e se ci sono pi� specie 
			// Allora divide ulteriormente la media
			if ((nraces>0) && (test_nrace>0))
				*(distanceaverage+run)=*(distanceaverage+run) * nraces;		// per uniformare la media alla met� di nteam, allora devo moltiplicare

			// Misura statistica n.13
			if /*(test_statistics)*/ ((test_distance_average_per_cycle)||(test_distance_average_per_cycle_all_gen)) {
				*(distanceaverage_ave_epoch+run)+=*(distanceaverage+run);	// somma che serve per il calcolo della media sulle epoche
			}
		
			if (test_distance_average) {
				// Misura statistica n.9
				// Determina la media della prima met� della vita, della seconda. e dell'intera vita
				*(entirelifedistave+epoch)+=*(distanceaverage+run);				// somma il valore trovato
				if (run<=sweeps/3)
					*(firsthalflifedistave+epoch)+=*(distanceaverage+run);		// somma il valore trovato, se appartiene al primo terzo della vita
				if (run>2*(sweeps/3))
					*(secondhalflifedistave+epoch)+=*(distanceaverage+run);		// somma il valore trovato, se appartiene all' ultimo terzo della vita
			}
		}

		if /*(test_statistics)*/ (test_nindintoneighborhood) {
			// Misura statistica n.10
			for(team=0,pind=ind;team<nteam;team++, pind++)
			{
				if (((nraces>0) && (test_nrace>0) && (pind->race==test_nrace-1)) || 
					(test_nrace==0) || (nraces==0)) {		// seleziona eventualmente solo gli individui della razza richiesta
					nindnear=0;													// azzera il contatore dei vicini		
					for(n=0, cind=ind; n < nteam; n++, cind++) {				// seleziona tutti gli altri individui 
						if (((nraces>0) && (test_nrace>0) && (cind->race==test_nrace-1)) || 
							(test_nrace==0) || (nraces==0)){		// confronta eventualmente solo gli individui della razza richiesta
							if (cind!=pind) {										// verifica che sia differente da pind (il corrente)
								// Determina la distanza tra l'individuo corrente e uno dei trovato nell'intorno
								dist=mdist(pind->pos[0],pind->pos[1],cind->pos[0],cind->pos[1]);
								if (dist<=ipar->rgbcolorretinamaxdistance)			// se l'individuo � all'interno della distanza di visione 
									nindnear++;									// incrementa il numero di individui presenti nell'intorno
							}
						}
					}
					*(nindintoneighborhood+run)+=nindnear;
				}
			}
			*(nindintoneighborhood+run)=*(nindintoneighborhood+run)/nteam;

			// Se deve valutare la media su una specie sola e se ci sono pi� specie 
			// Allora divide ulteriormente la media
			if ((nraces>0) && (test_nrace>0))
				*(nindintoneighborhood+run)=*(nindintoneighborhood+run) * nraces;	// per uniformare la media alla met� di nteam, allora devo moltiplicare

			// Misura statistica n.10
			// Determina la media della prima met� della vita, della seconda. e dell'intera vita
			*(entirelifenindintoneighborhoodave+epoch)+=*(nindintoneighborhood+run);				// somma il valore trovato
			if (run<=sweeps/3)
				*(firsthalflifenindintoneighborhoodave+epoch)+=*(nindintoneighborhood+run);			// somma il valore trovato, se appartiene al primo terzo della vita
			if (run>2*(sweeps/3))
				*(secondhalflifenindintoneighborhoodave+epoch)+=*(nindintoneighborhood+run);		// somma il valore trovato, se appartiene all'ultimo terzo della vita
		}
			// Misura statistica n.4
	/*		for(team=0,pind=ind;team<nteam;team++, pind++)
			{
				if (((nraces>0) && (test_nrace>0) && (pind->race==test_nrace-1)) || 
					(test_nrace==0) || (nraces==0)) {		// seleziona eventualmente solo gli individui della razza richiesta
					// Determina l'individuo con distanza minima 
					mindist=envdx+envdy;
					for(n=0, cind=ind; n < nteam; n++, cind++) {	// seleziona tutti gli altri individui 
						if (((nraces>0) && (test_nrace>0) && (cind->race==test_nrace-1)) || 
							(test_nrace==0) || (nraces==0)){		// confronta eventualmente solo gli individui della razza richiesta
							if (cind!=pind) {		// verifica che sia differente da pind (il corrente)
								// Determina la distanza tra l'individuo corrente e uno dei trovato nell'intorno
								dist=mdist(pind->pos[0],pind->pos[1],cind->pos[0],cind->pos[1]);
								if (dist<=mindist) 
									mindist=dist;					// la nuova distanza minima � dist
							}
						}
					}
				}
				*(distancestdev+run)+=(mindist-*(distanceaverage+run))*(mindist-*(distanceaverage+run));	// per ogni individuo calcola lo scarto quadratico della distanza rispetto alla media
			}
			*(distancestdev+run)=*(distancestdev+run)/nteam;
			
			// Se deve valutare la distanza su una specie e se ci sono pi� specie 
			// Allora divide ulteriormente la dev standard
			if ((nraces>0) && (test_nrace>0))
				*(distancestdev+run)=*(distancestdev+run) * nraces;		// per uniformare la dev standard alla met� di nteam, allora devo moltiplicare

			*(distancestdev+run)=sqrt(*(distancestdev+run));			// calcola la radice quadrata, ovvero la deviazione standard delle distanze per ogni ciclo	
		*/
		//}

#ifdef EVOWINDOWS
		if (real==1)
        {
			//sprintf(dbgString,"Time: %d mms\r\n",(int)eTime-(int)sTime);
			//display_warning(dbgString);
			eTime=clock();
			dtime=(int)eTime-(int)sTime;
			while( ((int)clock()-(int)sTime)<100) {}; //waits until 100mms
        }
#endif

		if (userbreak == 1)
		{
			run = sweeps;
			//stopping real robots if connected
			if (real==1)
			{
			    for(team=0,pind=ind;team<nteam;team++,pind++) 
				    setpos_real(pind,0.5f, 0.5f);
			}
		}
	}   // end for-sweeps


	// Misura della mobilit� in ecologia	
	// ==2 effettua il test solo fuori dalle food zone
	if ((test_mobility_all_gen_eco==1) || ((test_mobility_all_gen_eco==2) && (!(read_ground(pind)>0)))) {			// avvia il test della mobilit� dell'individuo
		x_size=envdx/xcell;
		y_size=envdy/ycell;	

		for(team=0,pind=ind;team<nteam;team++,pind++)
			for (explorx=0;explorx<x_size;explorx++) 
				for (explory=0;explory<y_size;explory++)
					if (explorationarray_eco[explorx][explory][team]>0) 				
						pind->mobility+=explorationarray_eco[explorx][explory][team];	// Somma tutti i valori di mobilit� presenti nella matrice, ovvero quante volte 
																						// un robotha visitato una cella anche ripetutamente
	}

	// Misura statistica n.9
	// Determina la media della prima met� della vita, della seconda. e dell'intera vita
	if /*(test_statistics)*/ (test_distance_average) {
		*(entirelifedistave+epoch)=(*(entirelifedistave+epoch))/sweeps;					// calcola la media sull'intera durata della vita
		*(firsthalflifedistave+epoch)=(*(firsthalflifedistave+epoch))/(sweeps/3);		// calcola la media su met� della durata della vita
		*(secondhalflifedistave+epoch)=(*(secondhalflifedistave+epoch))/(sweeps/3);		// calcola la media su met� della durata della vita
	}
	
	// Misura statistica n.10
	// Determina la media della prima met� della vita, della seconda. e dell'intera vita
	if /*(test_statistics)*/ (test_nindintoneighborhood) {
		*(entirelifenindintoneighborhoodave+epoch)=(*(entirelifenindintoneighborhoodave+epoch))/sweeps;					// calcola la media sull'intera durata della vita
		*(firsthalflifenindintoneighborhoodave+epoch)=(*(firsthalflifenindintoneighborhoodave+epoch))/(sweeps/3);		// calcola la media su un terzo della durata della vita
		*(secondhalflifenindintoneighborhoodave+epoch)=(*(secondhalflifenindintoneighborhoodave+epoch))/(sweeps/3);		// calcola la media su un terzo della durata della vita
	}

	// compute fitness 3 (at the end of the trial)
	if ((n_fixed_evolution_is_running == 1) || (testindividual_is_running == 1))
		if (fitmode == 3 && *pffitness != NULL)
			ind->fitness += ind_fitness;

	/*
		Allow us to output the calculated fitness at the end of the trial
	*/
	if (outputFitnessAtEndOfTrial > 0)
	{
		FILE *popFitnessFile;

		//	Note: this will continue writing to any files currently within
		//	the directory.  It is best to move / rename prior to running
		//	the data collection exercise
		popFitnessFile = fopen("pop_fitness.fit","a");

		//	In the distributedLeadership functions all agent fitnesses will be the same
		//	so output this to the file
		fprintf(popFitnessFile, "Trial, %d, Population Fitness, %f\n", epoch, ind_fitness);

		fflush(popFitnessFile);
		
		fclose(popFitnessFile);
	}
}

/*
 * we evaluate an individual or a team of individuals
 * by letting them leave for several trials while evaluating their performance
 */
double evaluate(int gen, int nind)

{
	struct  individual *pind;
	int initial_nteam;
	int i,k,j;
	double totfitness=0.0;
	int success_rate;
    FILE   *fout;
	char   fileout[128];	 
	float  timeaverage, stdev;		
	float  difftimeaverage, diffstdev;	
	int ntimes;
	double distave_average;
	double diststdev_average;
	double aveentirelife=0, avefirsthalflife=0, avesecondhalflife=0;
	double  **ea;				// cursore dell'array
    int x_size, y_size;
	float max_fit;

	// initialize array of success times with "amoebaepochs" elements
	if (((test_times)||(test_times_all_gen)) /* && (test_statistics) */) {
		// from epoch start success time
		successruntime = (int *)malloc(amoebaepochs * sizeof(int)); 
		successfirstsideruntime = (int *)malloc(amoebaepochs * sizeof(int));
		successsecondsideruntime = (int *)malloc(amoebaepochs * sizeof(int));
			  
		// times vector setting to 0
		for (k=0;k<amoebaepochs;k++) {
			*(successruntime+k)=0;
			*(successfirstsideruntime+k)=0;
			*(successsecondsideruntime+k)=0;
		}		  
		success_counter=0;
		if (ipar->lab_ntestingteam > 2) {
			first_success_counter=0;
			second_success_counter=0;
		}
	}

	// initialize array of success times with "amoebaepochs" elements
	if ((test_times_all_gen_eco) /* && (test_statistics) */) {
		// times for eating % of population
		successruntime = (int *)malloc(amoebaepochs * sizeof(int)); 
			  
		// times vector setting to 0
		for (k=0;k<amoebaepochs;k++) 
			*(successruntime+k)=0;
	}

	// inizializza l'array di dimensioni pari al numero di cicli di vita, il quale contiente
	// la distanza media individuo-individuo pi� vicino e le deviazioni standard, per ogni ciclo. 
		// from epoch start success time
	if ((test_distance_average) /*&& (test_statistics)*/ ) {
		distanceaverage = (double *)malloc(sweeps * sizeof(double)); 
		distancestdev   = (double *)malloc(sweeps * sizeof(double)); 

		firsthalflifedistave = (double *)malloc(amoebaepochs * sizeof(double));						/* distanza media della prima parte della vita relativa all'epoca k */
		secondhalflifedistave = (double *)malloc(amoebaepochs * sizeof(double));					/* distanza media della seconda parte della vita relativa all'epoca k */
		entirelifedistave = (double *)malloc(amoebaepochs * sizeof(double));						/* distanza media dell'intera vita relativa all'epoca k */
	
		for (i=0;i<sweeps;i++) {
			*(distanceaverage+i)=0;
			*(distancestdev+i)=0;
		}

		for (k=0;k<amoebaepochs;k++) {
			*(firsthalflifedistave+k) = 0;
			*(secondhalflifedistave+k) = 0;
			*(entirelifedistave+k) = 0;
		}
	}

	if (((test_distance_average_per_cycle)||(test_distance_average_per_cycle_all_gen)) /*&& (test_statistics)*/) {
		distanceaverage = (double *)malloc(sweeps * sizeof(double)); 
		distanceaverage_ave_epoch = (double *)malloc(sweeps * sizeof(double)); 
	
		for (i=0;i<sweeps;i++) {
			*(distanceaverage+i)=0;
			*(distanceaverage_ave_epoch+i)=0;
		}
	}	

	if ((test_nindintoneighborhood) /*&& (test_statistics)*/) {
		nindintoneighborhood = (double *)malloc(sweeps * sizeof(double)); 
		
		firsthalflifenindintoneighborhoodave = (double *)malloc(amoebaepochs * sizeof(double));		/* distanza media della prima parte della vita relativa all'epoca k */
		secondhalflifenindintoneighborhoodave = (double *)malloc(amoebaepochs * sizeof(double));	/* distanza media della seconda parte della vita relativa all'epoca k */
		entirelifenindintoneighborhoodave = (double *)malloc(amoebaepochs * sizeof(double));		/* distanza media dell'intera vita relativa all'epoca k */

		for (i=0;i<sweeps;i++) 
			*(nindintoneighborhood+i)=0;

		for (k=0;k<amoebaepochs;k++) {
			*(firsthalflifenindintoneighborhoodave+k) = 0;
			*(secondhalflifenindintoneighborhoodave+k) = 0;
			*(entirelifenindintoneighborhoodave+k) = 0;
		}
	}

	// Misura statistica n.18 (test delle distanze dal baricentro) - inzializzazione dell'array
	// il test media le distanze su tutte le epoche, quindi � necessario azzerarle all'inizio di ogni generazione
	if ((test_barycentre_distances) || (test_barycentre_distances_all_gen)	/*&& (test_statistics)*/) {
		x_size=sweeps;
		y_size=nteam;

		for (i=0;i<x_size;i++)
			for (j=0;j<y_size;j++)
				barycentredistancesarray[i][j] = 0;					// Inizializzazione dell'array delle distanze dai baricentri
	}

	// Test delle distanze di ogni individuo dalle rispettive food zone
	if ((test_distance_from_fz) && (envnobjs[GROUND] > 0)) {
		x_size=nteam;
		y_size=envnobjs[GROUND];

		for (i=0;i<x_size;i++)
			for (j=0;j<y_size;j++)
				distances_from_fz_array[i][j]=0;
	}

	// Memorizza eventualmente la struttura originaria degli individui
	if (n_variable_evolution_is_running == 1) 
		initial_nteam=nteam;

	// All'inizio di ogni generazione, prima dell'avvio delle epoche, effettua un azzeramento
	// delle fitness
	for(i=0,pind=ind; i<nteam; i++, pind++)
	{
		pind->fitness   = 0.0;
		pind->virtualfitness = 0.0;
		pind->oldfitness= 0.0;
		pind->totcycles = 0;
		//pind->number=i;		// verificare se da mettere o da togliere
		pind->ncyclessawsomething = 0;	   // azzera il contatore del numero di cicli in cui l'individuo ha visto qualcosa
										   // all'inizio di tutte le epoche	
		pind->explorationability = 0;	   // azzera il contatore delle nuove caselle esplorate 	
		pind->mobility = 0;				  // azzera il contatore della mobilit�

	}   

	// Effettua il salvataggio solo per un individuo, l'individuo 0. Apre il file e lo chiude per svuotarlo. 
	if ((ipar->save_leader_followers_percent == 1) && (ipar->leadersfollowerspercepsystem >=4) && 
		((testindividual_is_running) || (testpopulation_is_running))) {
		// Misura statistica dei leaders e followers
		fout=fopen("LeadersFollowers.txt","w");							// file di analisi statistica
		fclose (fout);
	}		

	// Ciclo di vita di ogni individuo per un totale di epoche pari ad amoebaepochs
	for (cepoch=0; cepoch<amoebaepochs;cepoch++)
	{
		// Initialization of Accumulators
		if ((pffitness == ffitness_distributedLeadership) || (pffitness == ffitness_distributedLeadership_Simplified)
			|| (pffitness == ffitness_distributedLeadershipUnnormalised))
		{
			foodAccumulator=0;
			waterAccumulator=0;
			
			//	Make sure the hunger and thirst arrays are
			//	reinitialised for the next trial
			//	this is mainly of concern when using
			//	hunger / thirst sensor type 4
			//	where readings can persist between trials else
			for(i=0,pind=ind; i<nteam; i++, pind++)
			{
				initHungerArray(pind);
				initThirstArray(pind);
				pind->hungerAccumulator = 0;
				pind->thirstAccumulator = 0;
			}
		}

		// Effettua il salvataggio per ogni epoca delle percentuali. 
		if ((ipar->save_leader_followers_percent == 1) && (ipar->leadersfollowerspercepsystem >=4) && 
			((testindividual_is_running) || (testpopulation_is_running))) {
			// Misura statistica dei leaders e followers
			fout=fopen("LeadersFollowers.txt","a");						// file di analisi statistica
			fprintf (fout, "\n\nEpoch (Restart) : %d\n\n",cepoch);		// salva il numero di epoca
			fclose (fout);
		}		
		
		trial(gen, cepoch, nind); 
	
		// Aggiustamenti vari delle fitness, alla fine di un trial, per alcune funzioni di fitness
		if (pffitness == ffitness_groupdiscrim_2) {						// Alla fine del trial in questo caso di fitness, ogni individuo avr� la fitness dell'individuo massimo (in fitness)
			max_fit=0.0;	

			for (i=0, pind = ind; i < nteam; i++, pind++) {
				if (pind->fitness>max_fit)
					max_fit=pind->fitness;								// Trova il massimo
			}		

			for (i=0, pind = ind; i < nteam; i++, pind++) 
					pind->fitness=max_fit;								// Ogni individuo avr� la fitness massima, che diventer� la fitness di gruppo 
		}

		// we change environment every trial (if more that one environment are defined)
		if (userbreak == 1)
		{
			cepoch = amoebaepochs;
			testindividual_is_running = 0;
			testpopulation_is_running = 0;
			n_fixed_evolution_is_running = 0;
			n_fixed_evolution_social_is_running = 0;
			n_variable_evolution_is_running = 0;
		}

		// Alla fine dell'epoca riazzera tutto e parte dall'inizio
		if (n_variable_evolution_is_running == 1) {
			nteam=initial_nteam;
				
			if (ipar->visible_individual) {
				free(envobjs);			// Libera tutte la memoria precedentemente allocata
				envobjs=NULL;			// Disassocia anche l'indirizzo di memoria
				init_env();				// Inizializza l'ambiente (al momento prima del robot per il colore-cilindro)
			}
			free(ind);					// Libera tutte la memoria precedentemente allocata
			ind=NULL;					// Disassocia anche l'indirizzo di memoria
			init_robot(0);				// Inizializza i robot e le posizioni a partire dal robot di indice 0
			free(ncontrollers);			// Libera tutte la memoria precedentemente allocata
			ncontrollers=NULL;			// Disassocia anche l'indirizzo di memoria
			init_network(0);			// Inizializza le reti neurali a disposizione dei robot a partire dal robot di indice 0
			init_evolution();			// Inizializza le variabili di evoluzione
		}
	}
	
	// we normalize fitness
	// Se � in corso l'evoluzione in vita artificiale su agenti evolutivi allora i computi
	// sulla fitness non sono necessari
	if (n_variable_evolution_is_running == 0) {
		for (i=0, pind = ind; i < nteam; i++, pind++)
		{
			if (n_fixed_evolution_social_is_running == 0) {
				switch(fitmode)		// Nel caso classico di evoluzione effettua lo switch tra le due modalit� di calcolo della 
				{					// fitness (fitmode=1 : calcola con gli sweeps; fitmode = 2 : calcola mediando sul numero di epoche totali
					case 1:
						pind->fitness = pind->fitness / (double) (sweeps * amoebaepochs);
						break;
					case 4:
						if (nraces>1)
							pind->fitness = pind->fitness / (double) (sweeps * amoebaepochs);
						else						
							pind->fitness = pind->fitness / (double) (sweeps * amoebaepochs * nteam);
						break;
					case 2:
						pind->fitness = pind->fitness / amoebaepochs;
						break;
				}
			} else {	// Nel caso di evoluzione senza cloni, ogni pind ha la propria fitness come somma delle fitness totalizzate
						// in tutte le opeche per cui ne calcola la media
				pind->fitness = pind->fitness / amoebaepochs;
			}
		}

		// we assume a cooperative selection schema
		for(i=0,totfitness=0.0, pind = ind; i < nteam; i++, pind++)
			totfitness += pind->fitness;
	}
	

	// saving of number of success task and times, only with some fitnesses
	if ((test_times) && (test_statistics) && ((pffitness == ffitness_corvids15) || (pffitness == ffitness_corvids14))) {
		// calculates success percentade of individual 1 and 2
		success_rate=(success_counter*100)/amoebaepochs;

		// Misura statistica n.1
		sprintf(fileout,"StatisticsE%dS%d.txt",amoebaepochs,test_seed);
		fout=fopen(fileout,"w");							// file di analisi statistica

		if (fout!=NULL) {
		    fprintf (fout, "\t  Individual \n");
			fprintf (fout, "\t-------------\n");
			fprintf (fout, "N. successful trials : %d\n",success_counter);
			fprintf (fout, "Percentade of successful trials : %d %%\n",success_rate);
			fprintf (fout, "\n\"From start of epoch\" success times for each epoch\n");
			
			// Save "from start epoch success times" and calculate average					
			timeaverage=0;
			ntimes=0;
			for (i=0;i<amoebaepochs;i++) {
				fprintf (fout, "%d� epoch : %d\n",i,*(successruntime+i));
				if (*(successruntime+i)!=0) 
					ntimes++;				// nel calcolo del tempo medio elimina gli 0
				timeaverage+=(float) *(successruntime+i);
			}
			timeaverage=timeaverage/ntimes;
			
			// Calculate "from start epoch success times" standard deviation
			stdev=0;
			for (i=0;i<amoebaepochs;i++) 
				if (*(successruntime+i)!=0)	// elimina gli 0
					stdev+=(float) (*(successruntime+i)-timeaverage)*(*(successruntime+i)-timeaverage)/ntimes;
			stdev=sqrt(stdev);

			fprintf (fout, "\nTimes average (without the zeros)) : %d\n",(int) timeaverage);
		    fprintf (fout, "Standard deviation (without the zeros) : %.2f\n",stdev);

			fclose (fout);
		}

		// Misura statistica n.2
		sprintf(fileout,"StatisticsN2E%dS%d.txt",amoebaepochs,test_seed);
		fout=fopen(fileout,"w");							// file di analisi statistica n.2

		if (fout!=NULL) {
		    fprintf (fout, "\t  Individual \n");
			fprintf (fout, "\t-------------\n");
			fprintf (fout, "\n\n\"From start of epoch\" success differential times for each epoch (simultaneity test)\n");

			// Save "from start epoch success differential times" and calculate average					
			difftimeaverage=0;
			for (i=0;i<amoebaepochs;i++) {
				fprintf (fout, "%d� epoch : %d\n",i,abs((*(successfirstsideruntime+i))-(*(successsecondsideruntime+i))));
				difftimeaverage+=(float) abs((*(successfirstsideruntime+i))-(*(successsecondsideruntime+i)))/amoebaepochs;
			}
		
			// Calculate "from start epoch success differential times" standard deviation
			diffstdev=0;
			for (i=0;i<amoebaepochs;i++) 
				diffstdev+=(float) (abs((*(successfirstsideruntime+i))-(*(successsecondsideruntime+i)))-difftimeaverage)*(abs((*(successfirstsideruntime+i))-(*(successsecondsideruntime+i)))-difftimeaverage)/amoebaepochs;
			diffstdev=sqrt(diffstdev);
		
			fprintf (fout, "\nDifferential Times average : %d\n",(int) difftimeaverage);
		    fprintf (fout, "Standard deviation : %.2f\n",diffstdev);
		
			fclose (fout);
		}
	}

	// Misura statistica n.13
	// salvataggio del numero della media delle distanze individuo-individuo pi� vicino per
	// ogni ciclo
	if /*(test_statistics)*/ (test_distance_average_per_cycle) {
		if ((nraces>0) && (test_nrace>0))			// se deve calcolarla solo su una specie
			sprintf(fileout,"DistanceAverage_S%d_species%d.txt",test_seed,test_nrace-1);
		else
			sprintf(fileout,"DistanceAverage_S%d.txt",test_seed);

		fout=fopen(fileout,"w");							// file di analisi statistica

		if (fout!=NULL) {
			if (test_output_type==0) {
			    fprintf (fout, "\t   Distance Average per cycle (average between epochs)   \n");
				fprintf (fout, "\t--------------------------------\n\n");
			
				for (i=1;i<sweeps;i++) {
					*(distanceaverage_ave_epoch+i)=*(distanceaverage_ave_epoch+i)/amoebaepochs; // calcola la media sulle epoche
					fprintf (fout, "%d� cycle : %d\n",i,(int) (*(distanceaverage_ave_epoch+i)));
				}
			} else if (test_output_type==1) {
				fprintf (fout, "Distance\n");			
				
				for (i=1;i<sweeps;i++) {
					*(distanceaverage_ave_epoch+i)=*(distanceaverage_ave_epoch+i)/amoebaepochs; // calcola la media sulle epoche
					fprintf (fout, "%d\n",(int) (*(distanceaverage_ave_epoch+i)));
				}
			}
		}

		fclose (fout);
		// Misura statistica n.4
		/*if ((nraces>0) && (test_nrace>0))			// se deve calcolarla solo su una specie
			sprintf(fileout,"DistanceStandardDeviation_S%d_species%d.txt",test_seed,test_nrace-1);
		else
			sprintf(fileout,"DistanceStandardDeviation_S%d.txt",test_seed);

		fout=fopen(fileout,"w");							// file di analisi statistica

		if (fout!=NULL) {
		    fprintf (fout, "\t   Distance Standard Deviation per cycle   \n");
			fprintf (fout, "\t-------------------------------------------\n\n");
			
			diststdev_average=0.0;
			for (i=1;i<sweeps;i++) {
				fprintf (fout, "%d� cycle : %f\n",i,*(distancestdev+i));
				diststdev_average+=*(distancestdev+i);	
			}
			fprintf (fout, "\nAverage           : %f\n",diststdev_average/(sweeps-1));

			fclose (fout);
		}*/
	}

	// Misura statistica n.15
	// salvataggio del numero della media delle distanze individuo-individuo pi� vicino per
	// ogni ciclo, su tutte le generazioni
	if /*(test_statistics)*/ (test_distance_average_per_cycle_all_gen) {
		if ((nraces>0) && (test_nrace>0))			// se deve calcolarla solo su una specie
			sprintf(fileout,"DistanceAverage_S%d_species%d_all_gen.txt",test_seed,test_nrace-1);
		else
			sprintf(fileout,"DistanceAverage_S%d_all_gen.txt",test_seed);

		fout=fopen(fileout,"a");							// file di analisi statistica

		if (fout!=NULL) {
			fprintf (fout, "\nGeneration %d : ",gen);

			distave_average=0.0;
			for (i=1;i<sweeps;i++) {
				*(distanceaverage_ave_epoch+i)=*(distanceaverage_ave_epoch+i)/amoebaepochs; // calcola la media sulle epoche
				distave_average=distave_average+*(distanceaverage_ave_epoch+i);
			}
			distave_average=distave_average/sweeps;
			fprintf (fout, "%d ",(int) (distave_average));

			fclose (fout);
		}
	}

	// salvataggio del numero della media delle distanze della prima parte della vita, della seconda, e dell'intera vita
	if /*(test_statistics)*/ (test_distance_average) {
		// Misura statistica n.9
		if ((nraces>0) && (test_nrace>0))			// se deve calcolarla solo su una specie
			sprintf(fileout,"DistAverage_firsthalf_secondhalf_entirelife_S%d_species%d.txt",test_seed,test_nrace-1);
		else
			sprintf(fileout,"DistAverage_firsthalf_secondhalf_entirelife_S%d.txt",test_seed);

		fout=fopen(fileout,"w");							// file di analisi statistica

		if (fout!=NULL) {

			avefirsthalflife=0;
			// Calculate average with 0
			for (k=0;k<amoebaepochs;k++) 
				avefirsthalflife+=*(firsthalflifedistave+k);
			avefirsthalflife=avefirsthalflife/amoebaepochs;

			avesecondhalflife=0;
			// Calculate average with 0
			for (k=0;k<amoebaepochs;k++) 
				avesecondhalflife+=*(secondhalflifedistave+k);
			avesecondhalflife=avesecondhalflife/amoebaepochs;

			aveentirelife=0;
			// Calculate average with 0
			for (k=0;k<amoebaepochs;k++) 
				aveentirelife+=*(entirelifedistave+k);
			aveentirelife=aveentirelife/amoebaepochs;

			fprintf (fout, "Distance Average - First Half Life : %d\n",(int) avefirsthalflife);
			fprintf (fout, "Distance Average - Second Half Life : %d\n",(int) avesecondhalflife);
			fprintf (fout, "Distance Average - Entire life : %d\n\n",(int) aveentirelife);
		}
	}

	// salvataggio del numero della media degli individui nel vicinato della prima parte della vita, della seconda, e dell'intera vita
	if /*(test_statistics)*/ (test_nindintoneighborhood) {
		// Misura statistica n.10
		if ((nraces>0) && (test_nrace>0))			// se deve calcolarla solo su una specie
			sprintf(fileout,"neighborhood_firsthalf_secondhalf_entirelife_S%d_species%d.txt",test_seed,test_nrace-1);
		else
			sprintf(fileout,"neighborhood_firsthalf_secondhalf_entirelife_S%d.txt",test_seed);

		fout=fopen(fileout,"w");							// file di analisi statistica

		if (fout!=NULL) {

			avefirsthalflife=0;
			// Calculate average with 0
			for (k=0;k<amoebaepochs;k++) 
				avefirsthalflife+=*(firsthalflifenindintoneighborhoodave+k);
			avefirsthalflife=avefirsthalflife/amoebaepochs;

			avesecondhalflife=0;
			// Calculate average with 0
			for (k=0;k<amoebaepochs;k++) 
				avesecondhalflife+=*(secondhalflifenindintoneighborhoodave+k);
			avesecondhalflife=avesecondhalflife/amoebaepochs;

			aveentirelife=0;
			// Calculate average with 0
			for (k=0;k<amoebaepochs;k++) 
				aveentirelife+=*(entirelifenindintoneighborhoodave+k);
			aveentirelife=aveentirelife/amoebaepochs;

			fprintf (fout, "Neighborhood Average - First Half Life : %d\n",(int) avefirsthalflife);
			fprintf (fout, "Neighborhood Average - Second Half Life : %d\n",(int) avesecondhalflife);
			fprintf (fout, "Neighborhood Average - Entire life : %d\n\n",(int) aveentirelife);
		}
	}

	// Misura statistica n.14 (test di velocit� per una popolazione di predatori di mangiare una percentuale della popolazione di prede)
	if ((!labtest_is_running) && (test_times_all_gen_eco) /*&& (test_statistics) */) {
		// calculates success percentade of individual 1 and 2
		sprintf(fileout,"Measure_of_Times_Predators_eat_Preies_one_gen_testS%d.txt",test_seed);
		fout=fopen(fileout,"w");							// file di analisi statistica	

		if (fout!=NULL) {
			timeaverage=0;
			ntimes=0;
			// Calculate average with 0
			for (k=0;k<amoebaepochs;k++) {
				if (*(successruntime+k)==0)
					*(successruntime+k)=sweeps;			// Gli 0 li imposta al numero massimo dei cicli

				ntimes++;				// nel calcolo del tempo medio gli 0 li considera pari al tempo di vita

				timeaverage+=(float) *(successruntime+k);
			}
			timeaverage=timeaverage/ntimes;

			fprintf (fout, "%d ",(int) timeaverage);

		}
		fflush(fout);
		if (!ferror(fout))
			fclose (fout);
	}

	// Misura statistica n.18 (test delle distanze dal baricentro) - calcolo delle medie a fine generazione
	// E' necessario mediare su tutte le epoche a fine generazione
	if ((test_barycentre_distances) || (test_barycentre_distances_all_gen) /*&& (test_statistics)*/) {
		x_size=sweeps;
		y_size=nteam;

		for (i=0;i<x_size;i++)
			for (j=0;j<y_size;j++)
				barycentredistancesarray[i][j] = barycentredistancesarray[i][j] / amoebaepochs;			// Calcolo della media sulle epoche
	}
	
	return(totfitness);
}

float ffitness_wallfollowing(struct individual *pind) 
{
	float maxIn; //proximity to the wall
	float velAhead;//velocity
	float maxIR;
	int mt;
	mt=0;
	if(ipar->motmemory>0) mt=2;

	//wall following component
	if (pind->input[0+mt]>0.7) //12cm
	maxIn=1.0;
	else
	maxIn=pind->input[0+mt];

	maxIR=pind->input[mt+1];
	if (pind->input[mt+2]>maxIR) maxIR=pind->input[mt+2];

	velAhead=((pind->wheel_motor[0]+pind->wheel_motor[1])/2.0)*(1-sqrt(fabs(pind->wheel_motor[0]-pind->wheel_motor[1])));
	//velAhead=fabs((pind->wheel_motor[0]+pind->wheel_motor[1])/2.0-0.5)*(1-sqrt(fabs(pind->wheel_motor[0]-pind->wheel_motor[1])));
	
	//return ((maxIn-(pind->input[mt+1]+pind->input[mt+2])/2.0)+velAhead*2)/3.0;
	return ((maxIn-maxIR)+2*velAhead)/3.0;
	//return velAhead; only speed ahead

}

float ffitness_exploration(struct individual *pind) //usa la distanza dal primo ostacolo rosso
{
	//exploration component
	int r,c;
	float res=0;
	float maxIn;
	
	r=((pind->pos[0]/(float)3000.0)*100.0);
	c=((pind->pos[1]/(float)3000.0)*100.0);
	if (pind->grid[r][c]==0)
	{
		res=1.0;
		pind->grid[r][c]=1;
		
	}
	if(r>100 || c>100) exit(0);
	//wall following component
	if (pind->input[0]>0.7)
	maxIn=1.0;
	else
	maxIn=pind->input[0];
	//here we add red obstacle component
	return res*2+pind->input[5]+(maxIn-(pind->input[1]+pind->input[2])/2.0)/2.0;


}
//fitness function that reward robot if it is near the red obstacle inner fitness
float ffitness_near_red(struct individual *pind)
{
	return pind->input[5]; //che se cis ono 6 sensori, tre ir e tre camere, il 6 � cx2 che si attiva con la vicinanza del blob

}
//fitness function that reward robot if it is near the red obstacle
float ffitness_near_red2(struct individual *pind)
{
	struct envobject *obj;
   float vel1, vel2;
   float ave_mot;              
   float dif_m;                
   float maxs;  
   int   s;

   vel1 = pind->speed[0];
   vel2 = pind->speed[1];
   ave_mot = (float) ((vel1 / (float) 10.0) + (vel2 / (float) 10.0)) / (float) 2.0;
   dif_m   = (float) sqrt(fabs( ((vel1 + (float) 10.0) / (float) 20.0) - ((vel2 + (float) 10.0) / (float) 20.0) ) );
   for(s=0, maxs = (float) 0.0; s < ipar->nifsensors; s++)
       if (pind->cifsensors[s] > maxs)
         maxs = pind->cifsensors[s];
   
	
	
	//da finire
	
	obj=*(envobjs + SROUND);//first sround object
	/*
	if (cepoch%2==0) 

	obj->c[0]=0.0;
	else
	obj->c[0]=1.0;*/
	
	if(pind->input[5]>0.0)
	{
	return pind->input[5];
	//return (1.0-(mdist((float)obj->x,(float)obj->y, (float)pind->pos[0],(float)pind->pos[1])/4240.0));
	}
	else
	{
	
	return (ave_mot*(1.0 - dif_m));// * (1.0 - maxs));
	}

	//return (1.0-(mdist((float)obj->x,(float)obj->y, (float)pind->pos[0],(float)pind->pos[1])/4240.0));
	
}


/*
 * obstacle-avoidance fitness function
 * reward for moving foward far from obstacles 
 */
float
ffitness_obstacle_avoidance(struct  individual  *pind)

{
   float vel1, vel2;
   float ave_mot;              
   float dif_m;                
   float maxs;  
   int   s;

   vel1 = pind->speed[0];
   vel2 = pind->speed[1];
   ave_mot = (float) ((vel1 / (float) 10.0) + (vel2 / (float) 10.0)) / (float) 2.0;
   dif_m   = (float) sqrt(fabs( ((vel1 + (float) 10.0) / (float) 20.0) - ((vel2 + (float) 10.0) / (float) 20.0) ) );
   for(s=0, maxs = (float) 0.0; s < ipar->nifsensors; s++)
       if (pind->cifsensors[s] > maxs)
         maxs = pind->cifsensors[s];
   return(ave_mot * (1.0 - dif_m) * (1.0 - maxs));
}


/*
 * stay_on_area fitness function
 * reward for staying on a target area 
 */
float
ffitness_discrim(struct  individual  *pind)

{
	if (read_ground(pind) > 0)
      return(1.0);
    else
      return(0.0);
}

/*
 * stay_on_area fitness function
 * reward for staying on a target area, a single race 
 */
float
ffitness_discrim_race(struct  individual  *pind)

{
	if ((read_ground(pind) > 0) && (((test_nrace>0) && (pind->race==test_nrace-1))||(test_nrace==0)))		// Razza che prende fitness
 	  return(1.0);
    else
      return(0.0);
}

/*
 * stay_on_area fitness function
 * reward for staying on a target area, a single race
 * one race can eat another race
 */
float
ffitness_discrim_predator_eats_prey(struct  individual  *pind)

{
	struct individual *otherpind;

	// Gestisce il feeding
	if ((pind->race==1) && (!pind->died_individual)) {	// per gli individui "carnivori" - predatori
		if (ncrashedind>=0) {							// Vuol dire che il predatore ha incontrato una possibile preda
			otherpind=(ind+ncrashedind);				// Seleziona l'individuo crashato 
			if ((otherpind->race==0) && (!otherpind->died_individual)) {	// se l'altro individo crashato � una preda, allora riceve una premio di fitness
				//if (!(read_ground(otherpind) > 0)) 
					if (!ipar->resurrect_after_die) {
						otherpind->RColor=RColorPreyDeadInd;		// Indviduo mangiato ha un colore da morto	
						otherpind->GColor=GColorPreyDeadInd;
						otherpind->BColor=BColorPreyDeadInd;
						modify_individual_color(otherpind);			// assegna il nuovo colore all'individuo morto	
						otherpind->died_individual = 1;				// preda mangiata e morta
					} else { 
						reset_pos_from_crash_around(otherpind,1);	// riposiziona il robot da un'altra parte dell'ambiente
						move_individual_color(otherpind);			// aggiorna la posizione del suo cilindro-colore
					}
			}
		}
	}

	if ((read_ground(pind) > 0) && (((test_nrace>0) && (pind->race==test_nrace-1))||(test_nrace==0)))		// Razza che prende fitness
		return(1.0);
    else
		return(0.0);
}

/*
 * stay_on_area fitness function
 * reward for staying all together in team on a target area 
 */
float
ffitness_groupdiscrim(struct  individual  *pind)

{
	int i=0;
	int not_in_food_zone=0;
	int	is_around=0;
	struct individual *cind;
	float ground_pind, ground_cind;
	float fit_score=0.0;
	float fit_point=0.0;
	int	nteam_without_ghosts;
	float max;

	nteam_without_ghosts=nteam;
	// Determina il numero di individui totali da considerare nel gruppo per il calcolo della fitess, non considerando gli individui ghost
	for (cind=ind,i=0; i<nteam;i++,cind++) {					// cicla su tutti gli individui
		if (cind->ghost_individual) 
			nteam_without_ghosts--;								// se � individuo fantasma allora decrementa il numero totale di individui nel gruppo
	}

	fit_point=1.0 / nteam_without_ghosts;						// normalizza il valore della fitness per tutto il gruppo, non considera i ghosts
	ground_pind=read_ground(pind);								// legge il ground	
	if ((ground_pind>0) && (!pind->ghost_individual)) {	// non considera gli individui fantasma
		for (cind=ind,i=0; i<nteam;i++,cind++) {				// cicla su tutti gli individui che non siano pind
			if ((cind!=pind) && (!cind->ghost_individual)) {
				
				ground_cind=read_ground(cind);

				// se l'individuo � diverso da pind oppure se non � ghost, controlla che non sia fuori dalla food zone oppure che non sia in una food zone differente
				if (((ground_cind>0) && (ground_pind!=ground_cind)) || (ground_cind<=0))	
					not_in_food_zone=1;
			}
		}

		// Componente di fitness individuale
		if (not_in_food_zone==0)
			fit_score+=fit_point;
	
		//pind->virtualfitness+=fit_point;						// assegna all'individuo una fitness virtuale
		pind->virtualfitness+=1.0;								// assegna all'individuo una fitness virtuale
	}

	// Misura statistica n.14
	if (((test_times_all_gen)||(test_times_all_gen_eco)) && (ipar->motionlessind>0)) /* && (test_statistics)*/ {   
		int	is_around=1;											// imposta ad 1 inizialmente, questo parametro significa che tutti i follower sono in un intorno del leader
		
		// misura il tempo che il follower ci mette per arrivare in un intorno del leader
		for (cind=ind,i=0; i<nteam;i++,cind++) {					// cicla su tutti gli individui
			if (!cind->ghost_individual) 
				if (!((cind->pos[0]<ipar->Xmotionlessind+500) && (cind->pos[0]>ipar->Xmotionlessind-500) && (cind->pos[1]<ipar->Ymotionlessind+500) && (cind->pos[1]>ipar->Ymotionlessind-500)))
					is_around=0;									// se almeno un'individuo non � nell'intorno allora imposta a 0 questo valore
		}

		if ((is_around) && (*(successruntime+cepoch)==0)) {			// se i follower si trovano in un intorno allora incrementa il contatore dei successi
			success_counter++;										// incrementa il numero delle prede mangiate
					
			// initialize array of success times with "amoebaepochs" elements
			*(successruntime+cepoch)=run;							// salva il tempo che impiega a divorare una determinata percentuale di prede		
		}
	}
					
	// Normalizzazione tra 0 e 1 della fitness
	
	max=fit_point;
	fit_score=fit_score/max;

	
	return(fit_score);
}

/*
 * stay_on_area fitness function
 * reward for staying all together in team on a target area 
 */
float ffitness_distributedLeadership(struct  individual  *pind)
{
	float fit_score=0.0;
	
	int i=0;

	int not_in_food_zone=0;
	int not_in_water_zone=0;

	int	is_around=0;
	struct individual *cind;
	
	float food_pind, food_cind;
	float water_pind, water_cind;

	float fit_point=0.0;
	int	nteam_without_ghosts;
	float max;

	nteam_without_ghosts=nteam;

	double IncreaseFoodUnitsMinimumSize=200/pow(2,3.0);
	double IncreaseWaterUnitsMinimumSize=200/pow(2,3.0);

	// Determina il numero di individui totali da considerare nel gruppo per il calcolo della fitess, non considerando gli individui ghost
	// Counts the number of individuals to consider for the fitness computation, because there could be ghost indiviudals in the simulation
	for (cind=ind,i=0; i<nteam;i++,cind++) {					// cicla su tutti gli individui
		if (cind->ghost_individual) 
			nteam_without_ghosts--;								// se � individuo fantasma allora decrementa il numero totale di individui nel gruppo
	}

	//	return a fraction of 1 based upon how many robots are in the same environment
	fit_point=1.0 / nteam_without_ghosts;						// normalizza il valore della fitness per tutto il gruppo, non considera i ghosts
																//	normalised fitness across the population in the range (0 >> 1/number of individuals)
	//	Use read_food_colour / read_water_colour
	food_pind = read_food_color(pind);								
	water_pind = read_water_color(pind);

	// legge il ground	
	//	read the ground sensor (of current individual)
	
	//	If we are using day and night cycles only allow accumulation of
	//	food during the night, if not just collect as normal
	if ((ipar->switchDayNightEvery > 0) && (ipar->dayNightSensor > 0) && (isDaytime))
	{
		//	Do nothing
	}
	else
	{
		//	Check that all the robots are in the food zone
		if ((food_pind>0) && (!pind->ghost_individual)) 
		{			// non considera gli individui fantasma
			//	if in the zone and not a ghost individual, check if all the other partners are in the food zone too

			for (cind=ind,i=0; i<nteam;i++,cind++) 
			{				// cicla su tutti gli individui che non siano pind
				//	loop through the other inidividuals (not the current)
				if ((cind!=pind) && (!cind->ghost_individual)) 
				{

					food_cind=read_food_color(cind);

					// se l'individuo � diverso da pind oppure se non � ghost, controlla che non sia fuori dalla food zone oppure che non sia in una food zone differente
					//	If the ground sensor of the other individual is not activated or in the same zone
					if (((food_cind>0) && (food_pind!=food_cind)) || (food_cind<=0))	
						//	Set the flag that we are not in the same zone
						not_in_food_zone=1;
				}
			}

			//	if this flag is zero it means all agents are in
			//	the same zone.  therefore increase the fitness 
			//	accumulator of the current individual
			//	and set a flag to increase the hungerAccumulator
			if (not_in_food_zone==0)
			{
				//	hungerAccumulator will be incremented
				incrementHunger=true;
				
				if (foodAccumulator < sweeps/2)
					foodAccumulator+=fit_point;
			}
			else
			{
				//	hungerAccumulator will be decremented
				incrementHunger=false;
			}

			//pind->virtualfitness+=fit_point;						// assegna all'individuo una fitness virtuale
			//pind->virtualfitness+=1.0;								// assegna all'individuo una fitness virtuale
			//	used to keep track of the number of time steps the individual is in the zone for evaluation
		}
	}

	//	If we are using day and night cycles only allow accumulation of
	//	water during the day, if not just collect as normal
	if ((ipar->switchDayNightEvery > 0) && (ipar->dayNightSensor > 0)  && (!isDaytime))
	{
		//	Do nothing
	}
	else
	{
		//	Check that all the robots are in the food zone
		if ((water_pind>0) && (!pind->ghost_individual)) 
		{			// non considera gli individui fantasma
			//	if in the zone and not a ghost individual, check if all the other partners are in the food zone too

			for (cind=ind,i=0; i<nteam;i++,cind++) 
			{				// cicla su tutti gli individui che non siano pind
				//	loop through the other inidividuals (not the current)
				if ((cind!=pind) && (!cind->ghost_individual)) 
				{

					water_cind=read_water_color(cind);

					// se l'individuo � diverso da pind oppure se non � ghost, controlla che non sia fuori dalla food zone oppure che non sia in una food zone differente
					//	If the ground sensor of the other individual is not activated or in the same zone
					if (((water_cind>0) && (water_pind!=water_cind)) || (water_cind<=0))	
						//	Set the flag that we are not in the same zone
						not_in_water_zone=1;
				}
			}

			// Componente di fitness individuale

			//	if flag is zero it means all agents are in 
			//	the same zone.  therefore increase the fitness
			//	accumulator of the current individual 
			//	and set a flag to increase the thirstAccumulator
			if (not_in_water_zone==0)
			{
				//	thirst accumulator will be incremented
				incrementThirst=true;

				if (waterAccumulator < sweeps/2)
					waterAccumulator+=fit_point;
			}
			else
			{
				//	thirst accumulator will be decremented
				incrementThirst=false;
			}

			/*else if ((not_in_water_zone!=0) && (thirstAccumulator > 0))
			{
				if ((((int) run) % ((int) IncreaseWaterUnitsMinimumSize)==0) && (pind->number==nteam_without_ghosts-1))
					thirstAccumulator-=1;
			}*/
			//pind->virtualfitness+=fit_point;						// assegna all'individuo una fitness virtuale
			//pind->virtualfitness+=1.0;								// assegna all'individuo una fitness virtuale
			//	used to keep track of the number of time steps the individual is in the zone for evaluation
		}
	}
	
	//	TODO:Define global variables for food and water
	//	TODO:increase food and water (when all robots are in the relevant zone)
	
	return((foodAccumulator + waterAccumulator - abs(foodAccumulator - waterAccumulator))/(double)sweeps);
}

/*
 * stay_on_area fitness function
 * reward for staying all together in team on a target area 
 */
float ffitness_distributedLeadership_Simplified(struct  individual  *pind)
{
	float fit_score=0.0;
	
	int i=0;

	int not_in_food_zone=0;
	int not_in_water_zone=0;

	int	is_around=0;
	struct individual *cind;
	
	float food_pind, food_cind;
	float water_pind, water_cind;

	float fit_point=0.0;
	int	nteam_without_ghosts;
	float max;

	nteam_without_ghosts=nteam;

	double IncreaseFoodUnitsMinimumSize=200/pow(2,3.0);
	double IncreaseWaterUnitsMinimumSize=200/pow(2,3.0);

	// Determina il numero di individui totali da considerare nel gruppo per il calcolo della fitess, non considerando gli individui ghost
	// Counts the number of individuals to consider for the fitness computation, because there could be ghost indiviudals in the simulation
	for (cind=ind,i=0; i<nteam;i++,cind++) {					// cicla su tutti gli individui
		if (cind->ghost_individual) 
			nteam_without_ghosts--;								// se � individuo fantasma allora decrementa il numero totale di individui nel gruppo
	}

	//	return a fraction of 1 based upon how many robots are in the same environment
	fit_point=1.0 / nteam_without_ghosts;						// normalizza il valore della fitness per tutto il gruppo, non considera i ghosts
																//	normalised fitness across the population in the range (0 >> 1/number of individuals)
	//	Use read_food_colour / read_water_colour
	food_pind = read_food_color(pind);								
	water_pind = read_water_color(pind);

	// legge il ground	
	//	read the ground sensor (of current individual)
	
	//	If we are using day and night cycles only allow accumulation of
	//	food during the night, if not just collect as normal
	if ((ipar->switchDayNightEvery > 0) && (ipar->dayNightSensor > 0) && (isDaytime))
	{
		//	Do nothing
	}
	else
	{
		//	Check that all the robots are in the food zone
		if ((food_pind>0) && (!pind->ghost_individual)) 
		{			// non considera gli individui fantasma
			//	if in the zone and not a ghost individual, check if all the other partners are in the food zone too

			for (cind=ind,i=0; i<nteam;i++,cind++) 
			{				// cicla su tutti gli individui che non siano pind
				//	loop through the other inidividuals (not the current)
				if ((cind!=pind) && (!cind->ghost_individual)) 
				{

					food_cind=read_food_color(cind);

					// se l'individuo � diverso da pind oppure se non � ghost, controlla che non sia fuori dalla food zone oppure che non sia in una food zone differente
					//	If the ground sensor of the other individual is not activated or in the same zone
					if (((food_cind>0) && (food_pind!=food_cind)) || (food_cind<=0))	
						//	Set the flag that we are not in the same zone
						not_in_food_zone=1;
				}
			}

			// Componente di fitness individuale
			//	if flag is zero increase the fitness of the current individual
			if (not_in_food_zone==0)
			{
				incrementHunger=true;
				if (foodAccumulator < sweeps/2)
					foodAccumulator+=fit_point;
			}
			else
			{
				incrementHunger=false;
			}

			//pind->virtualfitness+=fit_point;						// assegna all'individuo una fitness virtuale
			//pind->virtualfitness+=1.0;								// assegna all'individuo una fitness virtuale
			//	used to keep track of the number of time steps the individual is in the zone for evaluation
		}
	}

	//	If we are using day and night cycles only allow accumulation of
	//	water during the day, if not just collect as normal
	if ((ipar->switchDayNightEvery > 0) && (ipar->dayNightSensor > 0)  && (!isDaytime))
	{
		//	Do nothing
	}
	else
	{
		//	Check that all the robots are in the food zone
		if ((water_pind>0) && (!pind->ghost_individual)) 
		{			// non considera gli individui fantasma
			//	if in the zone and not a ghost individual, check if all the other partners are in the food zone too

			for (cind=ind,i=0; i<nteam;i++,cind++) 
			{				// cicla su tutti gli individui che non siano pind
				//	loop through the other inidividuals (not the current)
				if ((cind!=pind) && (!cind->ghost_individual)) 
				{

					water_cind=read_water_color(cind);

					// se l'individuo � diverso da pind oppure se non � ghost, controlla che non sia fuori dalla food zone oppure che non sia in una food zone differente
					//	If the ground sensor of the other individual is not activated or in the same zone
					if (((water_cind>0) && (water_pind!=water_cind)) || (water_cind<=0))	
						//	Set the flag that we are not in the same zone
						not_in_water_zone=1;
				}
			}

			// Componente di fitness individuale
			//	if flag is zero increase the fitness of the current individual
			if (not_in_water_zone==0)
			{
				incrementThirst=true;
				if (waterAccumulator < sweeps/2)
					waterAccumulator+=fit_point;
			}
			else
			{
				incrementThirst=false;
			}

			/*else if ((not_in_water_zone!=0) && (thirstAccumulator > 0))
			{
				if ((((int) run) % ((int) IncreaseWaterUnitsMinimumSize)==0) && (pind->number==nteam_without_ghosts-1))
					thirstAccumulator-=1;
			}*/
			//pind->virtualfitness+=fit_point;						// assegna all'individuo una fitness virtuale
			//pind->virtualfitness+=1.0;								// assegna all'individuo una fitness virtuale
			//	used to keep track of the number of time steps the individual is in the zone for evaluation
		}
	}
	
	//	TODO:Define global variables for food and water
	//	TODO:increase food and water (when all robots are in the relevant zone)
	
	return((foodAccumulator + waterAccumulator)/(double)sweeps);
}

/*
 * stay_on_area fitness function
 * reward for staying all together in team on a target area 
 */
float ffitness_distributedLeadershipUnnormalised(struct  individual  *pind)
{
	float fit_score=0.0;
	
	int i=0;

	int not_in_food_zone=0;
	int not_in_water_zone=0;

	int	is_around=0;
	struct individual *cind;
	
	float food_pind, food_cind;
	float water_pind, water_cind;

	float fit_point=0.0;
	int	nteam_without_ghosts;
	float max;

	nteam_without_ghosts=nteam;

	double IncreaseFoodUnitsMinimumSize=200/pow(2,3.0);
	double IncreaseWaterUnitsMinimumSize=200/pow(2,3.0);

	// Determina il numero di individui totali da considerare nel gruppo per il calcolo della fitess, non considerando gli individui ghost
	// Counts the number of individuals to consider for the fitness computation, because there could be ghost indiviudals in the simulation
	for (cind=ind,i=0; i<nteam;i++,cind++) {					// cicla su tutti gli individui
		if (cind->ghost_individual) 
			nteam_without_ghosts--;								// se � individuo fantasma allora decrementa il numero totale di individui nel gruppo
	}

	//	return a fraction of 1 based upon how many robots are in the same environment
	fit_point=1.0 / nteam_without_ghosts;						// normalizza il valore della fitness per tutto il gruppo, non considera i ghosts
																//	normalised fitness across the population in the range (0 >> 1/number of individuals)
	//	Use read_food_colour / read_water_colour
	food_pind = read_food_color(pind);								
	water_pind = read_water_color(pind);

	// legge il ground	
	//	read the ground sensor (of current individual)
	
	//	If we are using day and night cycles only allow accumulation of
	//	food during the night, if not just collect as normal
	if ((ipar->switchDayNightEvery > 0) && (ipar->dayNightSensor > 0) && (isDaytime))
	{
		//	Do nothing
	}
	else
	{
		//	Check that all the robots are in the food zone
		if ((food_pind>0) && (!pind->ghost_individual)) 
		{			// non considera gli individui fantasma
			//	if in the zone and not a ghost individual, check if all the other partners are in the food zone too

			for (cind=ind,i=0; i<nteam;i++,cind++) 
			{				// cicla su tutti gli individui che non siano pind
				//	loop through the other inidividuals (not the current)
				if ((cind!=pind) && (!cind->ghost_individual)) 
				{

					food_cind=read_food_color(cind);

					// se l'individuo � diverso da pind oppure se non � ghost, controlla che non sia fuori dalla food zone oppure che non sia in una food zone differente
					//	If the ground sensor of the other individual is not activated or in the same zone
					if (((food_cind>0) && (food_pind!=food_cind)) || (food_cind<=0))	
						//	Set the flag that we are not in the same zone
						not_in_food_zone=1;
				}
			}

			// Componente di fitness individuale
			//	if flag is zero increase the fitness of the current individual
			if (not_in_food_zone==0)
			{
				incrementHunger=true;
				
				if (foodAccumulator < sweeps/2)
					foodAccumulator+=fit_point;
			}
			else
			{
				incrementHunger=false;
			}

			//pind->virtualfitness+=fit_point;						// assegna all'individuo una fitness virtuale
			//pind->virtualfitness+=1.0;								// assegna all'individuo una fitness virtuale
			//	used to keep track of the number of time steps the individual is in the zone for evaluation
		}
	}

	//	If we are using day and night cycles only allow accumulation of
	//	water during the day, if not just collect as normal
	if ((ipar->switchDayNightEvery > 0) && (ipar->dayNightSensor > 0)  && (!isDaytime))
	{
		//	Do nothing
	}
	else
	{
		//	Check that all the robots are in the food zone
		if ((water_pind>0) && (!pind->ghost_individual)) 
		{			// non considera gli individui fantasma
			//	if in the zone and not a ghost individual, check if all the other partners are in the food zone too

			for (cind=ind,i=0; i<nteam;i++,cind++) 
			{				// cicla su tutti gli individui che non siano pind
				//	loop through the other inidividuals (not the current)
				if ((cind!=pind) && (!cind->ghost_individual)) 
				{

					water_cind=read_water_color(cind);

					// se l'individuo � diverso da pind oppure se non � ghost, controlla che non sia fuori dalla food zone oppure che non sia in una food zone differente
					//	If the ground sensor of the other individual is not activated or in the same zone
					if (((water_cind>0) && (water_pind!=water_cind)) || (water_cind<=0))	
						//	Set the flag that we are not in the same zone
						not_in_water_zone=1;
				}
			}

			// Componente di fitness individuale
			//	if flag is zero increase the fitness of the current individual
			if (not_in_water_zone==0)
			{
				incrementThirst=true;
				if (waterAccumulator < sweeps/2)
					waterAccumulator+=fit_point;
			}
			else
			{
				incrementThirst=false;
			}

			/*else if ((not_in_water_zone!=0) && (thirstAccumulator > 0))
			{
				if ((((int) run) % ((int) IncreaseWaterUnitsMinimumSize)==0) && (pind->number==nteam_without_ghosts-1))
					thirstAccumulator-=1;
			}*/
			//pind->virtualfitness+=fit_point;						// assegna all'individuo una fitness virtuale
			//pind->virtualfitness+=1.0;								// assegna all'individuo una fitness virtuale
			//	used to keep track of the number of time steps the individual is in the zone for evaluation
		}
	}
	
	//	TODO:Define global variables for food and water
	//	TODO:increase food and water (when all robots are in the relevant zone)
	
	return(foodAccumulator + waterAccumulator - abs(foodAccumulator - waterAccumulator));
}
/*
 * stay_on_area fitness function
 * reward for staying all together in team on a target area
 * reward for staying individually on a target area
 * NOTA : ha due grandi problemi : 1) le fitness degli individui vengono asimmetriche e non si pu� stabilire qual'� la fitness di gruppo
								   2) non si pu� stabilire una correlazione tra fitness e leadership di una replica in quanto non si 
									  sa se la fitness dipende dal coordinamento di gruppo o dalle fitness individuali che per qualche
									  ragione in un seed sono alte. 
 */
float
ffitness_groupdiscrim_1(struct  individual  *pind)

{
	int i=0;
	int not_in_food_zone=0;
	struct individual *cind;
	float ground_pind, ground_cind;
	float fit_score=0.0;
	float fit_point=0.0;
	int	nteam_without_ghosts;

	nteam_without_ghosts=nteam;
	// Determina il numero di individui totali da considerare nel gruppo per il calcolo della fitess, non considerando gli individui ghost
	for (cind=ind,i=0; i<nteam;i++,cind++) {					// cicla su tutti gli individui
		if (cind->ghost_individual) 
			nteam_without_ghosts--;								// se � individuo fantasma allora decrementa il numero totale di individui nel gruppo
	}

	fit_point=1.0 / nteam_without_ghosts;						// normalizza il valore della fitness per tutto il gruppo, non considera i ghosts
	ground_pind=read_ground(pind);								// legge il ground	
	if ((ground_pind>0) && (!pind->ghost_individual)) {			// non considera gli individui fantasma
		for (cind=ind,i=0; i<nteam;i++,cind++) {				// cicla su tutti gli individui che non siano pind
			if ((cind!=pind) && (!cind->ghost_individual)) {
				
				ground_cind=read_ground(cind);

				// se l'individuo � diverso da pind oppure se non � ghost, controlla che non sia fuori dalla food zone oppure che non sia in una food zone differente
				if (((ground_cind>0) && (ground_pind!=ground_cind)) || (ground_cind<=0))	
					not_in_food_zone=1;
			}
		}

		// Componente di fitness collettiva reale
		if (not_in_food_zone==0)
			fit_score+=fit_point;

		// Componente di fitness individuale reale
		fit_score+=fit_point;					
		
		// Assegna la fitness individuale virtuale
		pind->virtualfitness+=fit_point;			
	}
	
	return(fit_score);
}

/*
 * stay_on_area fitness function
 * reward for staying all together in team on a target area
 * example
 * .25 if one stay alone
 * .50 if two are in food zone
 * .75 if three are in food zone
 * 1 if four are in food zone
 * at the end each individual receive the fitness of the best
 */
float
ffitness_groupdiscrim_2(struct  individual  *pind)

{
	int i=0;
	int not_in_food_zone=0;
	int n_ind_in_the_same_fz=0;									// numero di individui nella medesima food zone
	struct individual *cind;
	float ground_pind, ground_cind;
	float fit_score=0.0;
	float fit_point=0.0;
	int	nteam_without_ghosts;
	float max, major;
	
	nteam_without_ghosts=nteam;
	// Determina il numero di individui totali da considerare nel gruppo per il calcolo della fitess, non considerando gli individui ghost
	for (cind=ind,i=0; i<nteam;i++,cind++) {					// cicla su tutti gli individui
		if (cind->ghost_individual) 
			nteam_without_ghosts--;								// se � individuo fantasma allora decrementa il numero totale di individui nel gruppo
	}

	fit_point=1.0 / nteam_without_ghosts;						// normalizza il valore della fitness per tutto il gruppo, non considera i ghosts
	major=fit_point*nteam_without_ghosts*4;						// premio di maggioranza

	ground_pind=read_ground(pind);								// legge il ground	

	if ((ground_pind>0) && (!pind->ghost_individual)) {			// non considera gli individui fantasma
		n_ind_in_the_same_fz=0;									// inizializza il contatore degli individui nella medesima food zone
		for (cind=ind,i=0; i<nteam;i++,cind++) {				// cicla su tutti gli individui che non siano pind e che non siano fantasmi a loro volta
			if ((cind!=pind) && (!cind->ghost_individual)) {
				
				ground_cind=read_ground(cind);

				if ((ground_cind>0) && (ground_pind==ground_cind))							// se invece � nella medesima food zone allora incrementa il contatore degli individui nella medesima food zone
					n_ind_in_the_same_fz++;
			}
		}

		// Componente di fitness collettiva reale
		fit_score+=n_ind_in_the_same_fz*fit_point;											// premio proporzionale al numero di individui nella medesima food zone 

		// Componente di fitness individuale reale
		fit_score+=fit_point;					
		
		// Componente premio di maggioranza : ossia fornisce un premio sostanzioso quando tutto il gruppo � presente
		// nella medesima food zone. Questo dovrebbe ridurre le componenti spurie nei valori di fitness dovute alle componenti
		// parziali della funzione di fitness. Se molti gruppi da 1 o 2 individui vanno nella food zone competono con i valori 
		// di fitness ottenuti in presenza di coordinamento globale.
		if (n_ind_in_the_same_fz==nteam_without_ghosts-1)	
			fit_score+=major;
		
		// Assegna la fitness individuale virtuale
		pind->virtualfitness+=fit_point;			
	}

	// Normalizzazione tra 0 e 1 della fitness
	max=fit_point*nteam_without_ghosts+major;
	fit_score=fit_score/max;
	
	return(fit_score);
}


/*
 * predator - prey fitness function
 * prey have to eat food
 */
float
ffitness_predator_prey1(struct  individual  *pind)
{
	struct individual *otherpind;

	if ((pind->race==0) && (!pind->died_individual))	{			// fitness degli individui "erbivori" - prede, se una preda � morta non riceve fitness, non pu� mangiare
		if (read_ground(pind) > 0)								// alla prima specie applica la fitness del cibo
			return(1.0);
		else
			return(0.0);
	} else if ((pind->race==1) && (!pind->died_individual)) {	// fitness degli individui "carnivori" - predatori
		if (ncrashedind>=0) {									// Vuol dire che il predatore ha incontrato una possibile preda
			otherpind=(ind+ncrashedind);						// Seleziona l'individuo crashato 
			if ((otherpind->race==0) && (!otherpind->died_individual)) {	// se l'altro individo crashato � una preda, allora riceve una premio di fitness
				otherpind->RColor=RColorPreyDeadInd;				// Indviduo mangiato ha un colore da morto	
				otherpind->GColor=GColorPreyDeadInd;
				otherpind->BColor=BColorPreyDeadInd;
				modify_individual_color(otherpind);				// assegna il nuovo colore all'individuo morto	
				otherpind->died_individual = 1;					// preda mangiata e morta
				otherpind->fitness = 0.0;						// azzeramento della fitness della preda in modo che non posso partecipare al ranking
				return (1.0);									// mangia l'individuo e l'individuo mangiato muore
			} else {
				return (0.0);				
			}

		} else return (0.0);
	} else return (0.0);
}

/*
 * predator - prey fitness function
 * prey have just to run far from predator
 */
float
ffitness_predator_prey2(struct  individual  *pind)
{
	struct individual *otherpind;
	float fitscore=1.0/sweeps;									// punteggio parziale di fitness per le prede

	if ((pind->race==0) && (!pind->died_individual))	{		// fitness degli individui "erbivori" - prede, se una preda � morta non riceve fitness, non pu� mangiare
			return(fitscore);									// alla prima specie delle prede applica la 
																// fitness che premia chi sopravvive di pi�
	} else if ((pind->race==1) && (!pind->died_individual)) {	// fitness degli individui "carnivori" - predatori
		if (ncrashedind>=0) {									// Vuol dire che il predatore ha incontrato una possibile preda
			otherpind=(ind+ncrashedind);						// Seleziona l'individuo crashato 
			if ((otherpind->race==0) && (!otherpind->died_individual)) {	// se l'altro individo crashato � una preda, allora riceve una premio di fitness
				if (!ipar->resurrect_after_die) {
					otherpind->RColor=RColorPreyDeadInd;		// Indviduo mangiato ha un colore da morto	
					otherpind->GColor=GColorPreyDeadInd;
					otherpind->BColor=BColorPreyDeadInd;
					modify_individual_color(otherpind);			// assegna il nuovo colore all'individuo morto	
					otherpind->died_individual = 1;				// preda mangiata e morta
					
					// Misura statistica n.5
					if ((test_times)||(test_times_all_gen)) /* && (test_statistics)*/ {   
						// misura il tempo in cui una preda viene divorata					
						if (*(successruntime+cepoch)==0) {
							*(successruntime+cepoch)=run;	// memorizza il tempo in cui una preda viene mangiata
							success_counter++;				// incrementa il numero delle prede mangiate
						}

						if (ipar->lab_ntestingteam > 2) {
							if (pind->number==0) {			// Memorizza i tempi del second leader
								*(successfirstsideruntime+cepoch)=run;
								first_success_counter++;				// incrementa il numero delle prede mangiate per il second leader
							} else if (pind->number==2) { 	// Memorizza i tempi del first leader
								*(successsecondsideruntime+cepoch)=run;
								second_success_counter++;				// incrementa il numero delle prede mangiate per il first leader
							}
						}
					}

					// Misura statistica n.14
					if (test_times_all_gen_eco) /* && (test_statistics)*/ {   
						// misura il numero di prede divorate					
						success_counter++;				// incrementa il numero delle prede mangiate
					
						// initialize array of success times with "amoebaepochs" elements
						if (success_counter==((nindividuals/nraces)*test_times_ntestpop / 100))					// 100:75=20:x
							*(successruntime+cepoch)=run;			// salva il tempo che impiega a divorare una determinata percentuale di prede		
					}
				} else { 
					//otherpind->fitness=otherpind->fitness/2;	// se l'individuo muore e pu� resuscitare allora la sua fitness viene dimezzata al fine di penalizzarlo
					otherpind->fitness=0.0;						// se l'individuo muore la sua fitness viene azzerata	
					reset_pos_from_crash_around(otherpind,1);	// riposiziona il robot da un'altra parte dell'ambiente
					move_individual_color(otherpind);			// aggiorna la posizione del suo cilindro-colore

					// Misura statistica n.14
					if (test_times_all_gen_eco) /* && (test_statistics)*/ {   
						// misura il numero di prede divorate					
						success_counter++;				// incrementa il numero delle prede mangiate
					
						// initialize array of success times with "amoebaepochs" elements
						if (success_counter==((nindividuals/nraces)*test_times_ntestpop / 100))					// 100:75=20:x
							*(successruntime+cepoch)=run;			// salva il tempo che impiega a divorare una determinata percentuale di prede		
					}
				}
				return (1.0);									// mangia l'individuo e l'individuo mangiato muore
			} else {
				return (0.0);				
			}

		} else return (0.0);
	} else return (0.0);
}

/*
 * Premia se la sbarra interseca entrambe le due target area. 
 * Inoltre la sbarra viene sbloccata se entrambi emettono un segnale intenzionale di 
 * spinta attivo-alto. Altrimenti la sbarra � bloccata.
 * Se un robot emette un segnale alto fuori da un'area critica intorno alle estremit�
 * allora entrambi i robot perdono dei cicli di vita. Il sistema perde cicli. 
 * Infine la fitness non verr� calolata se entrambi i robot non conducono la sbarra ad 
 * obiettivo nello stesso istante di tempo. 
 * Il test se ha successo nel caso "poche ripartenze" viene ripetuto nel 
 * caso "molte ripartenze random".
 */
float
ffitness_corvids14(struct  individual  *pind)

{
	/* Attenzione : per funzionare la fitness necessita che la barra oggetto della funzione sia
		inserita in prima posizione all'interno della lista delle barre 
	    Anche le due target area oggetto dell'intersezione devono essere le prime della lista.
		*/
	float fitscore1=0.0, fitscore2=0.0;			 // punteggio parziale di fitness 1 e 2
	struct envobject *obj_bar;					 // memorizza il primo oggetto barra 
	struct envobject *obj_ground1, *obj_ground2; // memorizza le prime due ground zone 
	double d1,d2,min_d1, min_d2;				 // distanze dalle estremit� della sbarra
	int bool_intersect1=0, bool_intersect2=0;	 // segnalano che la sbarra interseca una delle due target areas 
	struct individual *otherpind;

	float xi,yi;						// coordinate del punto d'intersezione

	// Controlla che un'estremit� della sbarra sia all'interno della food-zone, in tal caso pu� 
	// prendere punteggio di fitness, ossia se e soltanto se sia il robot che la sbarra sono all'
	// interno della foodzone
	obj_bar = *(envobjs + BAR);				// seleziona la prima sbarra
	obj_ground1 = *(envobjs + GROUND);		// seleziona la prima ground zone
	obj_ground2 = (*(envobjs + GROUND))+1;	// seleziona la second ground zone

	if (pind->number==0) otherpind=(pind+1);	// seleziona e punta all'altro individuo 
	if (pind->number==1) otherpind=(pind-1);	// seleziona e punta all'altro individuo

	// Calcolo della distanza minima del robot corrente dalle estremit� della sbarra
	d1=mdist(pind->pos[0],pind->pos[1],obj_bar->x,obj_bar->y);
	d2=mdist(pind->pos[0],pind->pos[1],obj_bar->X,obj_bar->Y);
	min_d1=d1 <= d2 ? d1 : d2;

	// Calcolo della distanza minima dell'altro robot dalle estremit� della sbarra
	d1=mdist(otherpind->pos[0],otherpind->pos[1],obj_bar->x,obj_bar->y);
	d2=mdist(otherpind->pos[0],otherpind->pos[1],obj_bar->X,obj_bar->Y);
	min_d2=d1 <= d2 ? d1 : d2;

	if (ipar->pushingunits>1) {
		// 1) Sono penalizzati con 3 extra-cicli se emettono 1 dalla PU fuori dalle aree critiche
		// Ancora da decidere il numero di cicli di vita. Dovrebbe funzionare bene con 4000 cicli 
		if (((min_d1>Critical_dist) && (pind->pushing_units[0]>0.5)) || 
		   ((min_d2>Critical_dist) && (otherpind->pushing_units[0]>0.5))) {
				// Riduce i cicli di vita di entrambi i robot, in questo modo li punisce se manten-
				// gono per troppo tempo attivo-alto il segnale intenzionale
			    if (n_fixed_evolution_is_running) {		// incrementa gli extracicli solo in fase di evoluzione
					pind->lifecycle+=T_inc;
					pind->totcycles+=T_inc;
					otherpind->lifecycle+=T_inc;
					otherpind->totcycles+=T_inc;
				}
		}	

		// 2) Sono penalizzati con 3 extra-cicli se emettono dentro le aree critiche 
		//			entrambi 0 dalla PU dentro le aree critiche
		//          uno 1 e l'altro 0 dentro le aree critiche
		//	  mentre se emettono entrambi 1 dalla PU allora non sono penalizzati
		if (((min_d1<Critical_dist) && (min_d2<Critical_dist)) && 
			((pind->pushing_units[0]<=0.5) || (otherpind->pushing_units[0]<=0.5))) {
				// Riduce i cicli di vita di entrambi i robot, in questo modo li punisce se manten-
				// gono per troppo tempo attivo-alto il segnale intenzionale
			    if (n_fixed_evolution_is_running) {		// incrementa gli extracicli solo in fase di evoluzione
					pind->lifecycle+=T_inc;
					pind->totcycles+=T_inc;
					otherpind->lifecycle+=T_inc;
					otherpind->totcycles+=T_inc;
				}
		}
	
		// 3) Sono penalizzati molto con 3 extra-cicli se uno tocca la sbarra e l'altro no 
		// Questo dovrebbe simulare e sostituire l'accorciamento della stringa
		if ((pind->touchedbar && !otherpind->touchedbar) || (!pind->touchedbar && otherpind->touchedbar)) {
			// Riduce i cicli di vita di entrambi i robot, in questo modo li punisce se manten-
			// gono per troppo tempo attivo-alto il segnale intenzionale
			if (n_fixed_evolution_is_running) {		// incrementa gli extracicli solo in fase di evoluzione
				pind->lifecycle+=T_inc;
				pind->totcycles+=T_inc;
				otherpind->lifecycle+=T_inc;
				otherpind->totcycles+=T_inc;
			}
		}
	}
	
	// Componente di fitness aggiuntiva per creare un attrattore comportamentale verso lo 
	// spostamento comntemporaneo della sbarra
	/*if ((min_d1<=Critical_dist) && (min_d2<=Critical_dist))  
		if (pind->touchedbar && otherpind->touchedbar)
			fitscore2=0.1;			// recupera il valore della fitness  
	*/

	bool_intersect1=intersect_line_circle(obj_bar->x, obj_bar->y, obj_bar->X, obj_bar->Y , obj_ground1->x, obj_ground1->y, obj_ground1->r, &xi, &yi);
	bool_intersect2=intersect_line_circle(obj_bar->x, obj_bar->y, obj_bar->X, obj_bar->Y , obj_ground2->x, obj_ground2->y, obj_ground2->r, &xi, &yi);
		
	// Memorizza il lifecycle del primo e secondo robot, allorquando essi giungono nella target area
	if ((bool_intersect1) && (pind->number==0) && (memlifecycle1==0)) 
		memlifecycle1=pind->lifecycle;		// memorizza il tempo del primo robot
	if ((bool_intersect2) && (pind->number==1) && (memlifecycle1!=pind->lifecycle) && (memlifecycle2==0)) 
		memlifecycle2=pind->lifecycle;		// memorizza il tempo del secondo robot

	// misura statistica n.2
	if (test_statistics) {  
		// misura il tempo della prima volta di un estremo nella prima target area
		if (intersect_line_circle(obj_bar->x, obj_bar->y, obj_bar->X, obj_bar->Y , obj_ground1->x, obj_ground1->y, obj_ground1->r, &xi, &yi) == 1) {
			if (*(successfirstsideruntime+cepoch)==0) 
				*(successfirstsideruntime+cepoch)=run;	// memorizza il tempo in cui avviene l'intersezione con la prima target area
		}		

		// misura il tempo della prima volta di un estremo nella seconda target area
		if (intersect_line_circle(obj_bar->x, obj_bar->y, obj_bar->X, obj_bar->Y , obj_ground2->x, obj_ground2->y, obj_ground2->r, &xi, &yi) == 1) {
			if (*(successsecondsideruntime+cepoch)==0) 
				*(successsecondsideruntime+cepoch)=run;	// memorizza il tempo in cui avviene l'intersezione con la seconda target area
		}			
	}
	
	// se la sbarra si trova all'interno della prima e della seconda ground zone, e il robot 
	// all'interno di una target area, e la sbarra � sempre stata mantenuta verticale, allora 
	// prendono un punteggio di fitness
	if ((memlifecycle1!=0) && (memlifecycle2!=0) && (abs(memlifecycle1-memlifecycle2)<=differtime) && 
		(bool_intersect1 == 1) && (bool_intersect2 == 1)) { 
			fitscore1=1.0;	// prende 1.0 se la sbarra interseca entrambe le food zone
	
			// misura statistica n.1
			if (((test_times)||(test_times_all_gen)) && (test_statistics))  
				// misura il primo successo utile all'interno di un' epoca
				if (*(successruntime+cepoch)==0) {
					*(successruntime+cepoch)=run;	// memorizza il tempo in cui avviene il successo
					success_counter++;				// incrementa il numero dei successi	
				}		

	} else 
			fitscore1=0.0;
	
	return (fitscore1+fitscore2);
}


/*
 * Premia se la sbarra interseca entrambe le due target area. 
 * Inoltre la sbarra viene sbloccata se entrambi emettono un segnale intenzionale di 
 * spinta attivo-alto. Altrimenti la sbarra � bloccata.
 * Se un robot emette un segnale alto fuori da un'area critica intorno alle estremit�
 * allora entrambi i robot perdono dei cicli di vita. Il sistema perde cicli. 
 * Infine la fitness non verr� calolata se entrambi i robot non conducono la sbarra ad 
 * obiettivo nello stesso istante di tempo. 
 * Il test se ha successo nel caso "poche ripartenze" viene ripetuto nel 
 * caso "molte ripartenze random".
 * C'� anche una componente di fitness esplorativa
 */
float
ffitness_corvids15(struct  individual  *pind)

{
	/* Attenzione : per funzionare la fitness necessita che la barra oggetto della funzione sia
		inserita in prima posizione all'interno della lista delle barre 
	    Anche le due target area oggetto dell'intersezione devono essere le prime della lista.
		*/
	float fitscore1=0.0, fitscore2=0.0;			 // punteggio parziale di fitness 1 e 2
	struct envobject *obj_bar;					 // memorizza il primo oggetto barra 
	struct envobject *obj_ground1, *obj_ground2; // memorizza le prime due ground zone 
	double d1,d2,min_d1, min_d2;				 // distanze dalle estremit� della sbarra
	int bool_intersect1=0, bool_intersect2=0;	 // segnalano che la sbarra interseca una delle due target areas 
	struct individual *otherpind;

	float xi,yi;						// coordinate del punto d'intersezione
	float speed1, speed2;
	float   ave_mot;					// velocit� media dei motori di un robot
	float   dif_m;						// differenza di velocit� tra i motori

	// Calcola la fitness solo sui primi 2 individui altrimenti va in violazione di accesso della memoria
	if (pind->number < 2) {
		// Componente di fitness calcolata per aumentare la capacit� esplorativa del robot
		// devono anche schizzare fuori da situazioni di stallo quando si trovano in prossimit�
		// le velocit� di entrambi i motori devono essere alte
	/*	speed1 = pind->speed[0];
		speed2 = pind->speed[1];
		ave_mot = (float) fabs((speed1 / (float) 14.4) + (speed2 / (float) 14.4)) / (float) 2.0;	
		dif_m   = (float) sqrt(fabs( ((speed1 + (float) 14.4) / (float) 28.8) - ((speed2 + (float) 14.4) / (float) 28.8) ) );
		fitscore2=ave_mot*(1-dif_m); // sommo 1 perch� quando il valore se < 1 la moltiplicazione
								     // pu� produrre una riduzione della fitness anzich� un aumento
	*/	
		// Controlla che un'estremit� della sbarra sia all'interno della food-zone, in tal caso pu� 
		// prendere punteggio di fitness, ossia se e soltanto se sia il robot che la sbarra sono all'
		// interno della foodzone
		obj_bar = *(envobjs + BAR);				// seleziona la prima sbarra
		obj_ground1 = *(envobjs + GROUND);		// seleziona la prima ground zone
		obj_ground2 = (*(envobjs + GROUND))+1;	// seleziona la second ground zone

		if (pind->number==0) otherpind=(pind+1);	// seleziona e punta all'altro individuo 
		if (pind->number==1) otherpind=(pind-1);	// seleziona e punta all'altro individuo

		// Calcolo della distanza minima del robot corrente dalle estremit� della sbarra
		d1=mdist(pind->pos[0],pind->pos[1],obj_bar->x,obj_bar->y);
		d2=mdist(pind->pos[0],pind->pos[1],obj_bar->X,obj_bar->Y);
		min_d1=d1 <= d2 ? d1 : d2;

		// Calcolo della distanza minima dell'altro robot dalle estremit� della sbarra
		d1=mdist(otherpind->pos[0],otherpind->pos[1],obj_bar->x,obj_bar->y);
		d2=mdist(otherpind->pos[0],otherpind->pos[1],obj_bar->X,obj_bar->Y);
		min_d2=d1 <= d2 ? d1 : d2;

		// 1) Sono penalizzati con 3 extra-cicli se emettono 1 dalla PU fuori dalle aree critiche
		// Ancora da decidere il numero di cicli di vita. Dovrebbe funzionare bene con 4000 cicli 
		if (((min_d1>Critical_dist) && (pind->pushing_units[0]>0.5)) || 
			((min_d2>Critical_dist) && (otherpind->pushing_units[0]>0.5))) {
			// Riduce i cicli di vita di entrambi i robot, in questo modo li punisce se manten-
			// gono per troppo tempo attivo-alto il segnale intenzionale
		    if (n_fixed_evolution_is_running) {	// incrementa gli extracicli solo in fase di evoluzione
				pind->lifecycle+=T_inc;
				pind->totcycles+=T_inc;
				otherpind->lifecycle+=T_inc;
				otherpind->totcycles+=T_inc;
			}
		}

		// 2) Sono penalizzati con 3 extra-cicli se emettono dentro le aree critiche 
		//			entrambi 0 dalla PU dentro le aree critiche
		//          uno 1 e l'altro 0 dentro le aree critiche
		//	  mentre se emettono entrambi 1 dalla PU allora non sono penalizzati
	/*	if (((min_d1<Critical_dist) && (min_d2<Critical_dist)) && 
			((pind->intentional[0]<=0.5) || (otherpind->intentional[0]<=0.5))) {
				// Riduce i cicli di vita di entrambi i robot, in questo modo li punisce se manten-
				// gono per troppo tempo attivo-alto il segnale intenzionale
			    if (n_fixed_evolution_is_running) {		// incrementa gli extracicli solo in fase di evoluzione
					pind->lifecycle+=T_inc;
					pind->totcycles+=T_inc;
					otherpind->lifecycle+=T_inc;
					otherpind->totcycles+=T_inc;
				}
		}
	*/	
		
		// 3) Sono penalizzati molto con 3 extra-cicli se uno tocca la sbarra e l'altro no 
		// Questo dovrebbe simulare e sostituire l'accorciamento della stringa
		if ((pind->touchedbar && !otherpind->touchedbar) || (!pind->touchedbar && otherpind->touchedbar)) {
			// Riduce i cicli di vita di entrambi i robot, in questo modo li punisce se manten-
			// gono per troppo tempo attivo-alto il segnale intenzionale
		    if (n_fixed_evolution_is_running) {		// incrementa gli extracicli solo in fase di evoluzione
				pind->lifecycle+=T_inc;
				pind->totcycles+=T_inc;
				otherpind->lifecycle+=T_inc;
				otherpind->totcycles+=T_inc;
			}
		}
	
		// Componente di fitness aggiuntiva per creare un attrattore comportamentale verso lo 
		// spostamento comntemporaneo della sbarra
	/*	if ((min_d1<=Critical_dist) && (min_d2<=Critical_dist))  
			if (pind->touchedbar && otherpind->touchedbar)
				fitscore2=0.1;			// recupera il valore della fitness  
	*/
		bool_intersect1=intersect_line_circle(obj_bar->x, obj_bar->y, obj_bar->X, obj_bar->Y , obj_ground1->x, obj_ground1->y, obj_ground1->r, &xi, &yi);
		bool_intersect2=intersect_line_circle(obj_bar->x, obj_bar->y, obj_bar->X, obj_bar->Y , obj_ground2->x, obj_ground2->y, obj_ground2->r, &xi, &yi);
		
		// Memorizza il lifecycle del primo e secondo robot, allorquando essi giungono nella target area
		if ((bool_intersect1) && (pind->number==0) && (memlifecycle1==0)) 
			memlifecycle1=pind->lifecycle;		// memorizza il tempo del primo robot
		if ((bool_intersect2) && (pind->number==1) && (memlifecycle1!=pind->lifecycle) && (memlifecycle2==0)) 
			memlifecycle2=pind->lifecycle;		// memorizza il tempo del secondo robot

		// misura statistica n.2
		if (test_statistics) {  
			// misura il tempo della prima volta di un estremo nella prima target area
			if (intersect_line_circle(obj_bar->x, obj_bar->y, obj_bar->X, obj_bar->Y , obj_ground1->x, obj_ground1->y, obj_ground1->r, &xi, &yi) == 1) {
				if (*(successfirstsideruntime+cepoch)==0) 
					*(successfirstsideruntime+cepoch)=run;	// memorizza il tempo in cui avviene l'intersezione con la prima target area
			}		

			// misura il tempo della prima volta di un estremo nella seconda target area
			if (intersect_line_circle(obj_bar->x, obj_bar->y, obj_bar->X, obj_bar->Y , obj_ground2->x, obj_ground2->y, obj_ground2->r, &xi, &yi) == 1) {
				if (*(successsecondsideruntime+cepoch)==0) 
					*(successsecondsideruntime+cepoch)=run;	// memorizza il tempo in cui avviene l'intersezione con la seconda target area
			}			
		}
	
		// se la sbarra si trova all'interno della prima e della seconda ground zone, e il robot 
		// all'interno di una target area, e la sbarra � sempre stata mantenuta verticale, allora 
		// prendono un punteggio di fitness
		if ((memlifecycle1!=0) && (memlifecycle2!=0) && (abs(memlifecycle1-memlifecycle2)<=differtime) && 
			(bool_intersect1 == 1) && (bool_intersect2 == 1)) { 
				fitscore1=1.0;	// prende 1.0 se la sbarra interseca entrambe le food zone
		
				// misura statistica n.1
				if (((test_times)||(test_times_all_gen)) && (test_statistics))  
					// misura il primo successo utile all'interno di un' epoca
					if (*(successruntime+cepoch)==0) {
						*(successruntime+cepoch)=run;	// memorizza il tempo in cui avviene il successo
						success_counter++;				// incrementa il numero dei successi	
					}		
		} else 
			fitscore1=0.0;
	}
	return (fitscore1+fitscore2);
}

/*
 * Fitness per lo studio della Leadership. Un Follower (inseguitore del leader o individuo
 * comune) viene premiato (+1 o incremento energia vitale) se entra in una food zone. */
float
ffitness_leadersfollowers(struct  individual  *pind)

{
	float fitscore1=0.0, fitscore2=0.0;			 // punteggio parziale di fitness 1 e 2

	// Componente del ground
	if ((read_ground(pind) > 0) && (pind->categorization[0]<=0.5)) // solo i follower prendono
																   // fitness i leader al momento no	
	    fitscore1=1.0;
    else
		fitscore1=0.0;
	
	return (fitscore1+fitscore2);
}

/*
 * Fitness per lo studio della Leadership. Un Follower (inseguitore del leader o individuo
 * comune) viene premiato (+1 o incremento energia vitale) se entra in una food zone. Il 
 * Leader pi� vicino a lui viene premiato allo stesso tempo (+1 o incremento energia vitale).
 */
float
ffitness_leadersfollowers2(struct  individual  *pind)

{
	float fitscore1=0.0, fitscore2=0.0;		// punteggio parziale di fitness 1 e 2
    struct  individual  *cind,*mincind;		// current individual
	double dist,mindist;					// distanze per il calcolo della food zone di minima distanza
	float min_posx,min_posy;				// coordinate della food zone di distanza minima
	int n;

	// Componente che premia i follower nel ground
	if ((read_ground(pind) > 0) && (pind->categorization[0]<=0.5)) { // solo i follower prendono
																	 // fitness i leader al momento no	
	    fitscore1=1.0;

		// Componente che premia il leader pi� vicino
		// Determina il leader con distanza minima 
		mindist=envdx+envdy;
		min_posx=0.0;
		min_posy=0.0;
		mincind=pind;
		for(n=0, cind=ind; n < nteam; n++, cind++) {	// seleziona tutti gli altri individui 
			if ((cind!=pind) && (cind->categorization[0]>0.5)) {		// verifica che sia leader e differente da pind (il corrente)
				// Determina la distanza tra l'individuo corrente e uno dei leader trovati nell'intorno
				dist=mdist(pind->pos[0],pind->pos[1],cind->pos[0],cind->pos[1]);
				if (dist<=mindist) { 		
					mindist=dist;			// la nuova distanza minima � dist
					mincind=cind;			// memorizza l'individuo leader a distanza minima
				}
			}
		}
		if (mincind!=pind) {	// se � stato individuato un leader pi� vicino
			mincind->fitness++;
		}
	}
	else
		fitscore1=0.0;

	return (fitscore1+fitscore2);
}

/*
 * re-initialize the characteristics of the environment
 */
void initialize_world_discrim()
{

    int    i;
    int    ta,tt;
    int    tw;
    double d,mind;
    double w1, w2;
    int    repetitions;
    double min_dist_ob;
    struct envobject *obj;
    struct envobject *objb;
    struct envobject *objc;

    //randomize foodzone positions & corresponding small round objects
    if (random_groundareas == 1)
	{
		//	GROUND areas
		for (ta=0, obj = *(envobjs + GROUND); ta < envnobjs[GROUND]; ta++, obj++)
		{
			mind = 0.0;
			repetitions = 0;
			min_dist_ob = obj->r * 2.0f;
			min_dist_ob += min_dist_ob * 0.1;
			while (mind < min_dist_ob && repetitions < 2000)
			{
				mind = 9999.9;
				repetitions++;
				if ((repetitions > 199) && (repetitions % 10) == 0)
					min_dist_ob -= 1;
				obj->x = (float) fabs(rans((float) envdx - min_dist_ob)) + (min_dist_ob / 2);
				obj->y = (float) fabs(rans((float) envdy - min_dist_ob)) + (min_dist_ob / 2);
				if (envnobjs[1] > 0)
				{
					objc = *(envobjs + 1);
					objc->x = obj->x;
					objc->y = obj->y;
				}

				//	Check for overlap between GROUND objs
				for (tt=0, objb = *(envobjs + GROUND); tt < ta; tt++, objb)
				{
					d = mdist(objb->x,objb->y,obj->x, obj->y);
					if (d < mind)
						mind = d;
				}
			}
		}

		//	WATER areas
		for (ta=0, obj = *(envobjs + WATER); ta < envnobjs[WATER]; ta++, obj++)
		{
			mind = 0.0;
			
			repetitions = 0;

			//	Minimum distance between objects is twice the radius
			min_dist_ob = obj->r * 2.0f;
			//	Plus 10%
			min_dist_ob += min_dist_ob * 0.1;

			while ((mind < min_dist_ob) && (repetitions < 2000))
			{
				//	Start just below 10000
				mind = 9999.9;

				//	Avoid getting into an infinite loop
				repetitions++;

				//	Every tenth attempt (after initial 200 tries) 
				//	reduce the minimum distance between objects by one
				if ((repetitions > 199) && (repetitions % 10) == 0)
					min_dist_ob -= 1;

				//	The 'rans' function returns a double in the range -n..n
				//	The 'fabs' function returns the absolute value of a number
				obj->x = (float) fabs(rans((float) envdx - min_dist_ob)) + (min_dist_ob / 2);
				obj->y = (float) fabs(rans((float) envdy - min_dist_ob)) + (min_dist_ob / 2);
				
				//	Checks and updates the minimum distance between water zones
				for (tt=0, objb = *(envobjs + WATER); tt < ta; tt++, objb++)
				{
					d = mdist(objb->x,objb->y,obj->x, obj->y);
					if (d < mind)
						mind = d;
				}
			}
		}

		//	FOOD areas
		for (ta=0, obj = *(envobjs + FOOD); ta < envnobjs[FOOD]; ta++, obj++)
		{
			mind = 0.0;
			repetitions = 0;
			
			//	Minimum distance between objects is twice the radius
			min_dist_ob = obj->r * 2.0f;
			//	Plus 10%
			min_dist_ob += min_dist_ob * 0.1;
			
			while (mind < min_dist_ob && repetitions < 2000)
			{
				//	Start just below 10000
				mind = 9999.9;
				
				//	Avoid getting into an infinite loop
				repetitions++;
				
				//	Every tenth attempt (after initial 200 tries) 
				//	reduce the minimum distance between objects by one
				if ((repetitions > 199) && (repetitions % 10) == 0)
					min_dist_ob -= 1;

				//	The 'rans' function returns a double in the range -n..n
				//	The 'fabs' function returns the absolute value of a number	
				obj->x = (float) fabs(rans((float) envdx - min_dist_ob)) + (min_dist_ob / 2);
				obj->y = (float) fabs(rans((float) envdy - min_dist_ob)) + (min_dist_ob / 2);
				
				//	Checks and updates the minimum distance between food zones
				for (tt=0, objb = *(envobjs + FOOD); tt < ta; tt++, objb++)
				{
					d = mdist(objb->x,objb->y,obj->x, obj->y);
					if (d < mind)
						mind = d;
				}

				//	Additionally we need to check there is no overlap between food and
				//	water zones
				for (tt=0, objb = *(envobjs + WATER); tt < envnobjs[WATER]; tt++, objb++)
				{
					d = mdist(objb->x,objb->y,obj->x, obj->y);
					if (d < mind)
						mind = d;
				}
			}
		}
	}
}


/*
 * stay2_on_areas fitness function
 * reward for staying on a target area 
 */
float
ffitness_stay2_on_areas(struct  individual  *pind)

{

  struct  individual  *cind;
  int nxarea1, nxarea2;
  int n;
  float score;

  nxarea1 = 0;
  nxarea2 = 0;
  score = 0.0;

  for(n=0, cind=pind; n < nteam; n++, cind++)
   {
     if (read_ground(cind) == 1)
      {
       nxarea1++;
       if (nxarea1 <= 2)
         score += 0.25;
        else
         score -= 1.0;
      }
     if (read_ground(cind) == 2)
      {
       nxarea2++;
       if (nxarea2 <= 2)
         score += 0.25;
        else
         score -= 1.0;
      }
    }
    return(score);
}

/*
 * set the initial position of the robots
 * robots start outside target area
 */
void
initialize_robot_position_stay2()
{

    struct individual *pind;
    int    volte;
    int    team;
	float  xi,yi;
	int	   n;


    for(team=0,pind=ind; team < nteam; team++, pind++)
     if (pind->dead == 0)
     {
       volte=0;
       do
	{
	  volte++;
	  pind->pos[0] = (float) fabs(rans((float) (envdx - 80))) + 40;
	  pind->pos[1] = (float) fabs(rans((float) (envdy - 80))) + 40;
          pind->direction = fabs(rans(360.0));
	}
       while (volte == 0 || (volte < 1000 && ((check_crash(pind->pos[0],pind->pos[1],-1,&xi,&yi,&n,pind->died_individual,pind->ghost_individual) == 1) || (read_ground(pind) > 0)) ));
     }

}


/*
 * stay_on_different_areas fitness function
 * 1.0 or -1.0 for staying in different or equal areas
 */
float
ffitness_stay_on_different_areas(struct  individual  *pind)

{

  struct  individual *cind;

  if (nteam == 2)
    {
      cind = (pind + 1);
      if ((read_ground(pind) > 0) && (read_ground(cind) > 0))
        {
	  if (((read_ground(pind) % 2) != (read_ground(cind) % 2)))
	    return(1.0);
	  else
	    return(-1.0);
	}
    }
  return(0.0);
}

/*
 * tswitch fitness function
 * 1.0 every time the two individuals are in different areas for the first time or after a shift
 */
float
ffitness_tswitch(struct  individual  *pind)

{

  struct  individual  *pind2;

  if (nteam == 2)
    {
	pind2 = (pind + 1);
	if ((read_ground(pind) > 0) && (read_ground(pind2) > 0) 
	    && ((read_ground(pind) % 2) != (read_ground(pind2) % 2)) 
	    && (pind->visited[0] == 0 || pind->visited[0] != read_ground(pind)))
	  {
            pind->visited[0] = read_ground(pind);
            pind2->visited[0] = read_ground(pind2);
	    return(1.0);
	  }

     }
   return(0.0);

}


/*
 * re-initialize the characteristics of the environment
 */
void
initialize_world_tswitch()
{

    int    ta,tt;
    int    tw;
    double d,mind;
    double w1, w2;
    int    volte;
    double min_dist_ob;
    struct envobject *obj;
    struct envobject *objb;
    struct envobject *objc;

    //randomize foodzone positions (joachim)
    if (random_groundareas == 1)
	{
	  for (ta=0, obj = *(envobjs + 3); ta < envnobjs[3]; ta++, obj++)
	  {
		mind = 0.0;
		volte = 0;
		min_dist_ob = obj->r*3.0f;
		while (mind < min_dist_ob && volte < 20000)
		{
			mind = 9999.9;
			volte++;
			if ((volte > 199) && (volte % 10) == 0)
				min_dist_ob -= 1;
			obj->x = (float) (fabs(rans((float) envdx - (obj->r * 2.0f))) + obj->r);
			obj->y = (float) (fabs(rans((float) envdy - (obj->r * 2.0f))) + obj->r);
			//check for other foodzone
			for (tt=0, objb = *(envobjs + 3); tt < ta; tt++, objb++)
			{
				d = mdist(objb->x,objb->y,obj->x,obj->y);
				if (d < mind)
					mind = d;
			}

		}
	  }

     }


}


/*
 * global variables arbitrate
 * we assume that the environment contain a recharging area and a feading area.
 * The position of the food and of the predator is initialized in
 *  the feeding area
 * The position of the prey is initialized outside ground areas
 */
int     crashed_step;       // whether a robot crashed during the current step 
float   max_punishment;     // the max punishement which can be provided with one trial

/*
 * randomize the position of the round objects
 * inside the feeding area (ground-2)
 * and initialize arbitrate variables
 */
void
initialize_world_arbitrate()


{

    int    i;
    int    ta,tt;
    int    tw;
    double d,mind;

    double w1, w2;
    int    volte;
    double min_dist_ob;
    struct envobject *obj;
    struct envobject *objb;
    float feedx,feedy,feeddx,feeddy;

    crashed_step = 0;
    max_punishment = 0.0;
    // feedarea
    if (envnobjs[GROUND] >= 2)
	{
	  obj = *(envobjs + GROUND);
	  obj++;
	  feedx = obj->x;
	  feedy = obj->y;
	  feeddx = obj->X;
	  feeddy = obj->Y;
        }
       else
	{
	  feedx = 50;
	  feedy = 50;
	  feeddx = envdx - 50;
	  feeddy = envdy - 50;
        }
    // we randomize sround positions
    if (random_round == 1)
	{
	  for (ta=0,obj = *(envobjs + SROUND); ta<envnobjs[SROUND]; ta++, obj++)
		{
		  mind = 0.0;
		  volte = 0;
		  min_dist_ob = 150;
		  while (volte < 1000 && mind < min_dist_ob)
		  {
		    volte++;
		    if ((volte > 199) && (volte % 10) == 0)
		      min_dist_ob -= 1;
		    obj->x = (float) (fabs(rans(feeddx)) + feedx);
		    obj->y = (float) (fabs(rans(feeddy)) + feedy);
		    for (tt=0, objb = *(envobjs + 1), mind = 9999.9; tt < ta; tt++, objb)
			{
			 d = mdist(objb->x, objb->y, obj->x, obj->y);
			 if (d < mind)
			   mind = d;
			}

		  }
		}
	}

    // we randomize round positions
    if (random_round == 1)
	{
	  for (ta=0, obj = *(envobjs + ROUND); ta<envnobjs[ROUND]; ta++, obj++)
	  {
	    mind = 0.0;
	    volte = 0;
	    min_dist_ob = 150;
	    while (mind < min_dist_ob && volte < 1000)
		{
		  mind = 9999.9;
		  volte++;
		  if ((volte > 199) && (volte % 10) == 0)
		      min_dist_ob -= 1;
		  obj->x = (float) (fabs(rans(feeddx)) + feedx);
		  obj->y = (float) (fabs(rans(feeddy)) + feedy);
		  for (tt=0, objb = *(envobjs + 2); tt < ta; tt++, objb++)
			{
			  d = mdist(objb->x, objb->y, obj->x, obj->y);
			  if (d < mind)
			    mind = d;
			}
		  for (tt=0, objb = *(envobjs + 1); tt < envnobjs[1]; tt++, objb++)
			{
			  d = mdist(objb->x,objb->y,obj->x,obj->y);
			  if (d < mind)
			    mind = d;
			}
		}
	  }
	}

}

/*
 * set the initial position of the predator and prey robots
 * the first robot (prey) is placed outside target areas (groud color = < 0.25)
 * the second robot (predator) is placed in the feeding area (i.e. the second ground area)
 */
void
initialize_robot_position_arbitrate()
{

    struct individual *pind;
    int    volte;
    float  feedx,feedy,feeddx,feeddy;
    struct envobject *obj;
	float  xi,yi;
	int	   n;

    // feedarea
    if (envnobjs[GROUND] >= 2)
	{
	  obj = *(envobjs + GROUND);
	  obj++;
	  feedx = obj->x;
	  feedy = obj->y;
	  feeddx = obj->X;
	  feeddy = obj->Y;
        }
       else
	{
	  feedx = 50;
	  feedy = 50;
	  feeddx = envdx - 50;
	  feeddy = envdy - 50;
        }
    if (nteam > 0)
      {
        pind = ind;
        pind->pos[0] = 50.0 + (double) mrand(envdx - 100);
        pind->pos[1] = 50.0 + (double) mrand(envdy - 100);
	pind->direction = (double) mrand(360);
	volte = 0;
	while (volte < 1000 && (read_ground_color(pind) > 0.25 || (check_crash(pind->pos[0],pind->pos[1],-1,&xi,&yi,&n,pind->died_individual,pind->ghost_individual) == 1) ) )
	    {
	      volte++;
	      pind->pos[0]    = 50.0 + (double) mrand(envdx - 100);
	      pind->pos[1]    = 50.0 + (double) mrand(envdy - 100);
	      pind->direction = (double) mrand(360);
	    }
	    
      }

    if (nteam > 1)
      {
        pind = (ind + 1);
        pind->pos[0] = mrand((int) feeddx) + feedx;
        pind->pos[1] = mrand((int) feeddy) + feedy;
	pind->direction = (double) mrand(360);
	volte = 0;
	while (volte < 1000 && (read_ground_color(pind) < 0.25 || read_ground_color(pind) > 0.75 || (check_crash(pind->pos[0],pind->pos[1],-1,&xi,&yi,&n,pind->died_individual,pind->ghost_individual) == 1) ) )
	    {
	      volte++;
              pind->pos[0] = mrand((int) feeddx) + feedx;
              pind->pos[1] = mrand((int) feeddy) + feedy;
	      pind->direction = (double) mrand(360);
	    } 
      }
    


}


/*
 * recharge, forage, & avoid predator
 * get a 1.0 score for every object reached but only if energy level is above 0.0 
 * collisions with the predator terminate the current trial
 */
float
ffitness_arbitrate(struct  individual  *pind)

{

  double dist;
  int    t;
  float  score;
  struct envobject *obj;
  float  xi,yi;
  int	 n;


  score = 0.0;

  if (read_ground_color(pind) > 0.75)
      pind->energylevel = 1.0f;
	else
      pind->energylevel -=  0.001f *  ((float) (abs(pind->speed[0]) + abs(pind->speed[1])) / 20.0);
      
  for (t=0, obj = *(envobjs + SROUND); t < envnobjs[SROUND]; t++, obj++)
	 {
	   dist = mdist((float)pind->pos[0],(float)pind->pos[1],(float) obj->x,(float) obj->y);
	   if ((dist - 3) < (robotradius + obj->r))
	    {
                 if (pind->energylevel >= 0.0)
                   score += 1.0;
		 // better check the new object does not crash with other objects of with the predator 
		 obj->x = (float) (fabs(rans((float) envdx - (double) (120 * 2))) + 120);
		 obj->y = (float) (fabs(rans((float) envdy - (double) (120 * 2))) + 120);
		 while (check_crash(pind->pos[0],pind->pos[1],-1,&xi,&yi,&n,pind->died_individual,pind->ghost_individual) == 1)
	          {
		   obj->x = (float) (fabs(rans((float) envdx - (double) (120 * 2))) + 120);
		   obj->y = (float) (fabs(rans((float) envdy - (double) (120 * 2))) + 120);
	      }
	    }
	 }
 
  if (pind->crashed == run && max_punishment <= 1.0)
	{
	  max_punishment += 0.1;
	  score -= 0.1;
	}

  if (individual_crash == 1)
     stop = 1;

  return(score);

}

//	Switch from day to night and vice-versa.  This could be implemented in
//	work with actual hardware by simply using artificial lighitng.  Agents 
//	could then be equipped with light dependant resistors to sense this.
void switchDayNight()
{
	isDaytime = !isDaytime;
}