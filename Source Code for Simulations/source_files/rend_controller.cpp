/* 
 * Evorobot* - rend_controllers.cpp
 * Copyright (C) 2009, Stefano Nolfi
 * LARAL, Institute of Cognitive Science and Technologies, CNR, Roma, Italy 
 * http://laral.istc.cnr.it

 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "public.h"
#include "ncontroller.h"
#include "rend_controller.h"

#ifdef EVOGRAPHICS  

#include <QtGui>        // Widget
#include <QKeyEvent>    // Dialog
#include <QFileDialog>  // Dialog
#include <QMessageBox>  // Dialog

RendNetwork *rendNetwork;      // The network rendering widget
NetworkDialog *networkDialog;  // The network Dialog

extern long int trace_run;	   // current cycle for tracing of robots
extern int n_variable_evolution_is_running;
extern int n_fixed_evolution_social_is_running;  // social n-fixed evolution is currently running
extern int testpopulation_is_running;		 // test entire population

int drawblocks=0;              // whether we display or not connections and update blocks 
int wnetwork=0;                // whether the window has been already opened or not
float   ngraph[1000][40];      // value to be displayed for neurons
int     ngraphid[40];          // id of the neuron to be displayed
int     cneuron[4];            // selected neurons
int     cneuronn=0;            // n. of selected cneurons
int     neurondmode=0;         // display 1=delta, 2=weight, 3=biases instead of labels
int     rnrun;                 // the current cycle
int     rnmousex;              // mouse left-clicked x position
int     rnmousey;              // mouse left-clicked y position
//float   *vectorv;              // vector of selected parameters 
float   **graphicp;            // vector of parameter displayed through the graphic interface (max length = nneurons)

void pseudo_activate_net(float *p, int nneurons, int ddelta, int dweight, int dbias);

//----------------------------------------------------------------------
//-------------------  PUBLIC FUNCTIONS  -------------------------------
//----------------------------------------------------------------------

/*
 * mode=1, create the network widget
 * mode=2  create the network dialog and associate the widget to it
 */

void
init_rendnetwork(int mode)
{

    if (mode == 1 && wnetwork == 0)
      {
       rendNetwork = new RendNetwork;
       wnetwork = 1;
      }

    if (mode == 2 && wnetwork == 1)
    {
     networkDialog = new NetworkDialog(NULL);
     networkDialog->setWindowTitle("Network");
     networkDialog->setWidget(rendNetwork, 0, 0);
     networkDialog->show();
     update_rendnetwork(0);
     //vectorv = (float *) malloc(nneurons * sizeof(float *));
     graphicp = (float **) malloc(nneurons * sizeof(float **));
     //create_fvparameter("vectorv", "network",vectorv, 3, 0, "selected free parameters");
     create_fvpparameter("graphicp", "#interface",graphicp, nneurons, 0, "selected free parameters");
     create_fvpparameter("graphicp", "#interface",graphicp, 0, 0, "");
     create_fvparameter("neuronlesion", "#interface",neuronlesion, nneurons, 0, "activation values of frozen neurons");
     wnetwork = 2;
    }
    if (mode == 2 && wnetwork == 2)
    {
     networkDialog->show();
     update_rendnetwork(0);
    }
}

void
hide_rendnetwork()
{
    if (wnetwork == 2)
      networkDialog->hide();
}

void
add_network_widget(QGridLayout *mainLayout, int row, int column)
{
  mainLayout->addWidget(rendNetwork, row, column);
}


void
update_rendnetwork(int cycle)


{

     rnrun = cycle;

     rendNetwork->repaint();
     QCoreApplication::processEvents();

}

//----------------------------------------------------------------------
//-------------------  PRIVATE FUNCTIONS  ------------------------------
//----------------------------------------------------------------------

// -----------------------------------------------------
// Dialog and Toolbar
// -----------------------------------------------------

void NetworkDialog::createToolBars()
{
    QToolBar *networktoolbar;

    networktoolbar = toolBar();

    networktoolbar->addAction(openAct);
    networktoolbar->addAction(saveAct);
    networktoolbar->addAction(set_neurondisplayAct);
    networktoolbar->addAction(set_neurondeltaAct);
    networktoolbar->addAction(set_neuronbiasAct);
    networktoolbar->addAction(set_lesionAct);
    networktoolbar->addAction(display_weightAct);
    networktoolbar->addAction(display_deltaAct);
    networktoolbar->addAction(display_biasAct);
    networktoolbar->addAction(display_blocksAct);
    networktoolbar->addAction(erase_netAct);
    networktoolbar->addAction(add_ublockAct);
    networktoolbar->addAction(add_cblockAct);


}

void NetworkDialog::createActions()
{

    openAct = new QAction(QIcon(":/images/open.png"), tr("&Open"), this);
    openAct->setStatusTip(tr("Open a network architecture or a phenotype file"));
    connect(openAct, SIGNAL(triggered()), this, SLOT(open()));

    saveAct = new QAction(QIcon(":/images/save.png"), tr("&Save"), this);
    saveAct->setStatusTip(tr("Save the network architecture or the phenotype from a file"));
    connect(saveAct, SIGNAL(triggered()), this, SLOT(save()));

    set_neurondisplayAct = new QAction(QIcon(":/images/show.png"), tr("&Set/Unset neurons to be displayed as graph"), this);
    set_neurondisplayAct->setShortcut(tr("Ctrl+N"));
    set_neurondisplayAct->setStatusTip(tr("Select/Deselect neurons to be displayed as graph"));
    connect(set_neurondisplayAct, SIGNAL(triggered()), this, SLOT(set_neurondisplay()));

    set_neurondeltaAct = new QAction(QIcon(":/images/delta.png"), tr("&Add/Remove neurons deltas"), this);
    set_neurondeltaAct->setShortcut(tr("Ctrl+W"));
    set_neurondeltaAct->setStatusTip(tr("Add/Remove neurons delta"));
    connect(set_neurondeltaAct, SIGNAL(triggered()), this, SLOT(set_neurondelta()));

    set_neuronbiasAct = new QAction(QIcon(":/images/bias.png"), tr("&Add/Remove neurons biases"), this);
    set_neuronbiasAct->setShortcut(tr("Ctrl+B"));
    set_neuronbiasAct->setStatusTip(tr("Add/Remove neurons bias"));
    connect(set_neuronbiasAct, SIGNAL(triggered()), this, SLOT(set_neuronbias()));

    set_lesionAct = new QAction(QIcon(":/images/lesion.png"), tr("&Lesion/Restore neurons"), this);
    set_lesionAct->setShortcut(tr("Ctrl+B"));
    set_lesionAct->setStatusTip(tr("Lesion/Restore neurons"));
    connect(set_lesionAct, SIGNAL(triggered()), this, SLOT(set_lesion()));

    display_weightAct = new QAction(QIcon(":/images/weight.png"), tr("&Display/Undisplay weights"), this);
    display_weightAct->setShortcut(tr("Ctrl+W"));
    display_weightAct->setStatusTip(tr("Display the incoming and outcoming weight of a neuron"));
    connect(display_weightAct, SIGNAL(triggered()), this, SLOT(display_weight()));

    display_deltaAct = new QAction(QIcon(":/images/ddelta.png"), tr("&Display/Undisplay neurons delta"), this);
    display_deltaAct->setShortcut(tr("Ctrl+Y"));
    display_deltaAct->setStatusTip(tr("Display neurons delta"));
    connect(display_deltaAct, SIGNAL(triggered()), this, SLOT(display_delta()));

    display_biasAct = new QAction(QIcon(":/images/dbias.png"), tr("&Display/Undisplay biases"), this);
    display_biasAct->setShortcut(tr("Ctrl+Z"));
    display_biasAct->setStatusTip(tr("Display neurons biases"));
    connect(display_biasAct, SIGNAL(triggered()), this, SLOT(display_bias()));

    display_blocksAct = new QAction(QIcon(":/images/displayblocks.png"), tr("&Display/Undisplay blocks"), this);
    display_blocksAct->setShortcut(tr("Ctrl+B"));
    display_blocksAct->setStatusTip(tr("Display blocks description"));
    connect(display_blocksAct, SIGNAL(triggered()), this, SLOT(display_blocks()));

    erase_netAct = new QAction(QIcon(":/images/erase.png"), tr("&Erase net"), this);
    erase_netAct->setShortcut(tr("Ctrl+E"));
    erase_netAct->setStatusTip(tr("Erase all connections and update blocks"));
    connect(erase_netAct, SIGNAL(triggered()), this, SLOT(erase_net()));

    add_ublockAct = new QAction(QIcon(":/images/addublock.png"), tr("&Add Update Block"), this);
    add_ublockAct->setShortcut(tr("Ctrl+A"));
    add_ublockAct->setStatusTip(tr("add an update block"));
    connect(add_ublockAct, SIGNAL(triggered()), this, SLOT(add_ublock()));

    add_cblockAct = new QAction(QIcon(":/images/addblock.png"), tr("&Add Connection Block"), this);
    add_cblockAct->setShortcut(tr("Ctrl+A"));
    add_cblockAct->setStatusTip(tr("add a connection block"));
    connect(add_cblockAct, SIGNAL(triggered()), this, SLOT(add_cblock()));

}


// define or undefine neurons to be displayed
void NetworkDialog::set_neurondisplay()
{
   int i;

   if (cneuronn == 1)
   {
     if (neurondisplay[cneuron[0]] == 1)
       neurondisplay[cneuron[0]] = 0;
      else
       neurondisplay[cneuron[0]] = 1;
   }
   if (cneuronn == 2)
   {
     if (neurondisplay[cneuron[0]] == 1)
      for (i=cneuron[0]; i <= cneuron[1]; i++)
       neurondisplay[i] = 0;
     else
      for (i=cneuron[0]; i <= cneuron[1]; i++)
       neurondisplay[i] = 1;
   }
   if (cneuronn < 1 || cneuronn > 2)
     display_warning("you should select one neuron or one block of neurons first");

   cneuronn = 0;
   update_rendnetwork(0);

}

// define or undefine neurons delta
void NetworkDialog::set_neurondelta()
{
   int i;

   if (cneuronn == 1)
   {
     if (neurondelta[cneuron[0]] == 1)
       neurondelta[cneuron[0]] = 0;
      else
       neurondelta[cneuron[0]] = 1;
   }
   if (cneuronn == 2)
   {
     if (neurondelta[cneuron[0]] == 1)
      for (i=cneuron[0]; i <= cneuron[1]; i++)
       neurondelta[i] = 0;
     else
      for (i=cneuron[0]; i <= cneuron[1]; i++)
       neurondelta[i] = 1;
   }
   if (cneuronn < 1 || cneuronn > 2)
     display_warning("you should select one neuron or one block of neurons first");

    cneuronn = 0;
    update_rendnetwork(0);

}

// define or undefine neurons bias
void NetworkDialog::set_neuronbias()
{
   int i;

   if (cneuronn == 1)
   {
     if (neuronbias[cneuron[0]] == 1)
       neuronbias[cneuron[0]] = 0;
      else
       neuronbias[cneuron[0]] = 1;
   }
   if (cneuronn == 2)
   {
     if (neuronbias[cneuron[0]] == 1)
      for (i=cneuron[0]; i <= cneuron[1]; i++)
       neuronbias[i] = 0;
     else
      for (i=cneuron[0]; i <= cneuron[1]; i++)
       neuronbias[i] = 1;
   }
   if (cneuronn < 1 || cneuronn > 2)
     display_warning("you should select one neuron or one block of neurons first");

   cneuronn = 0;
   update_rendnetwork(0);
}

/*
 * Lesion/Restore neurons
 */
void NetworkDialog::set_lesion()
{
   int i;
   float *nl;

   // Restore the functionality of all neurons
   if (cneuronn == 0)
   {
     for(i=0, nl=neuronlesion; i < ncontrollers->nneurons; i++, nl++)
	*nl = -9.9;
     neuronlesions = 0;
   }
   // lesion one neuron 
   if (cneuronn == 1)
	{
	  nl = (neuronlesion + cneuron[0]);
	  *nl = 0.0;
	}

   // lesion a block of neurons
   if (cneuronn == 2)
     for(i=0, nl=neuronlesion; i < ncontrollers->nneurons; i++, nl++)
	{
	  if (i >= cneuron[0] && i <= cneuron[1])
	      *nl = 0.0;
	}

   if (cneuronn > 2)
     display_warning("you should select one neuron or one block of neurons");

   cneuronn = 0;
   neuronlesions = 0;
   for(i=0, nl=neuronlesion; i < ncontrollers->nneurons; i++, nl++)
	{
	  if (*nl < -1.0)
	   {
            strcpy(neuroncl[i],neuronl[i]);
	   }
	  else
	   {
            sprintf(neuroncl[i],"%.1f", *nl);
	    neuronlesions = 1;
	   }
	}
   update_rendnetwork(0);
}




// Display/Undisplay neurons delta
void NetworkDialog::display_delta()
{
   struct ncontroller *cncontroller;

   if (neurondmode > 0)
   {
     cncontroller = (ncontrollers + cindividual);
     pseudo_activate_net(cncontroller->freep, cncontroller->nneurons,0,0,0);
     neurondmode = 0;
   }
   else
   {
     neurondmode = 1;
     cncontroller = (ncontrollers + cindividual);
     pseudo_activate_net(cncontroller->freep, cncontroller->nneurons,1,0,0);
   }

   update_rendnetwork(0);
}


// Display/Undisplay the weights of a neuron
void NetworkDialog::display_weight()
{

   struct ncontroller *cncontroller;

   if (neurondmode > 0)
   {
     cncontroller = (ncontrollers + cindividual);
     pseudo_activate_net(cncontroller->freep, cncontroller->nneurons,0,0,0);
     neurondmode = 0;
   }
   else
   {
     if (cneuronn == 1)
     {
      neurondmode = 2;
      cncontroller = (ncontrollers + cindividual);
      pseudo_activate_net(cncontroller->freep, cncontroller->nneurons,0,1,0);
     }
     else
     {
      if (cneuronn != 1)
        display_warning("you should select one first");
     }
   }
   update_rendnetwork(0);
}

// Display/Undisplay neurons biases
void NetworkDialog::display_bias()
{
   struct ncontroller *cncontroller;

   if (neurondmode > 0)
   {
     cncontroller = (ncontrollers + cindividual);
     pseudo_activate_net(cncontroller->freep, cncontroller->nneurons,0,0,0);
     neurondmode = 0;
   }
   else
   {
     neurondmode = 3;
     cncontroller = (ncontrollers + cindividual);
     pseudo_activate_net(cncontroller->freep, cncontroller->nneurons,0,0,1);
   }

   update_rendnetwork(0);
}

// Display/Undisplay blocks description
void NetworkDialog::display_blocks()
{

  if (drawblocks == 0)
    drawblocks = 1;
  else
    drawblocks = 0;
  update_rendnetwork(0);

}

// Erase all connections and update blocks
void NetworkDialog::erase_net()
{

   int i;

   net_nblocks = 0;
   for(i=0;i < ncontrollers->nneurons; i++)
	   {
             neuronbias[i] = 0;
             neurondelta[i] = 0;
	     neurondisplay[i] = 1;
	   }
   update_rendnetwork(0);
}

// Add an update block
void NetworkDialog::add_ublock()
{

  int i;
  char message[64];

  i = net_nblocks;

  if (cneuronn != 1 && cneuronn != 2)
    {
      sprintf(message, "this command require to select 1 or 2 neurons");
      display_error(message);
    }

  if (cneuronn == 1)
    {
	net_block[i][0] = 1;
        net_block[i][1] = cneuron[0];
	net_block[i][2] = 1;
	net_block[i][3] = 0;
	net_block[i][4] = 0;
	net_nblocks++;
	cneuronn = 0;
    }

  if (cneuronn == 2)
    {
	net_block[i][0] = 1;
        net_block[i][1] = cneuron[0];
	net_block[i][2] = cneuron[1] - cneuron[0] + 1;
	net_block[i][3] = 0;
	net_block[i][4] = 0;
	net_nblocks++;
	cneuronn = 0;
    }

   update_rendnetwork(0);
}


// Add a connection block
void NetworkDialog::add_cblock()
{

  int i;
  char message[64];

  i = net_nblocks;

  if (cneuronn != 2 && cneuronn != 4)
    {
      sprintf(message, "this command require to select 2 or 4 neurons");
      display_error(message);
    }

  if (cneuronn == 2)
    {
	net_block[i][0] = 0;
	net_block[i][1] = cneuron[0];
        net_block[i][2] = 1;
	net_block[i][3] = cneuron[1];
        net_block[i][4] = 1;
	net_nblocks++;
	cneuronn = 0;
    }

  if (cneuronn == 4)
    {
	net_block[i][0] = 0;
	net_block[i][1] = cneuron[0];
        net_block[i][2] = cneuron[1] - cneuron[0] + 1;
	net_block[i][3] = cneuron[2];
        net_block[i][4] = cneuron[3] - cneuron[2] + 1;
	net_nblocks++;
	cneuronn = 0;
    }

   update_rendnetwork(0);
}


/*
 * open a .net or .phe file
 */
void NetworkDialog::open()
{

	char *f;
	QByteArray filen;
	char filename[256];

    QString fileName = QFileDialog::getOpenFileName(this,"Choose a filename to open","",
                    "*.net *.phe");

    if (fileName.endsWith("net"))
	{
	    filen = fileName.toAscii();
	    f = filen.data();
		strcpy(filename, f);
		load_net_blocks(filename);
	}
	else
	if (fileName.endsWith("phe"))
	{
		filen = fileName.toAscii();
        f = filen.data();
		strcpy(filename, f);
		load_phenotype_data(filename, ncontrollers->freep, ncontrollers->nfreep);
	}
}

/*
 * save a .net or .phe file
 */
void NetworkDialog::save()
{

    char *f;
    QByteArray filen;
    char filename[256];


        QString fileName = QFileDialog::getSaveFileName(this,
		    "Choose a filename to save",
                    "",
                    "*.net *.phe *.ant");


        if (fileName.endsWith("net"))
	{
          filen = fileName.toAscii();
          f = filen.data();
	  strcpy(filename, f);
	  save_net_blocks(filename);
	}
	else
	 if (fileName.endsWith("phe"))
	 {
          filen = fileName.toAscii();
          f = filen.data();
	  strcpy(filename, f);
	  save_phenotype_data(filename, ncontrollers->freep, ncontrollers->nfreep);
	  
	  
	 }
	 else
	 if (fileName.endsWith("ant"))
	 {
          filen = fileName.toAscii();
          f = filen.data();
	  strcpy(filename, f);
	  save_nn_data(filename, ncontrollers->freep, ncontrollers->nfreep);
	  compile_evonxt(ncontrollers->freep, ncontrollers->nfreep);
	 }

}

// -----------------------------------------------------
// Widget
// -----------------------------------------------------

RendNetwork::RendNetwork(QWidget *parent)
    : QWidget(parent)
{
    shape = Polygon;
    antialiased = false;
    pixmap.load(":/images/qt-logo.png");

    setBackgroundRole(QPalette::Base);
}

QSize RendNetwork::minimumSizeHint() const
{
    return QSize(550, 500);
}

QSize RendNetwork::sizeHint() const
{
    return QSize(550, 500);
}

void RendNetwork::setShape(Shape shape)
{
    this->shape = shape;
    update();
}

void RendNetwork::setPen(const QPen &pen)
{
    this->pen = pen;
    update();
}

void RendNetwork::setBrush(const QBrush &brush)
{
    this->brush = brush;
    update();
}

void RendNetwork::setAntialiased(bool antialiased)
{
    this->antialiased = antialiased;
    update();
}

void RendNetwork::setTransformed(bool transformed)
{
    this->transformed = transformed;
    update();
}


void RendNetwork::paintEvent(QPaintEvent *)
{
 

   int    i;
   int    u;
   int    c;
   int    t,b;
   int    counter;
   int    n_start, n_max;
   int    h1,h2;
   int    sx,sy,dx,dy;
   int    offx,offy;
   char   message[64];
   
   QRect labelxy(0,0,30,20);          // neuron labels 
   QPoint pxy;

   QPainter painter(this);
   painter.setPen(pen);
   QPen pen(Qt::black, 1);                 // black solid line, 1 pixels wide
   painter.setPen(pen);

   // draw neurons state
   if ((testindividual_is_running == 1) || (testpopulation_is_running == 1))
   {
     draw_neurons_update();
     offx = 50;
     if (drawn_alltogether == 1)
       h2 = height() / (ndu / nteam);
      else
       h2 = height() / ndu;
     h1 = h2 - 3;
     n_start = 0;
     //n_max   = rnrun;
     n_max   = trace_run;
     if (n_max > drawneuronsdx)
      n_max = drawneuronsdx;
     //if (rnrun > drawneuronsdx)
     if (trace_run > drawneuronsdx)
       //n_start = (rnrun-drawneuronsdx) % drawneuronsdx; 
       n_start = (trace_run-drawneuronsdx) % drawneuronsdx; 
      else
       n_start = 0; 
     c = n_start;
     counter = 0;
     while(counter < n_max)
       {
         offy = h2;
         for(u=0;u < ndu; u++)
	 {

	  if (counter == 0)
	  {
            painter.setPen(Qt::black);
            labelxy.setRect(offx-30, offy-(h2/2), 30, 30);
			painter.drawText(labelxy, neuronl[ngraphid[u]]);
            painter.drawLine(offx - 5, offy - h1, offx - 5, offy);
	  }

	  if (drawn_alltogether == 0)
	  {
	  if (ngraphid[u] < ncontrollers->ninputs)
            painter.setPen(QColor(255,0,0,255));
	  if (ngraphid[u] >= ncontrollers->ninputs && ngraphid[u] < (ncontrollers->nneurons - ncontrollers->noutputs))
            painter.setPen(QColor(180,180,180,255));
	  if (ngraphid[u] >= (ncontrollers->nneurons - ncontrollers->noutputs))
            painter.setPen(QColor(0,0,255,255));
	  if (ngraphid[u] == ncontrollers->nneurons)
            painter.setPen(QColor(0,255,0,255));
	  }
	  else
	  {
	  if (((u + 1) / (ndu / nteam)) == 0)
            painter.setPen(QColor(255,0,0,255));
	  if (((u + 1) / (ndu / nteam)) == 1)
	    painter.setPen(QColor(0,0,255,255));
	  if (((u + 1) / (ndu / nteam)) == 2)
            painter.setPen(QColor(0,255,0,255));
	  if (((u + 1) / (ndu / nteam)) == 3)
            painter.setPen(QColor(160,0,160,255));
	  }

	  if (drawnstyle == 0 && drawn_alltogether == 0)
            painter.drawLine(counter+offx, offy - (ngraph[c][u]*h1), counter+offx, offy);
	   else
	   if (c > 0)
             painter.drawLine(counter+offx-1, offy - (ngraph[c-1][u]*h1), counter+offx, offy - (ngraph[c][u]*h1));
	  offy += h2;
	  if (drawn_alltogether == 1 && ((u + 1) % (ndu / nteam)) == 0)
	    offy = h2;
	 }
	c++;
	if (c == drawneuronsdx)
	 c = 0;
	counter++;
       }
   }

   // draw neural architecture

   if (testindividual_is_running == 0 && testpopulation_is_running == 0 && n_fixed_evolution_is_running == 0 && n_variable_evolution_is_running == 0 && n_fixed_evolution_social_is_running == 0)
   {
    painter.setPen(Qt::gray);
    for (i=0; i < net_nblocks; i++)
    {
      if (net_block[i][0] == 0)
        for (t=net_block[i][1]; t < (net_block[i][1] + net_block[i][2]); t++)
          for (b=net_block[i][3]; b < (net_block[i][3] + net_block[i][4]); b++)
	  {
            if (abs(neuronxy[b][1] - neuronxy[t][1]) > 20) 
              painter.drawLine(neuronxy[t][0],neuronxy[t][1],neuronxy[b][0], neuronxy[b][1]);
	     else
	     {
	      dx = abs(neuronxy[t][0] - neuronxy[b][0]);
	      dy = abs(neuronxy[t][1] - neuronxy[b][1]);
              if (neuronxy[t][0] < neuronxy[b][0])
		sx = neuronxy[t][0];
	       else
		sx = neuronxy[b][0];
              if (neuronxy[t][1] < neuronxy[b][1])
		sy = neuronxy[t][1];
	       else
		sy = neuronxy[b][1];
	      painter.drawArc(sx,sy-20,dx,dy+40, 0 * 16, 180 * 16); 
	     }
	  }
    }
    for (i=0; i < ncontrollers->nneurons; i++)
	  {
           painter.setBrush(Qt::SolidPattern);

	   if (neuronbias[i] == 1)
	      painter.setPen(Qt::black);
	     else
	      painter.setPen(Qt::red);

	   if (neurondelta[i] == 1)
              painter.setBrush(QBrush(Qt::red, Qt::SolidPattern));
	     else
              painter.setBrush(QBrush(Qt::red, Qt::Dense5Pattern));

	   painter.drawEllipse(neuronxy[i][0] - 5, neuronxy[i][1] - 5, 10, 10);

	  }
    painter.setPen(Qt::black);
    for (i=0; i < ncontrollers->nneurons; i++)
	  {
	    if (neurondisplay[i] == 1)
	      painter.setPen(Qt::black);
	     else
	      painter.setPen(Qt::red);
	    if (i < ncontrollers->ninputs)
              labelxy.setRect(neuronxy[i][0] - 5, neuronxy[i][1] + 5, 30, 30);
	     else
              labelxy.setRect(neuronxy[i][0] - 5, neuronxy[i][1] - 18, 30, 30);
	    painter.drawText(labelxy, neuroncl[i]);
	  }

    painter.setBrush(Qt::black);
    if (cneuronn == 1)
          painter.drawEllipse(neuronxy[cneuron[0]][0] - 5, neuronxy[cneuron[0]][1] - 5, 10, 10);
    if (cneuronn >= 2)
     {
     if (cneuron[0] <= cneuron[1])
      {
       for (i=cneuron[0]; i <= cneuron[1]; i++)
          painter.drawEllipse(neuronxy[i][0] - 5, neuronxy[i][1] - 5, 10, 10);
      }
      else
      {
       painter.drawEllipse(neuronxy[cneuron[0]][0] - 5, neuronxy[cneuron[0]][1] - 5, 10, 10);
       painter.drawEllipse(neuronxy[cneuron[1]][0] - 5, neuronxy[cneuron[1]][1] - 5, 10, 10);
      }
     }
    painter.setBrush(Qt::blue);
    if (cneuronn == 3)
          painter.drawEllipse(neuronxy[cneuron[2]][0] - 5, neuronxy[cneuron[2]][1] - 5, 10, 10);
    if (cneuronn == 4 && cneuron[2] <= cneuron[3])
      for (i=cneuron[2]; i <= cneuron[3]; i++)
          painter.drawEllipse(neuronxy[i][0] - 5, neuronxy[i][1] - 5, 10, 10);

   // display connection and update blocks
   if (drawblocks > 0)
   {
     pxy.setY(20);
     pxy.setX(0);
     sprintf(message,"%d neurons (%d %d %d), blocks:", nneurons, ninputs, nhiddens, noutputs);
     painter.drawText(pxy, message);
     painter.setPen(Qt::black);
     for (b=0; b < net_nblocks; b++)
	 {
	   if (net_block[b][0] == 0)
             sprintf(message,"%d) %d %d %d %d",
	       b, net_block[b][1], net_block[b][2],net_block[b][3],net_block[b][4]);
           else
             sprintf(message,"%d) %d %d",
	       b, net_block[b][1], net_block[b][2]);
	   pxy.setY(b*20+40);
	   pxy.setX(0);
	   painter.drawText(pxy, message);
	 }


     }

   }

}

/*
 * compute and return the number of neurons to be drawn
 */ 
void
RendNetwork::draw_neurons_update()
{

	int	team;
	struct  ncontroller *cncontroller;
	int     u;
	int     nlabel;
	
	ndu = 0;
	nlabel = 0;

	// update the matrix of value with the current value
	// and compute the number of neurons to be drawn
	for (team=0,cncontroller=ncontrollers; team<nteam; team++, cncontroller++)
	{
	  if (team == cindividual || drawn_alltogether == 1)
	  {
	    for(u=(cncontroller->nneurons - cncontroller->noutputs); u < cncontroller->nneurons; u++)
		  if (neurondisplay[u] == 1 && ndu < 40)
		  {	 
            //ngraph[rnrun%drawneuronsdx][ndu] = cncontroller->activation[u];
            ngraph[trace_run%drawneuronsdx][ndu] = cncontroller->activation[u];
		    ngraphid[ndu] = u;
		    ndu++;
		  }
	    for(u=cncontroller->ninputs; u < (cncontroller->nneurons - cncontroller->noutputs); u++)
		  if (neurondisplay[u] == 1 && ndu < 40)
		  {	 
            //ngraph[rnrun%drawneuronsdx][ndu] = cncontroller->activation[u];
            ngraph[trace_run%drawneuronsdx][ndu] = cncontroller->activation[u];
		    ngraphid[ndu] = u;
		    ndu++;
		  }
	    for(u=0; u < cncontroller->ninputs; u++)
		  if (neurondisplay[u] == 1 && ndu < 40)
		  {	 
            //ngraph[rnrun%drawneuronsdx][ndu] = cncontroller->activation[u];
            ngraph[trace_run%drawneuronsdx][ndu] = cncontroller->activation[u];
		    ngraphid[ndu] = u;
		    ndu++;
		  }
	  }
	}
}




/*
 * handle mouse buttons
 */
void 
RendNetwork::mousePressEvent(QMouseEvent *event)
{
		
	int x,y,b;
	int olddrawi;
	int i;
	int mode;
	
	x=event->x();
	y=event->y();
	b=event->button();


	if (b == 2)
	{
	  // right button stop evolution or testindividual or testpopulation
	if (testindividual_is_running == 1 || testpopulation_is_running == 1 || n_fixed_evolution_is_running == 1 || n_variable_evolution_is_running == 1 || n_fixed_evolution_social_is_running == 1)
	   userbreak = 1;
	}
        else
	{
	 // left button
	 if (b == 1 && n_fixed_evolution_is_running == 0 && n_variable_evolution_is_running == 0 && n_fixed_evolution_social_is_running == 0)
	 {
	  mode = 0;
	  rnmousex = x;
	  rnmousey = y;

	  //   on a neuron: select the neuron (eventually as part of a block)
	  if (mode == 0)
	   for(i=0; i < ncontrollers->nneurons; i++)
	   {
	    if (mdist((float) x,(float) y, neuronxy[i][0], neuronxy[i][1]) < 10.0)
	    {
	      if (cneuronn < 4)
	      {
	       cneuron[cneuronn] = i;
	       cneuronn++;
	      }
	      else
	      {
	       cneuronn = 0;
	      }
	      mode = 2;
	    }
	   }
	  
	  // otherwise remove selected neurons
	  if (mode == 0)
	    cneuronn = 0;
	  repaint();
	 }
	}


}


/*
 * handle mouse move events
 */
void 
RendNetwork::mouseMoveEvent(QMouseEvent *event)
{

	int x,y;
	int i;
	double dist;
	int mode;
	QPoint xy;
	float *o;
	int   cn;
	char  nobject[24];
	int   idobject;
	char  sbuffer[128];


	if (n_fixed_evolution_is_running == 0 && n_variable_evolution_is_running == 0 && n_fixed_evolution_social_is_running == 0)
	{

	x=event->x();
	y=event->y();

	mode = 0;

	// move current neuron
        cneuronn = 0;
	if (mode == 0)
         {
	 for(i=0, cn=-1; i < ncontrollers->nneurons; i++)
	   if (mdist((float) rnmousex,(float) rnmousey, neuronxy[i][0], neuronxy[i][1]) < 10.0)
	     cn = i;
	 if (cn >= 0)
	      {
	        neuronxy[cn][0] = x;
	        neuronxy[cn][1] = y;
	        repaint();
		rnmousex = x;
		rnmousey = y;
		mode = 1;
		sprintf(sbuffer,"neuron #%d moved to %d %d", cn, neuronxy[cn][0], neuronxy[cn][1]);
		display_stat_message(sbuffer);
	      }
	 	 }
	}
}


/*
 * handle mouse release events by constraints movements to the grid (if any)
 */
void 
RendNetwork::mouseReleaseEvent(QMouseEvent *event)
{

	int i;
	int mode;
	QPoint xy;
	float *o;
	float oldp1,oldp2;
	int   cn;
	char  nobject[24];
	int   idobject;
	char  sbuffer[128];


	if (n_fixed_evolution_is_running == 0 && n_variable_evolution_is_running == 0 && n_fixed_evolution_social_is_running == 0 && grid > 0 )
	{

	rnmousex=event->x();
	rnmousey=event->y();

	mode = 0;

	// move current neuron
	
	if (mode == 0)
	 for(i=0, cn=-1; i < ncontrollers->nneurons; i++)
	   if (mdist((float) rnmousex,(float) rnmousey, neuronxy[i][0], neuronxy[i][1]) < 10.0)
	     cn = i;
	 if (cn >= 0)
	      {
		oldp1 = neuronxy[cn][0];
		oldp2 = neuronxy[cn][1];
		neuronxy[cn][0] = (float) (((int) neuronxy[cn][0]) / grid * grid);
		neuronxy[cn][1] = (float) (((int) neuronxy[cn][1]) / grid * grid);
		if ((oldp1 - neuronxy[cn][0]) > (grid / 2))
		  neuronxy[cn][0] += grid;
		if ((oldp2 - neuronxy[cn][1]) > (grid / 2))
		  neuronxy[cn][1] += grid;
	        repaint();
		sprintf(sbuffer,"neuron #%d released to %d %d", cn, neuronxy[cn][0], neuronxy[cn][1]);
		display_stat_message(sbuffer);
		mode = 1;
	      }

	if (mode == 3)
		{
		oldp1 = *o;
		oldp2 = *(o + 1);
		*o = (float) (((int) *o) / grid * grid);
		*(o + 1) = (float) (((int) *(o + 1)) / grid * grid);
		if ((oldp1 - *o) > (grid / 2))
		  *o += grid;
		if ((oldp2 - *(o + 1)) > (grid / 2))
		  *(o + 1) += grid;
		sprintf(sbuffer,"object %s #%d released to %.1f %.1f", nobject, idobject, *o, *(o + 1));
		display_stat_message(sbuffer);
	        repaint();
		}

	}


}

/*
 * handle mouse buttons
 */
void 
RendNetwork::mouseDoubleClickEvent(QMouseEvent *event)
{
	
}


/*
 * This function copy delta, or weight, or biases, or label into neurons clabels
 * and into the graphicp parameter vector
 */
void
pseudo_activate_net(float *p, int nneurons, int ddelta, int dweight, int dbias)

{
    int i;
    int t;
    int b;
    float delta;
    int   updated[40]; 
    float **gp;
    int   ngp;

    gp = graphicp;
    ngp = 0;

    if (ddelta == 0 && dweight == 0 && dbias == 0)
     {
      for(i=0;i < nneurons;i++)
        strcpy(neuroncl[i],neuronl[i]); 
      create_fvpparameter("graphicp", "#interface",graphicp, 0, 0, "");
     }
    else
     {
     for(i=0;i < nneurons;i++)
      updated[i] = 0;
     // biases
     for(i=0;i < nneurons;i++)
	 {
	   if (neuronbias[i] == 1)
	   {
	     if (dbias > 0)
	      {
               sprintf(neuroncl[i],"%.1f",*p);
	       *gp = p;
	       gp++;
	       ngp++;
              }
	      p++;
	   }
	 }

     // blocks
     for (b=0; b < net_nblocks; b++)
	 {	  
	   // connection block
	   if (net_block[b][0] == 0)
	   {
	     for(t=net_block[b][1]; t < net_block[b][1] + net_block[b][2];t++)
	      for(i=net_block[b][3]; i < net_block[b][3] + net_block[b][4];i++)
	      {
		// extract the weights
                if (dweight == 1 && cneuron[0] == t) 
	         {
                  sprintf(neuroncl[i],"%.1f",*p);
		  *gp = p;
	          gp++;
	          ngp++;
	         }
	        p++;
	      }
	     
	   } 
	   // update block
	   if (net_block[b][0] == 1)
	   {
	     for(t=net_block[b][1]; t < (net_block[b][1] + net_block[b][2]); t++)
	      {
                  updated[t] +=1;
		  if (neurondelta[t] == 0)
		   {
		    ;
		   }
		   else
		   {
		    delta = (float) (fabs((double) *p) / wrange);
		    if (ddelta > 0)
		     {
                      sprintf(neuroncl[t],"%.1f",delta);
		      *gp = p;
	              gp++;
	              ngp++;
                     }
		    p++;
		   }
	      }
	   }
	 }

      create_fvpparameter("graphicp", "#interface",graphicp, ngp, 0, "");
     }
}

// -----------------------------------------------------
// Dialog
// -----------------------------------------------------

NetworkDialog::NetworkDialog(QWidget *parent) :
	QDialog(parent),
	m_mainLayout(NULL),
	m_layout(NULL),
	m_toolBar(NULL)
{
	// Creating the main layout. Direction is BottomToTop so that m_layout (added first)
	// is at bottom, while the toolbar (added last) is on top
	m_mainLayout = new QBoxLayout(QBoxLayout::BottomToTop, this);
	//m_mainLayout->setContentsMargins(0, 0, 0, 0);

	// Creating the layout for external widgets
	m_layout = new QGridLayout();
	//m_layout->setContentsMargins(0, 0, 0, 0);
	m_mainLayout->addLayout(m_layout);

    createActions();
	createToolBars();
}

NetworkDialog::~NetworkDialog()
{
}

void NetworkDialog::setWidget(QWidget *w, int r, int c)
{
	m_layout->addWidget(w, r, c);
}

QToolBar* NetworkDialog::toolBar()
{
	if (m_toolBar == NULL) {
		// Creating toolbar and adding it to the main layout
		m_toolBar = new QToolBar(this);
		m_mainLayout->addWidget(m_toolBar);
	}

	return m_toolBar;
}

void NetworkDialog::closeEvent(QCloseEvent *)
{
	emit dialogClosed();
}

void NetworkDialog::keyReleaseEvent(QKeyEvent* event)
{
    /*
	if (event->matches(QKeySequence::Print)) {
		// Taking a screenshow of the widget
		shotWidget();
	} else {
		// Calling parent function
		QDialog::keyReleaseEvent(event);
	}*/
}

void NetworkDialog::shotWidget()
{
    /*
	// Taking a screenshot of this widget
	QPixmap shot(size());
	render(&shot);

	// Asking the user where to save the shot
	QString fileName = QFileDialog::getSaveFileName(this, tr("Save Shot"), "./widget.png", tr("Images (*.png *.xpm *.jpg)"));
	if (!fileName.isEmpty()) {
		shot.save(fileName);

		QMessageBox::information(this, QString("File Saved"), QString("The widget shot has been saved"));
	}*/
}



#endif
