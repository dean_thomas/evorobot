/* 
 * Evorobot* - utility.cpp
 * Copyright (C) 2009, Stefano Nolfi 
 * LARAL, Institute of Cognitive Science and Technologies, CNR, Roma, Italy 
 * http://laral.istc.cnr.it

 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
#include "public.h"

#ifdef EVOWINDOWS 
#include <windows.h>     // for Sleep function
#endif 
#ifdef EVOLINUX
#include <unistd.h>      // for Sleep function
#endif
#ifdef EVOMAC
#include <unistd.h>      // for Sleep function
#endif


/*
 * return a random float between -n and + n
 */ 
float
rans(double n)
	
{


#ifdef EVOWINDOWS 
   static	double max = 32767;
   return ((float) (n - (2.0 * n *((double) rand() / max))));
#endif
#ifdef EVOLINUX
   return 2.0*n*((double)rand()/(double)RAND_MAX) - n;
#endif
#ifdef EVOMAC
   return 2.0*n*((double)rand()/(double)RAND_MAX) - n;
#endif

}


/*
 * return an integer between 0 and (i-1)
 */
int
mrand(int i)


{
	int r;

	r = rand();
	r = r % (int) i;
	return r;
}


/* 
*  return a random float between min and max
*/
float randomize(float min, float max)
{
	float r;	

#ifdef EVOWINDOWS 
	r = (float)rand() / (RAND_MAX + 1) * (max - min) + min;
#endif
#ifdef EVOLINUX
	r = rand() / (32767 + 1) * (max - min) + min;
#endif
#ifdef EVOMAC
	r = rand() / (32767 + 1) * (max - min) + min;
#endif

	return r;
}

/*
 * set the seed for the random number generator
 */
void set_seed(int s)

{
   srand(s);			// imposta il seme dei numeri casuali sul seed della replica
}

/*
 * pause for n ms
 */
void pause(int n)

{
  #ifdef EVOWINDOWS
   Sleep(n);
  #endif 
  #ifdef EVOLINUX
   usleep(n * 1000);
  #endif
  #ifdef EVOMAC
   usleep(n * 1000);
  #endif
}

/*
 * return the distance between two points
 */

double
mdist(float x, float y, float x1, float y1)

{
   double qdist;

   qdist = ((x-x1)*(x-x1)) + ((y-y1)*(y-y1));
   return(sqrt(qdist));
}





