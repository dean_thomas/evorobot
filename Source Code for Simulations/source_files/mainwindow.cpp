/*
 * Evorobot* - mainwindow.cpp
 * Copyright (C) 2009, Stefano Nolfi
 * LARAL, Institute of Cognitive Science and Technologies, CNR, Roma, Italy
 * http://laral.istc.cnr.it

 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

//#include "defs.h"

#include "public.h"
#include "evolution.h"

#ifdef EVOGRAPHICS
#ifdef EVOWINDOWS
#include <QtGui>
#include <QApplication>
#include <QScrollArea>
#include <QCheckBox>
#endif
#ifdef EVOLINUX
#include <Qt/qapplication.h>
#include <Qt/qscrollarea.h>
#include <Qt/qgridlayout.h>
#include <Qt/qfiledialog.h>
#include <Qt/qmessagebox.h>
#include <Qt/qicon.h>
#include <Qt/qmenubar.h>
#include <Qt/qaction.h>
#include <Qt/qtoolbar.h>
#include <Qt/qstatusbar.h>
#include <Qt/qsettings.h>
#include <QtGui/qscrollbar.h>
#include <QtGui/qlineedit.h>
#endif
#ifdef EVOMAC
#include <Qt/qapplication.h>
#include <Qt/qscrollarea.h>
#include <Qt/qgridlayout.h>
#include <Qt/qfiledialog.h>
#include <Qt/qmessagebox.h>
#include <Qt/qicon.h>
#include <Qt/qmenubar.h>
#include <Qt/qaction.h>
#include <Qt/qtoolbar.h>
#include <Qt/qstatusbar.h>
#include <Qt/qsettings.h>
#include <QtGui/qscrollbar.h>
#include <QtGui/qlineedit.h>
#endif

#include "mainwindow.h"

#ifdef EVOWINDOWS
const int IdRole = Qt::UserRole;
#endif

MainWindow *mainWin;             // The main window
QLineEdit  *lineedit;            // The command line widget

int mainrendparam = 0;           // Whether the param window is on or off
int mainrendnetwork = 0;         // Whether the network window is on or off
int mainrendrobot = 0;           // Whether the robot window is on or off
int mainrendevolution = 0;       // Whether the evolution window is on or off
int mainrendrobot3d = 0;         // Whether the robot3d window is on or off
int userbreak=0;                 // Whether the user press the stop buttom   
void init_modules(); 

//------------------------------------------------------------------------
//---------------- Public Functions --------------------------------------
//------------------------------------------------------------------------

/*
 * display a message on the statusbar
 */
void display_stat_message(char *emessage)
{

	mainWin->statusBar()->showMessage(emessage);
    mainWin->statusBar()->repaint();
}


/*
 * display an error message in a box and exit (graphic)
 */
void
display_error(char *emessage)

{
   mainWin->error(emessage);
}

/*
 * display a warinig message in a box (graphic)
 */
void
display_warning(char *emessage)

{

   mainWin->warning(emessage);


}

/*
 * display a warinig message in a box (graphic)
 */
void
update_events()
{
    QCoreApplication::processEvents();
}




//------------------------------------------------------------------------
//---------------- Private Functions -------------------------------------
//------------------------------------------------------------------------


int main(int argc, char *argv[])
{


     Q_INIT_RESOURCE(application);	// Inizializza il sistema di risorse di QT
     QApplication app(argc, argv);  // Gestisce il flusso di controllo e le impostazioni

     mainWin = new MainWindow;		// Inizializza la finestra ed i men� in essa contenuti
     init_modules();				// Carica il .cf ed inizializza tutti i parametri del programma vero 
									// e proprio
     mainWin->show();				// Visualizza tutti i contenuti

	 // Verificare il funzionamento su MacOs
	 #ifdef EVOWINDOWS
	 if (argc>1) {					// il primo argomento � il nome dell'eseguibile
		 //if ((argv[1][0]=='-') && (argv[1][1]=='e') && (argv[1][2]=='1')) { 		// evolution
		 if ((argv[1][0]=='-e') && (argv[1][1]=='1')) { 		// evolution
			 run_n_fixed_evolution(0);  // run an individual n-fixed evolution
		 } else {
			 //printf("Formato : evorobotstar -e[1-n]");
			 display_error("Command line format : evorobotstar -e[1-n]");
			 getchar();
			 exit(1);
		 }
	 }
	 #endif

	 #ifdef EVOMAC 
	 #ifdef EVOLUTION 
	 run_n_fixed_evolution(0);		// run an individual n-fixed evolution
	 #endif
	 #ifdef EVOLUTION_SOCIAL 
	 run_n_fixed_evolution(1);		// run an individual n-fixed evolution
	 #endif
	 #ifdef LABORATORY_TESTING 
	 test(10);						// run a test in laboratory
	 #endif
	 #endif
    
	 return app.exec();				// entra nel ciclo principale di eventi
}


MainWindow::MainWindow()			// Ridefinzione del costruttore di MainWindow
{

    Window* window = new Window(this);

    QScrollArea* scroll = new QScrollArea( this );
    setCentralWidget( scroll );
    scroll->setWidget( window );

    createActions();
    createMenus();
    createToolBars();
    createStatusBar();

    setWindowTitle("Artificial Life / Evolutionary Agents");
}


/*
 * Create a gridlayout that include a graphic
 * and a lineedit widget
 */
Window::Window(MainWindow* main)
{
    QGridLayout *mainLayout = new QGridLayout;

	//load_envdim("evorobot.env");					// Legge eventualmente la dimenasione dell'ambiente reale

	init_rendevolution(1);
    add_evolution_widget(mainLayout, 0, 0);

	init_rendrobot(1);
    add_robot_widget(mainLayout, 0, 0);

    init_rendnetwork(1);
    add_network_widget(mainLayout, 0, 1);

    setLayout(mainLayout);
    
}

void MainWindow::about()
{
   QMessageBox::about(this, tr("About Evorobot*"),

   tr("<b>Evorobot* 2.0</b> is a tool for developing adaptive robots"
       "<br>Copyright (C) 2009"
       "<br>Stefano Nolfi & Onofrio Gigliotta, LARAL, ISTC-CNR, Roma, Italy, http://laral.istc.cnr.it"));
}




void MainWindow::createActions()
{
	//	Create the exit menu item
    exitAct = new QAction(tr("E&xit"), this);
    exitAct->setShortcut(tr("Ctrl+Q"));
    exitAct->setStatusTip(tr("Exit the application"));
    connect(exitAct, SIGNAL(triggered()), this, SLOT(close()));

	//	Create the about box menu item
    aboutAct = new QAction(tr("&About"), this);
    aboutAct->setStatusTip(tr("Show the application's About box"));
    connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));

	//	Create the 'n-fixed evolution' menu item
    runevoAct = new QAction(tr("&n-fixed Evolution"), this);
    runevoAct->setStatusTip(tr("Run the n-fixed evolutionary process"));
    connect(runevoAct, SIGNAL(triggered()), this, SLOT(qt_n_fixed_evolution()));

	//	Create the 'n-fixed social evolution' menu item
	runncevoAct = new QAction(tr("&n-fixed Social Evolution"), this);
    runncevoAct->setStatusTip(tr("Run the n-fixed social evolutionary process"));
    connect(runncevoAct, SIGNAL(triggered()), this, SLOT(qt_n_fixed_evolution_social()));

	//	Create the 'n-variable social evolution' menu item
    runalevoAct = new QAction(tr("&n-variable Social Evolution"), this);
    runalevoAct->setStatusTip(tr("Run the n-variable social evolutionary process"));
    connect(runalevoAct, SIGNAL(triggered()), this, SLOT(qt_n_variable_evolution()));

	//	Create the 'test' menu item 
	testAct = new QAction(tr("&Test"), this);
    testAct->setStatusTip(tr("Test current individual"));
    connect(testAct, SIGNAL(triggered()), this, SLOT(qt_test_individual()));

    testiAct = new QAction(tr("&Test selected individual"), this);
    testiAct->setStatusTip(tr("Test selected individual"));
    connect(testiAct, SIGNAL(triggered()), this, SLOT(qt_test_one_individual()));

	testpopAct = new QAction(tr("&Test selected population"), this);
    testpopAct->setStatusTip(tr("Test selected population"));
    connect(testpopAct, SIGNAL(triggered()), this, SLOT(qt_test_population()));

	testsiAct = new QAction(tr("&Laboratory : Test some selected individual"), this);
	testsiAct->setStatusTip(tr("Laboratory : Test some selected individual"));
    connect(testsiAct, SIGNAL(triggered()), this, SLOT(qt_test_some_individual()));

	testbiAct = new QAction(tr("&Test best"), this);
    testbiAct->setStatusTip(tr("Test the best individual"));
    connect(testbiAct, SIGNAL(triggered()), this, SLOT(qt_test_best_individual()));

    testallAct = new QAction(tr("&Test all"), this);
    testallAct->setStatusTip(tr("Test all individuals"));
    connect(testallAct, SIGNAL(triggered()), this, SLOT(qt_test_all_individuals()));

    testallseedbestAct = new QAction(tr("&Test all seed of bests individuals"), this);
    testallseedbestAct->setStatusTip(tr("Test all seed of best individuals simultaneously"));
    connect(testallseedbestAct, SIGNAL(triggered()), this, SLOT(qt_test_all_seed_best()));

	testallseedAct = new QAction(tr("&Test all seed of individuals of population"), this);
    testallseedAct->setStatusTip(tr("Test all seed of individuals of population simultaneously"));
    connect(testallseedAct, SIGNAL(triggered()), this, SLOT(qt_test_all_seed()));

	testsingleAct = new QAction(tr("&Test socially evolved individuals alone"), this);
    testsingleAct->setStatusTip(tr("Test socially evolved individuals individually in the environment"));
    connect(testsingleAct, SIGNAL(triggered()), this, SLOT(qt_test_alone()));

	testdoublepopAct = new QAction(tr("&Test double population (with clones)"), this);
    testdoublepopAct->setStatusTip(tr("Test socially evolved individuals in a double population by generating clones"));
    connect(testdoublepopAct, SIGNAL(triggered()), this, SLOT(qt_test_double()));

	testhalfpopAct = new QAction(tr("&Test an half of socially evolved population"), this);
    testhalfpopAct->setStatusTip(tr("Test socially evolved individuals in an half population by randomly eliminating individuals"));
    connect(testhalfpopAct, SIGNAL(triggered()), this, SLOT(qt_test_half()));

	//	Testing of leadership attempts
	testLeadershipAttemptsAct = new QAction(tr("Test leadership attempts"), this);
    testLeadershipAttemptsAct->setStatusTip(tr("Test attempts at leadership where a single robot enters a fitness zone"));
    connect(testLeadershipAttemptsAct, SIGNAL(triggered()), this, SLOT(qt_test_leadershipAttempt()));

	userbreakAct = new QAction(tr("&Stop"), this);
    userbreakAct->setStatusTip(tr("Stop evolution or Test"));
    connect(userbreakAct, SIGNAL(triggered()), this, SLOT(qt_userbreak()));

    showparamAct = new QAction(QIcon(":/images/parameters.png"), tr("Parameters window"), this);
    showparamAct->setStatusTip(tr("Open/close parameters window"));
    connect(showparamAct, SIGNAL(triggered()), this, SLOT(show_param()));

    showevolutionAct = new QAction(QIcon(":/images/fitness.png"), tr("Adaptation window"), this);
    showevolutionAct->setStatusTip(tr("Open/close adaptation window"));
    connect(showevolutionAct, SIGNAL(triggered()), this, SLOT(show_evolution()));

    shownetworkAct = new QAction(QIcon(":/images/showNetwork.png"), tr("Nervous-system window"), this);
    shownetworkAct->setStatusTip(tr("Open/close nervous-system window"));
    connect(shownetworkAct, SIGNAL(triggered()), this, SLOT(show_network()));

    showrobotAct = new QAction(QIcon(":/images/robot-env.png"), tr("Robot window"), this);
    showrobotAct->setStatusTip(tr("Open/close robot window"));
    connect(showrobotAct, SIGNAL(triggered()), this, SLOT(show_robot()));

    showrobot3dAct = new QAction(QIcon(":/images/epuck.png"), tr("Robot 3d window"), this);
    showrobot3dAct->setStatusTip(tr("Open/close robot 3dwindow"));
    connect(showrobot3dAct, SIGNAL(triggered()), this, SLOT(show_robot3d()));
}

void MainWindow::createMenus()
{
    mMenu = menuBar()->addMenu(tr("&Run"));

    mMenu->addAction(testAct);
    mMenu->addAction(testiAct);
    mMenu->addAction(testpopAct);
    mMenu->addAction(testsiAct);
    mMenu->addAction(testbiAct);
    mMenu->addAction(testallAct);
    mMenu->addAction(testallseedbestAct);
    mMenu->addAction(testallseedAct);
    mMenu->addAction(testsingleAct);
    mMenu->addAction(testdoublepopAct);
    mMenu->addAction(testhalfpopAct);
	//	Test attempts at leadership
	mMenu->addAction(testLeadershipAttemptsAct);
	//
    mMenu->addAction(runevoAct);
    mMenu->addAction(runncevoAct);
    mMenu->addAction(runalevoAct);
    mMenu->addAction(userbreakAct);
    mMenu->addAction(exitAct);

    helpMenu = menuBar()->addMenu(tr("&Help"));
    helpMenu->addAction(aboutAct);
}

void MainWindow::createToolBars()
{

    mToolBar = addToolBar(tr("Show"));
    mToolBar->addAction(showparamAct);
    mToolBar->addAction(showevolutionAct);
    mToolBar->addAction(shownetworkAct);
    mToolBar->addAction(showrobotAct);
    mToolBar->addAction(showrobot3dAct);

}

void MainWindow::createStatusBar()
{
    statusBar()->showMessage(tr("Ready"));
}


/*
 * show or hide the network window
 */
void MainWindow::show_network()
{
    if (mainrendnetwork == 0)
     {
      init_rendnetwork(2);
      mainrendnetwork = 1;
     }
    else
     {
      hide_rendnetwork();
      mainrendnetwork = 0;
     }
}

/*
 * show or hide the robot window
 */
void MainWindow::show_robot()
{
    if (mainrendrobot == 0)
    {
		init_rendrobot(2);
		mainrendrobot = 1;
    }
    else
    {
		hide_rendrobot();
		mainrendrobot = 0;
    }
}

/*
 * show or hide the robot window
 */
void MainWindow::show_robot3d()
{
#ifdef EVO3DGRAPHICS
//    init_rend3drobot();
#endif 
}

/*
 * show or hide the robot window
 */
void MainWindow::show_evolution()
{
    if (mainrendevolution == 0)
    {
		init_rendevolution(2);
		mainrendevolution = 1;
    }
    else
    {
		hide_rendevolution();
		mainrendevolution = 0;
	}
}

/*
 * show or hide the parameter window
 */
void MainWindow::show_param()
{
    if (mainrendparam == 0)
     {
      init_rendparam();
      mainrendparam = 1;
     }
    else
     {
      hide_rendparam();
      mainrendparam = 0;
     }

}


/****************************************************************************
 * SLOTS toward functions that do not belong to the graphic interface
 */

void MainWindow::qt_n_fixed_evolution()
{
	// run an individual n-fixed evolution
    run_n_fixed_evolution(0);		
}

void MainWindow::qt_n_fixed_evolution_social()
{
	// run a social n-fixed evolution
	run_n_fixed_evolution(1);		
}

void MainWindow::qt_n_variable_evolution()
{
	// run n-variable evolution
    run_n_variable_evolution();		
}

void MainWindow::qt_test_individual()
{
	//	run an inidivual test
    test(0);
}

void MainWindow::qt_test_one_individual()
{

    test(1);

}

void MainWindow::qt_test_some_individual()
{

    test(10);

}

void MainWindow::qt_test_population()
{

    test(4);
}

void MainWindow::qt_test_best_individual()
{

    test(2);

}

void MainWindow::qt_test_all_individuals()
{

    test(3);
}

void MainWindow::qt_test_all_seed_best()
{

    test(8);
}

void MainWindow::qt_test_all_seed()
{

    test(9);
}

void MainWindow::qt_test_alone()
{
    test(5);			// test socially evolved individuals alone
}

void MainWindow::qt_test_double()
{
    test(6);			// test double population by cloning each individual
}

void MainWindow::qt_test_half()
{
    test(7);			// test an half of population
}


void MainWindow::qt_test_leadershipAttempts()
{
	
}

void MainWindow::qt_userbreak()
{

    userbreak = 1;
}


void MainWindow::error(char *emessage)
{
   QMessageBox::about(this, "ERROR", emessage);
}

void MainWindow::warning(char *emessage)
{
   QMessageBox::about(this, "WARNING", emessage);
}

#else

int main()
{
 userbreak = 0;

 init_modules();
 run_n_fixed_evolution(0);
 return 0;
}


void
update_renderarea()

{

}

void
update_basic_graphic()

{

}

void
create_neurons_labels()

{


}

/*
 * display a message on the statusbar
 */
void display_stat_message(char *emessage)

{

    printf("%s\n", emessage);

}


void display_message(char *emessage)

{

    printf("%s\n", emessage);


}

void display_error(char *emessage)

{

    printf("ERROR:%s\n", emessage);


}

void display_warning(char *emessage)

{

    printf("WARNING:%s\n", emessage);


}

void update_rendevolution(int mode)
{

}

void update_rendnetwork(int cycle)
{

}

void update_rendrobot()
{

}

void update_events(){}
#endif

/*
 * ininitialize all define modules and load parameters from evorobot.cf
 */
void init_modules()
{
	int p;
	int team;
	struct  individual *pind;
	//  Dichiara l'ascissa di start di tutti i robot
	//	Declares the x-coordinate of start of all the robots
	extern int    *startx;				   
	//  Dichiara l'ordinata di start di tutti i robot
	//	Declares the y-coordinate of start of all the robots
	extern int    *starty;				   
	int *sx, *sy;

	//  NOTA ! : possono essere fissati max 10 individui !!
	//  Allora un numero fittizio di elementi dell'array per poter posizionare i robot 
	//  in posizioni fisse, non sapendo a priori quanto vale nteam
	//  Deve allocare startx a questo stadio, per posizionare il robot in posizioni fisse 
	//  Alloca e Inizializza i vettori che contengono le posizioni fisse dei robot
	//	========================================================================================
	//	NOTE! : Can be set up to 10 individuals! Then a fictive number of elements in the array 
	//	in order to position the robot in fixed positions, not knowing in advance what it's 
	//	worth. Nteam must allocate startx at this stage to position the robot in fixed positions
	//	Allocates and Initializes vectors containing the fixed locations robot
	if (startx == NULL) 
	{
		startx = (int *) malloc(MAXFIXEDPOS * sizeof(int)); 
		if (startx == NULL) 
		{
			display_error("startx malloc error");
		}

		for (team=0, sx=startx; team < MAXFIXEDPOS; team++, sx++)
		{
			*sx=0;
		}
	}
	//  Deve allocare starty a questo stadio, per posizionare il robot in posizioni fisse 
	//  Alloca e Inizializza i vettori che contengono le posizioni fisse dei robot
	//	=========================================================================================
	//  Must allocate starty at this stage to position the robot in fixed positions
	//	Allocates and Initializes vectors containing the fixed positions of the robots
	if (starty == NULL) 
	{
		starty = (int *) malloc(MAXFIXEDPOS * sizeof(int)); 
		if (starty == NULL)
		{
			display_error("starty malloc error");
		}

		for (team=0, sy=starty; team < MAXFIXEDPOS; team++, sy++)
		{
			*sy=0;
		}
	}

	//  1. we create the Robot, Network, and Evolution classes and parameters
	//	=====================================================================
	//  Create all the parameter of robots evolutionary process
	create_robot_env_par();		
	create_network_par();
	create_evolution_par();

	//  2. we load parameters
	//	=====================
	loadparameters();

	//  3. we configure the Robot, Network, and Evolution classes
	//	=========================================================
	//	Inizializza l'ambiente (al momento prima del robot per il colore-cilindro)
	//	Initialize the environment (at the time of the first robot for the color-cylinder)
	init_env();					

	//  Inizializza i robot e le posizioni a partire dal robot di indice 0
	//  Initialize the robots and robot positions from index 0
	init_robot(0);				

	//  Inizializza la fitness di ogni individuo
	//	Initialize the fitness of each individual
	init_fitness();				

	//  Inizializza le reti neurali a disposizione dei robot a partire dal robot di indice 0
	//	Initializes neural networks available to the robot from the robot to index 0
	init_network(0);

	init_evolution();	

	//  Inizializza la posizione di ciascuno dei robot
	//	Initializes the position of each of the robot
	(*pinit_robot_pos)();		
}

