/* 
 * Evorobot* - evolution.cpp
 * Copyright (C) 2009, Stefano Nolfi 
 * LARAL, Institute of Cognitive Science and Technologies, CNR, Roma, Italy 
 * http://laral.istc.cnr.it

 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

/*
 * evolution.cpp
 */

#include "public.h"
#include "evolution.h"
#include "robot-env.h"

int		seed=0;						 // random seed                              
int		test_seed=0;				 // test seed                              
int		test_gen=0;					 // test generation                              
int		test_trial=1;				 // test                              
int     generations=0;               // number of generations                    
int     nkids=0;                     // number of kids for father                
int     bestfathers=0;               // number of fathers that make kids         
int     startgeneration = 0;         // start generation (>=0 means load weig.)  
int     percmut=2;                   // percentage of mutations                  
int     elitism=0;                   // one copy of each best is preserved intact, if 2 we save previous fitness data 
int     save_each_generation=0;      // save genotype at each generation 
int		color_into_genotype=0;		 // save RGB color components into genotype
int     savebest=1;                  // save only the best N individual each gen. 
int     saveifit=0;                  // save individuals' fitness
long int learningc=0;                // learning cycles
int     homogeneous=1;               // whether the team is formed by homogeneous individuals or not
int     nindividuals;                // number of individuals
int     npopulations=1;              // number of populations   
int     nraces=1;					 // number of races   
int		evaluate_different_inds=0;	 // evaluate different individuals in a n-fixed evolution with more races. 1=different inds without repetition, 2=different inds with repetition 
int		ntrials_different_inds=0;	 // number of trials of different individuals comparisons 
int     races_with_diffcolors=0;	 // different races with different colors
int     same_initial_genotype=0;	 // same initial genotypes
int     nindinspecies0=0;			 // number of individuals in species 0
int     nindinspecies1=0;			 // number of individuals in species 1
int     pop_seed=0;                  // whether seed is reinitialized for individuals of the same generation
int     additional_ind=0;            // n. of additional genome to be allocated
int     nreplications=0;             // n. of replications 
int     nteam=1;					 // number of individual situated in the same environment
int     team_with_diffcolors=0;		 // different team members have different colors
int     dindividual=0;               // current individual of the population
int     cindividual=0;               // current individual of the team 
int     ngenes=0;                    // number of genes for each individual genome
int	    **genome;					 // genome of the population
int	    **secondgenome;				 // second loaded genome of the population
int	    **bestgenome;				 // genome of the best individuals
int	    **fixedgenome;				 // genome of fixed individuals
int		**explorationarray;			 // exploration array
int		**barycentrearray;			 // barycentre array, attenzione test da fare solo su un epoca altrimenti non ha senso !!
int		**barycentredistancesarray;	 // array of distances between each individual and barycentre of the entire group !!
int		**selected_ind_array;		 // array to track selected individuals in evaluate_different_inds mode
//int		***explorationarray_eco; // exploration array in ecology
int		**distances_from_fz_array;	 //	Average distances from food zones of individuals
int		explorationarray_eco[100][100][40];
int     loadfixedgenotypes=0;        // control whether we load fixed genotypes
int     *loadfixedgn;                // the id of the fixed individuals 
int     masterturnament=0;           // master turnament                      
int     testindividual_is_running=0; // one individual is currently tested
int		testpopulation_is_running=0; // entire population is currently tested
int     labtest_is_running=0;		 // laboratory testing is currently tested
int     n_fixed_evolution_is_running=0;			// n-fixed evolution is currently running
int     n_fixed_evolution_social_is_running=0;  // social n-fixed evolution is currently running
int     n_variable_evolution_is_running=0;		// n-variable evolutionary process is running

double  tfitness[2][MAXRACES][MAXINDIVIDUALS]; // populations fitness                  
double  pfitness[MAXINDIVIDUALS];    // fitness gathered in previous generations
int     plifes[MAXINDIVIDUALS];      // previous life periods in which fitness was gathered 
double  bpfitness[MAXINDIVIDUALS];   // best individuals - fitness gathered in previous generations
int     bplifes[MAXINDIVIDUALS];     // best individuals - previous life periods in which fitness was gathered 
double  statfit[MAXGENERATIONS][4];  // best and mean fit for each gen. & pop per race  
double  statfit_min;				 // minimum fitness per race
double  statfit_max;				 // maximum fitness per race
int     statfit_n;					 // n fitness data per race
double  astatfit[MAXGENERATIONS][4]; // statfit averaged for replications per race 
float   mem_bar_orig[10][4];		 // memorizza la posizione originaria della sbarra

extern double getindfitness(int nind);
extern int getindrace(int nind);
extern int display_under_evolution;					// display robots under evolution	
extern int n_ind_to_display_under_evolution;
extern struct individual *ind;
extern struct iparameters *ipar;					// individual parameters 
extern int	test_statistics;						// test statistics on trials 
extern void modify_individual_color(struct individual *pind);
extern int	*successruntime;						/* run time needful for successes of individuals	 */
extern float (*pffitness)(struct  individual  *pind);					// pointer to fitness function()
extern float ffitness_predator_prey2(struct  individual  *pind);	    // fitness nello studio della predazione per la formazione di gruppi, le prede sono premiate se vivono pi� a lungo
extern long int success_counter;					// Contatore dei successi nel conseguimento del compito
extern int   amoebaepochs;							// number of epochs                        
extern long int sweeps;							    // number of step for each trial
extern int	*successfirstsideruntime;	/* run time needful for successes of first side bar  */
extern int	*successsecondsideruntime;	/* run time needful for successes of second side bar */
extern long int first_success_counter;	// Contatore dei successi nel conseguimento del compito
extern long int second_success_counter;	// Contatore dei successi nel conseguimento del compito
extern int  explorindnumber;			// exploration test on individual of number X
extern int	test_measure_leadership;	// measure of leadership on/off
extern int	test_measure_leadership_eco;	// measure of leadership on/off
extern int  test_exploratory;			// measure of exploratory ability into 1 gen
extern int  test_exploratory_eco;		// measure of exploratory ability into 1 gen
extern int  test_times;					// measure of times
extern int  test_exploratory_all_gen;	// measure of exploratory ability into all gen
extern int  test_times_all_gen;			// measure of times into all gen
extern int  save_ind_fitness;			// save fitness of each individual
extern int  test_fitness_lab;			// test fitness into laboratory 
extern int  test_fitness_lab_all_gen;	// test fitness into laboratory all gen
extern int  test_times_all_gen_eco;		// test times into the ecology
extern double	*distanceaverage_ave_epoch;
extern int	test_nrace;					// test of only this number of race
extern int	test_distance_average_per_cycle;			// test distance average per cycle
extern int	test_distance_average_per_cycle_all_gen;	// test distance average per cycle
extern int  test_exploratory_all_gen_eco;
extern int  test_mobility_all_gen_eco;
extern int  test_barycentre_distances_all_gen;
extern int  test_barycentre;
extern int  test_barycentre_distances;
extern float ffitness_groupdiscrim(struct  individual  *pind);
extern int  test_output_type;
extern int  test_distance_from_fz;
extern int  test_hamming_distance;
extern int	subst_array[];
extern int	RColor_fake;				// Red Color component of substituted individual
extern int  GColor_fake;				// Green Color component of substituted individual
extern int	BColor_fake;				// Blue Color component of substituted individual
extern int	RColor_new;					// Red Color new component of substituted individual
extern int  GColor_new;					// Green Color new component of substituted individual
extern int	BColor_new;					// Blue Color new component of substituted individual

extern bool isDaytime;					//	True if it is daytime in the environment false if else

/*
 * create the evolution parameters
 */
void
create_evolution_par()

{
    int i;

   create_iparameter("npopulations","adaptation",&npopulations, 0, 0, "n. of evolving populations");
   create_iparameter("nraces","adaptation",&nraces, 0, MAXRACES, "number of races into population");
   create_iparameter("races_with_diffcolors","adaptation",&races_with_diffcolors , 0, 1, "Different races with different colors");
   create_iparameter("same_initial_genotype","adaptation",&same_initial_genotype , 0, 1, "Same initial genotypes");
   create_iparameter("evaluate_different_inds","adaptation",&evaluate_different_inds, 0, 2, "Evaluate different individuals in a n-fixed evolution with more races. 1=different inds without repetition, 2=different inds with repetition");
   create_iparameter("ntrials_different_inds","adaptation",&ntrials_different_inds, 0, 0, "Number of trials of different individusls comparisons");
   create_iparameter("nindinspecies0","adaptation",&nindinspecies0, 0, 0, "number of individuals into species 0");
   create_iparameter("nindinspecies1","adaptation",&nindinspecies1, 0, 0, "number of individuals into species 1");
   create_iparameter("nteam","adaptation",&nteam, 0, 0, "number of individuals forming a team");
   create_iparameter("team_with_diffcolors","adaptation",&team_with_diffcolors , 0, 1, "Different team members have different colors");
   create_iparameter("homogeneous","adaptation",&homogeneous, 0, 1, "whether the individual of the team are homogeneous or not");
   create_iparameter("ngenerations","adaptation",&generations, 0, 0, "number of generations");

   create_iparameter("nreproducing","adaptation",&bestfathers, 0, 0, "number of individuals allowed to reproduce");
   create_iparameter("offspring","adaptation",&nkids, 0, 0, "number of offspring");
   create_iparameter("learningc","adaptation",&learningc, 0, 1, "number of learning cycles");
   create_iparameter("nreplications","adaptation",&nreplications, 0, 0, "number of replications of the adaptive process");
   create_iparameter("seed","adaptation",&seed, 0, 0, "the seed of the random number generator");
   create_iparameter("pop_seed","adaptation",&pop_seed, 0, 0, "whether seed is the same for the individuals of the same generation");
   create_iparameter("mutation_rate","adaptation",&percmut, 0, 0, "percentage of bits that replaced with new random value");
   create_iparameter("elitism","adaptation",&elitism, 0, 2, "1=elitism 2=elitism and preservation of previous performace measure");
   create_iparameter("save_each_generation","adaptation",&save_each_generation, 0, 1, "1 save genotype of population for each generation");
   create_iparameter("color_into_genotype","adaptation",&color_into_genotype, 0, 1, "save RGB Color components into genotype");
   create_iparameter("savenbest","adaptation",&savebest, 0, 0, "number of best individuals saved for each generation");
   create_iparameter("saveifit","adaptation",&saveifit, 0, 0, "save individuals' performance");
   create_iparameter("additional_ind","adaptation",&additional_ind, 0, 0, "number of additional individuals to be allocated");
   create_iparameter("dindividual","adaptation",&dindividual, 0, 0, "current individual of the population");
   create_iparameter("cindividual","adaptation",&cindividual, 0, 0, "current individual of the team");
   create_iparameter("loadfixedgenotypes","adaptation",&loadfixedgenotypes, 0, 1, "load files evorobot?.gen for fixed individuals");

}

/*
 * allocate space for the genome of the population and for the reproducing individuals
 */
void
reset_genome()

{

    int	*gi,*gs;
    int	i,j,p;

	for (p = 0; p < npopulations; p++)						// per ogni genoma di una popolazione 
		for (i=0; i < nindividuals; i++) {					// scorre il genoma per ogni individuo
				gi = *(genome + (i + (p * (nindividuals + additional_ind))));		// carica il genoma ordinario e principale
				gs = *(secondgenome + (i + (p * (nindividuals + additional_ind))));	// carica un genoma secondario aggiuntivo per alcuni test di laboratorio	
			
				// Azzera i genomi
				for (j=0; j < ngenes; j++, gi++) {
					*gi=0;
					*gs=0;
				}
		}
}


/*
 * allocate space for the genome of the population and for the reproducing individuals
 */
void
init_evolution()

{
	int     i;
	int     j;
	int     p;
	int	    **gpi;
	int	    *gi;	
	int     team;
	int     g, gg;
	char    buffer[128];

	// update the number of free parameters for allocating enough space
    ncontroller_read_nfreep(&ngenes);
    if (homogeneous != 1)
		      ngenes = ngenes * ngenes;
	ngenes += 20;

	// Nel caso ogni individuo abbia colore diverso ed il colore venga codificato nel genotipo
	// crea spazio per tre geni in pi� per le componenti RGB
	if (color_into_genotype) 
		ngenes += 3;

	// compute the population size
    // misura della popolazione : es. 5 figli X 20 best genitori = 100 individui
	if (n_variable_evolution_is_running == 0) 
		nindividuals=nkids * bestfathers;	// nel caso classico il numero di individui � dato
											// dal prodotto del numero dei figli per i bests
	else {
		nindividuals=nteam;					// nel caso di evoluzione in vita artificiale il
											// numero di genotipi e di controllers da allocare 
											// � dato dal numero di individui del team
		free(genome);						// dealloca la struttura per il richiamo successivo
		free(bestgenome);					// della funzione
		if (loadfixedgenotypes == 1) {
			free(loadfixedgn);		
			free(fixedgenome);
		}
	}

	// allocate space for the genome of the population and initialize
	// alloca la struttura principale, ma � necessario allocare i geni individuo per individuo e
	// questo lo effettua dopo
 	// genome � un array bidimensionale di dimensioni pari al numero di invidui per il numero di geni
	genome = (int **) malloc(((nindividuals + additional_ind) * npopulations) * sizeof(int *));
	if (genome == NULL) 
	    display_error("System error: genome malloc");

	for (p = 0, gpi=genome; p < npopulations; p++)			// per ogni genoma di una popolazione 
	{
		for (i=0; i < nindividuals; i++,gpi++)				// scorre il genoma per ogni individuo
															// della popolazione corrente
		{
			*gpi = (int *)malloc(ngenes * sizeof(int));		// alloca ogni riga della matrice per
															// un numero di elementi pari al numero
															// dei geni
															// all'interno di ogni genoma ci sono 
															// tot geni, es 68
			if (*gpi == NULL)
				display_error("System error: gpi malloc");
			for (j=0,gi = *gpi; j < ngenes; j++,gi++)
		    {
				*gi = mrand(256);							// randomizza ogni gene
		    }
		}
	    // we allocate space for the genome of additional individuals
	    for (i=0; i < additional_ind; i++,gpi++)			// scorre il genoma degli individui 
															// aggiuntivi
		{
			*gpi = (int *)malloc(ngenes * sizeof(int));
			if (*gpi == NULL)
				display_error("System error: previous genome malloc");
			for (j=0,gi = *gpi; j < ngenes; j++,gi++)
		    {
				*gi = mrand(256);
		    }
		}
	}
	
	// At moment i enable the loading of the second genome array, because without it we have different placements of the robots.
	// This can cause different outcomes in results of analysis. 

	//if ((ipar->lab_test_team2_number_seed > 0)||(ipar->lab_test_subst_number_seed > 0)) {
		// alloca un eventuale genoma aggiuntivo, di un individuo di un altro seed e di un'altra generazione
		secondgenome = (int **) malloc(((nindividuals + additional_ind) * npopulations) * sizeof(int *));

		if (secondgenome == NULL) 
		    display_error("System error: genome malloc");

		for (p = 0, gpi=secondgenome; p < npopulations; p++)			// per ogni genoma di una popolazione 
		{
			for (i=0; i < nindividuals; i++,gpi++)				// scorre il genoma per ogni individuo
																// della popolazione corrente
			{
				*gpi = (int *)malloc(ngenes * sizeof(int));		// alloca ogni riga della matrice per
																// un numero di elementi pari al numero
																// dei geni
																// all'interno di ogni genoma ci sono 
																// tot geni, es 68
				if (*gpi == NULL)
					display_error("System error: gpi malloc");
				for (j=0,gi = *gpi; j < ngenes; j++,gi++)
			    {
					*gi = mrand(256);							// randomizza ogni gene
			    }
			}
		    // we allocate space for the genome of additional individuals
		    for (i=0; i < additional_ind; i++,gpi++)			// scorre il genoma degli individui 
																// aggiuntivi
			{
				*gpi = (int *)malloc(ngenes * sizeof(int));
				if (*gpi == NULL)
					display_error("System error: previous genome malloc");
				for (j=0,gi = *gpi; j < ngenes; j++,gi++)
			    {
					*gi = mrand(256);
			    }
			}
		}
	//}

	// allocate space for the genome of reproducing individuals
 	// bestgenome � un array bidimensionale di dimensioni pari al numero di migliori per il numero
	// di geni. In caso di evoluzione con popolazione n-variabile non serve.  
	bestgenome = (int **) malloc((bestfathers * npopulations) * sizeof(int *));
	if (bestgenome == NULL)
		display_error("System error: bestgenome malloc");
	for (p = 0, gpi=bestgenome; p < npopulations; p++)
	{
		for (i=0; i < bestfathers; i++,gpi++) 
		{
			*gpi = (int *)malloc(ngenes * sizeof(int));
	        if (*gpi == NULL) 
				display_error("System error: bestgi malloc");
	    }
	}

	// allocate space for the genome of fixed individuals
	if (loadfixedgenotypes == 1)
	{
		fixedgenome = (int **) malloc(nteam * sizeof(int *));
		if (fixedgenome == NULL)
			display_error("System error: fixedgenome malloc");	
		for (i=0, gpi=fixedgenome; i < nteam; i++, gpi++) 
		{
			*gpi = (int *)malloc(ngenes * sizeof(int));
			 if (*gpi == NULL) 
				display_error("System error: fixedgi malloc");
		}
 		
		loadfixedgn = (int *) malloc(nteam * sizeof(int));
		if (loadfixedgn == NULL)
			display_error("System error: fixedgenome malloc");
		for (i=0, gi=loadfixedgn; i < nteam; i++, gi++) 
			*gi = 0;
	}

	// load fixed genotype from the file robot.gen
	if (loadfixedgenotypes == 1)
		loadfixedg();

	// initialise statistics 
	for (g=0; g < MAXGENERATIONS; g++)
		for (gg=0; gg < 4; gg++)
			astatfit[g][gg] = 0.0;

	// initialize free parameters (to allow the use of test->individual before evolution)
	for(team=0; team<nteam; team++)	
		getgenome(0,team,0);
}


/*
 * select the best individual (on the basis of statfit)
 * place the results in dindividual
 */
void
select_besti()

{
       int     g;
       double  bestfit;
       int     besti;

       bestfit = -9999.9;
       besti   = -1;
       for(g = 0; g < generations; g++)
	{
	   if (statfit[g][0] == -9999.9)
	      break;
	   if (statfit[g][0] > bestfit)
		{
		  bestfit = statfit[g][0];
		  besti = g;
		}
       }
       if (besti < (nindividuals + additional_ind))
	  dindividual = besti;
}



/*
 * Initialize the individuals' genotype
 * and call evaluate() 
 */
double
init_and_evaluate_nf(int gen, int nind, int pop)
{
	struct  individual *pind;
	int team;
	int race;
    double totfitness;
	extern int cgen;
	int nindperrace;
	int nrandomind;
	int dim_sel_array;
	int limit;
	char   fileout[128], str_tmp[128], selected_sequence[256], file_line[256];
	FILE   *fout;
	int	   found=0;

	cgen=gen;

	// initialize individuals' genotype
	if ((n_fixed_evolution_social_is_running == 0) && (testpopulation_is_running == 0))	// individual evolution 
		if (nraces==1) // con una razza, prende il genoma di un individuo i e lo clona in due robot che testa nell'ambiente
			for(team=0; team<nteam; team++)
				getgenome(nind,team,0);						    // copy the same genome into different individuals
		else {													// con pi� razze, prende l'individuo i e l'individuo 
			if (n_fixed_evolution_is_running == 1) {			// codice da attivare in fase di evoluzione con pi� razze
				// Cnfronto tra individui con lo stesso indice
				nindperrace=nindividuals/nraces;					// Number of individuals per race
				
				if (evaluate_different_inds==0) {
					// Estrae nell'ordine gli individui
					for(team=0;team < nteam;team++) {		
						race=team;									// Each individual of the team is associated to a different race
						getgenome(nind+race*nindperrace,team,0);	// Estrae l'individuo i-esimo e tutti quelli i+nindperrace*race
					}
				} else {
					do {
						// Inizializza la sequenza degli elementi estratti
						strcpy(selected_sequence,"");

						// Estrae casualmente gli individui
						for(team=0;team < nteam;team++) {		
							race=team;										// Each individual of the team is associated to a different race
							dim_sel_array=nindperrace*nraces;				// Determina la dimensione dell'array
							limit=0;
							if (evaluate_different_inds==1) {					// Estrazione senza ripetizione
								do {
									nrandomind=mrand(nindperrace);				// Seleziona a caso uno degli individui della popolazione
									if (limit++==dim_sel_array)
										break;
								} while (selected_ind_array[nrandomind][race]);	// Cicla finch� � gi� stato selezionato, oppure finch� non lo ha visitato tutto	
								getgenome(nrandomind+race*nindperrace,team,0);	// Estrae un indivudo scelto a caso tra tutti quelli di una razza (popolazione)					}
								selected_ind_array[nrandomind][race]=2;			// inserisce 2 per indicare che � stato appena aggiornato
							} else if (evaluate_different_inds==2) {			// Estrazione con ripetizione
								nrandomind=mrand(nindperrace);					// Seleziona a caso uno degli individui della popolazione
								getgenome(nrandomind+race*nindperrace,team,0);	// Estrae un indivudo scelto a caso tra tutti quelli di una razza (popolazione)					}
								if (selected_ind_array[nrandomind][race]==1)
									selected_ind_array[nrandomind][race]=3;			// Se l'individuo era gi� stato estratto allora vuol dire che c'� una ripetizione ed inserisce 3
								else if (selected_ind_array[nrandomind][race]==0)
									selected_ind_array[nrandomind][race]=2;			// Altrimenti inserisce 2
							}
					
							// Compila la sequenza estratta
							sprintf(str_tmp,"%d",nrandomind);
							strcat(selected_sequence,str_tmp);
							if (team<nteam-1) 
								strcat(selected_sequence," - ");
							else
								strcat(selected_sequence,"\0");
						}
					
						// Controlla che la sequenza appena confezionata non sia stata gi� estratta in passato nella generazione corrente
						sprintf(fileout,"Individuals_random_sequence_Seed%d.ran",seed,gen);
						if (nind>0) {											// Solo per le estrazioni successive alla prima
							fout=fopen(fileout,"r");							// file delle sequenze estratte	
							if (fout != NULL) {									// Apre il file in lettura
								found=0;										// Inizializza la ricerca della stringa selezionata
								do {
									fgets(file_line,256,fout);
									if ((strstr(file_line, selected_sequence)) != NULL)			// Confronta la stringa letta e quella estratta
										found=1;								// Vuol dire che ha trovato una stringa uguale e deve ricominciare da capo l'estrazione
								} while (!feof(fout));
							} else {
								sprintf(str_tmp,"file %s could not be opened\n",fileout); 
								display_stat_message(str_tmp);					
							}
							fclose (fout);
						}
					} while (found);									// rigenera la sequenza fintantoch� ci sono doppioni

					// Salva la sequenza estratta in un file
					fout=fopen(fileout,"a");							// file di analisi statistica	
					if (fout!=NULL) 
						fprintf (fout, "%s\n",selected_sequence);
					fclose (fout);
				}
			} else if (testindividual_is_running == 1) {
				for(team=0; team<nteam; team++)
					getgenome(team,team,0);			  // copy each genome into each different individuals
			}		
		}
	else  
		for(team=0; team<nteam; team++)
			getgenome(team,team,0);			  // copy each genome into each different individuals

    // evaluate
	totfitness = evaluate(gen,nind);

    // evaluating of the fitness
	if ((n_fixed_evolution_social_is_running == 0) && (testindividual_is_running == 0) && (testpopulation_is_running == 0)) { // individual evolution
		if (nraces==1) 
			tfitness[0][0][nind] = totfitness;	// con mono razza funziona come nel caso classico, ossia assegna
												// la somma delle fitness totalizzate dai cloni
		else 
			if (evaluate_different_inds==0) {
				for(team=0; team < nteam; team++) 
					tfitness[0][team][nind+team*nindperrace] = getindfitness(team);	// con pi� razze assegna la fitness alla razza relativa senza
																					// effettuarne la somma. Nelle razze successive il vettore delle
																					// fitness deve essere slittato di un numero di individui pari a quello 
																					// previsto per ogni razza, per adattarlo al ranking fatto con l'evoluzione sociale.
			} else {
				if (evaluate_different_inds==1) {										// Estrazione senza ripetizione
					for (team=0;team<nteam;team++)										// team e race in questo caso sono la stessa cosa
						for (nrandomind=0;nrandomind<nindperrace;nrandomind++) 
							if (selected_ind_array[nrandomind][team]==2) {								// Solo per l'individuo appena selezionato sar� valutata la fitness
								tfitness[0][team][nrandomind+team*nindperrace] = getindfitness(team);	// Mantiene opportunamente shiftato l'array
								break;																	// Se trovato inutile proseguire
							}
				} else if (evaluate_different_inds==2) {								// Estrazione con ripetizione, effettua una media sulla fitness degli individui ripetuti
					for (team=0;team<nteam;team++)										// team e race in questo caso sono la stessa cosa
						for (nrandomind=0;nrandomind<nindperrace;nrandomind++) 
							if (selected_ind_array[nrandomind][team]==2) {								// Se l'individuo non era mai stato selezionato allora effettua il caricamento diretto della fitness
								tfitness[0][team][nrandomind+team*nindperrace] = getindfitness(team);	// Mantiene opportunamente shiftato l'array
								break;																	// Se trovato inutile proseguire
							} else if (selected_ind_array[nrandomind][team]==3) {						// Altrimenti se l'individuo era stato precedentemente selezionato allora effettua una media
								tfitness[0][team][nrandomind+team*nindperrace] += getindfitness(team);	// Mantiene opportunamente shiftato l'array
								tfitness[0][team][nrandomind+team*nindperrace] /= 2;					// Divide per due, effettuando di fatto la media tra i valori di fitness ottenuti dall'individuo riestratto
								break;																	// Se trovato inutile proseguire
							}
				}
			}
	} else {  
		for(team=0; team < nteam; team++) {
		    race=getindrace(team);									// legge la razza dell'individuo
			tfitness[0][race][team] = getindfitness(team);			// copia la fitness di ciascun individuo indistintamente     
		}
	}
	
	return(totfitness);
}

/*
 * Initialize the individuals' genotype
 * and evaluate some individuals. For alone tests. 
 */
double
init_and_evaluate_some_individuals(int seed, int gen, int nind, int pop, int test_team1, int test_team2)
{
	struct  individual *pind;
	struct envobject *obj;					// struttura temporanea per il controllo cilindri, ed altri elementi
	int i;
	int team[4];
	int memind1[5][4],memind2[5][4];					// memoria temporanea di alcuni dati di ind
	int race;
	int old_nind1=0,old_nind2=0,old_nind3=0,old_nind4=0;
    double totfitness;
	extern int cgen;
	cgen=gen;
	int save_old_nteam;							// memorizza l'nteam
	int type;
	int old_team;

	save_old_nteam=nteam;						// memorizza temporaneamente nteam
	
	for (i=0;i<4;i++)							// inizializza team, in modo da avere tutti valori differenti ed evitare dei problemi successivi
		team[i]=i;
	
	if (!ipar->lab_test_best_team1) 
		team[0]=ipar->lab_test_team1;
	else
		if (ipar->perceptualrace==0) 
			if (!ipar->lab_test_all_pairs)											// Se il test � a coppie allora il numero viene specificato da fuori con test_team1 e test_team2
				team[0]=search_bestind_num(seed,gen,0,&type);						// Individua il numero del best della generazione gen di specie 0
			else 																						
				team[0]=search_bestind_num(seed,gen,test_team1,&type);				// Individua il numero del best della generazione gen di specie test_team1
		else 
			team[0]=search_bestind_num(seed,gen,0,&type);							// Individua il numero del best della generazione gen di specie 1

	if (ipar->lab_ntestingteam>1)	
		if (!ipar->lab_test_best_team2) 
			team[1]=ipar->lab_test_team2;
		else
			if (nraces>1)															// Se ci sono pi� razze
				if (!ipar->lab_test_all_pairs)										// Se il test � a coppie allora il numero viene specificato da fuori con test_team1 e test_team2
					team[1]=search_bestind_num(seed,gen,1,&type);					// Individua il numero del best della generazione gen di specie 0
				else																						
					team[1]=search_bestind_num(seed,gen,test_team2,&type);			// Individua il numero del best della generazione gen di specie test_team1
			else 
				team[1]=search_bestind_num(seed,gen,0,&type);						// Individua il numero del best della generazione gen di specie 0
	
	if (ipar->lab_ntestingteam>2) 
		if (!ipar->lab_test_best_team3)
			team[2]=ipar->lab_test_team3;
		else 
			if (nraces>1)															// Se ci sono pi� razze
				team[2]=search_bestind_num(seed,gen,2,&type);						// Individua il numero del best della generazione gen di specie 2
			else
				team[2]=search_bestind_num(seed,gen,0,&type);						// Individua il numero del best della generazione gen di specie 0

	if (ipar->lab_ntestingteam>3) 
		if (!ipar->lab_test_best_team4) 
			team[3]=ipar->lab_test_team4;
		else 
			if (nraces>1) 													// Se ci sono pi� razze
				team[3]=search_bestind_num(seed,gen,3,&type);				// Individua il numero del best della generazione gen di specie 3
			else
				team[3]=search_bestind_num(seed,gen,0,&type);				// Individua il numero del best della generazione gen di specie 0

	if (ipar->lab_test_team1_all_ind_race)									// Imposta l'individuo se � necessario ciclare su tutti quelli di una specie determinata
		team[0]=test_team1;

	if (ipar->lab_test_team2_all_ind_race)									// Imposta l'individuo se � necessario ciclare su tutti quelli di una specie determinata
		team[1]=test_team2;
	
	if ((((team[0]!=team[1]) && (team[1]!=team[2]) && (team[2]!=team[3])) && (nraces>1)) || (nraces==1)) {		// Lo stesso individuo non pu� essere testato in pi� repliche nell'ambiente
		if (nraces>1) {															// il lavoro di memoria che segue da effettuare solo in assenza di pi� specie o razze, perch� nel caso mono razza non � necessario
			// Memorizza temporaneamente i dati di ind che saranno modificati
			for(i=0,pind=ind; i<ipar->lab_ntestingteam;i++,pind++) {
				memind1[0][i]=pind->RColor;
				memind1[1][i]=pind->GColor;
				memind1[2][i]=pind->BColor;
				memind1[3][i]=pind->race;
				memind1[4][i]=pind->orig_number;
			}
		
			// Carica nei primi individui, gli individui scelti
			for(i=0;i<ipar->lab_ntestingteam;i++) {
				memind2[0][i]=(ind+(team[i]))->RColor;
				memind2[1][i]=(ind+(team[i]))->GColor;
				memind2[2][i]=(ind+(team[i]))->BColor;
				memind2[3][i]=(ind+(team[i]))->race;
				memind2[4][i]=(ind+(team[i]))->orig_number;
			}

			// Carica in ind i dati scelti, per evitare sovrapposizioni
			for(i=0, pind=ind; i<ipar->lab_ntestingteam;i++,pind++) {
				pind->RColor=memind2[0][i];
				pind->GColor=memind2[1][i];
				pind->BColor=memind2[2][i];
				pind->race=memind2[3][i];
				pind->orig_number=memind2[4][i];
			}

			// Sistemare il caricamento cilindri
			// aggiustamento degli indici del cilindro
			for(i=0,obj = *(envobjs + SROUND);i<envnobjs[SROUND];i++,obj++)
				if (obj->subtype==10) {
					if (obj->nind==(team[0]) && (!ipar->lab_test_ghost_team1)) {
						old_nind1=obj->nind;	// conserva il numero di individuo associato al cilindro per poterlo ripristinare in seguito	
						obj->nind=0;			// diviene il primo individuo
					} else if ((obj->nind==(team[1]) && (ipar->lab_ntestingteam>1) && (!ipar->lab_test_ghost_team2))) { 
						old_nind2=obj->nind;	// conserva il numero di individuo associato al cilindro per poterlo ripristinare in seguito	
						obj->nind=1;			// diviene il secondo individuo
					} else if ((obj->nind==(team[2]) && (ipar->lab_ntestingteam>2) && (!ipar->lab_test_ghost_team3))) {
						old_nind3=obj->nind;	// conserva il numero di individuo associato al cilindro per poterlo ripristinare in seguito	
						obj->nind=2;			// diviene il terzo individuo
					} else if ((obj->nind==(team[3]) && (ipar->lab_ntestingteam>3) && (!ipar->lab_test_ghost_team4))) {
						old_nind4=obj->nind;	// conserva il numero di individuo associato al cilindro per poterlo ripristinare in seguito	
						obj->nind=3;			// diviene il terzo individuo
					} else {	
						obj->subtype=9;			// cilindro associato ad un individuo morto
					}		
				}
		} else {
			// Inibisce eventualmente un colore se richiesto
			for(i=0,obj = *(envobjs + SROUND);i<envnobjs[SROUND];i++,obj++)
				if (obj->subtype==10) {
					if ((obj->nind==0) && (ipar->lab_test_ghost_team1)) 
						obj->subtype=9;
					if ((obj->nind==1) && (ipar->lab_test_ghost_team2)) 
						obj->subtype=9;
					if ((obj->nind==2) && (ipar->lab_test_ghost_team3)) 
						obj->subtype=9;
					if ((obj->nind==3) && (ipar->lab_test_ghost_team4)) 
						obj->subtype=9;
				}

			// Carica in pind il numero di individuo
			for(i=0, pind=ind; i<ipar->lab_ntestingteam;i++,pind++) 
				pind->orig_number=team[i];
		}

		// Search the number of individual to put into genotype of individual specified by lab_test_subst_ind_dest
		if ((ipar->lab_test_subst_number_seed> 0) && (ipar->lab_test_subst_ind_dest>0)) {
			team[ipar->lab_test_subst_ind_dest-1]=search_bestind_num(ipar->lab_test_subst_number_seed,ipar->lab_test_subst_number_gen,ipar->lab_test_subst_ind_orig-1,&type);		// Rettifica nel caso di sostituzione		
			if (ipar->lab_test_subst_color>0) {
				(ind+(ipar->lab_test_subst_ind_dest-1))->can_see_fake_colors=1;				// substitued individual can see in fake color
				RColor_fake=(ind+(ipar->lab_test_subst_ind_orig-1))->RColor;				// its the color that the substituted individual shouldnt see anymore
				GColor_fake=(ind+(ipar->lab_test_subst_ind_orig-1))->GColor;
				BColor_fake=(ind+(ipar->lab_test_subst_ind_orig-1))->BColor;
				RColor_new=(ind+(ipar->lab_test_subst_ind_dest-1))->RColor;				// its the color that the substituted individual shouldnt see anymore
				GColor_new=(ind+(ipar->lab_test_subst_ind_dest-1))->GColor;
				BColor_new=(ind+(ipar->lab_test_subst_ind_dest-1))->BColor;
			}
		
		}

		// Inserire la lettura dei best, non va bene cos�
		if ((ipar->lab_test_subst_number_seed>0) && (ipar->lab_test_subst_ind_dest==1))
			getgenome(team[0],ipar->lab_test_subst_ind_dest-1,1);										// Copy genome of individual team of source individual of second genome in position ipar->lab_test_subst_ind_dest
		else 
			getgenome(team[0],0,0);																		// Copy genome of individual team[0] of first genome in position 0
	
		if (ipar->lab_ntestingteam>1) {
			if (ipar->lab_test_team2_number_seed > 0)													// Se � definito un altro seed ed un'altra generazione per il test del secondo individuo
				getgenome(team[1],1,1);																	// Copia il secondo genoma alla posizione lab_test_team2 di un individuo in posizione 1
			else
				if ((ipar->lab_test_subst_number_seed>0) && (ipar->lab_test_subst_ind_dest==2))
					getgenome(team[1],ipar->lab_test_subst_ind_dest-1,1);								// Copy genome of individual team of source individual of second genome in position ipar->lab_test_subst_ind_dest
				else	
					getgenome(team[1],1,0);																// Copy genome of individual team[1] of first genome in position 1
		}
			
		if (ipar->lab_ntestingteam>2) 
			if ((ipar->lab_test_subst_number_seed>0) && (ipar->lab_test_subst_ind_dest==3))
				getgenome(team[2],ipar->lab_test_subst_ind_dest-1,1);									// Copy genome of individual team of source individual of second genome in position ipar->lab_test_subst_ind_dest
			else
				getgenome(team[2],2,0);																	// Copy genome of individual team[2] of first genome in position 2
		
		if (ipar->lab_ntestingteam>3) 
			if ((ipar->lab_test_subst_number_seed>0) && (ipar->lab_test_subst_ind_dest==4))
				getgenome(team[3],ipar->lab_test_subst_ind_dest-1,1);									// Copy genome of individual team of source individual of second genome in position ipar->lab_test_subst_ind_dest
			else
				getgenome(team[3],3,0);																	// Copy genome of individual team[3] of first genome in position 3
	

		nteam=ipar->lab_ntestingteam;

		// evaluate
		totfitness = evaluate(gen,nind);								// Comunque effettua il test caricando tutto dentro in primo pind (ind) ma gli 
																		// altri parametri (race) li ottiene dall'individuo di numero team. La fitness dall'individuo 0
		if (nraces>1) {
			race=getindrace(team[0]);										// legge la razza dell'individuo
			tfitness[0][race][team[0]] = getindfitness(team[0]);			// copia la fitness di ciascun individuo indistintamente			
	
			if (ipar->lab_ntestingteam>1) {
				race=getindrace(team[1]);									// legge la razza dell'individuo
				tfitness[0][race][team[1]] = getindfitness(team[1]);		// copia la fitness di ciascun individuo indistintamente     
			}
			if (ipar->lab_ntestingteam>2) {
				race=getindrace(team[2]);									// legge la razza dell'individuo
				tfitness[0][race][team[2]] = getindfitness(team[2]);		// copia la fitness di ciascun individuo indistintamente     
			}
			if (ipar->lab_ntestingteam>3) {
				race=getindrace(team[3]);									// legge la razza dell'individuo
				tfitness[0][race][team[3]] = getindfitness(team[3]);		// copia la fitness di ciascun individuo indistintamente     
			}

			nteam=save_old_nteam;						// ripristina nteam al valore iniziale dopo la valutazione effettuata	

			// Resetta lo stato dei cilindri
			for(i=0,obj = *(envobjs + SROUND);i<envnobjs[SROUND];i++,obj++) {
				if (obj->subtype==10) {
					if (obj->nind==0) 							// il primo � stato senz'altro modificato
						obj->nind=old_nind1;					// ripristina il numero di individuo originale
					else if ((obj->nind==1) && (ipar->lab_ntestingteam>1)) 
						obj->nind=old_nind2;					// ripristina il numero di individuo originale
					else if ((obj->nind==2) && (ipar->lab_ntestingteam>2))
						obj->nind=old_nind3;					// ripristina il numero di individuo originale
					else if ((obj->nind==3) && (ipar->lab_ntestingteam>3))
						obj->nind=old_nind4;					// ripristina il numero di individuo originale
				} else if (obj->subtype==9) {					// cilindro associato ad un individuo morto	
						obj->subtype=10;						// resuscita l'individuo
				}
			}

			for(i=0,pind=ind; i<ipar->lab_ntestingteam;i++,pind++) {
				pind->RColor=memind1[0][i];
				pind->GColor=memind1[1][i];
				pind->BColor=memind1[2][i];
				pind->race=memind1[3][i];
				pind->orig_number=memind1[4][i];
			}
		} else {
			// Gestire la fitness nel caso monorazza
	
		}
	}

	return(totfitness);
}

/*
 * Initialize the individuals' genotype
 * and evaluate alone. For alone tests. 
 */
double
init_and_evaluate_alone(int gen, int nind, int pop)
{
	struct  individual *pind;
	int team;
	int race;
    double totfitness;
	extern int cgen;
	cgen=gen;
	int save_old_nteam;							// memorizza l'nteam

	save_old_nteam=nteam;						// memorizza temporaneamente nteam

	for(team=0; team<nteam; team++) {
		getgenome(team,0,0);					// copy each genome into 1 individual

		nteam=1;								// simula la presenza di un solo individuo
		// evaluate
		totfitness = evaluate(gen,nind);		// Comunque effettua il test caricando tutto dentro in primo pind (ind) ma gli 
												// altri parametri (race) li ottiene dall'individuo di numero team. La fitness dall'individuo 0

	    race=getindrace(team);					// legge la razza dell'individuo
		tfitness[0][race][team] = totfitness;	// copia la fitness di ciascun individuo indistintamente     

		nteam=save_old_nteam;					// ripristina nteam al valore iniziale dopo la valutazione effettuata
	}
	return(totfitness);
}

/*
 * Initialize the individuals' genotype
 * and evaluate double population.  
 */
double
init_and_evaluate_double(int gen, int nind, int pop)
{
	struct  individual *pind;
	int team;
	int race;
    double totfitness;
	extern int cgen;
	cgen=gen;

	for(team=0; team<nteam/2; team++) {			// Cicla fino alla met� del team in quanto fli individui contenuti nella lista dei genoma sono solo la met�
		getgenome(team,team*2,0);				// copy the same genome into 2 different individuals
		getgenome(team,team*2+1,0);				// clone genome into following individual
	}

	// evaluate
	totfitness = evaluate(gen,nind);			// Effettua il test caricando tutto dentro l'ambiente

	for(team=0; team < nteam; team++) {
	    race=getindrace(team);							// legge la razza dell'individuo
		tfitness[0][race][team] = getindfitness(team);	// copia la fitness di ciascun individuo indistintamente     
	}

	return(totfitness);
}

/*
 * Initialize the individuals' genotype
 * and evaluate an half of population.  
 */
double
init_and_evaluate_half(int gen, int nind, int pop)
{
	struct  individual *pind;
	int team;
	int race;
    double totfitness;
	extern int cgen;
	cgen=gen;

	for(team=0; team<nteam; team++) {			// Cicla fino alla met� del team 
		getgenome(team,team,0);					// copy the genome into each individual
	}

	// evaluate
	totfitness = evaluate(gen,nind);			// Effettua il test caricando tutto dentro l'ambiente

	for(team=0; team < nteam; team++) {
	    race=getindrace(team);							// legge la razza dell'individuo
		tfitness[0][race][team] = getindfitness(team);	// copia la fitness di ciascun individuo indistintamente     
	}

	return(totfitness);
}

/*
 * Initialize the individuals' genotype
 * and call evaluate() in evolution of artificial life evolutionary agents 
 */
double
init_and_evaluate_nv(int nind)
{
	int team;

	// Inizializza il genotipo dell'individuo nind caricandolo nella rispettiva rete neurale 
	getgenome(nind,nind,0);	// inserendo nind,nind come parametro si forza a caricare nel controller
							// n-esimo il genotipo dell'individuo n-esimo. In questo caso devono
							// essere generati n controllers e non t come nell'evoluzione base. 
	
    evaluate(0,nind); // lancia la vita dell'individuo per tot epoche

	return(1);
}



/*
 * return the mutated version of an integer
 * this function is called by getbestweights()
 */ 
int
mutate(int w)
{
    
      int b[8];
      int val;
      int ii;


      val = w;
      for(ii=0;ii < 8;ii++)
	  {
	    b[ii] = val % 2;			// estrae ogni bit dall'intero
	    val  = val / 2;
	  }    
      for(ii=0;ii < 8;ii++)
	  {
	     if (mrand(100) < percmut)	// genera un numero casuale tra 0 e 99 e verifica che sia al disotto della percentuale di mutazione
		 { 
	        b[ii] = mrand(2);
		 } 
	  } 
      w = 0;						// ricompone l'intero
      w += b[0] * 1;
      w += b[1] * 2;
      w += b[2] * 4;
      w += b[3] * 8;
      w += b[4] * 16;
      w += b[5] * 32;
      w += b[6] * 64;      
      w += b[7] * 128;   
 
      return(w);
	 
}


/*
 * return the hamming distance between two bytes
 */ 
int
hamming_distance(int w1, int w2)
{
    
      int b1[8], b2[8];
      
	  
	  int val;
      int i;
	  int hd;

      val = w1;
      for(i=0;i < 8;i++)
	  {
	    b1[i] = val % 2;			// estrae ogni bit dall'intero w1
	    val  = val / 2;
	  }    

      val = w2;
      for(i=0;i < 8;i++)
	  {
	    b2[i] = val % 2;			// estrae ogni bit dall'intero w2
	    val  = val / 2;
	  }    

	  hd=0;
	  // Conta in quanti bit differiscono i due bytes
      for(i=0;i < 8;i++)
	    if (b1[i] != b2[i])			// confronto bit a bit
			hd++;

	  return (hd);
}




/*
* produce a new generations
* if last generations it saves the best but does not generate the new population
* this function is for individuals with races
*/
void reproduce(int g)
{
	char sroot[64];
	char sbuffer[64];
	FILE *fp;
	int p;
	int f;
	int x;
	int n;
	int k;
	float v;
	int race;
	int nindperrace;
	int *perceptualbestinds;		   // Contiene gli individui migliori che sono anche percettivi
	extern int check_perceptualinds(int team);	
	extern int    *perceptualinds;		   // Contiene gli indici degli individui che possono percepire la food zone

	if ((ipar->onlyfewcanperceive) && (ipar->probpercepindinheritfromfather >0)) {
		perceptualbestinds = (int *) malloc(bestfathers/nraces * sizeof(int));	
	
		for (int i=0; i<bestfathers/nraces;i++) 
			*(perceptualbestinds+i)=-1;				// All'inizio nessuno � percettivo
	}

	nindperrace=nindividuals/nraces;		// Numero di individui per razza
	for (race=0;race<nraces;race++) {		// Cicla sulle razze
		for(p = 0; p < npopulations; p++)
		{
			for(f=0;f<bestfathers/nraces;f++)		// Determina i primi bestfathers individui. Il numero di genitori � diviso per il numero di razze Esempio i primi 20 individui. Del primo
													// miglior individuo ne salva il genoma.  
			{
				x = 0;
				v = (float) -9999.0;
				for(n = 0+race*nindperrace;n < nindperrace+race*nindperrace;n++)		// Effettua il ranking all'interno della popolazione di una stessa razza
				{
					if (elitism == 2)
					{
						if ((pfitness[n] / (double) plifes[n]) >= v)
						{
							x = n;
							v = (float) (pfitness[n] / (double) plifes[n]);
						}
					}
					else
					{
						if (tfitness[p][race][n] >= v)
						{
							x = n;
							v = (float) tfitness[p][race][n];
						}
					}
				}
				putbestgenome(x,f,p);						// Inserisce l'individuo x nel genoma dei padri perch� � l'individuo migliore per razza 	       
				tfitness[p][race][x] = -9999.0;				// Azzera la fitness totale dell'individuo x-esimo in modo da escluderlo dal ranking
															// successivo.
				// Memorizza l'indirizzo degli individui migliori che sono anche percettivi
				// Al meassimo gli individui memorizzabili possono essere pari al numero di invidiui percettivi
				if ((ipar->onlyfewcanperceive) && (ipar->probpercepindinheritfromfather >0)) {
					if (check_perceptualinds(x)) 
						*(perceptualbestinds+f)=x;						
				}
				
				if (elitism == 2)
				{
					bpfitness[f] = pfitness[x];
					bplifes[f] = plifes[x];
					pfitness[x] = -9999.0;
					plifes[x] = 1;
				}
				// if we restarted evolution we do not save the bests of the initial generation
				if ((f+1) <= savebest && (startgeneration == 0 || g > startgeneration))
				{
					if (!((n_fixed_evolution_is_running == 1) && (nraces>1)))  
						sprintf(sbuffer,"B%dP%dS%d.gen",f+1,p,seed);
					else 
						sprintf(sbuffer,"B%dP%dS%dR%d.gen",f+1,p,seed,race); // crea un file dei migliori per ogni razza
					
					if (g == 0)
					{
						if ((fp=fopen(sbuffer, "w")) == NULL)
						{
							sprintf(sroot,"I cannot open file %s",sbuffer);
							display_error(sroot);
						}
					}
					else
					{
						if ((fp=fopen(sbuffer, "a")) == NULL)
						{
							sprintf(sroot,"I cannot open file %s",sbuffer);
							display_error(sroot);
						}
					}
					if (nraces>1) 		// se con pi� razze salva il numero di razza del migliore
						fprintf(fp,"**NET : s%d_%d_%d.wts\n",g,x,race);
					else 
						fprintf(fp,"**NET : s%d_%d.wts\n",g,x);

					saveagenotype(fp,x,p);
					fflush(fp);
					fclose(fp);
				}
			}
		}	// end populations

		// reproduce
		if (g+1 < generations)
		{
			for(p=0; p < npopulations; p++)
			{
				for(f=0;f<bestfathers/nraces;f++)
				{
					for (k=0;k<nkids;k++)
					{
						if (elitism > 0 && k == 0)
							getbestgenome(k+(f*nkids)+(race*nindperrace),f,p,0);		// Non mutazione. Inserisce il primo figlio non mutato (elitismo). Il primo figlio � identico al padre, non viene mutato. 
																						// Ogni razza viene ordinata nel genoma dalla 0 alla nraces. Se ci sono 2 razze e 100 individui ci saranno 50 individui 
																						// della prima razza e 50 della seconda. Il caricamento successivo quindi dovr� tenere conto di questo. 
						else
							getbestgenome(k+(f*nkids)+(race*nindperrace),f,p,1);		// Inserisce file di 5 figli del padre mutati (tranne il primo in caso di elitismo).
					
						
						// Verifica che il padre sia percettivo in tal caso con una certa probabilit� potrebbe diventarlo anche il figlio
						// ATTENZIONE ! attualmente funziona solo nei casi in cui ipar->nperceptualindividuals=bestfathers/nraces
						// Probabili imprevisti negli altri casi
						if ((ipar->onlyfewcanperceive) && (ipar->probpercepindinheritfromfather >0)) {
							if(*(perceptualbestinds+f)!=-1)	{					
								if (mrand(100) < ipar->probpercepindinheritfromfather)	// genera un numero casuale tra 0 e 99 e verifica che sia al disotto della percentuale richiesta
								{ 
									*(perceptualinds+f)=k+(f*nkids)+(race*nindperrace);
								} 
							}
						}

						if (elitism == 2)
						{
							if (k == 0)
								pfitness[k+(f*nkids)] = bpfitness[f];
							else
								pfitness[k+(f*nkids)] = 0.0;
					
							if (k == 0)
								plifes[k+(f*nkids)] = bplifes[f];
							else
								plifes[k+(f*nkids)] = 0;
						}		       
					}
				}
			}
		}
	}	// end races
}
 
/*
* produce a new generations
* if last generations it saves the best but does not generate the new population
*/
void reproduce2(int g)
{
	
	
	
	char sroot[64];
	char sbuffer[64];
	FILE *fp;
	int p;
	int f;
	int x;
	int n;
	int k;
	float v;
	
	
	for(p = 0; p < npopulations; p++)
	{
	  for(f=0;f<bestfathers;f++)
	  {
	    x = 0;
	    v = (float) -9999.0;
	    for(n = 0;n < nindividuals;n++)
	     if (elitism == 2)
	      {
	       if ((pfitness[n] / (double) plifes[n]) >= v)
	       {
		x = n;
		v = (float) (pfitness[n] / (double) plifes[n]);
	       }
	       else
	       {
	        if (tfitness[p][0][n] >= v)
		{
		 x = n;
		 v = (float) tfitness[p][0][n];
		}
	       }
	      }
	    putbestgenome(x,f,p);	       
	    if (elitism == 2)
	      {
	       bpfitness[f] = pfitness[x];
	       bplifes[f] = plifes[x];
	       pfitness[x] = -9999.0;
	       plifes[x] = 1;
	      }
	    tfitness[p][0][x] = -9999.0;

	    // if we restarted evolution we do not save the bests of the initial generation
	    if ((f+1) <= savebest && (startgeneration == 0 || g > startgeneration))
		{
		 sprintf(sbuffer,"B%dP%dS%d.gen",f+1,p,seed);
		 if (g == 0)
		 {
		   if ((fp=fopen(sbuffer, "w")) == NULL)
		   {
		     sprintf(sroot,"I cannot open file B%dP%dS%d.gen",f+1,p,seed);
		     display_error(sroot);
		   }
		 }
		 else
		 {
		  if ((fp=fopen(sbuffer, "a")) == NULL)
		  {
		    sprintf(sroot,"I cannot open file B%dP%dS%d.gen",f+1,p ,seed);
		    display_error(sroot);
		  }
		}
	        fprintf(fp,"**NET : s%d_%d.wts\n",g,x);
	        saveagenotype(fp,x,p);
	        fflush(fp);
	        fclose(fp);
		}
	  }
	}
	// reproduce
	// if we have elitism, we do not mutate one of the offspring of each reproducing indvidual
	if (g+1 < generations)
	{
	  for(p=0; p < npopulations; p++)
	  {
	    for(f=0;f<bestfathers;f++)
	    {
	       for (k=0;k<nkids;k++)
		{
                  if (elitism > 0 && k == 0)
		    getbestgenome(k+(f*nkids),f,p,0);
		   else
		    getbestgenome(k+(f*nkids),f,p,1);
		   if (elitism == 2)
		   {
		     if (k == 0)
                       pfitness[k+(f*nkids)] = bpfitness[f];
		      else
                       pfitness[k+(f*nkids)] = 0.0;
		     if (k == 0)
                       plifes[k+(f*nkids)] = bplifes[f];
		      else
                       plifes[k+(f*nkids)] = 0;
		   }

		}
	    }
	  }
	}
}


/*
 * main evolutionary loop (calling eval_team and loop)
 * also used for testing all individuals
 */
void n_fixed_evolution()
{

	int    n,g,p,nn,pp;
    double bestfit[MAXRACES],avefit[MAXRACES];
    char   sbuffer[128], sbuffer_aux[128],sel_ind[128];
	int	   nindnow=0;
    FILE   *fp, *fout;
	int    race;
	int    t,r;
	int    nindperrace;
	char   filename[256];
	int    i,j;
	

    statfit_max = 0;
    statfit_min = 0;
    statfit_n = 0;
	nindperrace = nindividuals / nraces;		// Determina il numero di individui per razza
  
	if (save_ind_fitness) {						// Decide se salvare la fitness di ogni individuo
		sprintf(filename,"Fitness_of_each_individual_S%d.txt",seed);
		fout=fopen(filename, "w");
		fprintf (fout, "\t  Fitness of each individual\n\n\n");
		fclose(fout);
	}
	
	if (elitism == 2)							// con elitism=2 si preservano i dati della precedente
												// fitness
		for(n=0;n<nindividuals;n++)
		{
			pfitness[n] = 0.0;
			plifes[n] = 0;
		}

	for(g=startgeneration;g<generations;g++)	// ciclo di evoluzione durante le generazioni
	{
		for (p=0;p<npopulations;p++)
		{
			if (masterturnament == 1)
				nindividuals = nindividuals + additional_ind;
						
		    // determina il numero di individui su cui ciclare in base al tipo di evoluzione
			if (n_fixed_evolution_social_is_running == 0) { // individual evolution
			    if (nraces==1)
					nindnow=nindividuals;					// cicla su tutti gli individui
				else 
					nindnow=nindividuals/nraces;			// il numero di individui totali viene diviso per il numero di razze
			} else {  
				nindnow=1;								// effettua un solo ciclo perch� gli individui vengono testati 
														// tutti dentro trial ad ogni generazione     
			}

			// Inizializza eventualmente l'array che memorizza gli individui gi� estratti casualmente
			if ((n_fixed_evolution_is_running == 1) && (evaluate_different_inds) && (nraces>1)) {
				for (i=0;i<nindnow;i++)
					for (j=0;j<nraces;j++) {
						selected_ind_array[i][j]=0;
						tfitness[p][j][i+nindnow*j]=0.0;				// azzera il vettore delle fitness
					}				
			
					// Salva la sequenza estratta in un file
					sprintf(sbuffer,"Individuals_random_sequence_Seed%d.ran",seed,g);
					fout=fopen(sbuffer,"w");							// file di analisi statistica	
					if (fout!=NULL) 
						fclose(fout);

				if ((evaluate_different_inds==2) && (ntrials_different_inds>0))	// corregge il numero di combinazioni selezionate
					nindnow=ntrials_different_inds;
			}
						
			// attenzione non funziona con pi� di 20 individui sistemare
			for(n=0;n<nindnow;n++)
			{
				if (pop_seed == 1)
					set_seed(seed+g);

				if ((display_under_evolution == 0) && (n_ind_to_display_under_evolution==n) && (n_fixed_evolution_is_running))
					display_under_evolution = 1; // attiva momentaneamente la visualizzazione nell'evoluzione
				
				init_and_evaluate_nf(g,n,p);	// richiama il genotipo di un individuo e lo valuta 
												// in base alla fitness

				if ((display_under_evolution == 1) && (n_ind_to_display_under_evolution==n) && (n_fixed_evolution_is_running))
					display_under_evolution = 0; // per poi disattivarla successivamente

				if (elitism == 2)
				{
					plifes[n] += 1;
					pfitness[n] += tfitness[p][0][n];
				}
	
				if (!((n_fixed_evolution_is_running == 1) && (evaluate_different_inds) && (nraces>1))) 
					sprintf(sbuffer, "Seed %d Pop %d Gen.%d  Ind.%d  Fit.%.3f",seed,p,g,n,tfitness[p][0][n]);
				else {
					sprintf(sbuffer, "Seed %d Pop %d Gen.%d  Ind/Fit : ",seed,p,g);

					for (j=0;j<nraces;j++) {
						for (i=0;i<nindividuals/nraces;i++) {
							if (selected_ind_array[i][j]>=2) {		// se � stato appena aggiornato allora visualizza l'individuo, valori 2 o 3
								sprintf(sel_ind, "%d : %.3f",i,tfitness[p][j][i+j*nindperrace]);	
								if (j<nraces-1)
									strcat(sel_ind, " |");			// L'ultimo non lo inserisce
								strcat(sbuffer," ");
								strcat(sbuffer,sel_ind);
								selected_ind_array[i][j]=1;			// pone ormai come attiva ma obsoleta la selezione, segnala che l'individuo � gi� stato estratto
							}
						}	
					}
				}
				display_stat_message(sbuffer);

				if (userbreak == 1)
				{
					n_fixed_evolution_is_running = 0;
					n_fixed_evolution_social_is_running = 0;
					n_variable_evolution_is_running = 0;
					break;
				}
			}
		}

		if (save_ind_fitness) {		// salva la fitness di ogni individuo
			sprintf(filename,"Fitness_of_each_individual_S%d.txt",seed);
			fout=fopen(filename, "a");
			if (fout!=NULL) {
				fprintf (fout, "\nGeneration %d : ",g);
				for(r=0;r<nraces;r++) 	
					for(t=0; t<nindividuals/nraces; t++) 
						fprintf(fout,"%d : %.5f ",t+nteam/nraces*r,tfitness[0][r][t+nteam/nraces*r]);		  
			}
			fflush(fout);
			fclose(fout);	
		}

		// save fitness for each individual
		if (saveifit == 1)
			for(n = 0;n < npopulations; n++)
			{ 
				sprintf(sbuffer,"FitP%dS%d.txt",n,seed);
				if (g == 0)
					fp=fopen(sbuffer , "w");
				else
					fp=fopen(sbuffer , "a");
				if (fp == NULL)
					display_error("unable to save individual fitnesses on a file");
				for(nn = 0; nn < nindividuals; nn++)
					fprintf(fp,"%.2f ",tfitness[n][nn]);
				fprintf(fp,"\n");
				fflush(fp);
				fclose(fp);
			}
		// save statistics
		sprintf(sbuffer,"statS%d.fit",seed);
		if (g == 0)
			fp=fopen(sbuffer , "w");
		else
			fp=fopen(sbuffer , "a");
		if (fp == NULL)
			display_error("unable to save statistics on a file");
		for(pp = 0;pp < npopulations; pp++)
		{
			for (race=0;race<nraces;race++) {	
				bestfit[race] = -99990.0;
				avefit[race]  = 0.0;
				if (elitism == 2)											// Elitismo 2 non funziona con le razze
					for(n = 0;n < nindividuals;n++)	
					{
						avefit[race] += pfitness[n] / (double) plifes[n];
						if ((pfitness[n] / (double) plifes[n]) >= bestfit[race])
							bestfit[race] = (pfitness[n] / (double) plifes[n]);
					}
				else
					for(n = 0+race*nindperrace;n < nindperrace+(race*nindperrace);n++)		// Cicla sul numero di individui della razza, in modo tale che la media possa farla solo all'interno di quegli elementi
					{
						avefit[race] += tfitness[pp][race][n];				// Somma delle fitness totalizzate da ogni individuo in una medesima razza
						if (tfitness[pp][race][n] >= bestfit[race])			// Determina la fitness migliore 
							bestfit[race] = tfitness[pp][race][n];			// Memorizza il migliore di una razza
					}
				
				avefit[race] = avefit[race] / (double) nindperrace;	        // Calcola la media delle fitness totalizzate dagli individui, sul numero di individui per razza

				if (userbreak == 0)
				{
					fprintf(fp,"%.3f %.3f ",bestfit[race],avefit[race]);		   
					if (g < MAXGENERATIONS)
					{
						statfit[g][0+pp*2] = bestfit[race];
						statfit[g][1+pp*2] = avefit[race];
						astatfit[g][0+pp*2] += bestfit[race];
						astatfit[g][1+pp*2] += avefit[race];
						statfit_n = g + 1;
						if (bestfit[race] > statfit_max)
							statfit_max = bestfit[race];
						if (avefit[race] > statfit_max)
							statfit_max = avefit[race];
		                if (bestfit[race] < statfit_min)
							statfit_min = bestfit[race];
						if (avefit[race] < statfit_min)
							statfit_min = avefit[race];
						update_rendevolution(1);
					}
				}
			}	// end races			

			for (race=0;race<nraces;race++) {	
				sprintf(sbuffer, "gen %d) race %d) %.2f %.2f",g,race,bestfit[race],avefit[race]);		// Visualizza la fitness migliore e media per razza
			}	
			display_stat_message(sbuffer);	

			fprintf(fp,"\n");	// Termina la scrittura dei parametri di fitness delle diverse razze		   
		}	// end pop
		fflush(fp);
		fclose(fp);
         
		// eventually we save the last generation
		if (g == 0 || userbreak == 1 || (g>0 && (savebest==0 || g+1 == generations)))
		{
			if (userbreak == 1)
				saveallg(g-1);
			else
				saveallg(g);
		}

		// nel caso di richiesto salva le generazioni parziali
		if ((save_each_generation) && (g>0)) 	
			saveallg(g);

		// nel caso di evoluzione con n-fisso e senza cloni allora salva le generazioni parziali
		if ((n_fixed_evolution_social_is_running == 1) && !(save_each_generation))
			saveallg(g);

		if (userbreak == 1)
			break;

		if (g < generations && masterturnament == 0)
			reproduce(g);			// for more races
	} // end generations
}

/*
 * main evolutionary loop for artificial life evolutionary agents
 */
void n_variable_evolution()
{

	int    n,nn,pp;
    double bestfit,avefit;
    char   sbuffer[128];
    FILE   *fp;	
	
	statfit_max = 0;
    statfit_min = 0;
    statfit_n = 0;

	for (n=0; n < nindividuals; n++) 	// scorre il genoma per ogni individuo
	{
		init_and_evaluate_nv(n);		// richiama il genotipo di un individuo e lo inserisce nell'ambiente 
				
	
			/*sprintf(sbuffer, "Seed %d Pop %d Gen.%d  Ind.%d  Fit.%.3f",seed,p,g,n,tfitness[p][n]);
			display_stat_message(sbuffer);
			*/
		if (userbreak == 1)
		{
			n_fixed_evolution_is_running = 0;
			n_fixed_evolution_social_is_running = 0;
			n_variable_evolution_is_running = 0;
			break;
		}
	}

/*
	
	if (elitism == 2)							// con elitism=2 si preservano i dati della precedente
												// fitness
		for(n=0;n<nindividuals;n++)
		{
			pfitness[n] = 0.0;
			plifes[n] = 0;
		}
	for(g=startgeneration;g<generations;g++)	// ciclo di evoluzione durante le generazioni
	{
		for (p=0;p<npopulations;p++)
		{
			if (masterturnament == 1)
				nindividuals = nindividuals + additional_ind;
			for(n=0;n<nindividuals;n++)
			{
				if (pop_seed == 1)
					set_seed(seed+g);

				init_and_evaluate_nv(g,n,p);	// richiama il genotipo di un individuo e lo valuta 
												// in base alla fitness
				if (elitism == 2)
				{
					plifes[n] += 1;
					pfitness[n] += tfitness[p][n];
				}
	
				sprintf(sbuffer, "Seed %d Pop %d Gen.%d  Ind.%d  Fit.%.3f",seed,p,g,n,tfitness[p][n]);
				display_stat_message(sbuffer);

				if (userbreak == 1)
				{
					n_fixed_evolution_is_running = 0;
					n_fixed_evolution_social_is_running = 0;
					n_variable_evolution_is_running = 0;
					break;
				}
			}
		}
		// save fitness for each individual
		if (saveifit == 1)
			for(n = 0;n < npopulations; n++)
			{ 
				sprintf(sbuffer,"FitP%dS%d.txt",n,seed);
				if (g == 0)
					fp=fopen(sbuffer , "w");
				else
					fp=fopen(sbuffer , "a");
				if (fp == NULL)
					display_error("unable to save individual fitnesses on a file");
				for(nn = 0; nn < nindividuals; nn++)
					fprintf(fp,"%.2f ",tfitness[n][nn]);
				fprintf(fp,"\n");
				fflush(fp);
				fclose(fp);
			}
		// save statistics
		sprintf(sbuffer,"statS%d.fit",seed);
		if (g == 0)
			fp=fopen(sbuffer , "w");
		else
			fp=fopen(sbuffer , "a");
		if (fp == NULL)
			display_error("unable to save statistics on a file");
		for(pp = 0;pp < npopulations; pp++)
		{
			bestfit = -99990.0;
			avefit  = 0.0;
			if (elitism == 2)
				for(n = 0;n < nindividuals;n++)	
				{
					avefit += pfitness[n] / (double) plifes[n];
					if ((pfitness[n] / (double) plifes[n]) >= bestfit)
						bestfit = (pfitness[n] / (double) plifes[n]);
				}
			else
				for(n = 0;n < nindividuals;n++)	
				{
					avefit += tfitness[pp][n];
					if (tfitness[pp][n] >= bestfit)
						bestfit = tfitness[pp][n];
				}
				
			avefit = avefit / (double) nindividuals;
			sprintf(sbuffer, "gen %d) %.2f %.2f",g,bestfit,avefit);
			display_stat_message(sbuffer);

			if (userbreak == 0)
			{
				fprintf(fp,"%.3f %.3f \n",bestfit,avefit);		   
				if (g < MAXGENERATIONS)
				{
					statfit[g][0+pp*2] = bestfit;
					statfit[g][1+pp*2] = avefit;
					astatfit[g][0+pp*2] += bestfit;
					astatfit[g][1+pp*2] += avefit;
					statfit_n = g + 1;
					if (bestfit > statfit_max)
						statfit_max = bestfit;
					if (avefit > statfit_max)
						statfit_max = avefit;
                    if (bestfit < statfit_min)
						statfit_min = bestfit;
					if (avefit < statfit_min)
						statfit_min = avefit;
					update_rendevolution(1);
				}
			}
		}
		fflush(fp);
		fclose(fp);
         
		// eventually we save the last generation
		if (g == 0 || userbreak == 1 || (g>0 && (savebest==0 || g+1 == generations)))
		{
			if (userbreak == 1)
				saveallg(g-1);
			else
				saveallg(g);
		}

		if (userbreak == 1)
			break;

		if (g < generations && masterturnament == 0)
			reproduce(g);	 	 
	} // end generations

*/	
}



/*
 * test one individual
 */

void test_one_individual()
{


  init_and_evaluate_nf(startgeneration, dindividual,0);

  testindividual_is_running = 0;
  userbreak = 0;
}


/*
 * test all best individuals of all replications
 * results are save in file master%s.fit
 * assume that we only have a single population
 */
void test_all_individuals()
{

  int    replication;
  int    n;
  char   sbuffer[256];
  FILE   *fp;
  int    nloadedindividuals;

  for (replication=0; replication < nreplications; replication++)
	{
          statfit_max = 0;
          statfit_min = 0;
	  sprintf(sbuffer,"B1P0S%d.gen",seed + replication);
	  nloadedindividuals = loadallg(-1, sbuffer, 0, 0);
	  if (nloadedindividuals > (nindividuals + additional_ind))
	  {
	    nloadedindividuals = nindividuals + additional_ind;
	    display_warning("please increase the number of additional_ind to allocate enough space");
	  }
	  for(n=0;n<nloadedindividuals;n++)
		{
	        sprintf(sbuffer,"testing replication %d (%d individuals) individual %d",seed + replication,nloadedindividuals,n);
	        display_stat_message(sbuffer);
		init_and_evaluate_nf(0,n,0);
		if (userbreak == 1)
		    break;
		statfit_n = n + 1;
		if (tfitness[0][0][n] > statfit_max)
		  statfit_max = tfitness[0][0][n];
		if (tfitness[0][0][n] < statfit_min)
		  statfit_min = tfitness[0][0][n];
                statfit[n][0] = tfitness[0][0][n];
                statfit[n][1] = 0.0;
	        update_rendevolution(1);
		}
	   if (userbreak == 1)
	     break;
           sprintf(sbuffer,"masterS%d.fit",seed + replication);
	   fp=fopen(sbuffer , "w");
	   if (fp == NULL)
             display_error("unable to save statistics on a file");
	   for(n=0;n<nloadedindividuals;n++)
		{
		  fprintf(fp,"%.3f 0.0\n",tfitness[0][n]);		   
		}
	   fflush(fp);
	   fclose(fp);
	}

}

/*
 * test individual
 *  mode=0: test the current individual team
 *  mode=1; test the best individial team of all generations (on the basis of the current statistics)
 *  mode=2; test all individuals of the population
 */
void test(int mode)
{
	struct individual *pind;
	FILE  *fp;
	FILE   *fout;
	char res[100];
	char   fileout[128];	 
	int team,race;
	char filename[256];
	int i, k, nteam_tmp, gen, s, testtrial1, testtrial2, j;
	extern void init_modules();
	float  timeaverage, stdev;		
	int ntimes;
	int success_rate, success_rate_leader;
	int startgen,endgen;
	int startseed,endseed;
	int starttesttrial1,endtesttrial1;
	int starttesttrial2,endtesttrial2;
	int team1, team2;
	int ni;
	int aveoversweeps;
	int type;
	int nteam_testing;
	int old_lab_ntestingteam;
	int hd;
	int p;

	//	genoma dell'intera popolazione p
	//	genome of the entire population p
	int **gpi, **gpi0;				

	//	gene dell'individuo i
	//	the individual gene
	int	*gi,*gi0;					

	//	numero di individui per razza
	//	number of individuals by race
	int nindperrace;					
  
	//	numero di coppie possibili
	//	number of possible pairs
	int n_couples;				

	int type_file;
	int best_team1, best_team2;
	int subst;

	//	maximum number of substitutions
	int startofsubstitutions;		
	//  maximum number of substitutions
	int endofsubstitutions;			

	switch (mode) 
	{
		case 0:
			testindividual_is_running = 1;
			n_fixed_evolution_is_running = 0;
			n_fixed_evolution_social_is_running = 0;
			n_variable_evolution_is_running = 0;

			evaluate(0,0); 
			// init_and_evaluate_nf(startgeneration, dindividual,0);

			testindividual_is_running = 0;
			userbreak = 0;	 
		break;
		case 1:
			testindividual_is_running = 1;
			n_fixed_evolution_is_running = 0;
			n_fixed_evolution_social_is_running = 0;
			n_variable_evolution_is_running = 0;

			if (nraces>1) 
			{							
				//	Richiama la visualizzazione di individui di genotipo (razze) differenti
				//	Selects the display of individuals of genotype (all races) different
				nteam=nraces;
				
				//	Reinizializza tutto per il disegno in caso di razze multiple
				//	Reset all for drawing in the case of multi-spoke
				init_robot(0);						

				//	Rialloca spazio per il team esteso di reti neurali di controllo
				//	Reallocates space for the extended team neural network control
				init_network(0);					
			}

			//	Se viene caricato il genotipo dei best es. B1P0S1.gen allora 
			//	carica l'ultimo genotipo dei best lo stesso avviene se viene 
			//	caricato il genotipo della popolazione
			//	If you load the genotype of the best example. B1P0S1.gen then 
			//	loads the last best genotype of the same happens if you load 
			//	the genotype of the population
			init_and_evaluate_nf(startgeneration, dindividual,0);	
			
			testindividual_is_running = 0;
			userbreak = 0;	 
		break;
		case 2:
			testindividual_is_running = 1;
			n_fixed_evolution_is_running = 0;
			n_fixed_evolution_social_is_running = 0;
			n_variable_evolution_is_running = 0;
			select_besti();
			
			//	vediamo chi � dinindividual
			//	let's see who is the best inindividual
			//	sprintf(res,"Best indi.: %d",dindividual);
			
			// display_error(res);
			//fone debug
			init_and_evaluate_nf(startgeneration, dindividual,0);
			testindividual_is_running = 0;
			userbreak = 0;	
		break;
		case 3:
			n_fixed_evolution_is_running=1;
			testindividual_is_running = 0;
			n_fixed_evolution_social_is_running = 0;
			n_variable_evolution_is_running = 0;
			test_all_individuals();
			n_fixed_evolution_is_running=0;
		break;
		case 4:
			testpopulation_is_running = 1;
			testindividual_is_running = 0;
			n_fixed_evolution_is_running = 0;
			n_fixed_evolution_social_is_running = 0;
			n_variable_evolution_is_running = 0;

			//	se ci sono individui caricati 
			//	if there are individuals loaded
			if (dindividual>0) 
			{
				//	Il numero di infividui del team � pari al 
				//	numero di individui letti dal .gen
				//	The number of invidual team is equal to the 
				//	number of individuals from the beds.
				nteam=dindividual+1;						
			}
			else
			{
				//	Imposta la numerosit� del team 
				//	uguale a quella del genoma
				//	Set the number of the team equal to that of the genome
				nteam=(nindividuals + additional_ind) * npopulations;	
			}

			//	Rialloca spazio per il team esteso di individui
			//	Reallocates space for the extended team of individuals
			init_robot(0);										

			//	Rialloca spazio per il team esteso di reti neurali di controllo
			//	Reallocates space for the extended team neural network control
			init_network(0);											

			//	solo dal secondo ciclo in poi 
			//	only from the second cycle onwards
			if (test_seed>1) 
			{											
				nteam_tmp=nteam;	
				nteam=1;

				//  Inizializza la posizione di ciascuno dei robot
				//	Initializes the position of each of the robot
				(*pinit_robot_pos)();									
				nteam=nteam_tmp;
			}

			//  Carica il genotipo di tutta la popolazione in individui differenti
			//	Upload genotype of the entire population in different individuals
			init_and_evaluate_nf(startgeneration, dindividual,0);		

			//  Salvataggio della fitness in fase di test
			//	Saving the fitness testing
			//if (test_statistics) 
			//{
				sprintf(filename,"Fitness_SociallyEvolvedTogetherTested_S%d.fit",test_seed);
				fp=fopen(filename, "w");
				for(race=0;race<nraces;race++) 
				{	
					for(team=0; team<nteam/nraces; team++) 
					{
						fprintf(fp,"Ind : %d Race : %d Fitness : %.5f \n",team+race*(nindividuals/nraces),race,tfitness[0][race][team+nteam/nraces*race]);		   
					}
				}
				fflush(fp);
				fclose(fp);	
			//}

			//  Resetta il parametro precedentemente impostato a 1
			//	Resets the parameter previously set to 1
			testpopulation_is_running = 0;								
			userbreak = 0;	 
		break;
		case 5:
			testpopulation_is_running = 1;
			testindividual_is_running = 0;
			n_fixed_evolution_is_running = 0;
			n_fixed_evolution_social_is_running = 0;
			n_variable_evolution_is_running = 0;

			//  se ci sono individui caricati 	
			//	if there are individuals loaded
			if (dindividual>0) 
			{
				//  Il numero di infividui del team � pari al numero di individui letti dal .gen
				//	The number of indvidual team is equal to the number of individuals from the beds.
				nteam=dindividual+1;						
			}
			else
			{
				//  Imposta la numerosit� del team 
				//  uguale a quella del genoma
				//	Set the number of the team equal to that of the genome
				nteam=(nindividuals + additional_ind) * npopulations;	
			}

			//  Rialloca spazio per il team esteso di individui
			//	Reallocates space for the extended team of individuals
			init_robot(0);												

			//  Rialloca spazio per il team esteso di reti neurali di controllo
			//	Reallocates space for the extended team neural network control
			init_network(0);											

			//  Carica il genotipo di tutta la popolazione in individui differenti
			//	Upload genotype of the entire population in different individuals
			init_and_evaluate_alone(startgeneration, dindividual,0);	

			//  Resetta il parametro precedentemente impostato a 1
			//	Resets the parameter previously set to 1
			testpopulation_is_running = 0;								

			//  Salvataggio della fitness in fase di test
			//	Saving the fitness testing
			if (test_statistics) 
			{
				fp=fopen("Fitness_SociallyEvolvedAloneTested.fit" , "w");
				for(race=0;race<nraces;race++) 
				{
					for(team=0; team<nteam/nraces; team++) 
					{
						fprintf(fp,"Ind : %d Race : %d Fitness : %.3f \n",team,race,tfitness[0][race][team+nteam/nraces*race]);		   
					}
				}
				fflush(fp);
				fclose(fp);
			}
			userbreak = 0;	 
		break;
		case 6:
			testpopulation_is_running = 1;
			testindividual_is_running = 0;
			n_fixed_evolution_is_running = 0;
			n_fixed_evolution_social_is_running = 0;
			n_variable_evolution_is_running = 0;

			// se ci sono individui caricati 	
			if (dindividual>0) 
			{
				// Il numero di infividui del team � pari al numero di individui letti dal .gen
				nteam=dindividual+1;						
			}
			else
			{
				// Imposta la numerosit� del team uguale a quella del genoma
				nteam=(nindividuals + additional_ind) * npopulations;	
			}

			// Raddoppia il numero di individui disponibili nel genoma
			nteam*=2;										

			// Rialloca spazio per il team esteso di individui
			init_robot(0);									

			// Rialloca spazio per il team esteso di reti neurali di controllo
			init_network(0);											

			// Carica il genotipo di tutti gli individui clonandoli per una volta ognuno
			init_and_evaluate_double(startgeneration, dindividual,0);

			// Resetta il parametro precedentemente impostato a 1
			testpopulation_is_running = 0;								

			// Salvataggio della fitness in fase di test
			if (test_statistics) {
				fp=fopen("Fitness_SociallyEvolvedDoublePopWithClones.fit" , "w");
				for(race=0;race<nraces;race++) 	
					for(team=0; team<nteam/nraces; team++) {
						fprintf(fp,"Ind : %d Race : %d Fitness : %.3f \n",team,race,tfitness[0][race][team+nteam/nraces*race]);		   
					}
				fflush(fp);
				fclose(fp);	
			}

			nteam/=2;													// Ripristina il numero di individui disponibili nel genoma	
			userbreak = 0;	 
		break;
     case 7:
		testpopulation_is_running = 1;
		testindividual_is_running = 0;
		n_fixed_evolution_is_running = 0;
		n_fixed_evolution_social_is_running = 0;
		n_variable_evolution_is_running = 0;
		if (dindividual>0) // se ci sono individui caricati 	
			nteam=dindividual+1;						// Il numero di infividui del team � pari al numero di individui letti dal .gen
		else
			nteam=(nindividuals + additional_ind) * npopulations;	// Imposta la numerosit� del team 
																	// uguale a quella del genoma
		nteam/=2;													// Dimezza il numero di individui disponibili nel genoma	
		init_robot(0);												// Rialloca spazio per il team ridotto di individui
		init_network(0);											// Rialloca spazio per il team ridotto di reti neurali di controllo
		init_and_evaluate_half(startgeneration, dindividual,0);		// Carica il genotipo della met� degli individui della popolazione
		testpopulation_is_running = 0;								// Resetta il parametro precedentemente impostato a 1
		
		// Salvataggio della fitness in fase di test
		if (test_statistics) {
			fp=fopen("Fitness_SociallyEvolvedHalfPop.fit" , "w");
			for(race=0;race<nraces;race++) 	
				for(team=0; team<nteam/nraces; team++) {
					fprintf(fp,"Ind : %d Race : %d Fitness : %.3f \n",team,race,tfitness[0][race][team+nteam/nraces*race]);		   
				}
			fflush(fp);
			fclose(fp);	
		}
		nteam*=2;													// Ripristina il numero di individui disponibili nel genoma	
		userbreak = 0;	 
		break;
     case 8:		// carica automaticamente l'ultima generazione di ogni seed le testa
		// Cicla su tutti i seed richiesti
		for (i=1;i<=nreplications;i++) {
			sprintf(filename,"B1P0S%d.gen",i);
			loadallg(-1, filename, 0, 0);	
			test_seed=i;
			test(1);		// Testa la popolazione
		}	
		testpopulation_is_running = 0;								// Resetta il parametro precedentemente impostato a 1
		userbreak = 0;	 
		break;
     case 9:		// carica automaticamente l'ultima generazione di ogni seed le testa
		// Cicla su tutti i seed richiesti
		for (i=1;i<=nreplications;i++) {
			sprintf(filename,"G%dP0S%d.gen",generations-1,i);
			loadallg(-1, filename, 0, 0);	
			test_seed=i;
			test(4);		// Testa la popolazione
		}	
		testpopulation_is_running = 0;								// Resetta il parametro precedentemente impostato a 1
		userbreak = 0;	 
		break;
     case 10:
		labtest_is_running = 1;
		testpopulation_is_running = 1;
		testindividual_is_running = 0;
		n_fixed_evolution_is_running = 0;
		n_fixed_evolution_social_is_running = 0;
		n_variable_evolution_is_running = 0;
		
		n_couples=((int) pow((float) 2, (float) ipar->lab_ntestingteam))-ipar->lab_ntestingteam;	// numero di acoppiamenti possibili con i ntestingteam individui

		if (nraces>1) {										// Solo nel caso di pi� razze riadatta nteam
			if (dindividual>0)								// se ci sono individui caricati 	
				nteam=dindividual+1;						// Il numero di infividui del team � pari al numero di individui letti dal .gen
			else
				nteam=(nindividuals + additional_ind) * npopulations;	// Imposta la numerosit� del team 
																		// uguale a quella del genoma
			nteam_testing=nteam;										// utilizzato per le analisi statistiche

			search_bestind_num(1,1,0,&type);
			if (type==2)											// significa che � stata effettuata una evoluzione n-fissa
				nteam_testing=nraces;								// non sociale, con pi� razze (evoluzioni singole con genotipi diversi)		
		} else {
			nteam_testing=nteam;									// Evoluzione con cloni
		}

		//init_robot(0);										// Rialloca spazio per il team esteso di individui
		//init_network(0);									// Rialloca spazio per il team esteso di reti neurali di controllo
		
		// Verifica quanti seed deve testare : se 1 o tutte
		if (!ipar->lab_test_all_seed) {
			startseed=ipar->lab_test_number_seed;		// Testa un solo seed
			endseed=ipar->lab_test_number_seed+1;
		} else {
			startseed=ipar->lab_test_startseed;			// Testa tutti i seed
			endseed=nreplications+startseed;
		}

		// Imposta gli individui che devono essere ghost
		for(i=0,pind=ind; i<ipar->lab_ntestingteam;i++,pind++) {
			if (((pind->number==0) && (ipar->lab_test_ghost_team1)) ||
			   ((pind->number==1) && (ipar->lab_test_ghost_team2)) ||
			   ((pind->number==2) && (ipar->lab_test_ghost_team3)) ||
			   ((pind->number==3) && (ipar->lab_test_ghost_team4)))
				pind->ghost_individual=1;
		}
			
		if ((ipar->lab_test_subst_number_seed > 0) && (ipar->lab_test_subst_with_array)) {
			if (ipar->lab_test_subst_startseed>0)						/* Number of seed from which to start */
				startofsubstitutions=ipar->lab_test_subst_startseed-1;
			else 
				startofsubstitutions=0;						

			endofsubstitutions=MAXSUBSTITUTIONS;						// The number of individual substitutionsis equal to the maximum. In other words, it performs the external cycle. 
		} else {
			startofsubstitutions=0;						
			endofsubstitutions=1;										// It doesn't perform the external cycle.
		}
		for (subst=startofsubstitutions;subst<endofsubstitutions;subst++) {
			
			if ((ipar->lab_test_subst_number_seed > 0) && (ipar->lab_test_subst_with_array)) {
				ipar->lab_test_subst_ind_orig=subst_array[subst];			// The source individual number is extracted from the array, for each time. 			
				ipar->lab_test_subst_number_seed=subst+1;					// The seed from it is extracted is subst+1				
			}

			// Cicla su alcuni o su tutti i seed
			for (s=startseed;s<endseed;s++) {
				//init_robot(0);											// Rialloca spazio per il team esteso di individui
				//init_network(0);											// Rialloca spazio per il team esteso di reti neurali di controllo
	
				if ((ipar->lab_test_subst_number_seed > 0) && (ipar->lab_test_subst_with_array)) 
					ipar->lab_test_subst_ind_dest=subst_array[s-1];			// The destination individual number is extracted from the array, for each time. 			
		
				// Misura statistica n.6 (Misura di Leadership)
				if ((test_measure_leadership) /*&& (test_statistics) */) {
					sprintf(fileout,"Measure of Leadership_Seed%d.txt",s);
					fout=fopen(fileout,"w");							// file di analisi statistica
					if (test_output_type==0) {
						fprintf (fout, "\t  Measure of Leadership \n");
						fprintf (fout, "\t---------------------------------\n\n");
						fprintf (fout, "\t  First individual | With Other individuals \n");
						fprintf (fout, "\t--------------------------------------------\n");
					} else if (test_output_type==1) {
						/*fprintf (fout, "Index ");			
						for (k=0; k<(nindividuals/nraces)-1; k++) 
							fprintf (fout, "Ind_%d ", (ind+k)->number);			
						fprintf (fout, "Ind_%d", (ind+k)->number);*/			
					}
					fclose (fout);
				}	

				// Misura statistica n.? (Misura di Leadership)
				if ((test_measure_leadership_eco) /*&& (test_statistics) */) {
					sprintf(fileout,"Measure of Leadership_in_ecology_Seed%d.txt",s);
					fout=fopen(fileout,"w");							// file di analisi statistica
					if (test_output_type==0) {
						fprintf (fout, "\t  Measure of Leadership in ecology \n");
						fprintf (fout, "\t---------------------------------\n\n");
							fprintf (fout, "\t  First individual | With Other individuals \n");
						fprintf (fout, "\t--------------------------------------------\n");
					} else if (test_output_type==1) {
						/*fprintf (fout, "Index ");			
						for (k=0; k<(nindividuals/nraces)-1; k++) 
							fprintf (fout, "Ind_%d ", (ind+k)->number);			
						fprintf (fout, "Ind_%d", (ind+k)->number);*/			
					}
					fclose (fout);
				}	

				// Misura statistica n.? (Misura di Distanza di Hamming)
				if ((test_hamming_distance) /*&& (test_statistics) */) {
					sprintf(fileout,"Measure_of_Hamming_Distance_Seed%d.txt",s);
					fout=fopen(fileout,"w");							// file di analisi statistica
					if (test_output_type==0) {
						fprintf (fout, "\t  Measure of Hamming Distance \n");
						fprintf (fout, "\t---------------------------------\n\n");
						fprintf (fout, "\t  First individual | With Other individuals \n");
						fprintf (fout, "\t--------------------------------------------\n");
					} else if (test_output_type==1) {
						for (k=0; k<n_couples-1; k++) 
							fprintf (fout, "Couple_%d ", k);			
						fprintf (fout, "Ind_%d", k);			
					}
					fclose (fout);
				}		

				if ((test_times) && (test_statistics) && ((pffitness == ffitness_predator_prey2))) {
					sprintf(fileout,"Statistics_Time_Predator_PreyS%d.txt",s);
					fout=fopen(fileout,"w");								// file di analisi statistica
					fprintf (fout, "\t  Best Prey - Best predator catching times \n");
					fprintf (fout, "\t---------------------------------\n\n");
					fprintf (fout, "\t  Gen | Trials Times | Time Ave without 0 | St dev without 0 | Time Ave with 0 | St dev with 0 | Success rate \n");
					fprintf (fout, "\t------------------------------------------------------------------------------------------------------------\n\n");
					fclose (fout);
				}

				if ((test_fitness_lab_all_gen)/*&& (test_statistics)*/) {
					if (ipar->lab_test_subst_number_seed==0)
						sprintf(fileout,"Fitness_SociallyEvolvedTogetherTested_S%d.fit",s);
					else
						sprintf(fileout,"Fitness_SociallyEvolvedTogetherTested_SubstSeed%d_S%d.fit",ipar->lab_test_subst_number_seed,s);

					fout=fopen(fileout,"w");								// file di analisi statistica
					fclose(fout);															
					
					fout=fopen(fileout,"a");								// file di analisi statistica
					//if (fout!=NULL) 
						if (test_output_type==0) {
							fprintf (fout, "\t  Fitness \n");
							fprintf (fout, "\t---------------------------------\n\n");
							fprintf (fout, "\t  Gen | Fitnesses \n");
							fprintf (fout, "\t-----------------\n\n");
						} else if (test_output_type==1) {
							for (k=0; k<nteam_testing-1; k++) 
								fprintf (fout, "Group_fit_Ind_%d Virtual_fit_Ind_%d ", k, k);			
							fprintf (fout, "Group_fit_Ind_%d Virtual_fit_Ind_%d", k, k);			
						}
					//fclose (fout);
					fflush(fout);
				}

				// Misura statistica n.7 (Misura di Fitness in fase di test, un predatore ed una preda)
				if ((test_times) && /*(test_statistics) && */((pffitness == ffitness_predator_prey2))) {
					sprintf(fileout,"Statistics_Time_All_Predators_BestPreyS%d_lastgen.txt",s);
					fout=fopen(fileout,"w");								// file di analisi statistica
					fprintf (fout, "\t  Best Prey - all predator catching times \n");
					fprintf (fout, "\t---------------------------------\n\n");
					fprintf (fout, "\t  Individual | Average Times in all trials | Standard Deviaton Times in all trials | Success rate \n");
					fprintf (fout, "\t------------------------------------------------------------------------------------------------------------\n\n");
					fclose (fout);
				}

				// Misura statistica n.8 (Misura di Fitness in fase di test, 2 predatori ed una preda)
				if ((test_times) && /*(test_statistics) && */((pffitness == ffitness_predator_prey2))) {
					sprintf(fileout,"Statistics_Time_All_Predators_One_Leader_BestPreyS%d_lastgen.txt",s);
					fout=fopen(fileout,"w");								// file di analisi statistica
					fprintf (fout, "\t  Best Prey - all predator catching times with one leader\n");
					fprintf (fout, "\t---------------------------------\n\n");
					fprintf (fout, "\t  Individual | Average Times in all trials | Standard Deviaton Times in all trials | Success rate \n");
					fprintf (fout, "\t--------------------------------------------------------------------------------------------------\n\n");
					fclose (fout);
				}

				// Misura statistica n.11 (test di esplorativit� degli individui tra pi� generazioni (media)) 
				if ((test_exploratory_all_gen) /* && (test_statistics) */) {
					sprintf(fileout,"Measure of Exploration ability_Seed%d.txt", s);
						fout=fopen(fileout,"w");								// svuota inizialmente il file
					fclose(fout);																	

					fout=fopen(fileout,"a");								// file di analisi statistica
					if (fout!=NULL) {
						fprintf (fout, "\t  Measure of Exploratory Abilities \n");
						fprintf (fout, "\t---------------------------------\n\n");
						fprintf (fout, "\t  Generation | Individuals Exploration ability \n");
						fprintf (fout, "\t--------------------------------------------\n");
						//fclose (fout);
					}
					fflush(fout);
				}

				// Misura statistica n.11 (test di esplorativit� degli individui tra pi� generazioni (media)) 
				if ((test_exploratory_all_gen_eco) /* && (test_statistics) */) {
					sprintf(fileout,"Measure of Exploration ability_eco_Seed%d.txt", s);
					fout=fopen(fileout,"w");								// svuota inizialmente il file
					fclose(fout);															

					fout=fopen(fileout,"a");								// file di analisi statistica
					if (fout!=NULL) {
						fprintf (fout, "\t  Measure of Exploratory Abilities in ecology\n");
						fprintf (fout, "\t--------------------------------------------\n\n");
						fprintf (fout, "\t  Generation | Individuals Exploration ability \n");
						fprintf (fout, "\t--------------------------------------------\n");
					}
					fflush(fout);
				}

				// Misura statistica n.? (test di esplorativit� degli individui tra pi� generazioni (media)) 
				if ((test_mobility_all_gen_eco) /* && (test_statistics) */) {
					sprintf(fileout,"Measure of Mobility_eco_Seed%d.txt", s);
					fout=fopen(fileout,"w");								// svuota inizialmente il file
					fclose(fout);															

					fout=fopen(fileout,"a");								// file di analisi statistica
					if (fout!=NULL) {
						fprintf (fout, "\t  Measure of Mobility in ecology\n");
						fprintf (fout, "\t------------------------------------\n\n");
						fprintf (fout, "\t  Generation | Individuals Mobility \n");
						fprintf (fout, "\t------------------------------------\n");
					}
					fflush(fout);
				}
	
				// Misura statistica n.12 (test di velocit� per un predatore di mangiare una preda)
				if ((test_times_all_gen) /* && (test_statistics) */) {
					sprintf(fileout,"Measure_of_Times_Predators_eat_a_PreyS%d.txt",s);
					fout=fopen(fileout,"w");								// svuota inizialmente il file
					fclose(fout);															
			
					fout=fopen(fileout,"a");								// file di analisi statistica
					if (fout!=NULL) { 
						fprintf (fout, "\t  Measure of Prey eating times\n");
						fprintf (fout, "\t---------------------------------\n\n");
						fprintf (fout, "\t  Generation | Times \n");
						fprintf (fout, "\t---------------------------------\n\n");
					}
					fflush(fout);
					//if (!ferror(fout))
					//	fclose (fout);
				}

				if ((test_times_all_gen_eco) /*&& (test_statistics) */) {
					sprintf(fileout,"Measure_of_Times_Predators_eat_some_Preies_S%d.txt",s);
					fout=fopen(fileout,"w");								// svuota inizialmente il file
					fclose(fout);															

					fout=fopen(fileout,"a");								// file di analisi statistica
					if (fout!=NULL) {
						fprintf (fout, "\t  Measure of Prey eating times\n");
						fprintf (fout, "\t----------------------------\n\n");
						fprintf (fout, "\t  Generation | Times \n");
						fprintf (fout, "\t-----------------------------\n\n");
					}
					fflush(fout);
					//if (!ferror(fout))
					//	fclose (fout);
				}

				// Misura statistica n.16
				if ((test_barycentre_distances_all_gen) /*&& (test_statistics)*/ ) {
					if (ipar->motionlessind==0)
						sprintf(fileout,"Measure_of_Barycentre_of_group_respect_to_position_of_each_member_S%d.txt",s);
					else
						sprintf(fileout,"Measure_of_Barycentre_of_group_respect_to_position_of_each_member_S%d_ind%d_stopped.txt",s,ipar->motionlessind);
					fout=fopen(fileout,"w");								// file di analisi statistica
					fclose(fout);	

					fout=fopen(fileout,"a");								// file di analisi statistica
					if (test_output_type==0) {
						fprintf (fout, "\t  Measure of Barycentre\n");
						fprintf (fout, "\t----------------------------\n\n");
						fprintf (fout, "\t  Individual | Barycentre \n");
						fprintf (fout, "\t-----------------------------\n\n");
					} else if (test_output_type==1) {
						for (k=0; k<nteam_testing-1; k++) 
							fprintf (fout, "Ind_%d ", (ind+k)->number);			
						fprintf (fout, "Ind_%d", (ind+k)->number);			
					}
					//fclose (fout);
					fflush (fout);
				}

				// Misura statistica n.
				// Test delle distanze degli individui dalle food zone
				if ((test_distance_from_fz) && (envnobjs[GROUND] > 0)) {
					sprintf(fileout,"Measure_of_Distance_of_each_individual_from_each_food_zone_S%d.txt",s);
					fout=fopen(fileout,"w");								// file di analisi statistica
					if (test_output_type==0) {
						fprintf (fout, "\t  Measure of Distance from food zones\n");
						fprintf (fout, "\t-------------------------------------\n\n");
						fprintf (fout, "\t      Individual   Distances         \n");
						fprintf (fout, "\t-------------------------------------\n\n");
					} else if (test_output_type==1) {
						for (k=0; k<nteam_testing-1; k++) 
							for(j=0; j<envnobjs[GROUND]; j++)				// tutte le food zone
								fprintf (fout, "Ind_%d_Fz_%d ", (ind+k)->number, j);			
					
						for(j=0; j<envnobjs[GROUND]-1; j++)					// tutte le food zone
							fprintf (fout, "Ind_%d_Fz_%d ", (ind+k)->number, j);				

						fprintf (fout, "Ind_%d_Fz_%d", (ind+k)->number, j);			

						fprintf (fout, "\n");				
					}
					fclose (fout);
				}

				// Verifica quante generazioni deve testare : se 1 o tutte
				if (!ipar->lab_test_all_gen) {
					startgen=ipar->lab_test_number_gen;			// Testa una sola generazione
					endgen=ipar->lab_test_number_gen+1;
				} else {
					startgen=ipar->lab_test_startgen;			// Testa tutte le generazioni
					endgen=generations;
				}

				// Cicla su alcune o su tutte le generazioni
				for (gen=startgen;gen<endgen;gen+=ipar->lab_test_gen_step) {		// incrementa di uno step deciso
					test_gen=gen;								// Generazione da visualizzare
				
					init_robot(0);										// Rialloca spazio per il team esteso di individui
					init_network(0);									// Rialloca spazio per il team esteso di reti neurali di controllo
					reset_genome;										// Azzera tutto il genoma

					// Misura statistica n.6 (Misura di Leadership)
					if ((test_measure_leadership) /*&& (test_statistics) */) {
						sprintf(fileout,"Measure of Leadership_Seed%d.txt",s);
						fout=fopen(fileout,"a");							// file di analisi statistica

						if (fout!=NULL) 
							if (test_output_type==0)
								fprintf (fout, "\nGeneration %d : ",test_gen);
							else if (test_output_type==1)
								fprintf (fout, "\n");

						fclose (fout);
					}

					if ((test_measure_leadership_eco) /*&& (test_statistics) */) {
						sprintf(fileout,"Measure of Leadership_in_ecology_Seed%d.txt",s);
						fout=fopen(fileout,"a");							// file di analisi statistica

						if (fout!=NULL) 
							if (test_output_type==0)
								fprintf (fout, "\nGeneration %d : ",test_gen);
							else if (test_output_type==1)
								fprintf (fout, "\n");

						fclose (fout);
					}

					// Misura statistica n.6 (Misura di Leadership)
					if ((test_fitness_lab_all_gen) /*&& (test_statistics) && */) {
						/*if (ipar->lab_test_subst_number_seed==0)
							sprintf(fileout,"Fitness_SociallyEvolvedTogetherTested_S%d.fit",s);
						else
							sprintf(fileout,"Fitness_SociallyEvolvedTogetherTested_SubstSeed%d_S%d.fit",ipar->lab_test_subst_number_seed,s);
						
						fout=fopen(fileout,"a");							// file di analisi statistica
						*/
						if (fout!=NULL) 
							if (test_output_type==0)
								fprintf (fout, "\nGeneration %d : ",test_gen);
							else if (test_output_type==1)
								fprintf (fout, "\n");

						//fclose (fout);
					}

					// Misura statistica n.11 (test di esplorativit� degli individui tra pi� generazioni (media)) 
					if ((test_exploratory_all_gen) /* && (test_statistics) */) {
						//sprintf(fileout,"Measure of Exploration ability_Seed%d.txt", s);
						//fout=fopen(fileout,"a");							// file di analisi statistica

						//if (fout!=NULL) {
							if (test_output_type==0)
								fprintf (fout, "\nGeneration %d : ",test_gen);
							else if (test_output_type==1)
								fprintf (fout, "\n");

						fflush(fout);
						//	fclose (fout);
						//}
					}							

					// Misura statistica n.12 (test di velocit� per un predatore di mangiare una preda)
					if ((test_times_all_gen) /* && (test_statistics) */) {
						//sprintf(fileout,"Measure_of_Times_Predators_eat_a_PreyS%d.txt", s);
						//fout=fopen(fileout,"a");							// file di analisi statistica

						//if (fout!=NULL) {
							if (test_output_type==0)
								fprintf (fout, "\nGeneration %d : ",test_gen);
							else if (test_output_type==1)
								fprintf (fout, "\n");
						//}
						fflush(fout);
						//if (!ferror(fout))
						//	fclose (fout);
					}

					// Misura statistica n.16 (test sul baricentro del gruppo rispetto alla posizione di ciascun individuo
					if ((test_barycentre_distances_all_gen) /*&& (test_statistics) */) {
						if (ipar->motionlessind==0)
							sprintf(fileout,"Measure_of_Barycentre_of_group_respect_to_position_of_each_member_S%d.txt",s);
						else
							sprintf(fileout,"Measure_of_Barycentre_of_group_respect_to_position_of_each_member_S%d_ind%d_stopped.txt",s,ipar->motionlessind);
						//fout=fopen(fileout,"a");							// file di analisi statistica

						//if (fout!=NULL) {
							if (test_output_type==0)
								fprintf (fout, "\nGeneration %d : ",test_gen);
							else if (test_output_type==1)
								fprintf (fout, "\n");
							//fclose (fout);
							fflush (fout);
						//}		
					}

					// Misura statistica n.17
					if ((test_barycentre) /* && (test_statistics) */) {
						sprintf(fileout,"Measure_of_Barycentre_of_group_respect_to_position_of_each_member_one_gen_S%d_Gen%d.txt",s,test_gen);
						fout=fopen(fileout,"w");								// file di analisi statistica
						if (test_output_type==0) {
							fprintf (fout, "\t  Measure of Barycentre\n");	
							fprintf (fout, "\t----------------------------\n\n");
							fprintf (fout, "\t  Individual | Barycentre \n");
							fprintf (fout, "\t-----------------------------\n\n");
						}
						fclose (fout);
					}

					// Misura statistica n.18
					if ((test_barycentre_distances) /* && (test_statistics) */) {
						sprintf(fileout,"Measure_of_Distances_Individuals_Barycentre_of_group_S%d_Gen%d.txt",s,test_gen);
						fout=fopen(fileout,"w");								// file di analisi statistica
						if (test_output_type==0) {
							fprintf (fout, "\t  Measure of Distances between each Individual and Group's Barycentre\n");
							fprintf (fout, "\t------------------------------------------- \n\n");
							fprintf (fout, "\t  Individual | Distance from the Barycentre \n");
							fprintf (fout, "\t------------------------------------------- \n\n");
						}
						fclose (fout);
					}

					if ((test_fitness_lab)/*&& (test_statistics)*/) {
						sprintf(fileout,"Fitness_SociallyEvolvedTogetherTested_S%d_Gen%d.fit",s,gen);
						fout=fopen(fileout,"w");								// file di analisi statistica
						if (test_output_type==0) {
							fprintf (fout, "\t  Fitness \n");
							fprintf (fout, "\t---------------------------------\n\n");
						}
						fclose (fout);
					}

					// Loading genome and seed associated to the specified generation
					sprintf(filename,"G%dP0S%d.gen",gen,s);
					loadallg(-1, filename, 0, 0);	
					
					// Loading alternative genome specified by "lab_test_team2_number_seed" parameter in the configuration file
					if (ipar->lab_test_team2_number_seed > 0) {
						sprintf(filename,"G%dP0S%d.gen",ipar->lab_test_team2_number_gen,ipar->lab_test_team2_number_seed);
						loadallg(-1, filename, 1, 0);			// carica nel secondo genoma, quello ausiliario	
					}
	
					// Loading alternative genome specified by "lab_test_team2_number_seed" parameter in the configuration file
					if (ipar->lab_test_subst_number_seed > 0) {
						sprintf(filename,"G%dP0S%d.gen",ipar->lab_test_subst_number_gen,ipar->lab_test_subst_number_seed);
						loadallg(-1, filename, 1, 0);			// carica nel secondo genoma, quello ausiliario	
					}

					test_seed=s;					// Imposta il seed di test	
					
					if (!ipar->lab_test_team1_all_ind_race) {
						starttesttrial1=0;			// Testa una sola generazione
						endtesttrial1=1;
					} else {
						if (nraces>0) { 
							starttesttrial1=(ipar->lab_test_team1_all_ind_race-1)*(nindividuals/nraces);			// Testa un'intera specie
							endtesttrial1=(ipar->lab_test_team1_all_ind_race-1)*(nindividuals/nraces)+(nindividuals/nraces);
						}						
					}

					if (!ipar->lab_test_team2_all_ind_race) {
						starttesttrial2=0;			// Testa una sola generazione
						endtesttrial2=1;
					} else {
						if (nraces>0) { 
							starttesttrial2=(ipar->lab_test_team2_all_ind_race-1)*(nindividuals/nraces);			// Testa un'intera specie
							endtesttrial2=(ipar->lab_test_team2_all_ind_race-1)*(nindividuals/nraces)+(nindividuals/nraces);
						}						
					}
				
					if (ipar->lab_test_all_pairs) {
						if (nraces>0) { 
							starttesttrial2=0;
							endtesttrial2=n_couples;
						}						
					}

					// Misura statistica n.8 (test di esplorativit� degli individui) 
					if (((test_exploratory) || (test_exploratory_eco)) &&/*(test_statistics) && */(pffitness == ffitness_predator_prey2)) {
						sprintf(fileout,"Measure of Exploration ability_Gen%d_Seed%d.txt",test_gen, test_seed);
						fout=fopen(fileout,"w");							// file di analisi statistica
						if (fout!=NULL) {
							fprintf (fout, "\t  Measure of Exploratory Abilities \n");
							fprintf (fout, "\t---------------------------------\n\n");
							fprintf (fout, "\t  Individual | Exploration ability \n");
							fprintf (fout, "\t--------------------------------------------\n");
							fclose (fout);
						}
					}
					test_trial=0;
					if (!ipar->lab_test_all_ind) {
						for (testtrial1=starttesttrial1;testtrial1<endtesttrial1;testtrial1++) {					// Cicla su ogni test trial 
							// Misura statistica n.6 (Misura di Leadership)	
							if ((test_measure_leadership) && (!ipar->lab_test_all_pairs)/* && (test_statistics) */ ) {
								sprintf(fileout,"Measure of Leadership_Seed%d.txt",test_seed);	
								fout=fopen(fileout,"a");							// file di analisi statistica			

								if (fout!=NULL) 
									if (test_output_type==0)
										fprintf (fout, "\nIndividual %d : ",testtrial1);
									else if (test_output_type==1)
										fprintf (fout, "%d ",testtrial1);
							
								fclose (fout);
							}	

							if (ipar->lab_test_all_pairs) { 								
								team1=0;
								team2=0;
							}	
							for (testtrial2=starttesttrial2;testtrial2<endtesttrial2;testtrial2++) {					// Cicla su ogni test trial 
		
								// In base al ciclo alterna le coppie da confrontare							
								if (ipar->lab_test_all_pairs) { 								
									// Misura statistica n.6 (Misura di Leadership)	
									// Effettua il calcolo per l'individuo 0
									if ((testtrial2==0) && (test_measure_leadership) /* && (test_statistics) */ ) {
										sprintf(fileout,"Measure of Leadership_Seed%d.txt",test_seed);	
										fout=fopen(fileout,"a");							// file di analisi statistica			

										if (fout!=NULL) 
											if (test_output_type==0)
												fprintf (fout, "\nIndividual %d : ",team1);
											else if (test_output_type==1)
												fprintf (fout, "%d ",team1);
								
										fclose (fout);
									}

									// Misura statistica n.? (Misura della Distanza di Hamming)	
									// Effettua il calcolo per l'individuo 0
									if ((testtrial2==0) && (test_hamming_distance) /* && (test_statistics) */ ) {
										sprintf(fileout,"Measure_of_Hamming_Distance_Seed%d.txt",test_seed);	
										fout=fopen(fileout,"a");							// file di analisi statistica				

										if (fout!=NULL) 
											if (test_output_type==0)
												fprintf (fout, "\nGeneration %d : ",gen);
											else if (test_output_type==1)
												fprintf (fout, "\n");
								
										fclose (fout);
									}

									// Di seguito sono codificate tutta una serie di regole di accoppiamento
									if ((testtrial2>0) && (testtrial2%(ipar->lab_ntestingteam-1)==0)) {	// incrementa per multipli di 4 per esempio
										team1++;
										team2=0;
								
										// Misura statistica n.6 (Misura di Leadership)	
										if ((test_measure_leadership) /* && (test_statistics) */ ) {
											sprintf(fileout,"Measure of Leadership_Seed%d.txt",test_seed);	
											fout=fopen(fileout,"a");							// file di analisi statistica			

											if (fout!=NULL) 
												if (test_output_type==0)
													fprintf (fout, "\nIndividual %d : ",team1);
												else if (test_output_type==1)
													fprintf (fout, "\n%d ",team1);
							
											fclose (fout);
										}

										// Misura statistica n.? (Misura della Distanza di Hamming)	
										if ((test_hamming_distance) /* && (test_statistics) */ ) {
											sprintf(fileout,"Measure_of_Hamming_Distance_Seed%d.txt",test_seed);	
											fout=fopen(fileout,"a");							// file di analisi statistica				

											if (fout!=NULL) 
												if (test_output_type==0)
													fprintf (fout, "\nGeneration %d : ",gen);
							
											fclose (fout);
										}
									} else 
										team2++;
									
									if (team2==team1)
										team2++;
								}

								if ((ipar->lab_test_team1_all_ind_race) && (nraces>0))
									//team1=testtrial1+(ipar->lab_test_team1_all_ind_race-1)*(nindividuals/nraces);		// Considera un individuo della prima specie
									team1=testtrial1;		// Considera un individuo della prima specie

								if ((ipar->lab_test_team2_all_ind_race) && (nraces>0))	
									//team2=testtrial2+(ipar->lab_test_team2_all_ind_race-1)*(nindividuals/nraces);		// Considera un individuo della seconda specie
									team2=testtrial2;		// Considera un individuo della seconda specie

								if (!ipar->lab_test_all_pairs) { 								
									init_and_evaluate_some_individuals(s,gen, dindividual,0,team1,team2);				// Carica il genotipo di tutti gli individui e li valuta singolarmente nell'ambiente

								} else {
									if (!test_hamming_distance) {	
										old_lab_ntestingteam=ipar->lab_ntestingteam;
										ipar->lab_ntestingteam=2;
										init_and_evaluate_some_individuals(s,gen, dindividual,0,team1,team2);						// Carica il genotipo di tutti gli individui e li valuta singolarmente nell'ambiente
										ipar->lab_ntestingteam=old_lab_ntestingteam;
									} else if (test_hamming_distance==1) {	// per determinare la distanza di hamming � necessario attivare il test a coppie
	
										// Confronto fra genomi delle popolazioni distinte (differenti razze)
										if (nraces>1) {
											nindperrace=nindividuals/nraces;					// Number of individuals per race
											hd=0;												// Azzera il conteggio delle distanze di hamming tra tutti i geni	

											for (p = 0, gpi=genome; p < npopulations; p++)
											{ 
												for (i=0; i < (nindperrace + additional_ind); i++) 
												{
													gpi=genome+i+team1*nindperrace;				// Considera il genoma i-esimo della popolazione team1 (o razza)	
													gpi0=genome+i+team2*nindperrace;			// Considera il genoma i-esimo della popolazione team2 (o razza)	
		
													for (j=0,gi = *gpi, gi0 = *gpi0; j < ngenes; j++,gi++,gi0++)
														hd=hd+hamming_distance(*gi,*gi0);			// Aggiorna il conteggio delle distanze di hamming tra tutti i geni
												}	
											}
										}
									} else if (test_hamming_distance==2) {	// per determinare la distanza di hamming � necessario attivare il test a coppie
										// Confronto fra genomi degli individui best delle popolazioni (differenti razze)
										if (nraces>1) {
											nindperrace=nindividuals/nraces;					// Number of individuals per race
											hd=0;												// Azzera il conteggio delle distanze di hamming tra tutti i geni

											best_team1=search_bestind_num(seed,gen,team1,&type_file);			// Individua il numero del best della generazione gen di specie 0	
											best_team2=search_bestind_num(seed,gen,team2,&type_file);			// Individua il numero del best della generazione gen d	i specie 0	

											for (p = 0, gpi=genome; p < npopulations; p++)
											{ 
												// search_bestind_num restituisce gi� un indice assoluto all'interno di genome e quindi
												// non c'� bisogno di effettuare trasformazioni varie
												gpi=genome+best_team1;							// Considera il best genoma della popolazione team1 (o razza)	
												gpi0=genome+best_team2;							// Considera il best genoma della popolazione team2 (o razza)	

												for (j=0,gi = *gpi, gi0 = *gpi0; j < ngenes; j++,gi++,gi0++)
													hd=hd+hamming_distance(*gi,*gi0);			// Aggiorna il conteggio delle distanze di hamming tra tutti i geni
											}
										}
									}							
								}

								test_trial++;
	
								// Misura statistica n.11 (test di esplorativit� degli individui) 
								if ((test_exploratory_all_gen_eco) /* && (test_statistics) */) {
									fprintf (fout, "\nGeneration %d : ",gen);

									timeaverage=0;
									ntimes=0;
									// Calculate average with 0
								
									for(i=0,pind=ind; i<ipar->lab_ntestingteam;i++,pind++) {
										// Cicla su tutti gli individui per calcolare la visione
										pind->explorationability=pind->explorationability / amoebaepochs;
										fprintf (fout, "%d ", (int) pind->explorationability);									// Memorizza l'esplorativit� media sulle epoche
									}
								}

								// Misura statistica n.? (test di mobilit� degli individui) 
								if ((test_mobility_all_gen_eco) /* && (test_statistics) */) {
									fprintf (fout, "\nGeneration %d : ",gen);	

									timeaverage=0;
									ntimes=0;
									// Calculate average with 0
							
									for(i=0,pind=ind; i<ipar->lab_ntestingteam;i++,pind++) {
										// Cicla su tutti gli individui per calcolare la visione
										pind->mobility=pind->mobility / amoebaepochs;
										fprintf (fout, "%d ", (int) pind->mobility);											// Memorizza la mobilit� media sulle epoche
									}
								}

								// Misura statistica n.6 (Misura di Leadership)
								if ((test_measure_leadership) /* && (test_statistics) */ ) {
									sprintf(fileout,"Measure of Leadership_Seed%d.txt",test_seed);
									fout=fopen(fileout,"a");							// file di analisi statistica	

									if (fout!=NULL) 
										if ((ind->ncyclessawsomething >= 0) && (ipar->lab_test_all_pairs)) {	// caso test in ecologia
											ind->ncyclessawsomething = ind->ncyclessawsomething / amoebaepochs;	// media su tutte le epoche		
											if ((testtrial2+1)%(ipar->lab_ntestingteam-1)==0)						// per l'ultimo elemento non mette lo spazio finale
												fprintf (fout, "%d", (int) ind->ncyclessawsomething);			// Memorizza il numero di cicli in cui il primo individuo (ind) ha visto qualcosa
											else 	
												fprintf (fout, "%d ", (int) ind->ncyclessawsomething);			// Memorizza il numero di cicli in cui il primo individuo (ind) ha visto qualcosa
										}
										if (((ind->ncyclessawsomething >= 0) && (testtrial1!=testtrial2)) && (!ipar->lab_test_all_pairs)) {	// caso test di laboratorio
											ind->ncyclessawsomething = ind->ncyclessawsomething / amoebaepochs;	// media su tutte le epoche		
											if (testtrial2==starttesttrial2+(nindividuals/nraces)-1)				// per l'ultimo elemento non mette lo spazio finale
												fprintf (fout, "%d\n", (int) ind->ncyclessawsomething);			// Memorizza il numero di cicli in cui il primo individuo (ind) ha visto qualcosa
											else 	
												fprintf (fout, "%d ", (int) ind->ncyclessawsomething);			// Memorizza il numero di cicli in cui il primo individuo (ind) ha visto qualcosa
										}
									fclose (fout);
								}

								// Misura statistica n.? (Misura di Leadership in ecologia)
								if ((test_measure_leadership_eco) /* && (test_statistics) */ ) {
									sprintf(fileout,"Measure of Leadership_in_ecology_Seed%d.txt",test_seed);
									fout=fopen(fileout,"a");							// file di analisi statistica	
		
									if (fout!=NULL) 
										for(i=0,pind=ind; i<ipar->lab_ntestingteam;i++,pind++) {
											if (pind->ncyclessawsomething >= 0) {													// caso test in ecologia, non a coppie, tutti insieme
											// Cicla su tutti gli individui per calcolare la visione
												pind->ncyclessawsomething = pind->ncyclessawsomething / amoebaepochs;		        // media su tutte le epoche		
												fprintf (fout, "%d %d\n", pind->number, (int) pind->ncyclessawsomething);			// Memorizza il numero di cicli in cui ogni individuo ha visto qualcosa
											}
										}
									fclose (fout);
								}

								// Misura statistica n.? (Misura della Distanza di Hamming)
								if ((test_hamming_distance) /* && (test_statistics) */ ) {
									sprintf(fileout,"Measure_of_Hamming_Distance_Seed%d.txt",test_seed);
									fout=fopen(fileout,"a");							// file di analisi statistica		

									if (fout!=NULL) 
										if (testtrial2==endtesttrial2-1)				// per l'ultimo elemento non mette lo spazio finale
											fprintf (fout, "%d", hd);			// Memorizza la distanza di hamming della coppia attuale
										else 	
											fprintf (fout, "%d ", hd);		// Memorizza la distanza di hamming della coppia attuale
									fflush (fout);
									fclose (fout);
								}

								// Misura statistica n.8 (test di esplorativit� degli individui) 
								if ((test_exploratory) &&/*(test_statistics) && */(pffitness == ffitness_predator_prey2)) {
									sprintf(fileout,"Measure of Exploration ability_Gen%d_Seed%d.txt",test_gen,test_seed);
									fout=fopen(fileout,"a");							// file di analisi statistica				

									fprintf (fout, "\nIndividual %d : ",testtrial1);

									if (fout!=NULL) { 
										if (((ind+explorindnumber)->explorationability >= 0) && (((testtrial1!=testtrial2)&&(ipar->lab_ntestingteam>1))||(ipar->lab_ntestingteam<=1))) {														// viene testato solo l'individuo l'individuo explorindnumber
											(ind+explorindnumber)->explorationability = (ind+explorindnumber)->explorationability / amoebaepochs;	// media su tutte le epoche		
											fprintf (fout, "%d ", (int) (ind+explorindnumber)->explorationability);									// Memorizza l'esplorativit� media sulle epoche	
										}
										fclose (fout);
									}
								}

								// Misura statistica n.17
								if ((test_barycentre) /*&& (test_statistics) */) {
									sprintf(fileout,"Measure_of_Barycentre_of_group_respect_to_position_of_each_member_one_gen_S%d_Gen%d.txt",test_seed,test_gen);
									fout=fopen(fileout,"a");								// file di analisi statistica

									if (fout!=NULL) { 
										for (i=0;i<sweeps;i++) {
											fprintf (fout, "\n%d ", i);									// Memorizza le posizioni degli individui e i baricentri
											for (k=0; k<nteam_testing; k++) {
												if (barycentrearray[i][k*2]>0) {
													if (test_output_type==0) {
														fprintf (fout, "%d : ", (ind+k)->orig_number);			
														fprintf (fout, "%d,", barycentrearray[i][k*2]);			
														fprintf (fout, "%d - ", barycentrearray[i][k*2+1]);
													} else if (test_output_type==1) {
														fprintf (fout, "%d ", barycentrearray[i][k*2]);			
														fprintf (fout, "%d ", barycentrearray[i][k*2+1]);
													}
												}
											}																	
											if (barycentrearray[i][k*2]>0) {
												if (test_output_type==0) {
													fprintf (fout, "%d,", barycentrearray[i][k*2]);			
													fprintf (fout, "%d", barycentrearray[i][k*2+1]);
												} else  if (test_output_type==1) {
													fprintf (fout, "%d ", barycentrearray[i][k*2]);			
													fprintf (fout, "%d", barycentrearray[i][k*2+1]);
												}
											}
										}
									}
								
									fclose (fout);
								}

								// Misura statistica n.18 - n.19
								if ((test_barycentre_distances) /*&& (test_statistics) */) {
									sprintf(fileout,"Measure_of_Distances_Individuals_Barycentre_of_group_S%d_Gen%d.txt",test_seed,test_gen);
									fout=fopen(fileout,"a");								// file di analisi statistica

									if (fout!=NULL) { 
										for (i=0;i<sweeps;i++) {
											fprintf (fout, "\n%d : ", i);									// Memorizza le distanze tra gli individui e il baricentro del gruppo
											for (k=0; k<nteam_testing; k++) {
												if (barycentredistancesarray[i][k]>0) {
													if (test_output_type)
														fprintf (fout, "%d : ", (ind+k)->orig_number);			
												
													fprintf (fout, "%d \t", barycentredistancesarray[i][k]);			
												}
											}																	
											if (barycentredistancesarray[i][k]>0) {
												if (test_output_type)
													fprintf (fout, "%d : ", (ind+k)->orig_number);			
											
												fprintf (fout, "%d", barycentredistancesarray[i][k]);			
											}
										}
									}

									fclose (fout);
								}

								// Test della fitness per generazione
								if ((test_fitness_lab)/* && (test_statistics)*/) {
									sprintf(filename,"Fitness_SociallyEvolvedTogetherTested_S%d_Gen%d.fit",test_seed,test_gen);
									fp=fopen(filename, "a");
									for(team=0; team<nteam_testing; team++) 
										if (test_output_type==0)
											fprintf(fp,"Ind : %d Fitness : %.3f Virtual Fitness : %.3f\n",(ind+team)->orig_number, (ind+team)->fitness*sweeps,((ind+team)->virtualfitness)/amoebaepochs);		   
										else if (test_output_type==1)
											fprintf(fp,"%d %.3f %.3f\n",(ind+team)->orig_number, (ind+team)->fitness*sweeps,((ind+team)->virtualfitness)/amoebaepochs);			
	
									fflush(fp);
									fclose(fp);	
								}

								// Misura statistica n.11 (test di esplorativit� degli individui) 
								if ((test_exploratory_all_gen) /*&& (test_statistics) */) {
									//sprintf(fileout,"Measure of Exploration ability_Seed%d.txt",test_seed);
									
									//fout=fopen(fileout,"a");							// file di analisi statistica	
	
									//if (fout!=NULL) {
										if (((ind+explorindnumber)->explorationability >= 0) && (((testtrial1!=testtrial2)&&(ipar->lab_ntestingteam>1))||(ipar->lab_ntestingteam<=1))) {													// viene testato solo l'individuo l'individuo explorindnumber
											(ind+explorindnumber)->explorationability = (ind+explorindnumber)->explorationability / amoebaepochs;	// media su tutte le epoche		
											fprintf (fout, "%d ", (int) (ind+explorindnumber)->explorationability);									// Memorizza l'esplorativit� media sulle epoche
										}
										//fclose (fout);
									//}
									//fflush(fp);
								}
					
								// Misura statistica n.12 (test di velocit� per un predatore di mangiare una preda)
								if ((test_times_all_gen) /* && (test_statistics) */) {
									// calculates success percentade of individual 1 and 2
									//success_rate=(success_counter*100)/amoebaepochs;	

									//sprintf(fileout,"Measure_of_Times_Predators_eat_a_PreyS%d.txt",test_seed);
									//fout=fopen(fileout,"a");							// file di analisi statistica		

									//if (fout!=NULL) {
										timeaverage=0;
										ntimes=0;
										// Calculate average with 0
										for (k=0;k<amoebaepochs;k++) {
											if (*(successruntime+k)==0)
												*(successruntime+k)=sweeps;			// Gli 0 li imposta al numero massimo dei cicli

											ntimes++;				// nel calcolo del tempo medio gli 0 li considera pari al tempo di vita

											timeaverage+=(float) *(successruntime+k);	
										}
										timeaverage=timeaverage/ntimes;

										fprintf (fout, "%d ",(int) timeaverage);
										//fprintf (fout, "%d\n",success_rate);
									//}
									fflush(fout);
									/*if (!ferror(fout))
										fclose (fout);*/
								}
							}
					
							// Misura statistica n.7 (Misura di Fitness in fase di test)
							if ((test_times) &&/*(test_statistics) && */((pffitness == ffitness_predator_prey2))) {
								// calculates success percentade of individual 1 and 2
								success_rate=(success_counter*100)/amoebaepochs;

								sprintf(fileout,"Statistics_Time_All_Predators_BestPreyS%d_lastgen.txt",test_seed);
								fout=fopen(fileout,"a");							// file di analisi statistica

								if (fout!=NULL) {
									fprintf (fout, "Individual %d : ",testtrial1);
									timeaverage=0;
									ntimes=0;
									// Calculate average with 0
									for (k=0;k<amoebaepochs;k++) {
										if (*(successruntime+k)==0)
											*(successruntime+k)=sweeps;			// Gli 0 li imposta al numero massimo dei cicli

										ntimes++;				// nel calcolo del tempo medio gli 0 li considera pari al tempo di	vita

										timeaverage+=(float) *(successruntime+k);
									}
									timeaverage=timeaverage/ntimes;

									// Calculate standard deviation with 0
									stdev=0;
									for (k=0;k<amoebaepochs;k++) 
										stdev+=(float) (*(successruntime+k)-timeaverage)*(*(successruntime+k)-timeaverage)/ntimes;

									stdev=sqrt(stdev);		// deviazione standard
	
									fprintf (fout, "%d ",(int) timeaverage);
									fprintf (fout, "%d ",(int) stdev);
									fprintf (fout, "%d\n",success_rate);

									fclose (fout);
								}
							}
	
							// Misura statistica n.8 (Misura di Fitness in fase di test, 2 predatori (1 leader) ed una preda)
							if ((test_times) &&/*(test_statistics) && */((pffitness == ffitness_predator_prey2)) && (testtrial1!=testtrial2) && (testtrial1!=ipar->lab_test_team3) && (testtrial2!=ipar->lab_test_team3)) {
								// calculates success percentade of individual 1 and 2
								success_rate=(first_success_counter*100)/amoebaepochs;
								success_rate_leader=(second_success_counter*100)/amoebaepochs;	

								sprintf(fileout,"Statistics_Time_All_Predators_One_Leader_BestPreyS%d_lastgen.txt",test_seed);
								fout=fopen(fileout,"a");							// file di analisi statistica	

								if (fout!=NULL) {
									fprintf (fout, "Individual Leader %d : ",ipar->lab_test_team3);
									timeaverage=0;
									ntimes=0;
									// Calculate average with 0
									for (k=0;k<amoebaepochs;k++) {
										if (*(successsecondsideruntime+k)==0)
											*(successsecondsideruntime+k)=sweeps;			// Gli 0 li imposta al numero massimo dei cicli	
	
										ntimes++;				// nel calcolo del tempo medio gli 0 li considera pari al tempo di vita

										timeaverage+=(float) *(successsecondsideruntime+k);	
									}
									timeaverage=timeaverage/ntimes;

									// Calculate standard deviation with 0
									stdev=0;
									for (k=0;k<amoebaepochs;k++) 
										stdev+=(float) (*(successsecondsideruntime+k)-timeaverage)*(*(successsecondsideruntime+k)-timeaverage)/ntimes;	

									stdev=sqrt(stdev);		// deviazione standard
	
									fprintf (fout, "%d ",(int) timeaverage);
									fprintf (fout, "%d ",(int) stdev);
									fprintf (fout, "%d\n",success_rate_leader);	

									fprintf (fout, "Individual Follower %d : ",testtrial1);
									timeaverage=0;
									ntimes=0;
									// Calculate average with 0
									for (k=0;k<amoebaepochs;k++) {
										if (*(successfirstsideruntime+k)==0)
											*(successfirstsideruntime+k)=sweeps;			// Gli 0 li imposta al numero massimo dei cicli

										ntimes++;				// nel calcolo del tempo medio gli 0 li considera pari al tempo di vita

										timeaverage+=(float) *(successfirstsideruntime+k);
									}
									timeaverage=timeaverage/ntimes;

									// Calculate standard deviation with 0
									stdev=0;
									for (k=0;k<amoebaepochs;k++) 
										stdev+=(float) (*(successfirstsideruntime+k)-timeaverage)*(*(successfirstsideruntime+k)-timeaverage)/ntimes;

									stdev=sqrt(stdev);		// deviazione standard	
	
									fprintf (fout, "%d ",(int) timeaverage);
									fprintf (fout, "%d ",(int) stdev);
									fprintf (fout, "%d\n",success_rate);	

									fclose (fout);
								}
							}
						}

						// Test della fitness per generazione
						if ((test_fitness_lab_all_gen)/* && (test_statistics)*/) {
							/*if (ipar->lab_test_subst_number_seed==0)
								sprintf(fileout,"Fitness_SociallyEvolvedTogetherTested_S%d.fit",s);
							else
								sprintf(fileout,"Fitness_SociallyEvolvedTogetherTested_SubstSeed%d_S%d.fit",ipar->lab_test_subst_number_seed,s);
							fout=fopen(fileout, "a");
							*/						
							//if (fout!=NULL) { 
								if (test_output_type==0) {
									for(team=0; team<nteam_testing; team++) 
										fprintf(fout,"%d : %.3f %.3f\t",(ind+team)->orig_number,(ind+team)->fitness*sweeps,((ind+team)->virtualfitness)/amoebaepochs);		   
								} else if (test_output_type==1) {
									for(team=0; team<nteam_testing-1; team++) 
										fprintf(fout,"%.3f %.3f ",(ind+team)->fitness*sweeps,((ind+team)->virtualfitness)/amoebaepochs);		   
								}
								if (test_output_type==1) 
									fprintf(fout,"%.3f %.3f",(ind+team)->fitness*sweeps,((ind+team)->virtualfitness)/amoebaepochs);		   
							//}
							fflush(fout);
							//fclose(fout);	
						}
				
						// Misura statistica n.
						// Test delle distanze degli individui dalle food zone
						if ((test_distance_from_fz) && (envnobjs[GROUND] > 0)) {
							sprintf(fileout,"Measure_of_Distance_of_each_individual_from_each_food_zone_S%d.txt",test_seed);
							fp=fopen(fileout, "a");
							
							for (k=0; k<nteam_testing-1; k++) 
								for(j=0; j<envnobjs[GROUND]; j++)				// tutte le food zone
									fprintf(fp,"%d ",distances_from_fz_array[k][j]);

							for(j=0; j<envnobjs[GROUND]-1; j++)					// tutte le food zone
								fprintf(fp,"%d ",distances_from_fz_array[k][j]);

							fprintf(fp,"%d",distances_from_fz_array[k][j]);
							fflush(fp);
							fclose(fp);	
						}
					} else {
						testpopulation_is_running = 1;
						testindividual_is_running = 0;
						n_fixed_evolution_is_running = 0;
						n_fixed_evolution_social_is_running = 0;
						n_variable_evolution_is_running = 0;
						
						if (dindividual>0) // se ci sono individui caricati 	
							nteam=dindividual+1;						// Il numero di infividui del team � pari al numero di individui letti dal .gen
						else
							nteam=(nindividuals + additional_ind) * npopulations;	// Imposta la numerosit� del team 
																					// uguale a quella del genoma
						init_robot(0);											// Rialloca spazio per il team esteso di individui
						init_network(0);										// Rialloca spazio per il team esteso di reti neurali di controllo
		
						if (test_seed>1) {										// solo dal secondo ciclo in poi 
							nteam_tmp=nteam;	
							nteam=1;
							(*pinit_robot_pos)();									// Inizializza la posizione di ciascuno dei robot
							nteam=nteam_tmp;
						}

						init_and_evaluate_nf(gen, dindividual,0);					// Carica il genotipo di tutta la popolazione in individui differenti

						// Misura statistica n.11 (test di esplorativit� degli individui) 
						if ((test_exploratory_all_gen_eco) /* && (test_statistics) */) {
							//sprintf(fileout,"Measure of Exploration ability_S%d.txt",test_seed);
							//fout=fopen(fileout,"a");							// file di analisi statistica	
							fprintf (fout, "\nGeneration %d : ",gen);	

							//if (fout!=NULL) {
								timeaverage=0;
								ntimes=0;
								// Calculate average with 0
							
								for(race=0;race<nraces;race++) {	
									for(team=0; team<nteam/nraces; team++) {
										ni=team+race*(nindividuals/nraces);
										if ((ind+ni)->race==test_nrace-1) {
											(ind+ni)->explorationability=(ind+ni)->explorationability / amoebaepochs;
											fprintf (fout, "%d ", (int) (ind+ni)->explorationability);									// Memorizza l'esplorativit� media sulle epoche
										}
									}
								}	

								//fclose (fout);
							//}
						}

						// Misura statistica n.11 (test di esplorativit� degli individui) 
						if ((test_exploratory_eco) &&/*(test_statistics) && */(pffitness == ffitness_predator_prey2)) {
							//sprintf(fileout,"Measure of Exploration ability_Gen%d_Seed%d.txt",test_gen,test_seed);
							//fout=fopen(fileout,"a");							// file di analisi statistica		

							//if (fout!=NULL) {
								timeaverage=0;
								ntimes=0;
								// Calculate average with 0
							
								for(race=0;race<nraces;race++) {	
									for(team=0; team<nteam/nraces; team++) {
										ni=team+race*(nindividuals/nraces);
										if ((ind+ni)->race==test_nrace-1) {
											fprintf (fout, "\nIndividual %d : ",ni);
											(ind+ni)->explorationability=(ind+ni)->explorationability / amoebaepochs;
											fprintf (fout, "%d", (int) (ind+ni)->explorationability);									// Memorizza l'esplorativit� media sulle epoche
										}
									}
								}	

								//fclose (fout);
							//}
						}

						// Funziona solo in ecologia, ossia con tutti gli individui
						if ((test_times_all_gen_eco) /*&&(test_statistics) */) {
							// calculates success percentade of individual 1 and 2
							//sprintf(fileout,"Measure_of_Times_Predators_eat_some_Preies_S%d.txt",test_seed);
							//fout=fopen(fileout,"a");							// file di analisi statistica	
	
							//if (fout!=NULL) {
								timeaverage=0;
								ntimes=0;
								// Calculate average with 0
								for (k=0;k<amoebaepochs;k++) {
									if (*(successruntime+k)==0)
										*(successruntime+k)=sweeps;			// Gli 0 li imposta al numero massimo dei cicli		

									ntimes++;				// nel calcolo del tempo medio gli 0 li considera pari al tempo di vita		

									timeaverage+=(float) *(successruntime+k);
								}
								timeaverage=timeaverage/ntimes;	

								fprintf (fout, "Generation %d : %d \n",gen, (int) timeaverage);
							//}
							fflush(fout);
							//if (!ferror(fout))
							//	fclose (fout);
						}

						if ((test_fitness_lab_all_gen)/* && (test_statistics)*/) {
							/*if (ipar->lab_test_subst_number_seed==0)
								sprintf(fileout,"Fitness_SociallyEvolvedTogetherTested_S%d.fit",s);
							else
								sprintf(fileout,"Fitness_SociallyEvolvedTogetherTested_SubstSeed%d_S%d.fit",ipar->lab_test_subst_number_seed,s);
							fout=fopen(fileout, "a");
							*/
							//if (fout!=NULL) 
								for(race=0;race<nraces;race++) {	
									for(team=0; team<nteam/nraces; team++) 
										if (test_output_type==0)
											fprintf(fout,"%d : %.5f ",team+race*(nindividuals/nraces),tfitness[0][race][team+nteam/nraces*race]);		   
										else if (test_output_type==1)	
											fprintf(fout,"%d %.5f ",team+race*(nindividuals/nraces),tfitness[0][race][team+nteam/nraces*race]);		   
								}
							fflush(fout);
							//fclose(fout);	
						}

						if ((test_fitness_lab)/* && (test_statistics)*/) {
							sprintf(filename,"Fitness_SociallyEvolvedTogetherTested_S%d.fit",test_seed);
							fp=fopen(filename, "w");
							for(race=0;race<nraces;race++) {	
								for(team=0; team<nteam/nraces; team++) 
									if (test_output_type==0)
										fprintf(fp,"Ind : %d Race : %d Fitness : %.f \n",team+race*(nindividuals/nraces),race,tfitness[0][race][team+nteam/nraces*race]);		   
									else if (test_output_type==1)	
										fprintf(fp,"%d %d %.f \n",team+race*(nindividuals/nraces),race,tfitness[0][race][team+nteam/nraces*race]);		   
							}
							fflush(fp);
							fclose(fp);	
						}	
					}

					// Misura statistica n.5
					// saving of number of success task and times, only with some fitnesses
					if ((test_times) && (test_statistics) && ((pffitness == ffitness_predator_prey2))) {
						// calculates success percentade of individual 1 and 2
						success_rate=(success_counter*100)/amoebaepochs;	

						sprintf(fileout,"Statistics_Time_Predator_PreyS%d.txt",test_seed);
						fout=fopen(fileout,"a");							// file di analisi statistica

						if (fout!=NULL) {
							fprintf (fout, "Generation %d : ",gen);
							/*timeaverage=0;
							ntimes=0;
							// Calculate average without 0
							for (k=0;k<amoebaepochs;k++) {
								fprintf (fout, "%d ",*(successruntime+k));	// Visualizza ogni singolo tempo per ogni epoca	
								if (*(successruntime+k)!=0) 
									ntimes++;				// nel calcolo del tempo medio elimina gli 0
								timeaverage+=(float) *(successruntime+k);
							}
							timeaverage=timeaverage/ntimes;

							// Calculate standard deviation without 0
							stdev=0;
							for (k=0;k<amoebaepochs;k++) 
								if (*(successruntime+k)!=0)	// elimina gli 0
									stdev+=(float) (*(successruntime+k)-timeaverage)*(*(successruntime+k)-timeaverage)/ntimes;
							stdev=sqrt(stdev);

							fprintf (fout, "%.2f ",timeaverage);
							fprintf (fout, "%.2f ",stdev);
							*/
							timeaverage=0;
							ntimes=0;
							// Calculate average with 0
							for (k=0;k<amoebaepochs;k++) {
								if (*(successruntime+k)==0)
									*(successruntime+k)=sweeps;			// Gli 0 li imposta al numero massimo dei cicli	

								//fprintf (fout, "%d ",*(successruntime+k));	// Visualizza ogni singolo tempo per ogni epoca
								ntimes++;				// nel calcolo del tempo medio gli 0 li considera pari al tempo di vita	

								timeaverage+=(float) *(successruntime+k);
							}
							timeaverage=timeaverage/ntimes;

							// Calculate standard deviation with 0
							stdev=0;
							for (k=0;k<amoebaepochs;k++) 
								stdev+=(float) (*(successruntime+k)-timeaverage)*(*(successruntime+k)-timeaverage)/ntimes;

							stdev=sqrt(stdev);		// deviazione standard

							fprintf (fout, "%.2f ",timeaverage);
							fprintf (fout, "%.2f ",stdev);
							fprintf (fout, "%d\n",success_rate);

							fclose (fout);
						}
					}

					// Misura statistica n.19
					if (test_barycentre_distances_all_gen) {
						if (ipar->motionlessind==0)
							sprintf(fileout,"Measure_of_Barycentre_of_group_respect_to_position_of_each_member_S%d.txt",test_seed);
						else
							sprintf(fileout,"Measure_of_Barycentre_of_group_respect_to_position_of_each_member_S%d_ind%d_stopped.txt",test_seed,ipar->motionlessind);

						//fout=fopen(fileout,"a");							// file di analisi statistica

						//if (fout!=NULL) {
							for (k=0; k<nteam_testing; k++) {
								if (barycentredistancesarray[0][k]>0) {									// Considera solo gli individui del team (non quelli con distanze negative)  
									aveoversweeps=0;		
									for (i=0;i<sweeps;i++) 
										aveoversweeps+=barycentredistancesarray[i][k];					// Somma le distanze dal baricentro per l'individuo k
		
									aveoversweeps/=sweeps;												// Media sul numero di cicli totale												
									if (test_output_type==0) { 
										fprintf (fout, "%d : ", (ind+k)->orig_number);						// Salva numero di individuo
										fprintf (fout, "%d \t", aveoversweeps);								// Salva media sui cicli 
									} else if ((test_output_type==1) && (k<nteam_testing-1)) {
										fprintf (fout, "%d ", aveoversweeps);								// Salva media sui cicli 
									}						
								}	
							}
							if (test_output_type==1) {
								fprintf (fout, "%d", aveoversweeps);								// Salva media sui cicli 
							}						
							//fclose (fout);
							fflush (fout);
						//}
					}

					/*if (gen==0)
						gen=-1;							// Correzione necessaria per poter coprire in ogni ciclo, l'ultima
														// generazione che altimenti non viene mai coperta. Questo perch� 
														// la generazione 0 � compresa nel ciclo.
					*/

					if (gen==startgen)
						gen-=1;							// Correzione necessaria per poter coprire in ogni ciclo, l'ultima
														// generazione che altimenti non viene mai coperta. Questo perch� 
														// la generazione 0 � compresa nel ciclo.
				}	// end generations cycle
		
				// Chiusura di tutti i files temporaneamente aperti
				// Misura statistica n.12 (test di velocit� per un predatore di mangiare una preda)
				if ((test_times_all_gen)||(test_times_all_gen_eco)||(test_exploratory_all_gen)||
					(test_exploratory_all_gen_eco)||(test_mobility_all_gen_eco)||(test_fitness_lab_all_gen) /*&& (test_statistics) */) {
					fflush(fout);
					if (!ferror(fout))
						fclose (fout);
				}

			}	// end seed 
		}	// end subst
		
		testpopulation_is_running = 0;								// Resetta il parametro precedentemente impostato a 1
		labtest_is_running = 0;										// Resetta il parametro precedentemente impostato a 1
		userbreak = 0;	 
		break;
  }
}



/*
 * randomize the genomes of the entire population
 */
void
randomize_pop()

{

	int i;
	int j;
	int p;
	int **gpi, **gpi0;		// genoma dell'intera popolazione p
	int	*gi,*gi0;			// gene dell'individuo i
	int nindperrace;		// numero di individui per razza
	int race;

	if (!same_initial_genotype) {
		for (p = 0, gpi=genome; p < npopulations; p++)
		{ 
			for (i=0; i < (nindividuals + additional_ind); i++,gpi++) 
			{
				for (j=0,gi = *gpi; j < ngenes; j++,gi++)
				{
					*gi = mrand(256);
			    }
			}
		}
	} else {
		nindperrace=nindividuals/nraces;					// Number of individuals per race

		for (p = 0, gpi=genome; p < npopulations; p++)
		{ 
			for(race=0;race < nraces;race++) {		
				for (i=race*nindperrace; i < (nindperrace + additional_ind)+race*nindperrace; i++) 
				{
					gpi=genome+i;							// Genoma dell'i-esimo individuo
					gpi0=genome+i-race*nindperrace;			// Considera il genoma della prima razza

					if (race==0) {							// Per la prima razza genera tutti numeri casuali
						for (j=0,gi = *gpi; j < ngenes; j++,gi++)
						{
							*gi = mrand(256);
					    }
					} else if (race>0) {					// Le altre razze sono identiche alla prima ma prendono corsi evolutivi differenti
						for (j=0,gi = *gpi, gi0 = *gpi0; j < ngenes; j++,gi++,gi0++)
						{
							*gi = *gi0;						// Per le altre razze copia il genotipo dalla prima in maniera tale che siano identici
					    }
					}
				}
			}
		}
	}
}


/*
 * run n replications of the n-fixed evolutionary
 */
void run_n_fixed_evolution(int social)
{
	int    replication;

	//	razza dell'individuo
	//	race of the individual
	int	   race;			

	int    g,gg;
	int    ng;
	int    oldseed;
	FILE   *fp;
	char   sbuffer[128];

	//	a non-zero parameter passed in will result in a 
	//	social evolution
	if (social)
	{
		// social evolution
		n_fixed_evolution_social_is_running=1;	
	
		n_fixed_evolution_is_running=0;
		
		// Imposta la numerosit� del team uguale a quella del genoma
		// Set the number of the team equal to that of the genome
		nteam=(nindividuals + additional_ind) * npopulations;	
		
		//	Rialloca spazio per il team esteso di individui
		//	Reallocates space for the extended team of individuals
		init_robot(0);							
		
		//	Rialloca spazio per il team esteso di reti neurali di controllo
		//	Reallocates space for the extended team neural network control
		init_network(0);						
	} 
	else 
	{ 
		n_fixed_evolution_social_is_running=0;
		
		// individual evolution
		n_fixed_evolution_is_running=1;			
	
		if (nraces>1) 
		{ 
			nteam=nraces;
			
			//	Reinizializza tutto per il disegno in caso di razze multiple
			//	Reset all for drawing in the case of multi-spoke
			init_robot(0);	

			//	Rialloca spazio per il team esteso di reti neurali di controllo
			//	Reallocates space for the extended team neural network control
			init_network(0);					
		}
	}
	n_variable_evolution_is_running=0;
	testindividual_is_running=0;
	testpopulation_is_running=0;

	oldseed = seed;

	//	cicla sulle repliche
	//	cycle through the replications
	for (replication=0; replication < nreplications; replication++)			
	{
		//	Imposta il seme dei numeri casuali al seed scelto
		//	Sets the random number seed to seed chosen
		set_seed(seed);						    

		//	Randomizza la popolazione iniziale indipendentemente dalle razze 
		//	Randomize the initial population regardless of race
		randomize_pop();						

		sprintf(sbuffer,"statS%d.fit",seed);	
		
		//	Questo permette di creare dinamicamente il file 
		//	da leggere in un determinato istante
		//	This allows you to dynamically create the file to read in 
		//	a given moment
		if ((fp=fopen(sbuffer, "r")) == NULL) 
		{
			n_fixed_evolution();
		}
		else
		{
			fclose(fp);
			ng = loadstatistics(sbuffer, 1);
			if (ng < generations)
			{
				startgeneration = ng;
				loadallg(startgeneration - 1, "nofile", 0, 0);
				n_fixed_evolution();
				startgeneration = 0;
			}
		}
		if (userbreak == 1)
		{
			n_fixed_evolution_is_running = 0;
			n_fixed_evolution_social_is_running = 0;
			testindividual_is_running = 0;
			testpopulation_is_running = 0;
			n_variable_evolution_is_running = 0;
			break;
		}
		else
		{
			seed++;
		}
	}	// end replications for 
    
	if (userbreak == 0 && nreplications > 1)
	{
		if ((fp=fopen("stat.fit", "w")) == NULL) 
	    {
			display_error("cannot write stat.fit file");
	    }	    
		for(g=0;g<generations;g++)
	    {
			for(gg=0;gg < (2*npopulations);gg++)
				fprintf(fp,"%.3f ",astatfit[g][gg] / (double) nreplications);		
			fprintf(fp,"\n");				  
	    } 
		fclose(fp);
	}
    seed = oldseed;
    userbreak = 0;
    n_fixed_evolution_is_running = 0;
	n_fixed_evolution_social_is_running=0;
	n_variable_evolution_is_running = 0;      
	testindividual_is_running = 0;
	testpopulation_is_running = 0;
}

/*
 * run n replications of the artificial life evolutionary process
 */
void run_n_variable_evolution()
{
	int    replication;
	int    g,gg;
	int    ng;
	int    oldseed;
	FILE   *fp;
	char   sbuffer[128];

    n_fixed_evolution_is_running = 0;
	n_fixed_evolution_social_is_running=0;
	n_variable_evolution_is_running = 1;      
	testindividual_is_running = 0;
	testpopulation_is_running = 0;

	//	Richiama l'inizializzazione delle strutture di evoluzione 
	//	per riallocare opportunamente le variabili nella memoria
	//	Recall the structure initialization of evolution to appropriately 
	//	reallocate the variables in memory
	init_evolution();				

	//	Imposta il seme dei numeri casuali al seed scelto
	//	Sets the random number seed to seed chosen
	set_seed(seed);				    

	//	Randomizza la popolazione iniziale
	//	Randomize the initial population
	randomize_pop();				 

	//	Avvia l'evoluzione vera e propria con popolazione n-variabile
	//	Starts the real evolution with population n-variable
	n_variable_evolution();					

	/*
	oldseed = seed;
	for (replication=0; replication < nreplications; replication++)
	{
		//	Imposta il seme dei numeri casuali al seed scelto 
		//	Sets the random number seed to seed chosen
		set_seed(seed);							    

		//	Randomizza la popolazione iniziale 
		//	Randomize the initial population
		randomize_pop();							

		sprintf(sbuffer,"statS%d.fit",seed);		
		
		//	Questo permette di creare dinamicamente il file 
		//	da leggere in un determinato istante
		//	This allows you to dynamically create the file
		//	to be read at a given time
		if ((fp=fopen(sbuffer, "r")) == NULL) 
		{
			n_fixed_evolution();
		}
		else
		{
			fclose(fp);
			ng = loadstatistics(sbuffer, 1);
			if (ng < generations)
			{
				startgeneration = ng;
				loadallg(startgeneration - 1, "nofile", 0, 0);
				n_fixed_evolution();						
				
				//	Avvia l'evoluzione vera e propria
				//	Start the real evolution
				startgeneration = 0;
			}
	    }
		if (userbreak == 1)
	    {
			n_fixed_evolution_is_running = 0;
			n_fixed_evolution_social_is_running = 0;
			n_variable_evolution_is_running = 0;
			testindividual_is_running = 0;
			testpopulation_is_running = 0;
			break;
	    }
	    else
	    {
			seed++;
	    }
	}
    if (userbreak == 0 && nreplications > 1)
	{
		if ((fp=fopen("stat.fit", "w")) == NULL) 
	    {
			display_error("cannot write stat.fit file");
	    }	    
		for(g=0;g<generations;g++)
	    {
			for(gg=0;gg < (2*npopulations);gg++)
	        fprintf(fp,"%.3f ",astatfit[g][gg] / (double) nreplications);		
			fprintf(fp,"\n");				  
	    } 
		fclose(fp);
	}
    seed = oldseed;
	*/
    userbreak = 0;
    n_fixed_evolution_is_running = 0;
	n_fixed_evolution_social_is_running=0;
	n_variable_evolution_is_running = 0;      
	testindividual_is_running = 0;
	testpopulation_is_running = 0;
}

/*
 * receive a weights as an integer between 0 and 255
 * and return a floating point value within a given range
 * if the range is negative randomly generate the value
 * (255+1 in order to have 128=0.0)
 */

float
getbits(int v, float range)
{
	
	float value;
	
	if (range < 0.0)
	{
		value = rans(0.0 - range);
		return(value);
	}
	else
	{
		// this is for compatibility with old weights file (which include also negative values)
		if (v < 0)
			v = 256 + v;
		
		value = (float) (v + 1) / (float) 256.0;
		value = range - (value * (range * (float) 2.0));
		
		return(value);
	}

}


/*
 * set the free parameter of individual n, team t
 */
void
getgenome(int n, int t, int first_or_second_genome)

{
	int		j;
	int		*gi;
	int		fixedi;
	float	*w;
	float   range;
	int     numc;

	if (loadfixedgenotypes == 1 && *(loadfixedgn + t) == 1)
    {
		gi = *(fixedgenome + t);
	}
	else
    {
	    if (!first_or_second_genome) 
			gi = *(genome + n);			// *(genome+n) � l'elenco dei geni dell'(n+1)-esimo individuo
										// (*(*genome+n)+67) � il 68� gene dell'(n+1)-esimo individuo 
		else 
			gi = *(secondgenome + n);	// attinge dal secondo genome ausiliario
	}

    genotype_to_phenotype(t, gi, 8, homogeneous);	// estrae il genotipo e lo traduce e inserisce
													// nella rete neurale relativa
}


/*
 * save the net n of population p on the fp file 
 */ 
void
saveagenotype(FILE *fp, int n, int p)

{
    int	j;
    int *gi;

    gi = *(genome + (n + (p * (nindividuals + additional_ind))));
     
    fprintf(fp, "DYNAMICAL NN\n");
    for (j=0; j < ngenes; j++, gi++) 
      fprintf(fp, "%d\n", *gi);
    fprintf(fp, "END\n");
}



/*
 * load the genome of individual n of population p from the fp file
 */ 
void
loadagenotype(FILE *fp, int n, int p, int first_or_second_genome)
{

    int	*gi;
    int	j;

	if (!first_or_second_genome)
		gi = *(genome + (n + (p * (nindividuals + additional_ind))));		// carica il genoma ordinario e principale
	else
		gi = *(secondgenome + (n + (p * (nindividuals + additional_ind))));	// carica un genoma secondario aggiuntivo per alcuni test di laboratorio

    fscanf(fp, "DYNAMICAL NN\n");
    for (j=0; j < ngenes; j++, gi++)
	{
	  fscanf(fp, "%d\n", gi);
	}
    fscanf(fp, "END\n");
}





/*
 * save the population genome
 */ 
void
saveallg(int gen)

{
	FILE	*fp;
    char    filename[64];
    char    errorm[64];	
    int     n;
	int     p;
	int		race, nindperrace;

	for(p=0;p<npopulations;p++)
	{
 		sprintf(filename,"G%dP%dS%d.gen",gen,p,seed);
		if ((fp=fopen(filename, "w+")) == NULL) 
		{
			sprintf(errorm,"cannot open %s file",filename);
			display_error(errorm);
		}
		nindperrace=nindividuals/nraces;
		for (race=0;race<nraces;race++) {
			for (n=0+race*nindperrace;n<nindperrace+race*nindperrace;n++)	   
			{
				if (nraces > 1) {
					fprintf(fp,"**NET : %d_%d_%d_%d.wts\n",gen,p,n,race);		
					saveagenotype(fp,n,p);
				} else {
					fprintf(fp,"**NET : %d_%d_%d.wts\n",gen,p,n);		
					saveagenotype(fp,n,p);
				}
			}
		}	// end race
		fflush(fp);	       
		fclose(fp);
	}
}

/*
* Determina il numero del best al seed specificato di generazione gen e di specie race
*/
int search_bestind_num(int seed, int gen, int race, int *type) 
{
	FILE	*fp;
	char    filename[64];
	char    message[128];
	char    g[10]="",b[10]="",r[10]="";
	int		ig=0,ib=0,ir=0;
	char    flag[64];
	int     i,k;
	int		type_file=0;

	// Check in apertura file dei best
	sprintf(filename,"B1P0S%d.gen",seed);								// Prova a vedere se ci sono file di formato BxPxSx.gen
	if ((fp=fopen(filename, "r")) == NULL) {							// se non esistono prova l'altro formato BxPxSxRx.gen, dove � specificata la specie o razza
		sprintf(filename,"B1P0S%dR%d.gen",seed,race);
		if ((fp=fopen(filename, "r")) != NULL)							// se non esistono neanche quelli in formato BxPxSxRx.gen, allora segnala errore
			type_file=2;												// Ok pu� procedere, tipo di file dei best con razze separate in file diversi
	} else type_file=1;													// Ok pu� procedere, tipo di file dei best con razze mescolate dentro
	*type=type_file;													// Restituisce il tipo di file trovato
	if (type_file)	{
		do {
			sprintf(flag,""); //flag = NULL;
			fscanf(fp, "%s : %s\n", flag, message);
			if (strcmp(flag,"**NET") == 0) {
				i=0;
				// legge la generazione
				k=0;
				do {
					if (message[i]!='s') { 
						g[k]=message[i];		
						k++;
					}
					i++;
				} while (message[i]!='_');
				g[k]='\0';
				i++;
				if (nraces>1) {						// se ci sono pi� razze effettua questo tipo di lettura altrimenti un'altra
					// legge il numero del best
					k=0;
					do {
						b[k]=message[i];		
						k++;
						i++;
					} while (message[i]!='_');
					b[k]='\0';
					i++;
					// legge il numero della specie
					k=0;
					do {
						r[k]=message[i];		
						k++;
						i++;
					} while (message[i]!='.');
					r[k]='\0';
				} else {							// nel caso monorazza
					// legge il numero del best
					k=0;
					do {
						b[k]=message[i];		
						k++;
						i++;
					} while (message[i]!='.');
					b[k]='\0';
				}
				// estrae gli interi dai valori letti
				ig=atoi(g);
				ib=atoi(b);
				if (nraces>1)  							// solo se ci sono pi� razze
					ir=atoi(r);
				else 
					ir=0;								// razza a 0 per default nel caso monorazza
				
				if ((ig==gen) && (race==ir))		// se la spcie e la generazioe coincidono
					return (ib);
				else 
					continue;						// altrimenti riprende il ciclo
			} else {
				continue;
			}
		} while (!((ig==generations-1) && (ir==nraces-1)));					// cicla su tutte le generazioni contenute nel file dei best
		fclose(fp);
	} else {
		sprintf(message,"file %s could not be opened\n",filename); 
		display_stat_message(message);
	}
	return(1);	// nei casi estremi restituisce 1
}


/*
* load the populations genome
* if called a number >=0 load all populations
* if called with a filename load only the population indicated in the filename
*  in this case it can load up to (individuals+additional_ind) individuals 
* we assume we have a population only
*/ 
int
loadallg(int gen, char *filew, int first_or_second_genome, int race)

{
	
	FILE	*fp;
	char    filename[64];
	char    message[128];
	char    flag[64];
	int     n;
	int     p;
	int     nloaded;
	int     max;
	
	for(p=0; p < 1; p++)
	{
		max = nindividuals;
		if (gen == -1)
			max = nindividuals + additional_ind;	

		if (gen >= 0)
			sprintf(filename,"G%dP%dS%d.gen",gen,p,seed);
		else
			sprintf(filename,"%s",filew);
		
		if ((fp=fopen(filename, "r")) != NULL) 
		{
			for(n=0,nloaded=0;n<max;n++)	   
			{
				sprintf(flag,""); //flag = NULL;
				fscanf(fp, "%s : %s\n", flag, message);
				if (strcmp(flag,"**NET") == 0)
				{
					if (race==0)		// Carica tutti i genotipi nel genoma solo se nn ci sono razze
						loadagenotype(fp,nloaded,p, first_or_second_genome);	// carica i genotipi che trova dentro genome o secondgenome
					else 
						loadagenotype(fp,race-1,p, first_or_second_genome);		// carica il genotipo dell'ultimo robot della razza race
																				// ma deve per forza eseguirlo per tutto il ciclo perch� 
																				// il file � ad accesso sequenziale
					dindividual = nloaded;
					nloaded++;
				}
				else
				{
					break;
				}
			}
			if (race==0) {
				sprintf(message,"loaded pop: %d ind: %d",p,nloaded);
				display_stat_message(message);
			} else {
				sprintf(message,"loaded pop: %d ind: %d",p,nraces);
				display_stat_message(message);
			}

			fclose(fp);
		}
		else
		{
			sprintf(message,"file %s could not be opened\n",filename); 
			display_stat_message(message);
		}
	}

	return(nloaded);
}


/*
 * load the genome of pre-evolved fixed individuals
 */ 
void
loadfixedg()

{
	
	FILE	*fp;
	int     n;
	char    flag[64];
	char    message[128];
	char    filename[128];
	int	*gi;
	int     nloaded;
	int	j;

	for(n=0, nloaded=0; n < nteam; n++)	   
	   {
	      sprintf(filename, "evorobot%d.gen", n + 1);
	      if ((fp=fopen(filename, "r")) != NULL) 
	         {
		  sprintf(flag,"");
		  fscanf(fp, "%s : %s\n", flag, message);
		  if (strcmp(flag,"**NET") == 0)
		    {
                      gi = *(fixedgenome + n); 
                      fscanf(fp, "DYNAMICAL NN\n");
                      for (j=0; j < ngenes; j++, gi++)
	                {
	                  fscanf(fp, "%d\n", gi);
	                }
		      fscanf(fp, "END\n");
		      *(loadfixedgn + n) = 1;
		      nloaded++;
		     }
		  sprintf(message,"loaded %d fixed genome", nloaded);
		  display_stat_message(message);
		  fclose(fp);
		 }
	     }
	
}


/*
 * copy the bestgenome nn into genome n making mutations
 */
void
getbestgenome(int n, int nn, int p, int domutate)

{
	int     *gi;
	int     *bgi;	
    int     i;

    gi = *(genome + (n + (p * (nindividuals + additional_ind))));
    bgi = *(bestgenome + (nn + (p * bestfathers)));
	
	for (i=0; i < ngenes; i++, gi++, bgi++)
		if (domutate == 0)
			*gi = *bgi;
	    else
            *gi = mutate(*bgi);
}

/*
 * create an individual's child and eventually mutate it. Only with n_variable_evolution_is_running = 1. 
 * Return the number of child. 
 */
int 
reproduce_and_create_child(int nfather, int domutate)

{
	int     **gf;			// genoma dell'individuo nfather
	int		**gc;			// genoma dell'individuo figlio
	int     *gf_gene;		// singolo gene dell'individuo nfather
	int		*gc_gene;		// singolo gene dell'individuo figlio
    int     i;
	int		nchildren=0;	// numero associato all'individuo figlio
	int	    **genome_tmp;	// genoma temporaneo con un individuo in pi�

	if (n_variable_evolution_is_running == 1) {
		nchildren = nindividuals;							// Il figlio ha numero pari ad un numero di nindividuals + 1
		
		// Rialloca spazio per il genoma aggiungendo un individuo in pi�
		genome_tmp = (int **) realloc(genome, (((nindividuals + additional_ind) * npopulations) +1) * sizeof(int *));
		if (genome_tmp == NULL)
		{
			display_error("genome reallocation error");
		}
		genome = genome_tmp;							// Assegna il genoma al nuovo blocco di memoria
		gf = genome + nfather;							// seleziona l'individuo con indice nfather			
		gc = genome + nchildren;						// seleziona l'individuo con indice nchildren
		
		*gc = (int *)malloc(ngenes * sizeof(int));		// alloca la riga aggiuntiva per creare spazio
														// per il nuovo individuo
		for (i=0,gf_gene=*gf,gc_gene=*gc; i < ngenes; i++, gf_gene++, gc_gene++)
			if (domutate == 0)
				*gc_gene = *gf_gene;				// copia il padre nel figlio direttamente
			else
				*gc_gene = mutate(*gf_gene);		// copia il padre nel figlio mutandolo

		nindividuals++;			// Incrementa a questo punto il numero di individui corrente
	}

	return (nchildren);		// Restituisce il numero dell'individuo figlio
}


/*
 * copy genome n in the bestgenome nn 
 */
void
putbestgenome(int n, int nn, int p)

{


    int    *gi;
    int    *bgi;	
    int     i;

    gi = *(genome + (n + (p * (nindividuals + additional_ind))));
    bgi = *(bestgenome + (nn + (p * bestfathers)));
	
    for (i=0;i < ngenes; i++, gi++, bgi++)
              *bgi = *gi;	      

}


/*
 * load fitness statistics (best and average performance)
 * mode=1 display graph, mode=0 do not display the graph
 */ 
int
loadstatistics(char *files, int mode)

{


     FILE	*fp;
     char       line[128];
     char       word[128];
     int n;
     int nc;

     statfit_max = 0.0;
     statfit_min = 0.0;
     statfit_n = 0;
     if ((fp=fopen(files, "r")) != NULL) 
	{
	  n = 0;
	  while (fgets(line,128,fp) != NULL && n < MAXGENERATIONS)
	  {
            statfit[n][0] = 0;
            statfit[n][1] = 0;
            nc = getword(word,line,' ');
	    if (nc > 0)
	      statfit[n][0] = atof(word);
            nc = getword(word,line,' ');
	    if (nc > 0)
	      statfit[n][1] = atof(word);
	    if (statfit[n][0] > statfit_max)
	      statfit_max = statfit[n][0]; 
	    if (statfit[n][1] > statfit_max)
	      statfit_max = statfit[n][1]; 
	    if (statfit[n][0] < statfit_min)
	      statfit_min = statfit[n][0]; 
	    if (statfit[n][1] < statfit_min)
	      statfit_min = statfit[n][1]; 
	    n++;
	  }
	  statfit[n][0] = -9999.0;
	  statfit_n = n;
	  if (mode == 1)
	  {
           sprintf(line,"loaded n: %d data from file %s",n, files);
           display_stat_message(line);
	   if (mode == 1)
             update_rendevolution(1);
	  }
	  return(n);
        }
       else
       {
	 statfit_n = 0;
         sprintf(line,"Error: the file %s could not be opended",files);
	 display_stat_message(line);
       }

}



/*
 * load multiseed statistics of the current population
 * display all graph and the ranking of replications
 * if evolution is running, simply show the statistics of the current running seed
 */ 
void
display_all_stat()

{


     double max_all;
     double min_all;
     int    n_all;
     char   filen[128];
     double max_seed[20];
     int    best_ids[20];
     int    nrep;
     int    r;
     int    rr;
     double seed_max;
     int    seed_id;
     char   message[256];

     if (n_fixed_evolution_is_running == 0 && n_variable_evolution_is_running == 0 && n_fixed_evolution_social_is_running == 0)
     {

     // we load all files to compute the fitness range and the best seeds
     max_all = 0.0;
     min_all = 0.0;
     n_all = 0;
     for (r=0; r < 20; r++)
     {
       max_seed[r] = 0.0;
       best_ids[r] = 0;
     }

     if (nreplications < 20)
       nrep = nreplications;
      else
       nrep = 20;
     for (r=0; r < nrep; r++)
     {
       sprintf(filen,"statS%d.fit",seed + r);
       loadstatistics(filen, 0);
       max_seed[r] = statfit_max;
       if (statfit_max > max_all)
	  max_all = statfit_max;
       if (statfit_min < min_all)
	  min_all = statfit_min;
       if (statfit_n > n_all)
	  n_all = statfit_n;
     }

     // we compute and display the ranking
     for (r=0; r < nrep; r++)
        {
	 seed_max = 0.0f;
	 seed_id  = 0;
         for (rr=0; rr < nrep; rr++)
          {
            if (max_seed[rr] > seed_max)
	       {
		seed_max = max_seed[rr];
		seed_id = rr;
	       }
          }
         best_ids[r] = seed_id + seed;
	 max_seed[seed_id] = 0.0f;
        }
        if (nrep <= 10)
          {
            sprintf(message,"best seeds: %d %d %d %d %d %d %d %d %d %d - %.1f %.1f %.1f",
	             best_ids[0], best_ids[1], best_ids[2], best_ids[3], best_ids[4], 
	             best_ids[5], best_ids[6], best_ids[7], best_ids[8], best_ids[9], 
		     max_seed[0], max_seed[1], max_seed[2]);
	    display_stat_message(message);
          }
	  else
          {
            sprintf(message,"best seeds: %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d",
	             best_ids[0], best_ids[1], best_ids[2], best_ids[3], best_ids[4], 
	             best_ids[5], best_ids[6], best_ids[7], best_ids[8], best_ids[9],
	             best_ids[10], best_ids[11], best_ids[12], best_ids[13], best_ids[14], 
	             best_ids[15], best_ids[16], best_ids[17], best_ids[18], best_ids[19]);
	    display_stat_message(message);          
	  }
     //we now update the renderarea
     statfit_max = max_all;
     statfit_min = min_all;
     statfit_n = n_all;
     update_rendevolution(2);
     }
     else
     {
      update_rendevolution(1);
     }
}


/*
* generate the file lineage.gen from best.gen files
*/ 
void
create_lineage()

{
     int     g;
     int     v1;
     int     v2;
     char    message[256];
     char    flag[128];
     int     gg;
     int     f;
     FILE    *fp;
     char    filename[64];
     int     replication;
     int     s;

     for (replication=0,s=seed; replication < nreplications; replication++, s++)
       {
	// we load the lineage starting from the last gen
	f = 1; 
	for(g = (generations - 1); g >= 0; g--)
	 {
	   sprintf(filename,"B%dP%dS%d.gen",f, 0, s);
	   if ((fp=fopen(filename, "r")) != NULL)
	    {
	      for(gg=0; gg <= g; gg++)
	        {
		   sprintf(flag,"");
		   fscanf(fp, "%s : s%d_%d.wts\n", flag, &v1,&v2);
		   if (strcmp(flag,"**NET") == 0)
	             loadagenotype(fp,gg,0, 0);
		   else
	            {
	             sprintf(message,"unable to load seed %d best %d generation %d",s,f,gg);
	             display_stat_message(message);
	             fclose(fp);
		     return;
	            }
		 }
	      sprintf(message,"loaded seed %d best %d generation %d",s,f,g);
	      display_stat_message(message);
	      fclose(fp);
	     }
	     else
	     {
	       sprintf(message,"file %s could not be opened\n",filename); 
	       display_warning(message);
	       return;
	     }
	   f = (v2 / nkids) + 1;
	 }
	// we save the lineage 
 	sprintf(filename,"lineageS%d.gen",s);
	if ((fp=fopen(filename, "w")) != NULL) 
	  {
	    for(g=0;g<generations;g++)	   
	     {
	        fprintf(fp,"**NET : s%d_%d.wts\n",g,0);		
	        saveagenotype(fp,g,0);
	     } 
	     fflush(fp);	       
	     fclose(fp);
	     sprintf(message,"lineageS%d.gen have been saved",s);
	     display_stat_message(message);
	  }
	  else
	  {
	    sprintf(message,"cannot open %s file",filename);
	    display_warning(message);
	  }
      }

}