
/* 
 * Evorobot* - Simulator.cpp
 * Copyright (C) 2009, Stefano Nolfi
 * LARAL, Institute of Cognitive Science and Technologies, CNR, Roma, Italy 
 * http://laral.istc.cnr.it

 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#include "public.h"
#include "robot-env.h"

double  P90,P180,P360;      // Pgreco constants

extern char robotwstatus[]; // stat message within rend_robot widget
extern float ffitness_corvids14(struct  individual  *pind);    // fitness per l'esperimento corvidi
extern float ffitness_corvids15(struct  individual  *pind);    // fitness per l'esperimento corvidi
extern int check_perceptualinds(int team);
extern int sincrobots;					// syncronization flag : 1 if both the robot emit pushing unit signal
extern int bar_without_crash;			// if 1 reset robot position to the 4 step old position when crash with bar 
extern int max_dist_rebound_after_crash;	// Max positions-distance after crash with bar
extern int norotationalbar;				// if 1 bar cannot rotate but only translate
extern int restart_from_n_pos_before;	// if 1 robot collides with wall and with each other in the same way of the bar
extern int restart_around;				// if 1 robot collides robot restart around
extern int widgetdx;					// x-size of the widget
extern int widgetdy;					// y-size of the widget
extern int evwidgetdx;			        // x-size of the widget
extern int evwidgetdy;					// y-size of the widget
extern int ind_cannot_crash;			// individuals cannot crash  
extern float   mem_bar_orig[10][4];		// memorizza la posizione originaria della sbarra
extern int RColorPreyDeadInd;				// componente rossa del colore dell'individuo morto
extern int GColorPreyDeadInd;				// componente verde del colore dell'individuo morto	
extern int BColorPreyDeadInd;				// componente blu del colore dell'individuo	morto
extern int	test_measure_leadership;		// measure of leadership on/off
extern int	test_measure_leadership_eco;	// measure of leadership in ecology on/off
extern int *startx;				   // Dichiara l'ascissa di start di tutti i robot
extern int *starty;				   // Dichiara l'ordinata di start di tutti i robot

extern bool isDaytime;					//	True if it is daytime in the environment false if else

extern double waterAccumulator;			//	allow access to the water accumulator
extern double foodAccumulator;			//	allow access to the food accumulator

extern double hungerAccumulator;		//	allow access to the hunger accumulator
extern double thirstAccumulator;		//	allow access to the thirst accumulator


extern long int sweeps;				    // number of step for each trial  

void intToThreeNodeEncoding(int value, float* bit0, float* bit1, float* bit2)
{
	switch (value)
	{
	case 0:
		*bit0 = 0.0f;
		*bit1 = 0.0f;
		*bit2 = 0.0f;
		break;
	case 1:
		*bit0 = 0.0f;
		*bit1 = 0.0f;
		*bit2 = 1.0f;
		break;
	case 2:
		*bit0 = 0.0f;
		*bit1 = 1.0f;
		*bit2 = 0.0f;
		break;
	case 3:
		*bit0 = 1.0f;
		*bit1 = 0.0f;
		*bit2 = 0.0f;
		break;
	//	Values over 3
	default:
		*bit0 = 1.0f;
		*bit1 = 0.0f;
		*bit2 = 0.0f;
		break;
	}
}

void intToThreeNodeMagnitudeEncoding(int value, float* bit0, float* bit1, float* bit2)
{
	switch (value)
	{
	case 0:
		*bit0 = 0.0f;
		*bit1 = 0.0f;
		*bit2 = 0.0f;
		break;
	case 1:
		*bit0 = 1.0f;
		*bit1 = 0.0f;
		*bit2 = 0.0f;
		break;
	case 2:
		*bit0 = 1.0f;
		*bit1 = 1.0f;
		*bit2 = 0.0f;
		break;
	case 3:
		*bit0 = 1.0f;
		*bit1 = 1.0f;
		*bit2 = 1.0f;
		break;
	//	Values over 3
	default:
		*bit0 = 1.0f;
		*bit1 = 1.0f;
		*bit2 = 1.0f;
		break;
	}
}

void intToThreeBitBinary(int value, float* bit0, float* bit1, float* bit2)
{
	switch (value)
		{
		case 0:
			//	0, 0, 0
			*bit0 = 0.0f;
			*bit1 = 0.0f;
			*bit2 = 0.0f;
			break;
		case 1:
			//	0, 0, 1
			*bit0 = 0.0f;
			*bit1 = 0.0f;
			*bit2 = 1.0f;
			break;
		case 2:
			//	0, 1, 0
			*bit0 = 0;
			*bit1 = 1;
			*bit2 = 0;
			break;
		case 3:
			//	0, 1, 1
			*bit0 = 0;
			*bit1 = 1;
			*bit2 = 1;
			break;
		case 4:
			//	1, 0, 0
			*bit0 = 1;
			*bit1 = 0;
			*bit2 = 0;
			break;
		case 5:
			//	1, 0, 1
			*bit0 = 1;
			*bit1 = 0;
			*bit2 = 1;
			break;
		case 6:
			//	1, 1, 0
			*bit0 = 1;
			*bit1 = 1;
			*bit2 = 0;
			break;
		case 7:
			//	1, 1, 1
			*bit0 = 1;
			*bit1 = 1;
			*bit2 = 1;
			break;
		default:
			//	This shouldn't happen
			*bit0 = 1;
			*bit1 = 1;
			*bit2 = 1;
			break;
		}
}

/*
 * just set few costants
 */
void
SETP ()

{
  P90 =    asin(1.0);
  P180=2.0*asin(1.0);
  P360=4.0*asin(1.0);
}

/*
 * trasform radiants to degrees
 */
double
RADtoDGR(double r)

{
   double x;

   SETP();
   if (r < 0.0)
    x=360.0-(r*(-1.0)/P90)*90;
     else
    x=(r/P90)*90;
   return(x);
 }


/*
 * transform degrees to radiants
 */
double
DGRtoRAD(double d)

{
    double   x;

    x=(d/90)*asin(1.0);
    return(x);
}

/*
 * compute the deltax given an angle and a distance
 */
float
SetX(float x, float d, double angle)


{
    float   res;

    res =  (float) (x + d * cos(DGRtoRAD(angle)) + 0.000001);
    return(res);
}

/*
 * compute the deltay given an angle and a distance
 */
float
SetY(float y, float d, double angle)

{
    float   res;

    res = (float) (y + d * sin(DGRtoRAD(angle)) + 0.000001);
    return(res);

}


/*
 * return the distance between a point and a line
 */
double
linedist(float px,float py,float ax,float ay,float bx,float by)

{
     double d1,d2,d3;

     d1 = ((px-ax)*(px-ax)) + ((py-ay)*(py-ay));
     d2 = ((px-bx)*(px-bx)) + ((py-by)*(py-by));
     d3 = ((ax-bx)*(ax-bx)) + ((ay-by)*(ay-by));

     return(sqrt(d1 - (((d2 - d3 - d1) * (d2 - d3 - d1)) / (4 * d3))));

}

/*
 * computes the end point of a segment:
 *
 * x1,y1 = starter point; 
 * directangle =  direction (in degreas) of segment;
 * displacement = lenght of segment;
 *
 */
void ToPoint(float x1, float y1, float directangle, float displacement, float *x2, float *y2)
{
	*x2 =  x1 + (displacement * cos(DGRtoRAD(directangle)));
	*y2 =  y1 + (displacement * sin(DGRtoRAD(directangle)));
}

/*
 * computes distance between two points
 */
double PointsDist(float x1, float y1, float x2,float y2)
{

     double d1 = sqrt(((x2-x1)*(x2-x1)) + ((y2-y1)*(y2-y1)));     

     return(d1);
}




int intersect_lines(float x0, float y0, float x1, float y1 , float x2, float y2, float x3, float y3, float *xi, float *yi)
{
// this function computes the intersection of the sent lines
// and returns the intersection point, note that the function assumes
// the lines intersect. the function can handle vertical as well
// as horizontal lines. note the function isn't very clever, it simply
//applies the math, but we don't need speed since this is a
//pre-processing step

double rm1, rm2;

double a1,b1,c1, // constants of linear equations
      a2,b2,c2,
      det_inv,  // the inverse of the determinant of the coefficient matrix

      m1,m2;    // the slopes of each line

// compute slopes, note the cludge for infinity, however, this will
// be close enough

if ((x1-x0)!=0)
   m1 = (y1-y0)/(x1-x0);
else
   m1 = (double)1e+10;   // close enough to infinity

if ((x3-x2)!=0)
   m2 = (y3-y2)/(x3-x2);
else
   m2 = (double)1e+10;   // close enough to infinity

// compute constants

a1 = m1;
a2 = m2;

b1 = -1;
b2 = -1;

c1 = (y0-m1*x0);
c2 = (y2-m2*x2);

// compute the inverse of the determinate

det_inv = 1/(a1*b2 - a2*b1);

// use Kramers rule to compute xi and yi

*xi=((b1*c2 - b2*c1)*det_inv);
*yi=((a2*c1 - a1*c2)*det_inv);
if(*xi>0 && *xi<0.000001f) *xi=0.0f;
if(*yi>0 && *yi<0.000001f) *yi=0.0f;


rm1=PointsDist(*xi,*yi,x2,y2)+PointsDist(*xi,*yi,x3,y3);
rm2=PointsDist(*xi,*yi,x0,y0)+PointsDist(*xi,*yi,x1,y1);

if ( (rm1<=PointsDist(x2,y2,x3,y3)+0.1) && (rm2<=PointsDist(x0,y0,x1,y1)+0.1))

return 1;
else

return 0;




}

/*
 * Computes the frontal intersection point between lines and circles: 
 *
 * &xi , &yi = frontal intersection point, if exist
 * x1,y1 - x2,y2 = segment
 * cx, cy, r =  the center and the ray of the circle
 * return:  0 = non intersection
 *	    1 = intersection
 *	    2 = segment is inside the circle
 *
 */
int intersect_line_circle(float x1,float y1, float x2,float y2, float cx,float cy, float r , float *xi,float *yi)
{
	*xi = 0.0f;
	*yi = 0.0f;
	int intersection = 0;
	float a  = (x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1);
	float b  = 2 * ((x2 - x1) * (x1 - cx) +(y2 - y1) * (y1 - cy));
	float cc = (cx * cx) + (cy * cy) + (x1 * x1) + (y1 * y1) - 2 * (cx * x1 + cy * y1) - (r * r);
	float deter = (b * b) - 4 * a * cc;

	if (deter <= 0 )
	{
		intersection = 0;
	}
	else 
	{
		float e  = sqrt(deter);
		float u1  = ( - b + e ) / (2 * a );
		float u2  = ( - b - e ) / (2 * a );
		if ((u1 < 0 || u1 > 1) && (u2 < 0 || u2 > 1))
		{
			if ((u1 < 0 && u2 < 0) || (u1 > 1 && u2 > 1))
				intersection = 0;
			else 
				intersection = 2; 
		} 
		else 
		{
				*xi = x1 + u2 * (x2 - x1);
				*yi = y1 + u2 * (y2 - y1);
				intersection = 1;
		}
	}

	return(intersection);
}


/*
 * normalize and angle between 0 and 360
 */
double
normang(double a)

{

   int v;

   v = 0;
   while((a < 0.0 || a > 360.0) && v < 10)
    {
      if (a < 0.0)
	a += 360.0;
      if (a > 360.0)
	a -= 360.0;
      v++;
    }
   if (v == 10)
	display_warning("normang: angle out of range");
   return(a);
}


/*
 * return the angle between two points with a given distance
 */

double
mang(int x, int y, int x1, int y1, double dist)

 {

   double    angolo;
   int       dxang,dyang;
   double    buffer;
   double    alfa,pi2;
   double    temp;

   if  (x==x1 && y==y1)
      angolo=0.0;
     else
      {
       dxang=(x1-x);
       dyang=(y1-y);
       buffer=abs(dyang);
       pi2 =asin(1.0);
       temp = buffer/dist;
       if (temp>1.0) temp=1.0;
       alfa=asin(temp);
       alfa=(alfa/pi2)*90;
       if (dxang>0)
	     {
              if (dyang>0);
	      if (dyang<0)
                 alfa=360-alfa;
	     }
        if (dxang<0)
	     {
              if (dyang>0) alfa=180-alfa;
	      if (dyang<0) alfa=alfa+180;
	      if (dyang==0)alfa=180;
	      }
	 if (dxang==0)
	     {
		 if (dyang>0) ;
		 if (dyang<0) alfa=270;
		 if (dyang==0)alfa=0;
	     }
	  angolo=alfa;
       }
     return(angolo);
}




/*
 * it calculate the relative angle
 */
double
mangrel(double absolute,double kepera)



{

    double  relative;


    relative = absolute - kepera;

    if (relative > 359.0)
        relative -= 360.0;
    if (relative < 0.0)
        relative += 360.0;

    return(relative);

}



/*
 * it calculate the relative angle
 */
double
mangrel2(double absolute,double kepera)

{

    double  relative;


    relative = 360 - (kepera - absolute);

    if (relative > 359.0)
        relative -= 360.0;
    if (relative < 0.0)
        relative += 360.0;

    return(relative);

}


/*
*  return the id number of the target area in which
*  the robot is located (0 if it is located outside target areas) 
*/
int read_ground(struct individual *pind)
{

	double distzone;
	int z;
	float p1;
	float p2;
	struct envobject *obj;

	//calculate location of floor sensor = in front of robot body, as in ePuck
	p1 = SetX(pind->pos[0], 37.0, pind->direction);
	p2 = SetY(pind->pos[1], 37.0, pind->direction);


	/*	p1 = SetX(pind->pos[0], robotradius, pind->direction);
	p2 = SetY(pind->pos[1], robotradius, pind->direction);
	*/

	if (envnobjs[GROUND] > 0)
	{
		for(z=0, obj = *(envobjs + GROUND); z < envnobjs[GROUND]; z++, obj++)
		{
			if (obj->subtype == 0)
			{
				distzone = ((p1 - obj->x) * (p1 - obj->x)) + ((p2 - obj->y) * (p2 - obj->y));
				distzone = sqrt(distzone);
				if (distzone < obj->r)
					return(z+1);
			}
			else
			{
				if ( (p1 >= obj->x) && (p1 < (obj->x + obj->X)) &&
					(p2 >= obj->y) && (p2 < (obj->y + obj->Y)) )
					return(z+1);
			}

		}

	}
	return(0);
}

/*
*  return the id number of the target area in which
*  the robot is located (0 if it is located outside target areas)
*	TARGET_TYPE can be WATER, FOOD, GROUND etc
*/
int read_target(struct individual *pind, const int TARGET_TYPE)
{

	double distzone;
	int z;
	float p1;
	float p2;
	struct envobject *obj;

	//calculate location of floor sensor = in front of robot body, as in ePuck
	p1 = SetX(pind->pos[0], 37.0, pind->direction);
	p2 = SetY(pind->pos[1], 37.0, pind->direction);


	/*	p1 = SetX(pind->pos[0], robotradius, pind->direction);
	p2 = SetY(pind->pos[1], robotradius, pind->direction);
	*/

	if (envnobjs[TARGET_TYPE] > 0)
	{
		for(z=0, obj = *(envobjs + TARGET_TYPE); z < envnobjs[TARGET_TYPE]; z++, obj++)
		{
			if (obj->subtype == 0)
			{
				distzone = ((p1 - obj->x) * (p1 - obj->x)) + ((p2 - obj->y) * (p2 - obj->y));
				distzone = sqrt(distzone);
				if (distzone < obj->r)
					return(z+1);
			}
			else
			{
				if ( (p1 >= obj->x) && (p1 < (obj->x + obj->X)) &&
					(p2 >= obj->y) && (p2 < (obj->y + obj->Y)) )
					return(z+1);
			}
		}
	}
	return(0);
}

/*
 *  return the id number of the target food area in which
 *  the robot is located (0 if it is located outside target areas) 
 */
int read_food(struct individual *pind)
{
	double distzone;
	int z;
	float p1;
	float p2;
	struct envobject *obj;

	//calculate location of floor sensor = in front of robot body, as in ePuck
	p1 = SetX(pind->pos[0], 37.0, pind->direction);
	p2 = SetY(pind->pos[1], 37.0, pind->direction);


	/*	p1 = SetX(pind->pos[0], robotradius, pind->direction);
	p2 = SetY(pind->pos[1], robotradius, pind->direction);
	*/

	if (envnobjs[FOOD] > 0)
	{
		for(z=0, obj = *(envobjs + FOOD); z < envnobjs[FOOD]; z++, obj++)
		{
			if (obj->subtype == 0)
			{
				distzone = ((p1 - obj->x) * (p1 - obj->x)) + ((p2 - obj->y) * (p2 - obj->y));
				distzone = sqrt(distzone);
				if (distzone < obj->r)
					return(z+1);
			}
			else
			{
				if ( (p1 >= obj->x) && (p1 < (obj->x + obj->X)) &&
					(p2 >= obj->y) && (p2 < (obj->y + obj->Y)) )
					return(z+1);
			}
		}
	}
	return(0);
}

/*
 *  return the id number of the target water area in which
 *  the robot is located (0 if it is located outside target areas) 
 */
int read_water(struct individual *pind)
{
	double distzone;
	int z;
	float p1;
	float p2;
	struct envobject *obj;

	//calculate location of floor sensor = in front of robot body, as in ePuck
	p1 = SetX(pind->pos[0], 37.0, pind->direction);
	p2 = SetY(pind->pos[1], 37.0, pind->direction);


	/*	p1 = SetX(pind->pos[0], robotradius, pind->direction);
	p2 = SetY(pind->pos[1], robotradius, pind->direction);
	*/

	if (envnobjs[WATER] > 0)
	{
		for(z=0, obj = *(envobjs + WATER); z < envnobjs[WATER]; z++, obj++)
		{
			if (obj->subtype == 0)
			{
				distzone = ((p1 - obj->x) * (p1 - obj->x)) + ((p2 - obj->y) * (p2 - obj->y));
				distzone = sqrt(distzone);
				if (distzone < obj->r)
					return(z+1);
			}
			else
			{
				if ( (p1 >= obj->x) && (p1 < (obj->x + obj->X)) &&
					(p2 >= obj->y) && (p2 < (obj->y + obj->Y)) )
					return(z+1);
			}
		}
	}
	return(0);
}

/*
 *  return the color of the ground (0.0 out target areas)
 */
float
read_ground_color(struct individual *pind)
{

	double distzone;
	int z;
	float p1;
	float p2;
        struct envobject *obj;


	//calculate location of floor sensor = in front of robot body, as in ePuck
	p1 = SetX(pind->pos[0], 37.0, pind->direction);
	p2 = SetY(pind->pos[1], 37.0, pind->direction);
	if (envnobjs[GROUND] > 0)
	{
	  for(z=0, obj = *(envobjs + GROUND); z < envnobjs[GROUND]; z++, obj++)
		{
		  if (obj->subtype == 0)
		   {
		    distzone = ((p1 - obj->x) * (p1 - obj->x))+((p2 - obj->y) * (p2 - obj->y));
		    distzone = sqrt(distzone);
		    if (distzone < obj->r)
			return(obj->c[0]);
		   }
		  else
		   {
		    if ( (p1 >= obj->x) && (p1 < (obj->x + obj->X)) &&
                         (p2 >= obj->y) && (p2 < (obj->y + obj->Y)) )
			return(obj->c[0]);
		   }
		}
	}
	return(0.0);
}

/*
*  return a non-zero value if the robot is within a FOOD zone
*/
float read_food_color(struct individual *pind)
{
	double distzone;
	int z;
	float p1;
	float p2;
	struct envobject *obj;

	//	calculate location of floor sensor = in front of robot body, as in ePuck
	p1 = SetX(pind->pos[0], 37.0, pind->direction);
	p2 = SetY(pind->pos[1], 37.0, pind->direction);

	if (envnobjs[FOOD] > 0)
	{
		//	loop through all FOOD objects
		for(z=0, obj = *(envobjs + FOOD); z < envnobjs[FOOD]; z++, obj++)
		{
			if (obj->subtype == 0)
			{
				//	Work out the distance between the sensor and the food zone
				distzone = ((p1 - obj->x) * (p1 - obj->x))+((p2 - obj->y) * (p2 - obj->y));
				distzone = sqrt(distzone);

				//	Distance is within the radius of the object
				if (distzone < obj->r)
				{
					return 1.0;
				}
			}
			else
			{
				//	Distance is within object area (for rectangles)
				if ( (p1 >= obj->x) && (p1 < (obj->x + obj->X)) &&
					(p2 >= obj->y) && (p2 < (obj->y + obj->Y)) )
				{
					return 1.0;
				}
			}
		}
	}
	return(0.0);
}

/*
 *  return a non-zero value if the robot is within a WATER zone
 */
float read_water_color(struct individual *pind)
{

	double distzone;
	int z;
	float p1;
	float p2;
	struct envobject *obj;

	//calculate location of floor sensor = in front of robot body, as in ePuck
	p1 = SetX(pind->pos[0], 37.0, pind->direction);
	p2 = SetY(pind->pos[1], 37.0, pind->direction);

	if (envnobjs[WATER] > 0)
	{
		//	loop through all WATER objects
		for(z=0, obj = *(envobjs + WATER); z < envnobjs[WATER]; z++, obj++)
		{
			if (obj->subtype == 0)
			{
				//	Work out the distance between the sensor and the water zone
				distzone = ((p1 - obj->x) * (p1 - obj->x))+((p2 - obj->y) * (p2 - obj->y));
				distzone = sqrt(distzone);

				//	Distance is within the radius of the object
				if (distzone < obj->r)
				{
					return 1.0;
				}
			}
			else
			{
				//	Distance is within the object area (for rectangles)
				if ( (p1 >= obj->x) && (p1 < (obj->x + obj->X)) &&
					(p2 >= obj->y) && (p2 < (obj->y + obj->Y)) )
				{
					return 1.0;
				}
			}
		}
	}
	return(0.0);
}

/*
 *  return the color of the ground (0.0 out target areas)
 */
float
read_ground_colornew(struct individual *pind)
{

	double distzone;
	int z;
	float p1;
	float p2;
	struct envobject *obj;


	//calculate location of floor sensor = in front of robot body, as in ePuck
	p1 = SetX(pind->pos[0], 37.0, pind->direction);
	p2 = SetY(pind->pos[1], 37.0, pind->direction);
	if (envnobjs[GROUND] > 0)
	{
	  for(z=0, obj = *(envobjs + GROUND); z < envnobjs[GROUND]; z++, obj++)
		{
		  distzone = (p1 - obj->x)*(p1 - obj->x) + (p2 - obj->y)*(p2 - obj->y);
		  distzone = sqrt(distzone);
		  if (distzone < obj->r)
		     return(obj->c[0]);
		}
	}
	return(0.0);
}

/*
 * checks whether a crash occurred between a robot and an obstacle
 * for robot-robot collision individual_crash is set to 1
 * esclude nella ricerca l'individuo di numero team	
 */
int
check_crash_old(float p1,float p2,int team)

{
  int i,j;
  float   x,y;
  float	  minx, maxx, miny, maxy;	// minime e massime coordinate di un segmento
  float   m1,q1,m2,q2;				// coefficienti m e q delle rette perpendicolari
  double  dist;
  double  mindist;
  struct individual *pind;
  struct envobject *obj;
  struct envobject *obj_tmp;		// struttura temporanea per il controllo cilindri, ed altri elementi
  int jump_crash=0;					// salta il check_crash

  individual_crash = 0;
  mindist = 9999.0;
  for(i=0, obj = *(envobjs + SWALL);i<envnobjs[SWALL];i++, obj++)
   {
    if (p1 >= obj->x && p1 <= obj->X)
       x = p1;
     else
       x = obj->x;
    if (p2 >= obj->y && p2 <= obj->Y)
       y = p2;
     else
       y = obj->y;
    dist = mdist(p1,p2,x,y);
    if (dist < mindist)
      mindist = dist;
   }

  for(i=0,obj = *(envobjs + SROUND);i<envnobjs[SROUND];i++,obj++)
  {
    /* Eventuale controllo che il cilindro non appartenga all'estremit� di una delle sbarre, in tal
       caso dovrebbe inibire il crash con il cilindro
	   Inibisce il controllo anche nel caso che il cilindro sia di colore 10 (verde), caso in cui il 
	   cilindro si trova sopra il robot. 
	*/
	for(j=0, obj_tmp = *(envobjs + BAR);j<envnobjs[BAR];j++, obj_tmp++) { 
		// controlla che il cilindro appartenga ad un estremo 
		jump_crash=0;
		if (((obj->x==obj_tmp->x) && (obj->y==obj_tmp->y))||((obj->x==obj_tmp->X) && (obj->y==obj_tmp->Y))||
			 (obj->subtype==10)) {
			// In tal caso inibisce il controllo del crash
			jump_crash=1;
		}
		if (!jump_crash) {
			dist = mdist(p1,p2,obj->x,obj->y)-obj->r;
			if (dist < mindist)
			mindist = dist;
		}
	}	  
  }

  for(i=0,obj = *(envobjs + ROUND);i<envnobjs[ROUND];i++,obj++)
   {
    dist = mdist(p1,p2,obj->x,obj->y)-obj->r;
    if (dist < mindist)
      mindist = dist;
   }

   // check if a crash occurred with another individuals
   // by ignoring individuals that have the same idential positions - theirself
   if (!ind_cannot_crash) {
		for (pind=ind,i=0; i < nteam; i++, pind++)
		{
			if ((p1 != pind->pos[0] || p2 != pind->pos[1]) && (pind->number != team)) 
			{
				dist = mdist(p1,p2,pind->pos[0],pind->pos[1])-robotradius;
				if (dist < mindist)
				{
					mindist = dist;
					if (dist < robotradius)
						individual_crash = 1;
				}
			}
		}
   }
   if (mindist < robotradius)
     return(1);
   else
     return(0);
}


/* new version, return intersection point
 * checks whether a crash occurred between a robot and an obstacle
 * for robot-robot collision individual_crash is set to 1
 * esclude nella ricerca l'individuo di numero team	
 * it uses intersect_lines
 *
 */
int
check_crash(float p1,float p2,int team,float *xi,float *yi, int *n, int dead, int ghost)

{
	int i,j;
	struct individual *pind;
	struct envobject *obj;
	struct envobject *obj_tmp;			// struttura temporanea per il controllo cilindri, ed altri elementi
	float   x,y;						// coordinate del punto di intersezione corrente sbarra-
	int crash=0;						// flag che identifica l'avvenuto crash
	int jump_crash=0;					// salta il check_crash
	double  dist;
	extern int stop_when_crash;		    // we stop lifetime if a crash occurred  

	*xi=0.0f;							// Svuota il punto d'intersezione
	*yi=0.0f;	
	*n=-3;								// Non � avvenuta una collisione

	// controlla che non sia avvenuta una collisione tra il robot ed un muro. 
	for(i=0, obj = *(envobjs + SWALL);i<envnobjs[SWALL];i++, obj++) {
		// se il robot interseca una parete, ne individua il punto d'intersezione
		if (intersect_line_circle(obj->x, obj->y, obj->X, obj->Y , p1, p2, robotradius, &x, &y)) {
			crash=1;	// � avvenuto il crash
			*n=-2;		// � avvenuta una collisione con un muro
			break;
		}
	}

	// controlla che non sia avvenuta una collisione tra il robot ed un cilindro. 
	for(i=0,obj = *(envobjs + SROUND);i<envnobjs[SROUND];i++,obj++)
	{
		/* Eventuale controllo che il cilindro non appartenga all'estremit� di una delle sbarre, in tal
		   caso dovrebbe inibire il crash con il cilindro
		   Inibisce il controllo anche nel caso che il cilindro sia di colore 10 (verde), caso in cui il 
		   cilindro si trova sopra il robot. 
		*/
		if (envnobjs[BAR]>0) {		// se ci sono sbarre controlla il cilindro non appartenda ad una delle sbarre
			for(j=0, obj_tmp = *(envobjs + BAR);j<envnobjs[BAR];j++, obj_tmp++) { 
				// controlla che il cilindro appartenga ad un estremo 
				jump_crash=0;
				if (/*(stop_when_crash == 1) && */(((obj->x==obj_tmp->x) && (obj->y==obj_tmp->y))||((obj->x==obj_tmp->X) && (obj->y==obj_tmp->Y)))) {
					// In tal caso inibisce il controllo del crash
					jump_crash=1;		// non salta il crash
				}
				if (obj->subtype==10)			// Meglio tenerli separati
					jump_crash=1;
			}	  
		} else {
			if (obj->subtype==10) {				// In questo caso deve controllare solo che nn sia verde sul robot
				// In tal caso inibisce il controllo del crash
				jump_crash=1;
			}
		}

		if (!jump_crash) {
			dist = mdist(p1,p2,obj->x,obj->y)-obj->r;
			if (dist<robotradius) {							// se � avvenuta una collisione
				// se � avvenuto il crash, determina l'intersezione tra la retta che congiunge il centro del 
				// robot e del cilindro, con il robot stesso per calcolare il punto di intersezione. 
				if (intersect_line_circle(p1, p2, obj->x, obj->y ,obj->x, obj->y, obj->r, &x, &y)) {
					crash=1;
					*n=-1;			// � avvenuta una collisione con un cilindro
					break;
				}
			}
		}
	}

	// controlla che non sia avvenuta una collisione tra due robot. 
	// un robot morto non pu� interagire nella collisione
	if ((!ind_cannot_crash) && (!dead) && (!ghost)) {
		for (pind=ind,i=0; i < nteam; i++, pind++)
		{
			if ((p1 != pind->pos[0] || p2 != pind->pos[1]) && (pind->number != team) && (!pind->died_individual) && (!pind->ghost_individual)) 
			{
				dist = mdist(p1,p2,pind->pos[0],pind->pos[1])-robotradius;
				if (dist < robotradius)
				{
					// se � avvenuto il crash, determina l'intersezione tra la retta che congiunge il centro dei 
					// due robot ed il robot stesso per calcolare il punto di intersezione. 
					if (intersect_line_circle(p1, p2, pind->pos[0],pind->pos[1] , pind->pos[0], pind->pos[1], robotradius, &x, &y)) {
						individual_crash = 1;
						*n=pind->number;		// memorizza il numero del robot con cui � avvenuta la collisione
						crash=1;
						break;
					}
				}
			}
		}
	}

	// Restituisce il punto d'intersezione letto
	*xi=x;
	*yi=y;

	return (crash);
}


/*
 * controlla se si verifica una collisione tra il robot ed una delle sbarre nell'ambiente 
 * e ne effettua l'aggiornamento della posizione senza generare un crash
 */
int
check_update_bar(struct  individual  *pind, long int T1, long int T2,float (*pffitness)(struct  individual  *pind))
{
	int i,j,min_i=0;
	int touch=0;						// flag che identifica se � avvenuto il tocco con una sbarra	
	float   x,y;						// coordinate del punto di intersezione corrente sbarra-
										// perpendicolare per il centro del robot, o sbarra-altro 
										// ogggetto dell'ambiente
	float   xb,yb;						// coordinate del punto di intersezione per l'aggiornamento dei bump-sensors
	float   xi_min=0,yi_min=0,r_min=0;	// coordinate del punto di intersezione sbarra-perpendicolare a distanza minima, e raggio del cilindro
	float   xm,ym;						// coordinate del punto medio della sbarra
	float	x0,y0;						// coordinate del centro di rotazione corrente
	float   theta=0.1;					// angolo iniziale di rotazione
	float   thetar=theta;				// angolo iniziale di rotazione in radianti
	float	minx, maxx, miny, maxy;		// minime e massime coordinate di un segmento
	float   m1,q1,m2,q2;				// coefficienti m e q delle rette e delle perpendicolari
	float	xN,yN;						// estremo nord della sbarra, necessario per decidere il 
										// senso della rotazione
	float   memx,memy,memX,memY;		// memorizza la posizione originale dei vertici della sbarra
										// per effettuare la rotazione dei cilindri se necessario
	float   mem2x,mem2y,mem2X,mem2Y;	// memorizza la posizione originale dei vertici della sbarra
										// per effettuare la traslazione dei cilindri se necessario
	float   ave_mot;					// velocit� media dei motori
	double  dist,mindistbar,d1,d2;
	double  barlength;					// lunghezza della sbarra
	double  mindist;
	struct envobject *obj;
	struct envobject *obj_tmp;			// struttura temporanea per il controllo pareti, ed altri elementi
	float p1;
	float p2;
	float velmotor1;
	float velmotor2;
	float intentoutput;
	int number;

	extern void update_bump_sensors(struct individual *pind, float xbump, float ybump);	   // aggiorna i bump sensors	

	
	mindist = 9999.0;

	p1=pind->pos[0];
	p2=pind->pos[1];
	velmotor1=pind->speed[0];
	velmotor2=pind->speed[1];
	intentoutput=pind->pushing_units[0];
	number=pind->number;

	// Ciclo preliminare di ottimizzazione per evitare che effettui il calcolo della collisione
	// con la sbarra anche quando il robot � a distanze ampie dalla sbarra
	mindistbar=envdx+envdy;		// inizializzo mindistbar con le dimensioni massime possibili
  	for(i=0, obj = *(envobjs + BAR);i<envnobjs[BAR];i++, obj++)
	{
		// Determina la distanza minima del centro del robot dagli estremi di tutte le sbarre
		d1=mdist(p1,p2,obj->x,obj->y);
		// Controlla che non ci sia un cilindro all'estremit�, in tal caso sottrae il raggio del cilindro
		for(j=0, obj_tmp = *(envobjs + SROUND);j<envnobjs[SROUND];j++, obj_tmp++) {
			// controlla che il cilindro appartenga ad un estremo 
			if ((obj_tmp->x==obj->x) && (obj_tmp->y==obj->y)) 
				d1-=obj_tmp->r;		// Sottrae il raggio alla distanza dall'estremita
		}
		d2=mdist(p1,p2,obj->X,obj->Y);
		// Controlla che non ci sia un cilindro all'estremit�, in tal caso sottrae il raggio del cilindro
		for(j=0, obj_tmp = *(envobjs + SROUND);j<envnobjs[SROUND];j++, obj_tmp++) {
			// controlla che il cilindro appartenga ad un estremo 
			if ((obj_tmp->x==obj->X) && (obj_tmp->y==obj->Y)) 
				d2-=obj_tmp->r;		// Sottrae il raggio alla distanza dall'estremita
		}
		// Verifica quale utilizzare per il calcolo della distanza minima
		if ((d1<=d2) && d1<mindistbar) { 
			mindistbar=d1;
			min_i=i;			// memorizza il numero di sbarra a minima distanza
			xi_min=obj->x;		// si considera il punto d'intersezione l'estremo pi� vicino
			yi_min=obj->y;
			r_min=obj->r;
		} else if (d2<mindistbar) {
			mindistbar=d2;
			min_i=i;			// memorizza il numero di sbarra a minima distanza
			xi_min=obj->X;		// si considera il punto d'intersezione l'estremo pi� vicino
			yi_min=obj->Y;
			r_min=obj->r;	
		}
	}

	// Verifica il crash con tutte le sbarre inserite nell'ambiente
	// Verifica il crash con gli estremi delle sbarre
	for(i=0, obj = *(envobjs + BAR);i<envnobjs[BAR];i++, obj++)
	{
		if (mindistbar<=obj->h+robotradius) {	// ottimizzazione : se la distanza del centro del robot da uno degli estremi � maggiore 
											// della lunghezza della sbarra + il raggio del robot allora non effettua il computo
			/*  0.
				Determino se la distanza da uno degli estremi � inferiore alla distanza minima
				generale. In questo modo contemplo anche i casi estremi di collisione del robot
				con gli estremi della sbarra. 
			*/
			if (mindistbar < mindist)			// aggiornamento della distanza minima  
				mindist = mindistbar;
			
			if ((obj->x==obj->X) && (p2>=obj->y) && (p2<=obj->Y)) {			// robot su sbarra verticale
				dist=fabs(obj->x-p1);			// la distanza � il modulo della differenza delle ascisse
				if (dist < mindist) {   
					mindist = dist;
					min_i=i;			// memorizza il numero di sbarra a minima distanza
					xi_min=obj->x;		// congela la x del punto d'intersezione pari alla x della sbarra
					yi_min=p2;			// congela la y del punto d'intersezione pari alla y del robot
				}
			} else if ((obj->y==obj->Y) && (p1>=obj->x) && (p1<=obj->X)) {	// robot su sbarra orizzontale
				dist=fabs(obj->y-p2);			// la distanza � il modulo della differenza delle ordinate
				if (dist < mindist) {   
					mindist = dist;
					min_i=i;			// memorizza il numero di sbarra a minima distanza
					xi_min=p1;			// congela la x del punto d'intersezione pari alla x del robot
					yi_min=obj->y;		// congela la y del punto d'intersezione pari alla y della sbarra
				}
			} else {							// robot su sbarra obliqua
				/* Valutazione della collisione se la retta non � ne orizzontale ne verticale
				/*  1.
					Determino i coefficienti della retta passante per gli estremi della sbarra secondo le seguenti formule. 
					Data la coppia di punti P1=(x1,y1) e P2=(x2,y2), la retta passante per i due punti di equazione in forma esplicita 
					y=mx+q, ha i seguenti coefficienti : 
						m1= (y2-y1)/(x2-x1)
						q1= y1-x1(y2-y1)/(x2-x1)
				*/
				m1=(obj->Y-obj->y)/(obj->X-obj->x);
				q1=obj->y-obj->x*(obj->Y-obj->y)/(obj->X-obj->x);
				/*  2.
					Determino la retta in forma implicita passante per il punto x0,y0 e perpendicolare alla retta passante per la sbarra
					secondo le seguenti formule : 
					C=(x0,y0)
					m2=-1/m1
					q2=1/m1*x0+y0
				*/
				m2=-1/m1;
				q2=1/m1*p1+p2;
				/*  3.
					Determino il punto di intersezione fra la retta 1 e la retta perpendicola 2 mediante le seguenti formule : 
					x=(q2-q1)/(m1-m2)
					y=m1x+q1
					e calcolo la distanza tra il centro del robot e il punto di intersezione, dopo aver verificato che quest'ultimo appartiene
					al segmento della sbarra. 
				*/
				x=(q2-q1)/(m1-m2);
				y=(m1*x+q1);
					
				// Trova preliminarmente il minimo tra le x e le y degli estremi della sbarra
				minx=obj->x <= obj->X ? obj->x : obj->X;
				maxx=obj->x >= obj->X ? obj->x : obj->X;
				miny=obj->y <= obj->Y ? obj->y : obj->Y;
				maxy=obj->y >= obj->Y ? obj->y : obj->Y;
			
				if ((x>=minx && x<=maxx) && (y>=miny && y<=maxy)) { // controlla che il punto d'intersezione appartenga al segmento
					dist = mdist(p1,p2,x,y);	// determina la distanza tra il centro del robot e il punto di intersezione tra il segmento e
												// la proiezione della parpendicolare tracciata dal centro del robot al segmento
					if (dist < mindist) {  
						mindist = dist;
						min_i=i;			// memorizza il numero di sbarra a minima distanza
						xi_min=x;			// congela la x del punto d'intersezione a distanza minima
						yi_min=y;			// congela la y del punto d'intersezione a distanza minima
					}
				}
			}
		} // fine if
	}  // fine for
	// Verifica se la distanza minima da qualsiasi oggetto � critica, ossia � inferiore al raggio
	if (mindist < robotradius) 
		touch=(min_i+1);	// memorizza il numero della sbarra con cui � avvenuto il contatto, nota deve essere sempre <> 0
	else 
		touch=0;

	// Attiva gli eventuali sensori di bump
	if (ipar->bumpsensors>0) {							// Attiva i sensori di urto se sono definiti
		// Verifica dove � avvenuta l'intersezione con il cilindro
		if (touch)	// se � avvenuto il contatto allora aggiorna i bump sensors altrimenti li azzera
			update_bump_sensors(pind,xi_min,yi_min);
	}

	// Aggiornamento della posizione della sbarra mediante rotazione se � avvenuto il contatto
	// La barra viene bloccata se la fitness � corvids9 e il segnale intenzionale � maggiore di 0.5
	if 	(((touch) && (sincrobots) && ((pffitness == ffitness_corvids15) || (pffitness == ffitness_corvids14))) ||
		((touch) && !((pffitness == ffitness_corvids15) || (pffitness == ffitness_corvids14)))) {
		obj = (*(envobjs + BAR))+(touch-1);		// seleziona la sbarra 
		// Aggiorna le coordinate della sbarra con cui � avvenuta la collisione	
		/*	1.	Seleziona il centro di rotazione, in base alla posizione del punto di intersezione
				della perpendicolare tracciata sulla sbarra. Lo seleziona in base a quale met� della
				sbarra il robot interseca. 
		*/
		// Calcolo delle coordinate punto medio della sbarra
		xm=(obj->x+obj->X)/2;
		ym=(obj->y+obj->Y)/2;

		// Trova preliminarmente il minimo tra un estremo e il punto medio
		minx=obj->x <= xm ? obj->x : xm;
		maxx=obj->x >= xm ? obj->x : xm;
		miny=obj->y <= ym ? obj->y : ym;
		maxy=obj->y >= ym ? obj->y : ym;
		
		// controlla che il punto d'intersezione retta-perpendicolare appartenga al primo semi-segmento
		if ((xi_min>=minx && xi_min<=maxx) && (yi_min>=miny && yi_min<=maxy)) { 
			x0=obj->X;		// allora il centro di rotazione � l'estremo opposto
			y0=obj->Y;
		} else {
			x0=obj->x;		// allora il centro di rotazione � l'estremo appartenente al 
			y0=obj->y;		// semi-segmento
		}
		// controlla che non sia avvenuta una collisione con un muro, o un'altra sbarra, 
		// o un altro oggetto dell'ambiente, in tal caso il centro di rotazione diventa il punto di intersezione con la parete
		for(j=0, obj_tmp = *(envobjs + SWALL);j<envnobjs[SWALL];j++, obj_tmp++) {
			// se la sbarra interseca una parete, ne individua il punto d'intersezione
			if(intersect_lines(obj_tmp->x,obj_tmp->y,obj_tmp->X,obj_tmp->Y,obj->x,obj->y,obj->X,obj->Y,&x,&y)) {
				x0=x;	// le coordinate del centro di rotazione diventano il punto d'intersezione
				y0=y;
				xm=x0;	// il punto medio fittizio diventa l'origine della rotazione
				ym=y0;
				obj->subtype=3;		// sbarra che intercetta un ostacolo
			}
		}

		/*	2.	Decide se far ruotare la sbarra in senso orario o antiorario a seconda di quale quadrante
				occupa il robot.
		*/
		// Determina il nord della sbarra
		if (obj->y<=obj->Y)	{
			xN=obj->x;
			yN=obj->y;
		
		} else {
			xN=obj->X;
			yN=obj->Y;
		}

		// Effettua una correzione delle coordinate del nord della sbarra nel caso in cui
		// ci sia attaccato un cilindro all'estremit� in questione della sbarra
		// In questo modo evita molti casi particolari di superamento inaspettato della sbarra. 
		for(j=0, obj_tmp = *(envobjs + SROUND);j<envnobjs[SROUND];j++, obj_tmp++) {
			// controlla che il cilindro appartenga ad un estremo 
			if ((obj_tmp->x==xN) && (obj_tmp->y==yN)) {
				intersect_line_circle(obj->x, obj->y, obj->X, obj->Y , obj_tmp->x, obj_tmp->y, obj_tmp->r, &x, &y);
				xN=x;		// corregge le coordinate del nord inserendo quelle del punto di
				yN=y;		// intersezione cilindro-barra superiore (pi� a nord)
				break;
			}
		}

		// Determina l'angolo di rotazione corretto in base alla velocit� dei motori e alla 
		// lunghezza della barra
		//ave_mot = (float) ((velmotor1 / (float) 10.0) + (velmotor2 / (float) 10.0)) / (float) 2.0;
		//theta=(ave_mot / (float) 10.0);	// costanti scelte empiricamente da una base di partenza
		barlength=mdist(obj->x,obj->y,obj->X,obj->Y);
			
		// Decide il senso di rotazione in base a quattro casi : sbarra orizzontale, verticale,
		// obliqua con nord ad est, obliqua con nord ad ovest
		if (obj->x==obj->X) 			// sbarra verticale
			if ((p2>=yN)&&(p2<=ym))		// il robot si trova nella parte nord, y compresa tra punto nord e punto medio
				if (p1<=xN)				 
					thetar=theta;		// 1� quadrante, senso orario
				else
					thetar=-theta;		// 2� quadrante, antiorario
			else //if ((p2>=ym)&&(p2<=yS))// il robot si trova nella parte sud, y compresa tra punto medio e sud
				if (p1<=xN) 
					thetar=-theta;		// 3� quadrante, senso antiorario
				else
					thetar=theta;		// 4� quadrante, senso orario
		else if (obj->y==obj->Y)		// sbarra orizzontale
			if (p1>xm)					// il robot si trova nella parte est
				if (p2<=ym)				 
					thetar=theta;		// 1� quadrante, senso orario
				else
					thetar=-theta;		// 2� quadrante, antiorario
			else						// il robot si trova nella parte ovest
				if (p2<=ym) 
					thetar=-theta;		// 3� quadrante, senso antiorario
				else
					thetar=theta;		// 4� quadrante, senso orario
		else if (xN>=xm) {				// sbarra obliqua con nord orientato ad est
			if ((xi_min>=xm)&&(p1<=xi_min)&&(p2<=yi_min))		// 1� quadrante, senso orario
				thetar=theta;
			else if ((xi_min>=xm)&&(p1>xi_min)&&(p2>yi_min))	// 2� quadrante, senso antiorario
				thetar=-theta;
			else if ((xi_min<xm)&&(p1<=xi_min)&&(p2<=yi_min))	// 3� quadrante, senso antiorario
				thetar=-theta;
			else if ((xi_min<xm)&&(p1>xi_min)&&(p2>yi_min))		// 4� quadrante, senso orario
				thetar=theta;
		} else {						// sbarra obliqua con nord orientato ad ovest
			if ((xi_min<xm)&&(p1<=xi_min)&&(p2>yi_min))			// 1� quadrante, senso orario
				thetar=theta;
			else if ((xi_min<xm)&&(p1>xi_min)&&(p2<=yi_min))	// 2� quadrante, senso antiorario
				thetar=-theta;
			else if ((xi_min>=xm)&&(p1<=xi_min)&&(p2>yi_min)) 	// 3� quadrante, senso antiorario
				thetar=-theta;
			else if ((xi_min>=xm)&&(p1>xi_min)&&(p2<=yi_min))	// 4� quadrante, senso orario
				thetar=theta;
			else {					// tutti casi estremi che non ricadono nei precedenti si
									// assume per convenzione che ruota in senso antiorario
				if ((xi_min>=xm)&&(p1<=xi_min)&&(p2<=yi_min)) 
					thetar=-theta;
			}						
		}

		/*	3. Rotazione rispetto al centro x0,y0 utilizza le seguenti formule :
					x'=(x-x0)*cos(th)-(y-y0)*sen(th)+x0
					y'=(x-x0)*sen(th)+(y-y0)*cos(th)+y0
			Si considera il centro della rotazione come origine di un sistema cartesiano 
			relativo. +x0 e +y0 servono per riportare tutto in coordinate assolute
		*/
		// Memorizza la posizione originaria prima della rotazione o della traslazione degli estremi della sbarra per 
		// verificare se ci sono cilindri attaccati alla sbarra, da ruotare anche
		memx=obj->x;
		memy=obj->y;
		memX=obj->X;
		memY=obj->Y;
		
		// Aggiorna le coordinate in base alla rotazione o alla traslazione
		// Effettua una rotazione della sbarra
		obj->x=((memx-x0)*cos(thetar)-(memy-y0)*sin(thetar))+x0;
		obj->y=((memx-x0)*sin(thetar)+(memy-y0)*cos(thetar))+y0;
		obj->X=((memX-x0)*cos(thetar)-(memY-y0)*sin(thetar))+x0;
		obj->Y=((memX-x0)*sin(thetar)+(memY-y0)*cos(thetar))+y0;

		if (norotationalbar) {
			thetar=-thetar;							// inverte il senso di rotazione
			if ((x0==obj->x) && (y0==obj->y)) {	// inverte il centro di rotazione
				x0=obj->X;
				y0=obj->Y;
			} else {
				x0=obj->x;
				y0=obj->y;
			}

			mem2x=obj->x;
			mem2y=obj->y;
			mem2X=obj->X;
			mem2Y=obj->Y;

			// effettua una rotazione nell'altro senso della sbarra per simulare una traslazione
			obj->x=((mem2x-x0)*cos(thetar)-(mem2y-y0)*sin(thetar))+x0;
			obj->y=((mem2x-x0)*sin(thetar)+(mem2y-y0)*cos(thetar))+y0;
			obj->X=((mem2X-x0)*cos(thetar)-(mem2Y-y0)*sin(thetar))+x0;
			obj->Y=((mem2X-x0)*sin(thetar)+(mem2Y-y0)*cos(thetar))+y0;
		}

		// controlla che non ci siano cilindri attaccati alla sbarra, in tal caso dovr� ruotare
		// anche questi.
		for(j=0, obj_tmp = *(envobjs + SROUND);j<envnobjs[SROUND];j++, obj_tmp++) {
			// controlla che il cilindro appartenga ad un estremo 
			if ((obj_tmp->x==memx) && (obj_tmp->y==memy)) {
				obj_tmp->x=obj->x;	// aggiorna le coordinate del cilindro sulle nuove dell'estremo
				obj_tmp->y=obj->y;
			}
			// o all'altro della sbarra
			if ((obj_tmp->x==memX) && (obj_tmp->y==memY)) {
				obj_tmp->x=obj->X;  // aggiorna le coordinate del cilindro sulle nuove dell'estremo
				obj_tmp->y=obj->Y;
			}
		}
	}

	return (touch);		// restituisce il numero di sbarra con cui � avvenuto il contatto
}

/*
 * we replace the robot in the previous position
 */
void
reset_pos_from_crash(struct individual *pind)

{

    //pind->direction = pind->oldirection;
	pind->direction=rans(360);		// Cambia direzione dopo il crash
   
	pind->pos[0]    = pind->oldpos[0];
    pind->pos[1]    = pind->oldpos[1];

    pind->speed[0]=0.0f;
    pind->speed[1]=0.0f;
}

/*
 * we replace the robot in the previous position
 */
void
reset_pos_from_crash_n_pos_before(struct individual *pind)

{
	int    i;
	float  thetar;	// angolo di rotazione
	float  xi,yi;
	int	   n;


	//pind->direction = fabs(rans(360.0));
	// Restituisce un valore di direzione nell'intorno di quello presente
	// prima dell'impatto. Ossia compreso tra direction +/- 50 gradi.
	pind->direction=pind->direction+rans(50);
	
	do {	// Alla fine del loop verifica che non vi sia una collisione con mura o individui	
		// Seleziona casualmente una delle posizioni memorizzate prima dell'
		// impatto - da sistemare
		i=mrand(max_dist_rebound_after_crash);
		// Seleziona una delle posizioni memorizzate prima dell'impatto in base
		// alla velocit� media dei motori
		if (restart_from_n_pos_before == 1) { // fa ripartire il robot sulla stessa traiettoria che aveva descritto prima
			pind->pos[0]    = pind->noldpos[i][0];
			pind->pos[1]    = pind->noldpos[i][1];
		} else {	// fa ripartire il robot su una traiettoria rotata - sperimentale, da sistemare
			// effettua una rotazione nell'altro senso della sbarra per simulare una traslazione
			thetar = fabs(rans(1));	// genera un numero compreso tra 0 e 1
			pind->pos[0]=((pind->noldpos[i][0]-pind->pos[0])*cos(thetar)-(pind->noldpos[i][1]-pind->pos[0])*sin(thetar))+pind->pos[0];
			pind->pos[1]=((pind->noldpos[i][0]-pind->pos[1])*sin(thetar)+(pind->noldpos[i][1]-pind->pos[1])*cos(thetar))+pind->pos[1];
			// Se esce fuori dall'ambiente lo posiziona sul muro
			if (pind->pos[0]<0+robotradius) {
				pind->pos[0]=0+robotradius;
			}
			if (pind->pos[0]>envdx-robotradius) pind->pos[0]=envdx-robotradius;
			if (pind->pos[1]<0+robotradius) pind->pos[1]=0+robotradius;
			if (pind->pos[1]>envdy-robotradius) pind->pos[1]=envdy-robotradius;
		}
	} while (check_crash(pind->pos[0],pind->pos[1],pind->number,&xi,&yi,&n,pind->died_individual,pind->ghost_individual) == 1);
		
	pind->speed[0]=rans(14.4);
    pind->speed[1]=rans(14.4);
	/*pind->speed[0]=0.0f;			// Sembra che non cambia molto nei corvidi
    pind->speed[1]=0.0f;*/			// nel caso riattivare
}

/*
 * we replace the robot around 
 *
 * whereis=0, il robot viene riposizionato nell'intorno
 * whereis=1, il robot viene riposizionato altrove, in qualsiasi posizione dell'ambiente
 */
void
reset_pos_from_crash_around(struct individual *pind, int whereis)
{
	float newposx,newposy;
	float xi,yi;
	int   n;
	int times=0;	// serve per evitare quei rari casi di loop infiniti

	if (((!ipar->motionlessind) && (!ipar->motionlessgroup_but_nind) && (!ipar->motionlessgroup_but_nind2))
		|| ((ipar->motionlessind) && (pind->number!=ipar->motionlessind-1)) 
		|| ((ipar->motionlessgroup_but_nind) && (pind->number==ipar->motionlessgroup_but_nind-1))
		|| ((ipar->motionlessgroup_but_nind2) && (pind->number==ipar->motionlessgroup_but_nind2-1))) {			// non deve effettuare il calcolo della ripartenza per gli individui stoppati
		// Restituisce un valore di direzione nell'intorno di quello presente
		// prima dell'impatto. Ossia compreso tra direction +/- 50 gradi.
		if (whereis==0) 
			pind->direction=pind->direction+rans(50);	// genera una direzione causale nell'intorno di quella precedente
		else
			pind->direction = fabs(rans(360.0));		// genera una direzione casuale tra 0 e 360�

		// Seleziona casualmente una delle posizioni memorizzate prima dell'
		// impatto
		// Seleziona una delle posizioni memorizzate prima dell'impatto in base
		// alla velocit� media dei motori
		// da sistemare
		times=0;
		do {	// Alla fine del loop verifica che non vi sia una collisione con mura o individui	
			if (whereis==0) {	
				newposx    = pind->pos[0]+rans(max_dist_rebound_after_crash);	// non pu� essere superiore al raggio altrimenti non individua il crash
				newposy    = pind->pos[1]+rans(max_dist_rebound_after_crash);
			} else {
				newposx    = (float) fabs(rans((float) (envdx - 80))) + 40;
				newposy    = (float) fabs(rans((float) (envdy - 80))) + 40;
			}
			times++;				// evita di incappare in rari casi di loop infiniti
	/*	} while ((check_crash(newposx,newposy,pind->number,&xi,&yi,&n,pind->died_individual,pind->ghost_individual) == 1) &&
			(((startx[pind->number]!=0) && (starty[pind->number]!=0)) && 
			(!((pind->pos[0]>startx[pind->number]-2*robotradius) && (pind->pos[0]<startx[pind->number]+2*robotradius) && (pind->pos[1]>starty[pind->number]-2*robotradius) && (pind->pos[1]<starty[pind->number]+2*robotradius)))));
	*/
	/*	} while ((check_crash(newposx,newposy,pind->number,&xi,&yi,&n,pind->died_individual,pind->ghost_individual) == 1)&&
			((pind->lifecycle>60) && (startx[pind->number]!=0) && (starty[pind->number]!=0)));
	*/
		} while (times < 1000 && (check_crash(newposx,newposy,pind->number,&xi,&yi,&n,pind->died_individual,pind->ghost_individual) == 1));

		pind->pos[0]=newposx;
		pind->pos[1]=newposy;

		pind->speed[0]=rans(14.4);
		pind->speed[1]=rans(14.4);
	}	// end if
}

/*
 * update the state of the bump sensors 
 */
void
update_bump_sensors(struct individual *pind, float xbump, float ybump)
{
	int i;
	double ang;				// angolo relativo alla posizione del punto di intersezione, rispetto alla direttrice del robot
	int	cod[2];				// codifica del settore dove � presente il punto d'intersezione 

	// Traslazione delle coordinate rispetto al centro del robot
	xbump=xbump-pind->pos[0];
	ybump=ybump-pind->pos[1];

	// Determina l'arcotangente 2 della posizione del punto di intersezione del robot con l'oggetto urtato. 
	// In questo modo si determinare il quadrante dove � avvenuto l'urto. 
	ang=RADtoDGR(atan2(ybump,xbump));	

	// Normalizzazione dell'angolo rispetto all'angolo di movimento del robot
	if (ang>=pind->direction) 
		ang=ang-pind->direction;
	else 
		ang=(360+ang)-pind->direction;
	
	// Determina il quadrante dove si trova 				
	if ((ang>=ipar->bpsensors_startang_sect1) && (ang<=ipar->bpsensors_endang_sect1)) {
		cod[0]=1;
		cod[1]=1;
	} else if ((ang>ipar->bpsensors_startang_sect2) && (ang<=ipar->bpsensors_endang_sect2)) {
		cod[0]=0;
		cod[1]=1;
	} else if ((ang>ipar->bpsensors_startang_sect3) && (ang<=ipar->bpsensors_endang_sect3)) {
		cod[0]=1;
		cod[1]=0;
	} else if ((ang>ipar->bpsensors_startang_sect4) && (ang<=ipar->bpsensors_endang_sect4)) {
		cod[0]=1;
		cod[1]=1;
	}	

	if (ipar->bumpsensors>0)
		for(i=0; i < 2; i++)	
			pind->bumpsensors[i] = cod[i];
}




/*
 * update the state of the infrared sensors 
 */
void
update_infrared_s(struct individual *pind)

{

	float  p1;
	float  p2;
	double direction;
	int    i,ii;
	float  x,y;
	double dist;
	double  angle;
	int     relang;
	int     reldist;
	int     *w;
	int     *o;
	double  ang;
	struct individual *cind;
	int    t;
    struct envobject *obj;

	p1        = pind->pos[0];
	p2        = pind->pos[1];
	direction = pind->direction;

	for(i=0;i<8;i++)
	  pind->cifsensors[i] = 0.0f;

	// walls
	for(i=0, obj = *(envobjs + SWALL);i < envnobjs[SWALL];i++, obj++)
	{
	  if (p1 >= obj->x && p1 <= obj->X)
	     x = p1;
	   else
	     x = obj->x;
	  if (p2 >= obj->y && p2 <= obj->Y)
	     y = p2;
	   else
	     y = obj->y;
	   dist = mdist((float)p1,(float)p2,(float)x,(float)y);
	   if (dist < (wallcf[3] + robotradius + (wallcf[2] * wallcf[4])))     // min.dist + khepera rad. + max. dist
		{                     // 5 + robotradius + 40 = 70.5
		  if (x == p1)
		  {
		    if (y < p2)
			angle = 270.0;
		       else
			angle = 90.0;
		  }
		  else
		  {
		   if (x < p1)
		      angle = 180.0;
			else
		      angle = 0.0;
		  }
		  relang  = (int) (mangrel(angle,direction) / 2.0);
		  dist -= robotradius;          // khepera rad. = robotradius
		  if (dist < wallcf[3])        // min. dist. = 5.0
		    dist = wallcf[3];
		  reldist = (int) (dist - wallcf[3]);  /* 5 for addressing */
		  reldist = (int) reldist / wallcf[4]; /* for addressing each 2 millimeters */

		  w = *(wall + reldist);
		  w = (w + (relang * 8));
		  for(ii=0;ii<8;ii++,w++)
		   pind->cifsensors[ii] += (*w / (float) 1024.0);
		}
	}

	// round obstacles
	for(i=0,obj = *(envobjs + ROUND);i < envnobjs[ROUND];i++, obj++)
	{
		dist = mdist((float)p1,(float)p2,(float)obj->x,(float)obj->y) - obj->r;
//		ang = mang((int)p1,(int)p2,(int)obj->x,(int)obj->y,(double)dist+obj->r);
//		relang = (int) mangrel2((double)ang,(double)direction) / (int) 2.0;
		if (dist < (obstcf[3] + robotradius + (obstcf[2] * obstcf[4])))       // min.dist + khepera rad. + (dists*inter)
		{                     // 72.5 = 5 + robotradius + 40 = 72.5
			ang = mang((int)p1,(int)p2,(int)obj->x,(int)obj->y,(double)dist+obj->r);
			relang = (int) mangrel2((double)ang,(double)direction) / (int) 2.0;
			dist -= robotradius;          // khepera rad.
			if (dist < obstcf[3])  // min. dist. = 5.0
				dist = obstcf[3];
			reldist = (int) (dist - obstcf[3]);  // 5 for addressing
			reldist = (int) reldist / obstcf[4]; // for addressing each 2 millimeters

			o = *(obst + reldist);
			o = (o + (relang * 8));
			for(ii=0;ii<8;ii++,o++)
				pind->cifsensors[ii] += (*o / (float) 1024.0);
		}
	}


	// small round obstacles
	for(i=0, obj = *(envobjs + SROUND);i < envnobjs[SROUND]; i++, obj++)
	{
			dist = mdist((float)p1,(float)p2,(float)obj->x,(float)obj->y) - obj->r;
			if (dist < (sobstcf[3] + robotradius + (sobstcf[2] * sobstcf[4])))       // min.dist + khepera rad. + max. dist
			{                     // 5 + robotradius + 40 = 72.5

				ang = mang((int)p1,(int)p2,(int)obj->x,(int)obj->y,(double)dist+obj->r);
				relang = (int) mangrel2((double)ang,(double)direction) / (int) 2.0;
				dist -= robotradius;          // khepera rad. = robotradius
				if (dist < sobstcf[3])        // min. dist. = 5.0
					dist = sobstcf[3];
				reldist = (int) (dist - sobstcf[3]);
				reldist = (int) reldist / (int) sobstcf[4];

				o = *(sobst + reldist);
				o = (o + (relang * 8));
				for(ii=0;ii<8;ii++,o++)
					pind->cifsensors[ii] += (*o / (float) 1024.0);
			}
	}

	// other robots based on sround
	// we used the sample of round obstacles assuming they are similar to robots
	for (t=0,cind=ind; t<nteam; t++, cind++)
	{
	 if ((p1 != cind->pos[0] || p2 != cind->pos[1]))
	 {
	   dist = mdist((float)p1,(float)p2,(float) cind->pos[0],(float)cind->pos[1]) - robotradius;
	   if (dist < (obstcf[3] + robotradius + (obstcf[2] * obstcf[4])))
		{
		  ang = mang((int)p1,(int)p2,(int)cind->pos[0],(int)cind->pos[1],(double)dist+robotradius);
		  relang = (int) mangrel2((double)ang,(double)direction) / (int) 2.0;
		  dist -= robotradius;
		  if (dist < obstcf[3])
		     dist = obstcf[3];
		  reldist = (int) (dist - obstcf[3]);
		  reldist = (int) reldist / (int) obstcf[4];

		  o = *(obst + reldist);
		  o = (o + (relang * 8));
		  for(ii=0;ii<8;ii++,o++)
		    pind->cifsensors[ii] += (*o / (float) 1024.0);
		}
	 }
	}

	// value over limits are truncated
	for(ii = 0; ii < 8; ii++)
	{
	  if (pind->cifsensors[ii] > 1.0)
	   pind->cifsensors[ii] = 1.0f;
	}

	// add noise to infrared sensors if requested
	if (sensornoise > 0.0)
	  for(i=0; i < 8; i++)
	  {
	   pind->cifsensors[i] += rans(sensornoise);
	   if (pind->cifsensors[i] < 0.0) pind->cifsensors[i] = (float) 0.0;
	   if (pind->cifsensors[i] > 1.0) pind->cifsensors[i] = (float) 1.0;
      }
}

/*
 * update the state of the infrared sensors
 * geometrical version
 */
void
update_ginfrared_s(struct individual *pind, struct  iparameters *pipar)

{	
	int    s;
	int    w;
	float  ang;
	float  x2,y2;//fine segmento
	float  x1,y1;//punto inizio segmento 
	float  xi,yi;
	struct envobject *obj;
	double dist;
	float  val;
//extern char robotwstatus[];
	int    t;
	struct individual *cind;

	for(s=0; s < pipar->nifsensors; s++)
		pind->cifsensors[s] = 0.0f;

	for(s=0; s < pipar->nifsensors; s++)
    {
		ang = pind->direction + pind->cifang[s];
		if (ang < 0) ang += 360.0;
		if (ang > 360) ang -= 360.0;
		ToPoint(pind->pos[0], pind->pos[1], (float) ang, (float)pipar->ifrange + robotradius, &x2, &y2);
		ToPoint(pind->pos[0], pind->pos[1], (float) ang, robotradius, &x1, &y1);
     
		obj = *(envobjs + SWALL);
		for (w=0, obj = *(envobjs + SWALL); w < envnobjs[SWALL]; w++, obj++)
		{
			if (intersect_lines(x1, y1, x2, y2 , obj->x, obj->y, obj->X, obj->Y, &xi, &yi) == 1)
			{
				dist = PointsDist(x1, y1, xi, yi);
				if (dist < pipar->ifrange)
				{ 
					val = 1.0 - (dist / (float) pipar->ifrange); 
					if (pind->cifsensors[s] < val)
						pind->cifsensors[s] = val;
				}
			}
		}

		// Controlla gli infrarossi sulla sbarra
		obj = *(envobjs + BAR);
		for (w=0, obj = *(envobjs + BAR); w < envnobjs[BAR]; w++, obj++)
		{
			if (intersect_lines(x1, y1, x2, y2 , obj->x, obj->y, obj->X, obj->Y, &xi, &yi) == 1)
			{
				dist = PointsDist(x1, y1, xi, yi);
				if (dist < pipar->ifrange)
				{ 
					val = 1.0 - (dist / (float) pipar->ifrange); 
					if (pind->cifsensors[s] < val)
						pind->cifsensors[s] = val;
				}
			}
		}

		obj = *(envobjs + ROUND);
		for (w=0, obj = *(envobjs + ROUND); w < envnobjs[ROUND]; w++, obj++)
		{
			if (intersect_line_circle(x1, y1, x2, y2 , obj->x, obj->y, obj->r, &xi, &yi) == 1)
			{
				dist = PointsDist(x1, y1, xi, yi);
				if (dist < pipar->ifrange)
				{ 
					val = 1.0 - (dist / (float) pipar->ifrange); 
					if (pind->cifsensors[s] < val)
						pind->cifsensors[s] = val;
				}
			}
		}

		// Controlla gli infrarossi sui cilindri
		obj = *(envobjs + SROUND);
		for (w=0, obj = *(envobjs + SROUND); w < envnobjs[SROUND]; w++, obj++)
		{
			if (!(obj->subtype==10)) {		// Se il cilindro � sul robot inibisce il controllo degli infrarossi
				if (intersect_line_circle(x1, y1, x2, y2 , obj->x, obj->y, obj->r, &xi, &yi) == 1)
				{
					dist = PointsDist(x1, y1, xi, yi);
					if (dist < pipar->ifrange)
					{ 
						val = 1.0 - (dist / (float) pipar->ifrange); 
						if (pind->cifsensors[s] < val)
							pind->cifsensors[s] = val;
					}
				}
			}
		}

		// Controlla gli infrarossi tra i robot
		// basato sul metodo dei cilindri
		for (t=0,cind=ind; t<nteam; t++, cind++)				// scansiona tutti gli individui presenti 
																// tranne quello corrente 
		{
			if ((pind->pos[0] != cind->pos[0] || pind->pos[1] != cind->pos[1])) // esclude quello corrente
			{
				if (intersect_line_circle(x1, y1, x2, y2 , cind->pos[0], cind->pos[1], robotradius, &xi, &yi) == 1)
				{
					dist = PointsDist(x1, y1, xi, yi);
					if (dist < pipar->ifrange)
					{ 
						val = 1.0 - (dist / (float) pipar->ifrange);	// normalizza tra 0 - 1 il valore della distanza
																		// e ne determina l'opposto
						if (pind->cifsensors[s] < val)					// prende sempre il valore pi� grande
							pind->cifsensors[s] = val;					// fra tutti gli infrarossi, perch� considera l'oggetto pi� vicino
					}
				}
			}
		}
	}
}


/*
 * update the state of the light sensors 
 */
void
update_lights_s(struct individual *pind)

{

     float  p1;
     float  p2;
     double direction;
     int    i;
     int    ii;
     double ang;
     int    relang;
     double dist;
     int    reldist;
     int    *o;
     struct envobject *obj;

     p1        = pind->pos[0];
     p2        = pind->pos[1];
     direction = pind->direction;

     for(i=0;i<8;i++)
	  pind->clightsensors[i] = 0.0f;

     for(i=0, obj = *(envobjs + LIGHT);i < envnobjs[LIGHT];i++, obj++)
	   {
	      dist = mdist((float)p1,(float)p2,(float) obj->x,(float)obj->y);
	      ang = mang((int)p1,(int)p2,(int)obj->x,(int)obj->y,(double)dist);
	      relang = (int) mangrel2((double)ang,(double)direction) / (int) 2.0;
	      dist  -= robotradius;
              if (obj->type > 0)                  // sensitivity magnifier
		  dist = dist / (float) (obj->type + 1);
	      if (dist <= (lightcf[3] + robotradius + (lightcf[2] * lightcf[4])))    // 560 = 13 + 27 + 520 (26*20)
		{
		   if (dist < lightcf[3])
		     dist = lightcf[3];
		   reldist = (int) (dist - lightcf[3]); // 13 for adressing
		   reldist = (int) (dist / lightcf[4]); // for addressing each 20mm       o = *(light + reldist);
		   o = *(light + reldist);
		   o = (o + (relang * 8));
		   for(ii=0;ii<8;ii++,o++)
		     pind->clightsensors[ii] += (*o / (float) 1024.0);
		}
	   }
      // we normalize between limits
      for(i = 0; i < 8; i++)
	  {
	     if (pind->clightsensors[i] > 1.0)
	       pind->clightsensors[i] = 1.0f;
	  }
      // noise
      if (sensornoise > 0.0)
	  for(i=0; i < 8; i++)
	    {
	      pind->clightsensors[i] += rans(sensornoise);
	      if (pind->clightsensors[i] < 0.0) 
		 pind->clightsensors[i] = (float) 0.0;
	      if (pind->clightsensors[i] > 1.0) 
		 pind->clightsensors[i] = (float) 1.0;
	    }

}

/*
 * update the state of signal sensors 
 */
void
update_signal_s(struct individual *pind, struct  iparameters *pipar)

{

    float p1;
    float p2;
    double direction;
    int i;
    int t;
    int s;
    int f;
    float fr;
    double dist;
    int relang;
    double ang;
    struct individual *cind;
    int nsensor;

    p1        = pind->pos[0];
    p2        = pind->pos[1];
    direction = pind->direction;

    for(i=0;i< pipar->signalss;i++)
	 {
	   pind->detected_signal[i][0] = 0.0f;
	   pind->detected_signal[i][1] = 999999.0f;
	 }
    for (t=0, cind=ind; t<nteam; t++, cind++)
	if (pind->n_team != cind->n_team)
	 {
	   dist = mdist((float)pind->pos[0],(float)pind->pos[1],(float) cind->pos[0],(float)cind->pos[1]) - robotradius;
           if (dist < (double) max_dist_attivazione)
		   {
		     if (pipar->signalss == 1)
		     // non-directional signals
		     {
		       if (dist < pind->detected_signal[0][1])
		        for(s=0;s < pipar->signalso; s++)
			{
                         pind->detected_signal[s][0] =  cind->signal_motor[s];  
                         pind->detected_signal[s][1] = (float) dist;
			}
		     }
		     else
		     // directional signals
		     {
		       ang = mang((int)pind->pos[0],(int)pind->pos[1],(int)cind->pos[0],cind->pos[1],dist);
		       relang = (int) mangrel2((double)ang,(double)direction);
		       if (relang > 315 || relang <= 45)
		       {
		        if (dist < pind->detected_signal[0][1])
		         for(s=0;s < pipar->signalso; s++)
			 {
                          pind->detected_signal[s*pipar->signalss+0][0] = cind->signal_motor[s]; 
                          pind->detected_signal[s*pipar->signalss+0][1] = (float) dist;
			 }
		       }
		       if (relang > 45 && relang <= 135)
		       {
		        if (dist < pind->detected_signal[1][1])
		         for(s=0;s < pipar->signalso; s++)
			 {
                          pind->detected_signal[s*pipar->signalss+1][0] = cind->signal_motor[s]; 
                          pind->detected_signal[s*pipar->signalss+1][1] = (float) dist;
			 }
		       }
		       if (relang > 135 && relang <= 225)
		       {
		        if (dist < pind->detected_signal[2][1])
		         for(s=0;s < pipar->signalso; s++)
			 {
                          pind->detected_signal[s*pipar->signalss+2][0] = cind->signal_motor[s]; 
                          pind->detected_signal[s*pipar->signalss+2][1] = (float) dist;
			 }
		       }
		       if (relang > 225 && relang <= 315)
		       {
		        if (dist < pind->detected_signal[3][1])
		         for(s=0;s < pipar->signalso; s++)
			 {
                          pind->detected_signal[s*pipar->signalss+3][0] = cind->signal_motor[s]; 
                          pind->detected_signal[s*pipar->signalss+3][1] = dist;
			 }
		       }
		     }

		   }

		}
	for(s=0,nsensor=0;s < pipar->signalss*pipar->signalso; s++, nsensor++)
		{
                 pind->csignals[nsensor] = pind->detected_signal[s][0];
		 if (pipar->signalssb == 1)
                  if (pind->csignals[nsensor] > 0.5)
                    pind->csignals[nsensor] = 1.0;
		  else
                    pind->csignals[nsensor] = 0.0;
		 for (f=0,fr = 0.0;f < pipar->signalssf; f++, fr += (1.0 / (float) pipar->signalssf))
		   if (pind->csignals[nsensor] >= fr && pind->csignals[nsensor] < (fr + (1.0 / (float) pipar->signalssf)))
                     pind->csignals[nsensor+1+f] = 1.0;
		   else
                     pind->csignals[nsensor+1+f] = 0.0;
		}

}


void  update_eyes(struct individual *pind, struct  iparameters *pipar)
{
	// VISIONE TOKEN
	/*for(i=0;i< env[cworld].nfoodzones;i++)
	{
		if ((env[cworld].foodzones[i][2] > 0 && env[cworld].foodzones[i][3] != predator) || (env[cworld].foodzones[i][3] == predator && predatormov == 1))//---> se il diametro della zona � > 0
		{
			typezone = (int) (env[cworld].foodzones[i][3]) - 1;

			dist = mdist((float)p1,(float)p2,(float)env[cworld].foodzones[i][0],(float)env[cworld].foodzones[i][1]); 
			ang = mang((int)p1,(int)p2,(int)env[cworld].foodzones[i][0],(int)env[cworld].foodzones[i][1],(double)dist);
			relang = (int) mangrel2((double)ang,(double)direction) / (int) 2.0;

			if (dist <= (lightcf[3] + 27 + (lightcf[2] * lightcf[4])))    // 560 = 13 + 27 + 520 (26*20)  
			{
				dist  -= 27;                         // khepera rad
				if (dist < lightcf[3])
					dist = lightcf[3];    
				reldist = (int) (dist - lightcf[3]); // 13 for adressing
				reldist = (int) (dist / lightcf[4]); // for addressing each 20mm       o = *(light + reldist); 
				o = *(light + reldist); 
				o = (o + (relang * 8));	
			   
				for(ii=0;ii<8;ii++,o++)
				{
					if (env[cworld].foodzones[i][3] == predator)
					{
					    if (ii==1)	pind->inputbody_sx[typezone] += (*o / (float) 1024.0);// * 1.5f;
						if (ii==2)	pind->inputbody_dx[typezone] += (*o / (float) 1024.0);// * 1.5f;
						if (ii==3)	pind->inputbody_sx[typezone] += (*o / (float) 1024.0);// * 1.5f;
						if (ii==4)	pind->inputbody_dx[typezone] += (*o / (float) 1024.0);// * 1.5f;
					}
					else
					{
						if (ii==1)	pind->inputbody_sx[typezone] += ((*o / (float) 1024.0) / (decrsfood)); //* (maxdzone / env[cworld].foodzones[i][2])));//era  1.5f
						if (ii==2)	pind->inputbody_dx[typezone] += ((*o / (float) 1024.0) / (decrsfood)); //* (maxdzone / env[cworld].foodzones[i][2])));
						if (ii==3)	pind->inputbody_sx[typezone] += ((*o / (float) 1024.0) / (decrsfood)); //* (maxdzone / env[cworld].foodzones[i][2])));//era  1.5f
						if (ii==4)	pind->inputbody_dx[typezone] += ((*o / (float) 1024.0) / (decrsfood)); //* (maxdzone / env[cworld].foodzones[i][2])));
					}
				}
			  
				// we normalize between limits
	            if (pind->inputbody_sx[typezone] > 1.0f)  pind->inputbody_sx[typezone] = 1.0f;  
	            if (pind->inputbody_dx[typezone] > 1.0f)  pind->inputbody_dx[typezone] = 1.0f;  
				if (pind->inputbody_sx[typezone] < minsensor)  pind->inputbody_sx[typezone] = 0.0f;
				if (pind->inputbody_dx[typezone] < minsensor)  pind->inputbody_dx[typezone] = 0.0f;
			}
		}
	} //---> for					
 	     
		for(ii=0;ii<pipar->ninputbody;ii++,nsensor++)
		{
		 if (test_flag[nsensor] == 1)  pind->inputbody_sx[ii] = test_val[nsensor];
 		 pind->input[nsensor] = pind->inputbody_sx[ii]; 
		 nsensor += 1;
		 if (test_flag[nsensor] == 1)  pind->inputbody_dx[ii] = test_val[nsensor];
		 pind->input[nsensor] = pind->inputbody_dx[ii]; 

		}
		


/*
// VISIONE ROBOT

	if (pipar->mvision == 1)  //-----------------------> VEDO IL MASCHIO
	{

	for (t=0,cind=ind; t<nindividuals; t++, cind++)
	{		 
	if ((p1 != cind->pos[0] || p2 != cind->pos[1]))
	 { 
	 if(cind->sesso == 1)
	 {
	   dist = mdist((float)p1,(float)p2,(float) cind->pos[0],(float)cind->pos[1]) - ROBOTRADIUS; 
	   if (dist < (lightcf[3] + ROBOTRADIUS + (lightcf[2] * lightcf[4])))       
		{                          
		  ang = mang((int)p1,(int)p2,(int)cind->pos[0],(int)cind->pos[1],(double)dist+ROBOTRADIUS);
		  relang = (int) mangrel2((double)ang,(double)direction) / (int) 2.0;
		  dist -= ROBOTRADIUS;          
		  if (dist < lightcf[3])        
		     dist = lightcf[3]; 
		  reldist = (int) (dist - lightcf[3]); 
		  reldist = (int) reldist / (int) lightcf[4]; 
						
		  o = *(light + reldist);
		  o = (o + (relang * 8));
		  
		  for(ii=0;ii<8;ii++,o++)
		  {
			  if (ii==1)	pind->mvision_sx += (*o / (float) 1024.0) / (visio);// * (float)nindividuals); // (decrsfood * (maxdzone / env[cworld].foodzones[i][2])));//era  1.5f
			  if (ii==2)	pind->mvision_dx += (*o / (float) 1024.0) / (visio);// * (float)nindividuals); // (decrsfood * (maxdzone / env[cworld].foodzones[i][2])));
			  if (ii==3)	pind->mvision_sx += (*o / (float) 1024.0) / (visio);// * (float)nindividuals); // (decrsfood * (maxdzone / env[cworld].foodzones[i][2])));//era  1.5f
			  if (ii==4)	pind->mvision_dx += (*o / (float) 1024.0) / (visio);// * (float)nindividuals); // (decrsfood * (maxdzone / env[cworld].foodzones[i][2])));
		  }
	   }
	 }
	 }
	}
		  
		  if (pind->mvision_dx > 1)
			  pind->mvision_dx = 1.0f;
		  if (pind->mvision_dx < minsensor)
			  pind->mvision_dx = 0.0f;
		  
		  if (pind->mvision_sx > 1)
			  pind->mvision_sx = 1.0f;
		  if (pind->mvision_sx < minsensor)
			  pind->mvision_sx = 0.0f;
      	  
		  if (check_crash_robot(pind->pos[0], pind->pos[1]))
		  {
			  pind->mvision_dx = 0.0f;
			  pind->mvision_sx = 0.0f;
		  }
		
		if (test_flag[nsensor] == 1)
		{
			pind->input[nsensor] = test_val[nsensor];
			nsensor++;
		}
		else
		{
			pind->input[nsensor] = pind->mvision_sx;
			nsensor++;
		}
		if (test_flag[nsensor] == 1)
		{
			pind->input[nsensor] = test_val[nsensor];
			nsensor++;
		}
		else
		{
			pind->input[nsensor] = pind->mvision_dx;
			nsensor++;
		}
	}

	//----------------------------------->> VISIONE SESSO f
	
	if (pipar->fvision == 1)     //-----------------------> VEDO LA FEMMINA
	{

	for (t=0,cind=ind; t<nindividuals; t++, cind++)
	{		 
	if ((p1 != cind->pos[0] || p2 != cind->pos[1]))
	 { 
	 if(cind->sesso == 0)
	 {
	   dist = mdist((float)p1,(float)p2,(float) cind->pos[0],(float)cind->pos[1]) - ROBOTRADIUS; 
	   if (dist < (lightcf[3] + ROBOTRADIUS + (lightcf[2] * lightcf[4])))       
		{                          
		  ang = mang((int)p1,(int)p2,(int)cind->pos[0],(int)cind->pos[1],(double)dist+ROBOTRADIUS);
		  relang = (int) mangrel2((double)ang,(double)direction) / (int) 2.0;
		  dist -= ROBOTRADIUS;          
		  if (dist < lightcf[3])        
		     dist = lightcf[3]; 
		  reldist = (int) (dist - lightcf[3]); 
		  reldist = (int) reldist / (int) lightcf[4]; 
						
		  o = *(light + reldist);
		  o = (o + (relang * 8));
		  
		  for(ii=0;ii<8;ii++,o++)
		  {
			  if (ii==1)	pind->fvision_sx += (*o / (float) 1024.0) / (visio);// * (float)nindividuals); // (decrsfood * (maxdzone / env[cworld].foodzones[i][2])));//era  1.5f
			  if (ii==2)	pind->fvision_dx += (*o / (float) 1024.0) / (visio);// (float)nindividuals); // (decrsfood * (maxdzone / env[cworld].foodzones[i][2])));
			  if (ii==3)	pind->fvision_sx += (*o / (float) 1024.0) / (visio);// (float)nindividuals); // (decrsfood * (maxdzone / env[cworld].foodzones[i][2])));//era  1.5f
			  if (ii==4)	pind->fvision_dx += (*o / (float) 1024.0) / (visio);// (float)nindividuals); // (decrsfood * (maxdzone / env[cworld].foodzones[i][2])));
		  }
	   }
	 }
	 }
	}
		  
		  if (pind->fvision_dx > 1)
			  pind->fvision_dx = 1.0f;
		  if (pind->fvision_dx < minsensor)
			  pind->fvision_dx = 0.0f;
		  
		  if (pind->fvision_sx > 1)
			  pind->fvision_sx = 1.0f;
		  if (pind->fvision_sx < minsensor)
			  pind->�fvision_sx = 0.0f;

      	if (check_crash_robot(pind->pos[0], pind->pos[1]))
		  {
			  pind->fvision_dx = 0.0f;
			  pind->fvision_sx = 0.0f;
		  }	 
			  
		if (test_flag[nsensor] == 1)
		{
			pind->input[nsensor] = test_val[nsensor];
			nsensor++;
		}
		else
		{
			pind->input[nsensor] = pind->fvision_sx;
			nsensor++;
		}
		if (test_flag[nsensor] == 1)
		{
			pind->input[nsensor] = test_val[nsensor];
			nsensor++;
		}
		else
		{
			pind->input[nsensor] = pind->fvision_dx;
			nsensor++;
		}
	}
*/
}








void  update_nxtcam(struct individual *pind, struct  iparameters *pipar)
{

    int i;
    int ii;
    int f;
    int ff;
    int c;
    int r;
    int a1,a2;
    float x_cam;
    float y_cam;
    float dir_cam;
    float objxa, objya;
    float objxb, objyb;
    double dist;
    double dista;
    double distb;
    double ang;
    double anga;
    double angb;
    double ang1, ang2;
    double angs;
    double angr;
    double a;
    double da;
    float  av[3];
    struct envobject *obj;
    struct envobject **cenvobjs;
    float  objax, objay;
    float  objbx, objby;
    float  cameradist[360];
    char   message[256];
    int    npixels;
    struct individual *cind;
    int    skip;
	int    tmpcolor;

    //initialize 1-degree photoreceptors state
    for(c=0; c < pipar->cameracolors; c++)
     for(f=0; f < pipar->cameraviewangle; f++)
     {
      pind->camera[c][f] = 0.0;
      cameradist[f] = 99999.0;
     }

    // camera located on the barycentre of the robot
    x_cam    = pind->pos[0];
    y_cam    = pind->pos[1];
    dir_cam  = pind->direction;

    cenvobjs = envobjs;
    // we only check round and sround objects
    for (i=0; i < NOBJS; i++)       
     {
      if (i == 0 || i == 1 || i == 2 || i == 5)
      for (ii=0, obj = *(cenvobjs + i); ii < envnobjs[i]; ii++, obj++)
       {
        skip = 0;
        // robots objects 
        if (obj->type == ROBOT)
	  if (ii < nteam && ii != pind->n_team)
	   {
	     cind = (ind + ii);
             obj->x = cind->pos[0];
             obj->y = cind->pos[1];
             obj->r = robotradius;
           }
	  else
	   {
             skip = 1;
           }
         // compute the distance between the camera and the object
         if (obj->type == SWALL)
	    dist = (mdist(x_cam, y_cam, obj->x, obj->y) + mdist(x_cam, y_cam, obj->X, obj->Y)) / 2.0;
	   else
	    dist = mdist(x_cam, y_cam, obj->x, obj->y);
	 if (dist < pipar->cameradistance)
	  {
          // cylindrical surface
		  // i cilindri posizionati sopra il robot non sono visti dalla cam, i cilindri non devono 
		  // essere inscritti nel robot
	  if ((obj->type == SROUND && (!((obj->subtype==10) && (pind->number==obj->nind)))) || obj->type == ROUND || obj->type == ROBOT)
	   {
        ang   = mang((int) x_cam,(int) y_cam, (int) obj->x, (int) obj->y, dist);
	    objxa = SetX(obj->x, obj->r, (double) ang + 90.0);
        objya = SetY(obj->y, obj->r, (double) ang + 90.0);
	    dista = mdist(x_cam,y_cam,objxa,objya);  
	    anga  = mang((int)x_cam,(int)y_cam,(int)objxa,(int)objya,dista);
	    angs  = fabs(ang-anga);
	    // the next line solve the limit problem
            if (angs > 180)
	      angs = 360 - angs;
	    angs = angs * 2;
	    angr = ang - pind->direction;
	    if (angr < 0)
	     angr += 360;
	    //sprintf(robotwstatus, "t%d abs %.0f abs2 %.0f dir %.0f span %.0f rel %.0f",obj->type, ang, anga, pind->direction, angs, angr);
           }

          // linear surface
	  if (obj->type == SWALL)
	   {
	    dista  = mdist(x_cam, y_cam, obj->x, obj->y);
	    distb  = mdist(x_cam, y_cam, obj->X, obj->Y);
        anga   = mang((int) x_cam,(int) y_cam, (int) obj->x, (int) obj->y, dista);
	    angb   = mang((int) x_cam,(int) y_cam, (int) obj->X, (int) obj->Y, distb);
	    angs   = fabs(angb-anga);
            if (angs > 180)
	      angs = 360 - angs;
	    if (((angb - anga) == angs) || (((angb + 360) - anga) == angs))
	      ang = anga + (angs / 2);
	     else
	      ang = angb + (angs / 2);
	    if (ang < 0) 
	      ang += 360;
	    angr = ang - pind->direction;
	    if (angr < 0)
	     angr += 360;
	    //sprintf(robotwstatus, "t%d abs %.0f %.0f dir %.0f span %.0f a %.0f rel %.0f",obj->type, anga, angb, pind->direction, angs, ang, angr);
           }

	   if (skip == 0)
	   for(f=0, a = angr - (angs / 2); f < angs; f++, a += 1.0)
	   {
            if (a < 0) a += 360;
	    if (a > 360) a -= 360;
	    ff = a;
	    a1 = (360 - (pipar->cameraviewangle / 2));
            a2 = (pipar->cameraviewangle / 2);
	    if (ff >= a1 && ff < 360 && dist < cameradist[ff-a1])
	      for(c=0;c < pipar->cameracolors;c++)
	       {
			   // Adattamento per far funzionare il sistema colori
			   tmpcolor = 0;
			   if ((obj->c[0]==255) && (obj->c[1]==0) && (obj->c[2]==0)) {
				   tmpcolor=1;	// Codifica colore verde
			   }
			   if ((obj->c[0]==0) && (obj->c[1]==255) && (obj->c[2]==0)) {
				   tmpcolor=2;	// Codifica colore rosso			   
			   }

			   // per cilindri posizionati sul robot non deve vederli              
			   if(!((obj->subtype==10) && (pind->number==obj->nind))) 
				   pind->camera[c][ff-a1] = tmpcolor;
                cameradist[ff-a1] = dist;
	       }
	    if (ff >= 0 && ff < a2 && dist < cameradist[ff+a2])
	      for(c=0;c < pipar->cameracolors;c++)
	       {
			   // Adattamento per far funzionare il sistema colori
			   tmpcolor = 0;
			   if ((obj->c[0]==255) && (obj->c[1]==0) && (obj->c[2]==0)) {
				   tmpcolor=1;	// Codifica colore verde
			   }
			   if ((obj->c[0]==0) && (obj->c[1]==255) && (obj->c[2]==0)) {
				   tmpcolor=2;	// Codifica colore rosso			   
			   }

			   // per cilindri posizionati sul robot non deve vederli 
			   if(!((obj->subtype==10) && (pind->number==obj->nind)))    
					pind->camera[c][ff+a2] = tmpcolor;
                cameradist[ff+a2] = dist;
	       }
	   }

          } // dist < cameradistance
       }
     }

	//fine nxtcam manca l'estrazione dei blob che la facciamo subito subito
	for (i=0;i<7;i++)
	{
		for(ii=0;ii<3;ii++)
		pind->trackedBlob[i][ii]=0;//resetting blob list

	}
	//blob extraction simple way: we supppose to have on blob per color
	for(i=0;i<360;i++)
	{
		if(pind->camera[0][i]==1)  //red color
		{
			pind->trackedBlob[0][0]+=i;
			pind->trackedBlob[0][1]++; //blob width
			pind->trackedBlob[0][2]=1; //blob color red
		}

		if(pind->camera[0][i]==2) //green color
		{
			pind->trackedBlob[1][0]+=i;
			pind->trackedBlob[1][1]++;  //blob width
			pind->trackedBlob[1][2]=2; //blob color green	
		}
	}
	for(i=0;i<8;i++)
	{
		if(pind->trackedBlob[i][1]>0)
		{
			pind->trackedBlob[i][0]=pind->trackedBlob[i][0]/pind->trackedBlob[i][1];
			//normalizing to have same result from nxtcam, in this way we have to apply
			//the same algorithm
			pind->trackedBlob[i][0]=(pind->trackedBlob[i][0]/42.0)*176.0; //camera spec: 176 x 144 pixels
			pind->trackedBlob[i][1]=(pind->trackedBlob[i][1]/42.0)*176.0;
		}


	}


}


void  update_RGBColorRetina(struct individual *pind, struct  iparameters *pipar)
{
	double dist,mindist;				// distanze per il calcolo della food zone di minima distanza
	int team,n;
	struct individual *cind;
	struct individual *mind;			// individuo a distanza minima

    // Veriabili necessarie al vecchio codice di evorobot
	int i, ii, f, ff, c;
    int a1,a2;
    float x_cam, y_cam, dir_cam;
    float objxa, objya, objxb, objyb;
    double dista, distb;
    double ang, anga, angb, angs, angr;
    double a;
    struct envobject *obj;
    struct envobject **cenvobjs;
    float  cameradist[360];
    int    skip;
	int    tmpcolor;
	int	   rgbcolorretinapartialviewangle;
	int	   retinacolors=3;			// RGB
	int	   hasseen;					// flag indicating indiviual has seen something
	
	if (pipar->rgbcolorretina==1) {			// attiva semplicemente il neurone del colore
		// Gli indvidui che possono percepire da lontanto (leaders), nel caso fosse espresso non possono vedere la posizione ed il colore degli altri
		if ((!pipar->onlyfewcanperceive)||(!((ipar->onlyfewcanperceive) && (check_perceptualinds(pind->number)) && (pipar->percepindcannotseeother)))) {
			// Determina l'individuo con distanza minima da pind
			mindist=envdx+envdy;
			mind=pind;
			for(n=0, cind=ind; n < nteam; n++, cind++) {	// seleziona tutti gli altri individui 
				if (cind!=pind) {		// verifica che sia differente da pind (il corrente)
					// Determina la distanza tra l'individuo corrente e uno dei trovato nell'intorno
					dist=mdist(pind->pos[0],pind->pos[1],cind->pos[0],cind->pos[1]);
					if (dist<=mindist) { 
						mindist=dist;					// la nuova distanza minima � dist
						mind=cind;						// memorizza l'individuo a distanza minima	
					}
				}
			}

			if (!pipar->rgbcolorretinagrayscale) {
				// Aggiorna i neuroni percettivi del colore con quelli dell'individuo pi� vicino
				pind->RColorPerceived[0] = mind->RColor;
				pind->GColorPerceived[0] = mind->GColor;
				pind->BColorPerceived[0] = mind->BColor;
			} else {
				// Formula utilizzata da Adobe Photoshop per covertire RGB in toni di grigio : 
				// Gray=0,33 * RR + 0,50 * GG + 0,17 * BB
				pind->GrayScaleColorPerceived[0] = 0.33*mind->RColor + 0.50*mind->GColor + 0.17*mind->BColor;
			}		
		} else {
			if (!pipar->rgbcolorretinagrayscale) {
				// Azzera i neuroni percettivi del colore con quelli dell'individuo pi� vicino
				pind->RColorPerceived[0] = 0.0;
				pind->GColorPerceived[0] = 0.0;
				pind->BColorPerceived[0] = 0.0;
			} else {
				pind->GrayScaleColorPerceived[0] = 0.0;
			}		
		}
	} else if (pipar->rgbcolorretina>1) {		// implementa una retina lineare a colori
		// Al momento utilizzo il codice funzionante (testato) di evorobot, anche se va riscritto da zero

		//initialize 1-degree photoreceptors state
		for(c=0; c < retinacolors; c++)
			for(f=0; f < pipar->rgbcolorretinaviewangle; f++)
			{
				pind->camera[c][f] = 0.0;
				cameradist[f] = 99999.0;
			}

	    // camera located on the barycentre of the robot
		x_cam    = pind->pos[0];
		y_cam    = pind->pos[1];
		dir_cam  = pind->direction;

		cenvobjs = envobjs;
		// we only check round and sround objects
		for (i=0; i < NOBJS; i++)       
		{
			if (i == 0 || i == 1 || i == 2 || i == 5)
				for (ii=0, obj = *(cenvobjs + i); ii < envnobjs[i]; ii++, obj++)
				{
					skip = 0;
					// robots objects 
					if (obj->type == ROBOT)
						if (ii < nteam && ii != pind->n_team)
						{
							cind = (ind + ii);
							obj->x = cind->pos[0];
							obj->y = cind->pos[1];
							obj->r = robotradius;
						}
						else
						{
							skip = 1;
						}
					// compute the distance between the camera and the object
					if (obj->type == SWALL)
						dist = (mdist(x_cam, y_cam, obj->x, obj->y) + mdist(x_cam, y_cam, obj->X, obj->Y)) / 2.0;
					else
						dist = mdist(x_cam, y_cam, obj->x, obj->y);
				
					if ((dist < pipar->rgbcolorretinamaxdistance) && (dist>0))	// (dist>2*robotradius)  con l'altra condizione esclude il caso del robot stesso che ha distanza 0
					{
						// cylindrical surface
						// i cilindri posizionati sopra il robot non sono visti dalla cam, i cilindri non devono 
						// essere inscritti nel robot
						if ((obj->type == SROUND && (!((obj->subtype==10) && (pind->number==obj->nind)))) || obj->type == ROUND || obj->type == ROBOT)
						{
							if (obj->subtype!=9) {
								ang   = mang((int) x_cam,(int) y_cam, (int) obj->x, (int) obj->y, dist);
								objxa = SetX(obj->x, obj->r, (double) ang + 90.0);
								objya = SetY(obj->y, obj->r, (double) ang + 90.0);
								dista = mdist(x_cam,y_cam,objxa,objya);  
								anga  = mang((int)x_cam,(int)y_cam,(int)objxa,(int)objya,dista);
								angs  = fabs(ang-anga);
								// the next line solve the limit problem
								if (angs > 180)
									angs = 360 - angs;
								angs = angs * 2;
								angr = ang - pind->direction;
								if (angr < 0)
									angr += 360;
								//sprintf(robotwstatus, "t%d abs %.0f abs2 %.0f dir %.0f span %.0f rel %.0f",obj->type, ang, anga, pind->direction, angs, angr);
							} else skip=1;
						} else skip=1;				// permette di uscire da alcune situazioni di stallo in cui non viene calcolato angs
						// linear surface
						if (obj->type == SWALL)
						{
						    dista  = mdist(x_cam, y_cam, obj->x, obj->y);
							distb  = mdist(x_cam, y_cam, obj->X, obj->Y);
							anga   = mang((int) x_cam,(int) y_cam, (int) obj->x, (int) obj->y, dista);
							angb   = mang((int) x_cam,(int) y_cam, (int) obj->X, (int) obj->Y, distb);
							angs   = fabs(angb-anga);
							if (angs > 180)
								angs = 360 - angs;
							if (((angb - anga) == angs) || (((angb + 360) - anga) == angs))
								ang = anga + (angs / 2);
							else
								ang = angb + (angs / 2);
							if (ang < 0) 
								ang += 360;
							angr = ang - pind->direction;
							if (angr < 0)
								angr += 360;
							//sprintf(robotwstatus, "t%d abs %.0f %.0f dir %.0f span %.0f a %.0f rel %.0f",obj->type, anga, angb, pind->direction, angs, ang, angr);
						}
						if (skip == 0)
							for(f=0, a = angr - (angs / 2); f < angs; f++, a += 1.0)
							{
								if (a < 0) a += 360;
								if (a > 360) a -= 360;
								ff = a;
								a1 = (360 - (pipar->rgbcolorretinaviewangle / 2));
								a2 = (pipar->rgbcolorretinaviewangle / 2);
								if (ff >= a1 && ff < 360 && dist < cameradist[ff-a1])
									for(c=0;c < retinacolors;c++)
									{
										// per cilindri posizionati sul robot non deve vederli              
										if(!((obj->subtype==10) && (pind->number==obj->nind))) 
											if (obj->subtype!=9) pind->camera[c][ff-a1] = obj->c[c];//tmpcolor;
										cameradist[ff-a1] = dist;
									}
								if (ff >= 0 && ff < a2 && dist < cameradist[ff+a2])
									for(c=0;c < retinacolors;c++)
									{
										// per cilindri posizionati sul robot non deve vederli 
										if(!((obj->subtype==10) && (pind->number==obj->nind)))    
											if (obj->subtype!=9) pind->camera[c][ff+a2] = obj->c[c];//tmpcolor;
										cameradist[ff+a2] = dist;
									}
							}	
					} // dist < rgbcolorretinamaxdistance
				}
		}
		
		if (!pipar->rgbcolorretinagrayscale) {
			if ((test_measure_leadership) || (test_measure_leadership_eco))
				hasseen=0;

			// Compattazione della retina per inserirla nei neuroni disponibili	
			// Azzera prima tutta la retina
			for (ii=0;ii<pipar->rgbcolorretina;ii++) {		// scansiona tutti i neuroni percettivi
				pind->RColorPerceived[ii] = 0.0;
				pind->GColorPerceived[ii] = 0.0;
				pind->BColorPerceived[ii] = 0.0;
			}

			if (!pind->died_individual) { 
				rgbcolorretinapartialviewangle=pipar->rgbcolorretinaviewangle/pipar->rgbcolorretina;
				for (ii=0;ii<pipar->rgbcolorretina;ii++) {		// scansiona tutti i neuroni percettivi
					for (i=0+ii*rgbcolorretinapartialviewangle;i<rgbcolorretinapartialviewangle+ii*rgbcolorretinapartialviewangle;i++) {	// scansiona il campo visivo
						if (((pind->camera[0][i]>0.0) || (pind->camera[1][i]>0.0) || (pind->camera[2][i]>0.0)) && // il primo oggetto nel campo visivo parziale lo inserisce nel neurone relativo
						   (!((pind->camera[0][i]==RColorPreyDeadInd) && (pind->camera[1][i]==GColorPreyDeadInd) && (pind->camera[2][i]==BColorPreyDeadInd))))	// i morti non possono essere visti	
						{ 
							
							if ((test_measure_leadership==1) || (test_measure_leadership_eco==1))// se si considera ovunque
								hasseen=1;														// ha visto qualcosa

							if (((test_measure_leadership==2) || (test_measure_leadership_eco==2)) && (!(read_ground(pind) > 0)))		// se si considera solo la visione fuori dalla food zone
								hasseen=1;														// ha visto qualcosa
							
							pind->RColorPerceived[ii] = pind->camera[0][i];
							pind->GColorPerceived[ii] = pind->camera[1][i];
							pind->BColorPerceived[ii] = pind->camera[2][i];

							// Se � disattivata la visione del colore blue da parte di una determinata specie allora azzera la visione del Blue
							if ((ipar->rgbcolorretinabluedisabledrace>0) && (pind->race==ipar->rgbcolorretinabluedisabledrace-1) &&
							   (pind->RColorPerceived[ii] == 0.0) && (pind->GColorPerceived[ii] == 0.0) && (pind->BColorPerceived[ii] == 255.0)) {
								pind->RColorPerceived[ii] = 0.0;
								pind->GColorPerceived[ii] = 0.0;
								pind->BColorPerceived[ii] = 0.0;
							}
							break;
						}
					}
				}
			}

			if ((hasseen) && ((test_measure_leadership) || (test_measure_leadership_eco)))
				pind->ncyclessawsomething++;		// incrementa il numero di cicli in cui ha visto 
		} else {
			if ((test_measure_leadership) || (test_measure_leadership_eco))
				hasseen=0;
			
			// Compattazione della retina per inserirla nei neuroni disponibili	
			//  Azzera prima tutta la retina
			for (ii=0;ii<pipar->rgbcolorretina;ii++) 		// scansiona tutti i neuroni percettivi
				pind->GrayScaleColorPerceived[ii] = 0.0;

			if (!pind->died_individual) {
				rgbcolorretinapartialviewangle=pipar->rgbcolorretinaviewangle/pipar->rgbcolorretina;
				for (ii=0;ii<pipar->rgbcolorretina;ii++) {		// scansiona tutti i neuroni percettivi
					for (i=0+ii*rgbcolorretinapartialviewangle;i<rgbcolorretinapartialviewangle+ii*rgbcolorretinapartialviewangle;i++) {	// scansiona il campo visivo
						if (((pind->camera[0][i]>0.0) || (pind->camera[1][i]>0.0) || (pind->camera[2][i]>0.0)) &&								// il primo oggetto nel campo visivo parziale lo inserisce nel neurone relativo
							(!((pind->camera[0][i]==RColorPreyDeadInd) && (pind->camera[1][i]==GColorPreyDeadInd) && (pind->camera[2][i]==BColorPreyDeadInd)))) {	// i morti non possono essere visti	 
							
							if ((test_measure_leadership)||(test_measure_leadership_eco))
								hasseen=1;						// ha visto qualcosa
	
							if (!pipar->rgbcolorretinamanualgrayscale) {
								// Formula utilizzata da Adobe Photoshop per covertire RGB in toni di grigio : 
								// Gray=0,33 * RR + 0,50 * GG + 0,17 * BB
								pind->GrayScaleColorPerceived[ii] = 0.33*pind->camera[0][i] + 0.50*pind->camera[1][i] + 0.17*pind->camera[1][i];
							} else {
								// Impostazine manuale delle scale di grigio
								if ((pind->camera[0][i]==255.0) && (pind->camera[1][i]==0.0) && (pind->camera[2][i]==0.0))						
									pind->GrayScaleColorPerceived[ii]=255.0;	// colore rosso	
								if ((pind->camera[0][i]==0.0) && (pind->camera[1][i]==255.0) && (pind->camera[2][i]==0.0))						
									pind->GrayScaleColorPerceived[ii]=70.0;		// colore verde
								if ((pind->camera[0][i]==0.0) && (pind->camera[1][i]==0.0) && (pind->camera[2][i]==255.0))						
									pind->GrayScaleColorPerceived[ii]=245.0;	// colore blue 	
								if ((pind->camera[0][i]==100.0) && (pind->camera[1][i]==0.0) && (pind->camera[2][i]==0.0))						
									pind->GrayScaleColorPerceived[ii]=245.0;	// colore rosso scuro (porpora) 	
							}

							// Se � disattivata la visione del colore blue da parte di una determinata specie allora azzera la visione del Blue
							if ((ipar->rgbcolorretinabluedisabledrace>0) && (pind->race==ipar->rgbcolorretinabluedisabledrace-1) &&
							   (pind->camera[0][i] == 0.0) && (pind->camera[1][i] == 0.0) && (pind->camera[2][i] == 255.0)) {

							   pind->GrayScaleColorPerceived[ii]=0.0;

							   if ((test_measure_leadership)||(test_measure_leadership_eco))
								   hasseen = 0;			// Non ha pi� visto
							}
						}
					}
				}
			}
		
			if ((hasseen) && ((test_measure_leadership)||(test_measure_leadership_eco)))
				pind->ncyclessawsomething++;		// incrementa il numero di cicli in cui ha visto 
		}
	}  // end if
} // end function

/*
 * update the camera characterized by
 *  a given cameraviewangle (e.g. +/- 18 degrees)
 *  a given number of camerareceptors for each cameracolor (1,2, or 3)
 *   with non-overlapping recepitive field
 *  a sensitivity up to a given cameradistance
 */
void
update_camera(struct individual *pind, struct  iparameters *pipar)

{

    int i;
    int ii;
    int f;
    int ff;
    int c;
    int r;
    int a1,a2;
    float x_cam;
    float y_cam;
    float dir_cam;
    float objxa, objya;
    float objxb, objyb;
    double dist;
    double dista;
    double distb;
    double ang;
    double anga;
    double angb;
    double ang1, ang2;
    double angs;
    double angr;
    double a;
    double da;
    float  av[3];
    struct envobject *obj;
    struct envobject **cenvobjs;
    float  objax, objay;
    float  objbx, objby;
    float  cameradist[360];
    char   message[256];
    int    npixels;
    struct individual *cind;
    int    skip;

    //initialize 1-degree photoreceptors state
	for(c=0; c < pipar->cameracolors; c++)
		for(f=0; f < pipar->cameraviewangle; f++)
		{
			pind->camera[c][f] = 0.0;
			cameradist[f] = 99999.0;
		}

    // camera located on the barycentre of the robot
    x_cam    = pind->pos[0];
    y_cam    = pind->pos[1];
    dir_cam  = pind->direction;

    cenvobjs = envobjs;
    // we only check round and sround objects
    for (i=0; i < NOBJS; i++)														// Cicla sul numero di oggetti possibili
    {
		if (i == 0 || i == 1 || i == 2 || i == 5)									// Oggetti considerati : wall, round, sround e robot
			for (ii=0, obj = *(cenvobjs + i); ii < envnobjs[i]; ii++, obj++)		// Considera l'oggetto
			{
				skip = 0;
				// robots objects 
				if (obj->type == ROBOT)												// se l'oggetto � un robot ne memorizza le coordinate
					if (ii < nteam && ii != pind->n_team)
					{
						cind = (ind + ii);
						obj->x = cind->pos[0];
						obj->y = cind->pos[1];
						obj->r = robotradius;
					}
					else
					{
						skip = 1;
					}
				// compute the distance between the camera and the object
				if (obj->type == SWALL)
					dist = (mdist(x_cam, y_cam, obj->x, obj->y) + mdist(x_cam, y_cam, obj->X, obj->Y)) / 2.0;		// distanza individuo - muro
				else
					dist = mdist(x_cam, y_cam, obj->x, obj->y);														// distanza individuo - oggetto sferico
				if (dist < pipar->cameradistance)
				{
					// cylindrical surface
					if (obj->type == SROUND || obj->type == ROUND || obj->type == ROBOT)							// oggetti cilindrici
					{
						ang   = mang((int) x_cam,(int) y_cam, (int) obj->x, (int) obj->y, dist);
						objxa = SetX(obj->x, obj->r, (double) ang + 90.0);
						objya = SetY(obj->y, obj->r, (double) ang + 90.0);
						dista = mdist(x_cam,y_cam,objxa,objya);  
						anga  = mang((int)x_cam,(int)y_cam,(int)objxa,(int)objya,dista);
						angs  = fabs(ang-anga);
						// the next line solve the limit problem
						if (angs > 180)
							angs = 360 - angs;
						angs = angs * 2;
						angr = ang - pind->direction;
						if (angr < 0)
							angr += 360;
						//sprintf(robotwstatus, "t%d abs %.0f abs2 %.0f dir %.0f span %.0f rel %.0f",obj->type, ang, anga, pind->direction, angs, angr);
					}
			        // linear surface
					if (obj->type == SWALL)
				    {
						dista  = mdist(x_cam, y_cam, obj->x, obj->y);
						distb  = mdist(x_cam, y_cam, obj->X, obj->Y);
						anga   = mang((int) x_cam,(int) y_cam, (int) obj->x, (int) obj->y, dista);
						angb   = mang((int) x_cam,(int) y_cam, (int) obj->X, (int) obj->Y, distb);
						angs   = fabs(angb-anga);
						if (angs > 180)
							angs = 360 - angs;
						if (((angb - anga) == angs) || (((angb + 360) - anga) == angs))
							ang = anga + (angs / 2);
						else
							ang = angb + (angs / 2);
						if (ang < 0) 
							ang += 360;
						angr = ang - pind->direction;
						if (angr < 0)
							angr += 360;
						//sprintf(robotwstatus, "t%d abs %.0f %.0f dir %.0f span %.0f a %.0f rel %.0f",obj->type, anga, angb, pind->direction, angs, ang, angr);
					}

					if (skip == 0)
						for(f=0, a = angr - (angs / 2); f < angs; f++, a += 1.0)
						{
							if (a < 0) a += 360;
							if (a > 360) a -= 360;
							ff = a;
							a1 = (360 - (pipar->cameraviewangle / 2));
							a2 = (pipar->cameraviewangle / 2);
							if (ff >= a1 && ff < 360 && dist < cameradist[ff-a1])
							for(c=0;c < pipar->cameracolors;c++)
							{
								pind->camera[c][ff-a1] = obj->c[c];
								cameradist[ff-a1] = dist;
							}
							
						
							if (ff >= 0 && ff < a2 && dist < cameradist[ff+a2])
							for(c=0;c < pipar->cameracolors;c++)
							{
								pind->camera[c][ff+a2] = obj->c[c];
								cameradist[ff+a2] = dist;
							}
						}
				} // dist < cameradistance
			}
	}
    // compress and average information for photoreceptors that encode more than one degree of view
    if (pipar->camerareceptors < pipar->cameraviewangle)
		for(f=0; f < pipar->camerareceptors; f++)
		{
			for(c=0; c < pipar->cameracolors; c++)
				av[c] = 0.0;
			npixels = pipar->cameraviewangle / pipar->camerareceptors;
			for(i=0; i < npixels; i++)
			{
				for(c=0; c < pipar->cameracolors; c++)
					av[c] += pind->camera[c][f*npixels+i];
			}
			for(c=0; c < pipar->cameracolors; c++)
				pind->camera[c][f] = av[c] / (float) npixels;
		}
}


/*
 * update the simple camera 
 * the simple camera detect the other robot only
 */
void
update_simplecamera(struct individual *pind, struct  iparameters *pipar)

{

    int    i;
    float  p1;
    float  p2;
    double direction;
    int    t;
    double dist;
    double ang;
    int    relang;
    struct individual *cind;

    p1        = pind->pos[0];
    p2        = pind->pos[1];
    direction = pind->direction;

    for (i=0; i < 5; i++)
      pind->scamera[i] = 0.0;

    if (pipar->simplevision == 1)
	{
	 for (t=0,cind=ind; t<nteam; t++, cind++)
	 {
	  if (pind->n_team != cind->n_team)
	  {
	   dist = mdist((float)p1,(float)p2,(float) cind->pos[0],(float)cind->pos[1]);
	   ang = mang((int)p1,(int)p2,(int)cind->pos[0],(int)cind->pos[1],(double)dist);
	   relang = (int) mangrel2((double)ang,(double)direction);
	   if (relang >= 342 && relang <= 351)
              pind->scamera[0] = 1.0;
	   if (relang > 351 && relang <= 360)
              pind->scamera[1] = 1.0;
	   if (relang >= 0 && relang <= 9)
              pind->scamera[2] = 1.0;
	   if (relang > 9 && relang <= 18)
              pind->scamera[3] = 1.0;
	   if (relang > 18 && relang < 342)
              pind->scamera[4] = 1.0;
	  }
	 }
	}
	// epuck camera is simulated through computing the relevant angle for a left and right input
	// if no other epuck is perceived, both inputs are 0
	// max angle of epuck camera is defined by pipar->camera
	// activation of neurons is 'relative angle_left*(100/(degrees/2))' and 'relative angle_right*(100/18)',
	// if other robot is in front, activation for both neurons is 100
	//
	if (pipar->simplevision == 2)
	{
		for (t=0,cind=ind; t<nteam; t++, cind++)
		{
			if ((p1 != cind->pos[0] || p2 != cind->pos[1]))
			{
				float input_left;
				float input_right;
				float input_not_visible;
				float degrees = 18;

				//compute distance and angle
				dist = mdist((float)p1,(float)p2,(float) cind->pos[0],(float)cind->pos[1]) - 27.5;
				ang = mang((int)p1,(int)p2,(int)cind->pos[0],(int)cind->pos[1],(double)dist+27.5);
				relang  = mangrel2(ang,direction);

				//other epuck is in view
				if (relang < degrees || relang > (360-degrees))
				{
						if ( relang < degrees )
						{
							input_left = (float) ((degrees-relang)*(100/degrees))/100;
							input_right = (float) 0.0f;
							input_not_visible = 0.0f;
						}
						else
						{
							input_left = (float) 0.0;
							input_right = (float) ((relang-(360-degrees))*(100/degrees))/100;
							input_not_visible = 0.0f;
						}
					}

				//no other epuck is visible
				else
				{
					input_left = 0.0;
					input_right = 0.0;
					input_not_visible = 1.0f;
				}

				if (input_left > 1.0)
					input_left = 1.0;
				if (input_left < 0.0)
					input_left = 0.0;
				if (input_right > 1.0)
					input_right = 1.0;
				if (input_right < 0.0)
					input_right = 0.0;

				pind->scamera[0] = input_left;
				pind->scamera[1] = input_right;
				pind->scamera[2] = input_not_visible;
			}
		}
	}
}


/*
 * update the ground sensors
 */
void
update_groundsensors(struct individual *pind, struct  iparameters *pipar)

{

    int g;
    int i;
    float c;
    float fi;
    float v;

    c = read_ground_color(pind);
    fi = 1.0 / (float) (pipar->groundsensor + 1);
    for (i=0, v=fi; i < pipar->groundsensor; i++, v += fi)
      if (c > v && c <= (v + fi))
         pind->ground[i] = (float) 1.0;
      else
         pind->ground[i] = (float) 0.0;

}

/*
 * update the food sensors
 */
void update_foodSensors(struct individual *pind, struct  iparameters *pipar)
{
	//	Obtain a non-zero value if the robots food
	//	sensor is above a food zone
	if (read_food_color(pind) > 0)
	{
		//	Allow more than one sensor to be used
		for (int i = 0; i < pipar->foodSensor; i++)
		{
			pind->food[i] = (float) 1.0;
		}
	}
	else
	{
		//	Allow more than one sensor to be used
		for (int i = 0; i < pipar->foodSensor; i++)
		{
			pind->food[i] = (float) 0.0;
		}
	}
}

/*
 * update the hunger sensors
 */
void update_hungerSensors(struct individual *pind, struct  iparameters *pipar)
{
	//	hunger threshold normalised to 8bits
	const int HUNGER_RESOLUTION_8 = pipar->hungerThreshold / 8;
	//	hunger threshold resolved to 4 outputs
	const int HUNGER_RESOLUTION_4 = pipar->hungerThreshold / 4;
	
    float bit0, bit1, bit2;

	//	Branch based upon sensor type
    if (pipar->hungerSensor == 1)
	{
		//	Sensor type 1 is an on/off switch - stored in element 0 of the array only
		if (foodAccumulator < pipar->hungerThreshold)
			pind->hunger[0] = (float) 1.0;
		else
			pind->hunger[0] = (float) 0.0;
	}
	else if (pipar->hungerSensor == 2)
	{
		//	Sensor type 2 is a normalisation of the food accumulator (stored in element 0)
		//	Normalised to the number of cycles in a lifetime divided by 2
		pind->hunger[0] = (float)(foodAccumulator / (double)(sweeps / 2));
	}
	else if (pipar->hungerSensor == 3)
	{
		//	Sensor type 3 is a 3bit binary representation
		//	Runing from 000 (hungry) to 111 (full)
		
		//	Check to see if 'hungerIncreaseRate' is defined
		//	this will allow us to check if we are using an
		//	old group accumulator based setup
		if ((pipar->hungerIncreaseRate == 0) && (pipar->hungerDecreaseRate == 0))
		{
			intToThreeBitBinary( (int) hungerAccumulator/HUNGER_RESOLUTION_8, &bit0, &bit1, &bit2);
		}
		else
		{
			intToThreeBitBinary( (int) pind->hungerAccumulator/HUNGER_RESOLUTION_8, &bit0, &bit1, &bit2);
		}

		pind->hunger[0] = bit0;
		pind->hunger[1] = bit1;
		pind->hunger[2] = bit2;
	}
	else if (pipar->hungerSensor == 4)
	{
		//	Sensor type 4 is a simplified 3bit represenation
		//	ie (hungry) 000 -> 001 -> 010 -> 100 (full)
		
		//	Check to see if 'hungerIncreaseRate' is defined
		//	this will allow us to check if we are using an
		//	old group accumulator based setup
		if ((pipar->hungerIncreaseRate == 0) && (pipar->hungerDecreaseRate == 0))
		{
			intToThreeNodeEncoding( (int) hungerAccumulator/HUNGER_RESOLUTION_4, &bit0, &bit1, &bit2);
		}
		else
		{
			intToThreeNodeEncoding( (int) pind->hungerAccumulator/HUNGER_RESOLUTION_4, &bit0, &bit1, &bit2);
		}
		pind->hunger[0] = bit0;
		pind->hunger[1] = bit1;
		pind->hunger[2] = bit2;
	}
	else if (pipar->hungerSensor == 5)
	{
		//	Sensor type 5 acts as a memory that the robot has visited a food area
		//	It will have a value of 111 upon visiting a food zone, this will stay
		//	at this level unit the robot visits a water zone where it is reset to 000
		if (pind->food[0] == 1)
		{
			//	Set sensor values to high
			pind->hunger[0] = 1.0f;
			pind->hunger[1] = 1.0f;
			pind->hunger[2] = 1.0f;
		}
		else if (pind->water[0] == 1)
		{
			//	Set sensor values to low
			pind->hunger[0] = 0.0f;
			pind->hunger[1] = 0.0f;
			pind->hunger[2] = 0.0f;
		}
	}
	else if (pipar->hungerSensor == 6)
	{
		//	Sensor type 6 gives 3 neurons of output for hunger, however the combined
		//	magnitude of the output will increase as the hunger accumulator reaches
		//	the target threshold
		
		//	Check to see if 'hungerIncreaseRate' is defined
		//	this will allow us to check if we are using an
		//	old group accumulator based setup
		if ((pipar->hungerIncreaseRate == 0) && (pipar->hungerDecreaseRate == 0))
		{
			intToThreeNodeMagnitudeEncoding( (int) hungerAccumulator/HUNGER_RESOLUTION_4, &bit0, &bit1, &bit2);
		}
		else
		{
			intToThreeNodeMagnitudeEncoding( (int) pind->hungerAccumulator/HUNGER_RESOLUTION_4, &bit0, &bit1, &bit2);
		}

		pind->hunger[0] = bit0;
		pind->hunger[1] = bit1;
		pind->hunger[2] = bit2;
	}
}

/*
 * update the water sensors
 */
void update_waterSensors(struct individual *pind, struct  iparameters *pipar)
{
	//	Obtain a non-zero value if the robots water
	//	sensor is above a water zone
	if (read_water_color(pind) > 0)
	{
		//	Allow more than one sensor to be used
		for (int i = 0; i < pipar->waterSensor; i++)
		{
			pind->water[i] = (float) 1.0;
		}
	}
	else
	{
		//	Allow more than one sensor to be used
		for (int i = 0; i < pipar->waterSensor; i++)
		{
			pind->water[i] = (float) 0.0;
		}
	}
}

/*
 * update the thirst sensors
 */
void update_thirstSensors(struct individual *pind, struct  iparameters *pipar)
{
	//	Thirst is normalised to 8 bits
	const int THIRST_RESOLUTION_8 = pipar->thirstThreshold / 8;
	//	Thirst is one of four possible values
	const int THIRST_RESOLUTION_4 = pipar->thirstThreshold / 4;

    float bit0;
	float bit1;
	float bit2;

    //	Branch based upon sensor type
	if (pipar->thirstSensor == 1)
	{
		//	TODO: set level to parameters in config
		//	Sensor type 1 is an on/off switch - stored in element 0 of the array only
		if (waterAccumulator < pipar->thirstThreshold)
			pind->thirst[0] = (float) 1.0;
		else
			pind->thirst[0] = (float) 0.0;
	}
	else if (pipar->thirstSensor == 2)
	{
		//	Sensor type 2 is a normalisation of the food accumulator (stored in element 0)
		//	Normalised to the number of cycles in a lifetime divided by 2
		pind->thirst[0] = (float)(waterAccumulator / (double)(sweeps / 2));
	}
	else if (pipar->thirstSensor == 3)
	{
		//	Sensor type 3 is a 3bit binary representation
		//	Runing from 000 (thirsty) to 111 (full)
		
		//	Check to see if 'thirstIncreaseRate' is defined
		//	this will allow us to check if we are using an
		//	old group accumulator based setup
		if ((pipar->thirstIncreaseRate == 0) && (pipar->thirstDecreaseRate == 0))
		{
			intToThreeBitBinary( (int)thirstAccumulator/THIRST_RESOLUTION_8, &bit0, &bit1, &bit2);
		}
		else
		{
			intToThreeBitBinary( (int)pind->thirstAccumulator/THIRST_RESOLUTION_8, &bit0, &bit1, &bit2);
		}

		pind->thirst[0] = bit0;
		pind->thirst[1] = bit1;
		pind->thirst[2] = bit2;
	}
	else if (pipar->thirstSensor == 4)
	{
		//	Sensor type 4 is a simplified 3bit represenation
		//	ie (thirsty) 000 -> 001 -> 010 -> 100 (full)
		
		//	Check to see if 'thirstIncreaseRate' is defined
		//	this will allow us to check if we are using an
		//	old group accumulator based setup
		if ((pipar->thirstIncreaseRate == 0) && (pipar->thirstDecreaseRate == 0))
		{
			intToThreeNodeEncoding( (int) thirstAccumulator/THIRST_RESOLUTION_4, &bit0, &bit1, &bit2);
		}
		else
		{
			intToThreeNodeEncoding( (int) pind->thirstAccumulator/THIRST_RESOLUTION_4, &bit0, &bit1, &bit2);
		}

		pind->thirst[0] = bit0;
		pind->thirst[1] = bit1;
		pind->thirst[2] = bit2;
	}
	else if (pipar->thirstSensor == 5)
	{
		//	Sensor type 5 acts as a memory that the robot has visited a water area
		//	It will have a value of 111 upon visiting a water zone, this will stay
		//	at this level unit the robot visits a food zone where it is reset to 000
		if (pind->water[0] == 1)
		{
			//	Set sensor values to high
			pind->thirst[0] = 1.0f;
			pind->thirst[1] = 1.0f;
			pind->thirst[2] = 1.0f;
		}
		else if (pind->food[0] == 1)
		{
			//	Set sensor values to low
			pind->thirst[0] = 0.0f;
			pind->thirst[1] = 0.0f;
			pind->thirst[2] = 0.0f;
		}
	}
	else if (pipar->thirstSensor == 6)
	{
		//	Sensor type 6 gives 3 neurons of output for thirst, however the combined
		//	magnitude of the output will increase as the thirst accumulator reaches
		//	the target threshold
		
		//	Check to see if 'thirstIncreaseRate' is defined
		//	this will allow us to check if we are using an
		//	old group accumulator based setup
		if ((pipar->thirstIncreaseRate == 0) && (pipar->thirstDecreaseRate == 0))
		{
			intToThreeNodeMagnitudeEncoding( (int)thirstAccumulator/THIRST_RESOLUTION_4, &bit0, &bit1, &bit2);	
		}
		else
		{
			intToThreeNodeMagnitudeEncoding( (int)pind->thirstAccumulator/THIRST_RESOLUTION_4, &bit0, &bit1, &bit2);
		}

		pind->thirst[0] = bit0;
		pind->thirst[1] = bit1;
		pind->thirst[2] = bit2;
	}
}

/*
 * update the food sensors
 */
void update_dayNightSensors(struct individual *pind, struct  iparameters *pipar)
{
	if (isDaytime)
	{
		pind->dayNight[0] = (float) 1.0;
	}
    else
	{
		pind->dayNight[0] = (float) 0.0;
	}
}

/*
 * update the age sensors
 */
void update_ageSensors(struct individual *pind, struct  iparameters *pipar)
{
	if (pipar->ageSensor == 1)
	{
		//	run: current frame in the cycle
		//	sweeps: maximum number of frames in a lifecycle
		
		//	Work out the size of each range
		float segmentSize = (float)(sweeps / 8);

		//	Work out the value in the range of 0-7
		float rank = run / segmentSize;

		//	Maximum of 3 bit resolution
		if (rank > 7)
		{
			rank = 7;
		}

		//	Round this down so that we can use a switch statement
		int rankI = (int)(rank);

		float bit0, bit1, bit2;

		//	Convert 'rankI' into three on / off values
		intToThreeBitBinary(rankI, &bit0, &bit1, &bit2);

		//	These are then assigned to the output
		pind->age[0] = bit0;
		pind->age[1] = bit1;
		pind->age[2] = bit2;
	}
}

/*
 * update the ground sensors encoding the last visited area
 */
void
update_lastground(struct individual *pind, struct  iparameters *pipar)

{

    int g;
    int i;
    float c;
    float fi;
    float v;

    c = read_ground_color(pind);
    fi = 1.0 / (float) (pipar->a_groundsensor2 + 1);
    if (c > fi)
     {
      for (i=0, v=fi; i < pipar->a_groundsensor2; i++, v += fi)
       if (c > v && c <= (v + fi))
         pind->lastground[i] = (float) 1.0;
        else
         pind->lastground[i] = (float) 0.0;
     }
	
}

/*
 * update the leaky-integrator ground sensors 
 */
void
update_deltaground(struct individual *pind, struct  iparameters *pipar)

{

    int g;
    int i;
    float c;
    float fi;
    float v;

    c = read_ground_color(pind);
    fi = 1.0 / (float) (pipar->a_groundsensor + 1);
    for (i=0, v=fi; i < pipar->a_groundsensor; i++, v += fi)
      if (c > v && c <= (v + fi))
         pind->ground[i] = pind->ground[i] * 0.99 + 0.01;
      else
         pind->ground[i] = pind->ground[i] * 0.99;

}

// Sposta il cilindro-colore insieme all'individuo
void move_individual_color(struct individual *pind)
{
	int i;
	struct envobject *obj;					// struttura temporanea per il controllo cilindri, ed altri elementi

    int nobjs=envnobjs[SROUND];
	
	// controlla che ci siano cilindri posizionati sul robot, in tal caso li muover� in sincronia con i 
    // robot
	for(i=0, obj = *(envobjs + SROUND);i<envnobjs[SROUND];i++, obj++) {
		// controlla che il cilindro sia posizionato sopra il robot, in tal caso lo sposta insieme al 
		// robot stesso 
		if ((obj->subtype==10) && (pind->number==obj->nind)) {
			obj->x=pind->pos[0];
			obj->y=pind->pos[1];
		} 	
	}
}


//function to update lego robot
void move_lego_robot(struct individual *pind)
{
	
	//lego robot data
    //max speed  : 144mm/s  da controllare
	//body radius: 115mm/2 =57.5

	
	
	//inizio funzione movimento
  double anglVari;             //Variation of angle as a consequence of movement
  double roboPivoDist;         //Distance from robot's perimeter to pivot of movement
  double roboCentPivoDist;     //Distance from robot's centre to pivot of movement
  double rightLegForc;		   //velocit� destra
  double  leftLegForc;		   // velocit� sinistra
  double PI=3.1415926535;
  double PGRE_HALF=PI/2.0;
  double bodyRadi=57.5;	       //distanza tra le ruote
  double bodyDiam=115;
  double maxSpeed=14.4; //mm/s
  double angl, oldAngl;
  struct envobject *obj;		// struttura temporanea per il controllo cilindri, ed altri elementi
  int i=0;

	pind->oldpos[0] = pind->pos[0];
	pind->oldpos[1] = pind->pos[1];

	if ((bar_without_crash && !pind->blocked_after_crash) || (restart_from_n_pos_before)) {
		i=pind->lifecycle % max_dist_rebound_after_crash;
		pind->noldpos[i][0]=pind->pos[0];
		pind->noldpos[i][1]=pind->pos[1];
	}
	
	//pind->direction=333.45087; x
	pind->oldirection = pind->direction;

	rightLegForc=pind->wheel_motor[0]*2*maxSpeed-maxSpeed;
	leftLegForc =pind->wheel_motor[1]*2*maxSpeed-maxSpeed;


  //rightLegForc=14.360478;;x
  //leftLegForc=14.360492;x gli x denotano i valori per cui se le variabili sono solo float il robot si blocca
	
	
	pind->speed[0] = rightLegForc;
	pind->speed[1] = leftLegForc;



 // oldAngl         = angl;
 // oldPosiX        = posiX;
 // oldPosiY        = posiY;
  angl=DGRtoRAD(pind->direction);
  oldAngl=angl;

  if(abs(rightLegForc - leftLegForc) < 0.00001f)  //Speed of two wheels is very similar
  {
   pind->pos[0]  += cos(angl) * rightLegForc;
   pind->pos[1]  += sin(angl) * rightLegForc;
   

  }
  else
  {
    if(leftLegForc < rightLegForc)   //Right wheel faster than left wheel
    {
      //These is the formula to compute the variation of the angle on the bases
      //of the arcs covered by the wheels, with centre on the pivot point
      anglVari         = (rightLegForc - leftLegForc) / bodyDiam;
      angl             += anglVari;
      //Distance from the left wheel to the pivot point
      roboPivoDist     = leftLegForc / anglVari;
      roboCentPivoDist = roboPivoDist + bodyRadi;
      pind->pos[0]            += (cos(angl - PGRE_HALF) - cos(oldAngl - PGRE_HALF)) * roboCentPivoDist;
      pind->pos[1]            += (sin(angl - PGRE_HALF) - sin(oldAngl - PGRE_HALF)) * roboCentPivoDist;

    }
    else                           //Left wheel faster than right wheel
    {
      anglVari         = (leftLegForc - rightLegForc) / bodyDiam;
      angl             -= anglVari;
      roboPivoDist     = rightLegForc / anglVari;
      roboCentPivoDist = roboPivoDist + bodyRadi;
      pind->pos[0]            += (cos(angl + PGRE_HALF) - cos(oldAngl + PGRE_HALF)) * roboCentPivoDist;
      pind->pos[1]            += (sin(angl + PGRE_HALF) - sin(oldAngl + PGRE_HALF)) * roboCentPivoDist;

   }
  }

  if(PI*2 < angl)
    angl -= PI*2;

	if(angl < 0.0f)
		angl += PI*2;
	pind->direction=RADtoDGR(angl);
    
	move_individual_color(pind);		// sposta il colore dell'individuo insieme ad esso stesso  
}


	//fine


/*
 * move the robot on the basis of sampled motion
 */
void move_robot(struct individual *pind)
 {

     int    v1,v2;
     float  **mo;
     float  *m;
     float  ang, dist;
     extern float mod_speed;
     extern int   selected_ind_mod_speeed;


	 pind->oldpos[0] = pind->pos[0];
	 pind->oldpos[1] = pind->pos[1];
	 pind->oldirection = pind->direction;

	 v1 = (int) (pind->wheel_motor[0] * 20.0);
	 v2 = (int) (pind->wheel_motor[1] * 20.0);
	 // approximate to the closest integer value
	 if ( ((pind->wheel_motor[0] * 200) - (v1 * 20)) > 5)
		 v1++;
	 if ( ((pind->wheel_motor[1] * 200) - (v2 * 20)) > 5)
	 	 v2++;

	 v1 -= 10;
	 v2 -= 10;

	 if (pind->wheel_motor[2] < 1.0f)
	 {
	  v1 = (int) ((float) v1 * pind->wheel_motor[1]);
	  v2 = (int) ((float) v2 * pind->wheel_motor[1]);
	 }

	 pind->speed[0] = (float) v1;
	 pind->speed[1] = (float) v2;

	 mo = motor;
	 m = *(mo + (v1 + 12));
	 m = (m + ((v2 + 12) * 2));
	 ang = *m;
	 m++;
	 dist = *m * (float) 10.0;

	 pind->pos[0] = SetX(pind->pos[0],dist,pind->direction);
	 pind->pos[1] = SetY(pind->pos[1],dist,pind->direction);


	 pind->direction += ang;
	 if (pind->direction >= 360.0)
		 pind->direction -= 360;
	 if (pind->direction < 0.0)
		 pind->direction += 360;
 }


/*
 * it eventually load worlds dimension from evorobot.env
 */
void 
load_envdim(char *filename)

{
/*	FILE   *fp;
    char   st[1024];
    char   message[512];
    char   word[256];
    char   *w;
    int    n;
    int    type;
    char   c;
    int    i;
    int    color;
    int    selected;

    if ((fp=fopen(filename, "r")) != NULL)
	{
		n = 0;
		while (fgets(st,1024,fp) != NULL && n < 1000)
		{
		selected = 0;
		if (strlen(st) > 2)
	   {
	   getword(word,st,' ');
	   if (strcmp(word, "swall") == 0 && envnobjs[SWALL] < nobjects)
	    {
	     selected = 1;
	     obj = *cenvobjs;
	     obj = (obj + envnobjs[SWALL]);
	     obj->id = envnobjs[SWALL];
             envnobjs[SWALL] += 1;
	     obj->type = SWALL;
	    }
	   if (strcmp(word, "sround") == 0 && envnobjs[SROUND] < nobjects)
	    {
	     selected = 1;
	     obj = *(cenvobjs + SROUND);
	     obj = (obj + envnobjs[SROUND]);
	     obj->id = envnobjs[SROUND];
             envnobjs[SROUND] += 1;
	     obj->type = SROUND;
	     obj->r = 12.5;
	    }
	   if (strcmp(word, "round") == 0 && envnobjs[ROUND] < nobjects)
	    {
	     selected = 1;
	     obj = *(cenvobjs + ROUND);
	     obj = (obj + envnobjs[ROUND]);
	     obj->id = envnobjs[ROUND];
             envnobjs[ROUND] += 1;
	     obj->type = ROUND;
	     obj->r = 27.0;
	    }
	   if (strcmp(word, "ground") == 0 && envnobjs[GROUND] < nobjects)
	    {
	     selected = 1;
	     obj = *(cenvobjs + GROUND);
	     obj = (obj + envnobjs[GROUND]);
	     obj->id = envnobjs[GROUND];
         envnobjs[GROUND] += 1;
	     obj->type = GROUND;
	     obj->subtype = 0;
	    }
	   if (strcmp(word, "light") == 0 && envnobjs[LIGHT] < nobjects)
	    {
	     selected = 1;
	     obj = *(cenvobjs + LIGHT);
	     obj = (obj + envnobjs[LIGHT]);
	     obj->id = envnobjs[LIGHT];
             envnobjs[LIGHT] += 1;
	     obj->type = LIGHT;
	    }
	   if (strcmp(word, "robot") == 0 && envnobjs[ROBOT] < nobjects)
	    {
	     selected = 1;
	     obj = *(cenvobjs + ROBOT);
	     obj = (obj + envnobjs[ROBOT]);
	     obj->id = envnobjs[ROBOT];
             envnobjs[ROBOT] += 1;
	     obj->type = ROBOT;
	     obj->r = robotradius;
	    }
	   if (strcmp(word, "wall") == 0 && envnobjs[WALL] < nobjects)
	    {
	     selected = 1;
	     obj = *cenvobjs;
	     obj = (obj + envnobjs[WALL]);
	     obj->id = envnobjs[WALL];
             envnobjs[WALL] += 1;
	     obj->type = WALL;
	    }
	   if (strcmp(word, "cylinder") == 0 && envnobjs[CYLINDER] < nobjects)
	    {
	     selected = 1;
	     obj = *(cenvobjs + CYLINDER);
	     obj = (obj + envnobjs[CYLINDER]);
	     obj->id = envnobjs[CYLINDER];
         envnobjs[CYLINDER] += 1;
	     obj->type = CYLINDER;
	     obj->r = 20.0;
	    }
	   if (strcmp(word, "bar") == 0 && envnobjs[BAR] < nobjects)
	   {
			selected = 1;
			obj = *(cenvobjs + BAR);
			obj = (obj + envnobjs[BAR]);	
			obj->id = envnobjs[BAR];
            envnobjs[BAR] += 1;
			obj->type = BAR;
	    }
	   if (strcmp(word, "randrecthallway") == 0 && envnobjs[RANDRECTHALLWAY] < nobjects)
	   {
			selected = 1;
			obj = *(cenvobjs + RANDRECTHALLWAY);
			obj = (obj + envnobjs[RANDRECTHALLWAY]);	
			obj->id = envnobjs[RANDRECTHALLWAY];
            envnobjs[RANDRECTHALLWAY] += 1;
			obj->type = RANDRECTHALLWAY;
	   }
	   if (strcmp(word, "envdim") == 0)
	    {
		 selected = 2;
   		 getword(word,st,' '); // legge il primo parametro di envdim
		 w = word;				
		 widgetdx=atoi(w);	   // assegna la nuova dimensione x dell'ambiente
  		 evwidgetdx=widgetdx;
		 getword(word,st,' '); // legge il primo parametro di envdim
		 widgetdy=atoi(w);	// assegna la nuova dimensione x dell'ambiente
  		 evwidgetdy=widgetdy;
	    }
		if (selected == 1)
        {
			color = 0;
			obj->subtype = 0;
			obj->x = 100;
			obj->y = 100;
			obj->X = 200;
			obj->Y = 200;
			if (obj->type != 1 && obj->type != 2)
				 obj->r = 0;
			obj->h = 20;
			obj->c[0] = 0.0;
			obj->c[1] = 0.0;
			obj->c[2] = 0.0;
            while (strlen(st) > 1)
            {
				getword(word,st,' ');
				c = *word;
				w = word;
				w++;
				switch(c)
				{
				case 't':
					obj->subtype = atoi(w);
					break;
				case 'x':
					obj->x = atof(w);
					if (obj->x > envdx)
						envdx = obj->x;
					break;
				case 'y':
					obj->y = atof(w);
					if (obj->y > envdy)
						envdy = obj->y;
					break;
				case 'X':
					obj->X = atof(w);
					if (obj->X > envdx)
						envdx = obj->X;
					break;
				case 'Y':
					obj->Y = atof(w);
					if (obj->Y > envdy)
						envdy = obj->Y;
					break;
				case 'h':
					obj->h = atof(w);
					break;
				case 'r':
					obj->r = atof(w);
					break;
				case 'c':
					if (color >= 0 && color < 3)
					obj->c[color] = atof(w);
					color++;
					break;
				case 'n':
					obj->nind = atoi(w);
					obj->subtype = 10;		// cilindro posizionato sul robot
					break;
				default:
					sprintf(message,"unknown env. parameter %c%s in file evorobot.env",c,w);
                    display_warning(message);
					break;
			}
	       }
	   }
	   else 
            {
				if (selected!=2) {	// modificato per aggiungere envdim		
					sprintf(st,"unknown object: %s", word);
					display_warning(st);
				}
			}
	   n++;
	   }
	  }
        fclose(fp);
			if(envnobjs[BAR]==0) { //se ci sono sbarre nell'ambiente non carica questo messaggio
				sprintf(message,"loaded %d objects from file: evorobot.env", n);
				display_stat_message(message);
			}
		}
       else
       {
         sprintf(message,"Error: the file evorobot.env could not be opended");
		 display_error(message);
       }
     
	  // se esistono sbarre determina la lunghezza di ognuna di esse
	  if(envnobjs[BAR]>0) {
		for(i=0, obj = *(envobjs + BAR);i<envnobjs[BAR];i++, obj++)	{		// tutte le sbarre
			obj->h=mdist(obj->x,obj->y,obj->X,obj->Y);	// determina la lunghezza della sbarra
		}
	  }

	 // calculate world size (if not specified manually)
     if (envdx == 0 || envdy == 0)
       {
        for(i=0,obj = *envobjs; i < envnobjs[SWALL]; i++, obj++)
          {
	    if (obj->x > envdx) envdx = obj->x;
	    if (obj->y > envdy) envdy = obj->y;
	    if (obj->X > envdx) envdx = obj->X;
	    if (obj->Y > envdy) envdy = obj->Y;
          } 
        for(i=0,obj = *envobjs; i < envnobjs[WALL]; i++, obj++)
          {
	    if (obj->x > envdx) envdx = obj->x;
	    if (obj->y > envdy) envdy = obj->y;
	    if (obj->X > envdx) envdx = obj->X;
	    if (obj->Y > envdy) envdy = obj->Y;
          } 
      }     
*/}



/*
 * it load worlds description from file evorobot.env
 */
void load_world(char *filename)
{
	FILE   *fp;
	char   st[1024];
	char   message[512];
	char   word[256];
	char   *w;
	int    n;
	int    type;
	char   c;
	int    i;
	int    color;
	int    selected;
	struct envobject **cenvobjs;	// list of objects inside the environment
	struct envobject *obj;			// one single current object (cursor - temporary object)

	cenvobjs = envobjs;
	for(i=0; i < NOBJS; i++)
		envnobjs[i] = 0;
	if ((fp=fopen(filename, "r")) != NULL)
	{
		n = 0;
		while (fgets(st,1024,fp) != NULL && n < 1000)
		{
			selected = 0;
			if (strlen(st) > 2)
			{
				getword(word,st,' ');
				//	Add the ability to add comments to the environment definition
				//	using the # tag before the comment
				if (word[1] != '#') 
				{
					if (strcmp(word, "swall") == 0 && envnobjs[SWALL] < nobjects)
					{
						selected = 1;
						obj = *cenvobjs;
						obj = (obj + envnobjs[SWALL]);
						obj->id = envnobjs[SWALL];
						envnobjs[SWALL] += 1;
						obj->type = SWALL;
					}

					if (strcmp(word, "sround") == 0 && envnobjs[SROUND] < nobjects)
					{
						selected = 1;
						obj = *(cenvobjs + SROUND);
						obj = (obj + envnobjs[SROUND]);
						obj->id = envnobjs[SROUND];
						envnobjs[SROUND] += 1;
						obj->type = SROUND;
						obj->r = 12.5;
					}

					if (strcmp(word, "round") == 0 && envnobjs[ROUND] < nobjects)
					{
						selected = 1;
						obj = *(cenvobjs + ROUND);
						obj = (obj + envnobjs[ROUND]);
						obj->id = envnobjs[ROUND];
						envnobjs[ROUND] += 1;
						obj->type = ROUND;
						obj->r = 27.0;
					}

					//	Create a new food object within the environment array
					if (strcmp(word, "food") == 0 && envnobjs[FOOD] < nobjects)
					{
						selected = 1;
						obj = *(cenvobjs + FOOD);
						obj = (obj + envnobjs[FOOD]);
						obj->id = envnobjs[FOOD];
						envnobjs[FOOD] += 1;
						obj->type = FOOD;
						obj->r = 100.0;
					}

					//	Create a new water object within the environment array
					if (strcmp(word, "water") == 0 && envnobjs[WATER] < nobjects)
					{
						selected = 1;
						obj = *(cenvobjs + WATER);
						obj = (obj + envnobjs[WATER]);
						obj->id = envnobjs[WATER];
						envnobjs[WATER] += 1;
						obj->type = WATER;
						obj->r = 100.0;
					}

					if (strcmp(word, "ground") == 0 && envnobjs[GROUND] < nobjects)
					{
						selected = 1;
						obj = *(cenvobjs + GROUND);
						obj = (obj + envnobjs[GROUND]);
						obj->id = envnobjs[GROUND];
						envnobjs[GROUND] += 1;
						obj->type = GROUND;
						obj->subtype = 0;
					}

					if (strcmp(word, "light") == 0 && envnobjs[LIGHT] < nobjects)
					{
						selected = 1;
						obj = *(cenvobjs + LIGHT);
						obj = (obj + envnobjs[LIGHT]);
						obj->id = envnobjs[LIGHT];
						envnobjs[LIGHT] += 1;
						obj->type = LIGHT;
					}

					if (strcmp(word, "robot") == 0 && envnobjs[ROBOT] < nobjects)
					{
						selected = 1;
						obj = *(cenvobjs + ROBOT);
						obj = (obj + envnobjs[ROBOT]);
						obj->id = envnobjs[ROBOT];
						envnobjs[ROBOT] += 1;
						obj->type = ROBOT;
						obj->r = robotradius;
					}

					if (strcmp(word, "wall") == 0 && envnobjs[WALL] < nobjects)
					{
						selected = 1;
						obj = *cenvobjs;
						obj = (obj + envnobjs[WALL]);
						obj->id = envnobjs[WALL];
						envnobjs[WALL] += 1;
						obj->type = WALL;
					}

					if (strcmp(word, "cylinder") == 0 && envnobjs[CYLINDER] < nobjects)
					{
						selected = 1;
						obj = *(cenvobjs + CYLINDER);
						obj = (obj + envnobjs[CYLINDER]);
						obj->id = envnobjs[CYLINDER];
						envnobjs[CYLINDER] += 1;
						obj->type = CYLINDER;
						obj->r = 20.0;
					}

					if (strcmp(word, "bar") == 0 && envnobjs[BAR] < nobjects)
					{
						selected = 1;
						obj = *(cenvobjs + BAR);
						obj = (obj + envnobjs[BAR]);	
						obj->id = envnobjs[BAR];
						envnobjs[BAR] += 1;
						obj->type = BAR;
					}

					if (strcmp(word, "randrecthallway") == 0 && envnobjs[RANDRECTHALLWAY] < nobjects)
					{
						selected = 1;
						obj = *(cenvobjs + RANDRECTHALLWAY);
						obj = (obj + envnobjs[RANDRECTHALLWAY]);	
						obj->id = envnobjs[RANDRECTHALLWAY];
						envnobjs[RANDRECTHALLWAY] += 1;
						obj->type = RANDRECTHALLWAY;
					}

					if (strcmp(word, "envdim") == 0)
					{
						selected = 2;
						getword(word,st,' '); // legge il primo parametro di envdim
						w = word;				
						/*widgetdx=atoi(w);	   // assegna la nuova dimensione x dell'ambiente
						evwidgetdx=widgetdx;*/
						getword(word,st,' '); // legge il primo parametro di envdim
						/*widgetdy=atoi(w);	// assegna la nuova dimensione x dell'ambiente
						evwidgetdy=widgetdy;*/
					}

					if (selected == 1)
					{
						color = 0;
						obj->subtype = 0;
						obj->x = 100;
						obj->y = 100;
						obj->X = 200;
						obj->Y = 200;

						if (obj->type != 1 && obj->type != 2)
						{
							obj->r = 0;
						}

						obj->h = 20;
						obj->c[0] = 0.0;
						obj->c[1] = 0.0;
						obj->c[2] = 0.0;

						while (strlen(st) > 1)
						{
							getword(word,st,' ');
							c = *word;
							w = word;
							w++;

							switch(c)
							{
							case 't':
								obj->subtype = atoi(w);
								break;
							case 'x':
								obj->x = atof(w);
								if (obj->x > envdx)
									envdx = obj->x;
								break;
							case 'y':
								obj->y = atof(w);
								if (obj->y > envdy)
									envdy = obj->y;
								break;
							case 'X':
								obj->X = atof(w);
								if (obj->X > envdx)
									envdx = obj->X;
								break;
							case 'Y':
								obj->Y = atof(w);
								if (obj->Y > envdy)
									envdy = obj->Y;
								break;
							case 'h':
								obj->h = atof(w);
								break;
							case 'r':
								obj->r = atof(w);
								break;
							case 'c':
								if (color >= 0 && color < 3)
									obj->c[color] = atof(w);
								color++;
								break;
							case 'n':
								obj->nind = atoi(w);
								obj->subtype = 10;		// cilindro posizionato sul robot
								break;
							default:
								sprintf(message,"unknown env. parameter %c%s in file evorobot.env",c,w);
								display_warning(message);
								break;
							}
						}
					}
				}
				else 
				{
					if (selected!=2) {	// modificato per aggiungere envdim		
						sprintf(st,"unknown object: %s", word);
						display_warning(st);
					}
				}
				n++;
			}
		}
		fclose(fp);
		if(envnobjs[BAR]==0) { //se ci sono sbarre nell'ambiente non carica questo messaggio
			sprintf(message,"loaded %d objects from file: evorobot.env", n);
			display_stat_message(message);
		}
	}
	else
	{
		sprintf(message,"Error: the file evorobot.env could not be opended");
		display_error(message);
	}

	// se esistono sbarre determina la lunghezza di ognuna di esse
	// e memorizza la posizione di partenza
	if(envnobjs[BAR]>0) {
		for(i=0, obj = *(envobjs + BAR);i<envnobjs[BAR];i++, obj++)	{		// tutte le sbarre
			obj->h=mdist(obj->x,obj->y,obj->X,obj->Y);	// determina la lunghezza della sbarra
			// memorizza la posizione di partenza di tutte le sbarre
			mem_bar_orig[i][0]=obj->x;			
			mem_bar_orig[i][1]=obj->y;			
			mem_bar_orig[i][2]=obj->X;			
			mem_bar_orig[i][3]=obj->Y;			
		}
	}

	// calculate world size (if not specified manually)
	if (envdx == 0 || envdy == 0)
	{
		for(i=0,obj = *envobjs; i < envnobjs[SWALL]; i++, obj++)
		{
			if (obj->x > envdx) envdx = obj->x;
			if (obj->y > envdy) envdy = obj->y;
			if (obj->X > envdx) envdx = obj->X;
			if (obj->Y > envdy) envdy = obj->Y;
		} 

		for(i=0,obj = *envobjs; i < envnobjs[WALL]; i++, obj++)
		{
			if (obj->x > envdx) envdx = obj->x;
			if (obj->y > envdy) envdy = obj->y;
			if (obj->X > envdx) envdx = obj->X;
			if (obj->Y > envdy) envdy = obj->Y;
		} 
	}     
}


/*
 * save the environment description into a file evorobot.env
 */
void 
save_world(char *filename)

{


     FILE   *fp;
     int    i;
     int    ii;
     struct envobject **cenvobjs;
     struct envobject *obj;

     if ((fp=fopen(filename, "w")) != NULL)
      {
       cenvobjs = envobjs;
       for (i=0; i < NOBJS; i++)       
       {
        for (ii=0, obj = *(cenvobjs + i); ii < envnobjs[i]; ii++, obj++)
        {
          if (obj->type == SWALL)
	   fprintf(fp, "swall x%.1f y%1.f X%.1f Y%.1f h%.1f c%.1f c%.1f c%.1f\n", obj->x, obj->y, obj->X, obj->Y, obj->h, obj->c[0], obj->c[1], obj->c[2]); 
          if (obj->type == SROUND)
	   fprintf(fp, "sround x%.1f y%1.f r%.1f h%.1f c%.1f c%.1f c%.1f\n", obj->x, obj->y, obj->r, obj->h, obj->c[0], obj->c[1], obj->c[2]); 
          if (obj->type == ROUND)
	   fprintf(fp, "round x%.1f y%1.f r%.1f h%.1f c%.1f c%.1f c%.1f\n", obj->x, obj->y, obj->r, obj->h, obj->c[0], obj->c[1], obj->c[2]);
          if (obj->type == GROUND)
	   fprintf(fp, "ground t%d x%.1f y%1.f r%.1f c%.1f\n", obj->subtype, obj->x, obj->y, obj->r, obj->c[0]);
          if (obj->type == LIGHT)
	   fprintf(fp, "light x%.1f y%1.f r%.1f h%.1f\n", obj->x, obj->y, obj->r, obj->h);
          if (obj->type == ROBOT)
	   fprintf(fp, "robot c%.1f c%.1f c%.1f\n", obj->c[0], obj->c[1], obj->c[2]);
          if (obj->type == WALL)
	   fprintf(fp, "wall x%.1f y%1.f X%.1f Y%.1f h%.1f c%.1f c%.1f c%.1f\n", obj->x, obj->y, obj->X, obj->Y, obj->h, obj->c[0], obj->c[1], obj->c[2]); 
          if (obj->type == CYLINDER)
	   fprintf(fp, "cylinder x%.1f y%1.f r%.1f h%.1f c%.1f c%.1f c%.1f\n", obj->x, obj->y, obj->r, obj->h, obj->c[0], obj->c[1], obj->c[2]); 
        }
       }
       display_stat_message("saved current environment in file evorobot.env");
       fclose(fp);
      }
     else
      {
       display_warning("unable to open file evorobot.env");
      }

}

/*
 * load an obstacle from a .sam file
 */
void
load_obstacle(char *filename, int **object, int  *objectcf)

{

   int  **ob;
   int  *o;
   FILE *fp;
   int  i,v,s;
   char sbuffer[128];

   if ((fp = fopen(filename,"r")) != NULL)
	  {
	  }
	  else
	  {
	    sprintf(sbuffer,"I cannot open file %s", filename);
	    display_error(sbuffer);
	  }
   /*
    * we read the object configuration data
    * 0-nsensors, 1-nangles, 2-ndistances,3-initdist,4-distinterval
    */
   for (i=0, o=objectcf; i < 5; i++, o++)
     fscanf(fp,"%d ",o);
   fscanf(fp,"\n",o);

   for (v=0, ob=object; v < objectcf[2]; v++, ob++)
    {
     fscanf(fp,"TURN %d\n",&v);
     for (i = 0, o = *ob; i < objectcf[1]; i++)
      {
       for (s=0; s < objectcf[0]; s++,o++)
        {
	   fscanf(fp,"%d ",o);
        }
       fscanf(fp,"\n");
      }
    }
   fscanf(fp, "%s\n",sbuffer);
   if (strcmp(sbuffer,"END") != 0)
     {
      sprintf(sbuffer,"loading file %s: file might be inconsistent", filename);
      display_error(sbuffer);
     }
   fflush(fp);
   fclose(fp);
}


/*
 * save an obstacle in a .sam file
 */
void
save_obstacle(char *filename, int **object, int  *objectcf)

{

   int  **ob;
   int  *o;
   FILE *fp;
   int  i,v,s;
   char sbuffer[64];

   if ((fp = fopen(filename,"w")) != NULL)
	  {
	  }
	  else
	  {
	    sprintf(sbuffer,"I cannot open file %s", filename);
	    display_error(sbuffer);
	  }
   /*
    * we write the object configuration data
    * 0-nsensors, 1-nangles, 2-ndistances,3-initdist,4-distinterval
    */
   for (i=0, o=objectcf; i < 5; i++, o++)
     fprintf(fp,"%d ",*o);
   fprintf(fp,"\n");

   for (v=0, ob=object; v < objectcf[2]; v++, ob++)
    {
     fprintf(fp,"TURN %d\n",v);
     for (i = 0, o = *ob; i < objectcf[1]; i++)
      {
       for (s=0; s < objectcf[0]; s++,o++)
        {
	   fprintf(fp,"%d ",*o);
        }
       fprintf(fp,"\n");
      }
    }
   fprintf(fp, "END\n");
   fflush(fp);
   fclose(fp);
}


/*
 * load motor samples
 * the first line should be the number of samples
 * for each sample we compute 3 simmitrical
 */
void
load_motor()

{

	extern  float **motor;
	float  **mo;
	float  *m;
	FILE   *fp;
	int    v,vv;
#ifdef EVOWINDOWS
	if ((fp = fopen("motor.sam","r")) != NULL)
#endif
#ifdef EVOLINUX
	if ((fp = fopen("../bin/sample_files/motor.sam","r")) != NULL)
#endif
#ifdef EVOMAC
	if ((fp = fopen("motor.sam","r")) != NULL)
#endif
  {
  }
	else
	{
		display_error("cannot open file motor.sam");
	}
	for (v=0,mo = motor; v < 25; v++,mo++)
	{
		for (vv=0,m = *mo; vv < 25; vv++,m++)
		{
			fscanf(fp,"%f\t",m);
			m++;
		}
		fscanf(fp,"\n");
	}
	for (v=0,mo = motor; v < 25; v++,mo++)
		{
			for (vv=0,m = *mo; vv < 25; vv++,m++)
			{
				m++;
				fscanf(fp,"%f\t",m);
			}
			fscanf(fp,"\n");
		}
		fclose(fp);
	}




/*
 * allocate space for sampled objects
 */
void
create_world_space()

{
    int 	**wa;
    int		**ob;
    float	**mo;
    int         *v;
    int         i;
    int         ii;

     /*
     * wall
     * allocate space for 30 distances for each sensor
     */
    wall = (int **) malloc(30 * sizeof(int *));
    wallcf = (int *) malloc(5 * sizeof(int));

    if (wall == NULL || wallcf == NULL)
	 display_error("system error: wall malloc");

    /*
     * allocate space for directions
     */
    wa = wall;
    for (i=0; i<30; i++,wa++)
	  {
	   *wa = (int *)malloc((8 * 180) * sizeof(int));
	   if (*wa == NULL)
	      display_error("system error: wa malloc");
	   for (ii=0,v=*wa; ii < (8 *180); ii++,v++)
		*v = 0;
	  }

    /*
     * round obstacles
     * allocate space for 30 distances for each sensor
     */
     obst = (int **) malloc(30 * sizeof(int *));
     obstcf = (int *) malloc(5 * sizeof(int));

     if (obst == NULL || obstcf == NULL)
	  display_error("system error: obst malloc");
      /*
       * allocate space for directions
       */
      ob = obst;
      for (i=0; i<30; i++,ob++)
	  {
	   *ob = (int *)malloc((8 * 180) * sizeof(int));
	   if (*ob == NULL)
	      display_error("system error: ob malloc");
	   for (ii=0,v=*ob; ii < (8 *180); ii++,v++)
		*v = 0;
	  }
    /*
     * small round obstacles
     * allocate space for 30 distances for each sensor
     */
     sobst = (int **) malloc(30 * sizeof(int *));
     sobstcf = (int *) malloc(5 * sizeof(int));

     if (sobst == NULL || sobstcf == NULL)
	 display_error("system error: sobst malloc");
      /*
       * allocate space for directions
       */
      ob = sobst;
      for (i=0; i<30; i++,ob++)
	  {
	   *ob = (int *)malloc((8 * 180) * sizeof(int));
	   if (*ob == NULL)
	     display_error("system error: ob malloc");
	   for (ii=0,v=*ob; ii < (8 *180); ii++,v++)
		*v = 0;
	  }
    /*
     * light
     * allocate space for 30 distances for each sensor
     */
    light = (int **) malloc(30 * sizeof(int *));
    lightcf = (int *) malloc(5 * sizeof(int));

    if (light == NULL || lightcf == NULL)
	 display_error("system error: light malloc");
      /*
       * allocate space for directions
       */
    ob = light;
    for (i=0; i<30; i++,ob++)
	  {
	   *ob = (int *)malloc((8 * 180) * sizeof(int));
	   if (*ob == NULL)
	    display_error("system error: light malloc");
	   for (ii=0,v=*ob; ii < (8 *180); ii++,v++)
		*v = 0;
	  }
    /*
     * allocate space for 25(v1) different speeds
     */
    motor = (float **) malloc(25 * sizeof(float *));
    if (motor == NULL)
	    display_error("system error: motor malloc");
      /*
       * allocate space for 25(v1) different speeds (ang & dist)
       */
    mo = motor;
    for (i=0; i<25; i++,mo++)
	  {
	   *mo = (float *)malloc((25 * 2) * sizeof(float));
	   if (*mo == NULL)
	    display_error("system error: mo malloc");
	  }


}







