######################################
# Evorobot* makefile #################
######################################

TEMPLATE = app
TARGET = evorobotstar 
DEPENDPATH += ./source_files ./header_files ./resources
INCLUDEPATH += ./source_files ./header_files ./resources
DESTDIR += "../bin"

#######################################
#comment the next 4 lines 
#if you do not need the 3Dgraphics
#or you did not installed QGLViewer yet
#######################################
#INCLUDEPATH += c:/QGLViewer
#CONFIG += qt opengl 
#LIBS *= -Lc:/QGLViewer -lQGLViewer2
#QT+= opengl \
#	 xml

# Input
HEADERS += mode.h \
           public.h \
           robot-env.h \
           ncontroller.h \
           evolution.h \
           mainwindow.h \
           parameters.h \
           rend_param.h \
           rend_controller.h \
           rend_robot.h \
#           rend_robot3d.h \
	   rend_evolution.h \
           epuck.h 
SOURCES += evolution.cpp \
           mainwindow.cpp \
           ncontroller.cpp \
           gcontroller.cpp \
           parameters.cpp \
           rend_param.cpp \
           rend_controller.cpp \
           rend_robot.cpp \
#           rend_robot3d.cpp \
	   rend_evolution.cpp \
           robot-env.cpp \
           simulator.cpp \
           utility.cpp \
           epuck_io.cpp 

RESOURCES += application.qrc
