/*
 * Evorobot* - mode.h
 * Copyright (C) 2009, Stefano Nolfi
 * LARAL, Institute of Cognitive Science and Technologies, CNR, Roma, Italy
 * http://laral.istc.cnr.it

 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */


#define EVOWINDOWS       1   // comment this line to not compile under windows
//#define EVOLINUX		 1   // comment this line to not compile under linux
//#define EVOMAC		     1	 // comment this line to not compile under mac os x
#define EVOGRAPHICS      1   // comment this line to compile without graphics
#define EVO3DGRAPHICS    1   // comment this line to compile without 3dgraphics
//#define EVOLUTION		 1		 // UNDER MAC OS : Evolution is running
//#define EVOLUTION_SOCIAL 1	 // UNDER MAC OS : Social Evolution is running
//#define LABORATORY_TESTING 1	 // UNDER MAC OS : Test in laboratory of individuals

#define KHEPERA          0   // Khepera robot
#define EPUCK            1   // Epuck robot
#define LEGO             2   // Lego robot 3 IR sensors and a NXTCam

#define ROBOTTYPE        LEGO
