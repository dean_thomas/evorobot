/* 
 * Evorobot* - evolution.h
 * Copyright (C) 2009, Stefano Nolfi
 * LARAL, Institute of Cognitive Science and Technologies, CNR, Roma, Italy 
 * http://laral.istc.cnr.it

 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */

#define MAXGENERATIONS   3000        // max number of generations 
#define MAXINDIVIDUALS   1000        // max numer of individuals for each generation 
#define MAXRACES		 10			 // max number of races for each population
#define MAXFIXEDPOS		 2			 // max fixed individual positions

extern int     nreplications;    // n. of replications of the experiment 
extern int     seed;             // seed of the replication
extern double  statfit_min;      // minimum fitness for race
extern double  statfit_max;      // maximum fitness for race
extern int     statfit_n;        // n fitness data
extern double  statfit[MAXGENERATIONS][4]; /*  best and mean fit for each gen. & pop */

int     loadstatistics(char *filew, int mode);
int     loadallg(int gen, char *filew, int first_or_second_genome, int race);
void    display_all_stat();
void    create_lineage();

void getgenome(int n, int t, int first_or_second_genome);
void saveagenotype(FILE *fp, int n, int p);
void getbestgenome(int n, int nn, int p, int domutate);
void putbestgenome(int n, int nn, int p);
int search_bestind_num(int seed, int gen, int race, int *type);
int  loadallg(int gen, char *filew, int first_or_second_genome, int race);
void saveallg(int gen);
void loadfixedg();
int  loadstatistics(char *filew, int mode);
double init_and_evaluate_nf(int gen, int nind, int pop);