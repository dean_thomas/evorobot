/* 
 * Evorobot* - robot-env.h
 * Copyright (C) 2009, Stefano Nolfi 
 * LARAL, Institute of Cognitive Science and Technologies, CNR, Roma, Italy 
 * http://laral.istc.cnr.it

 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
#include "mode.h"

extern struct iparameters *ipar; // individual parameters 
extern int timeadjust;     // used to slowdown the update on the robots or the rendering in simulation       
extern int real;           // whether we use real or simulated robots
extern long int run;       // current step of the trial
extern int manual_start;   // whether robots' position has been manually set by the user
extern float robotradius;  // the radius of the robot's body (KHEPERA RADIUS: 27.5   EPUCK RADIUS: 37.5)
extern float wheelr;		//distance between cnter of the axes and the wheel
extern float sensornoise;  // noise on sensors
extern float max_dist_attivazione;  //distanza massima percezione segnali
extern int  individual_crash;   // a crash occurred between two individuals
extern char ffitness_descp[255];// description of the current fitness function

void set_scale();
void remove_object(char *nobject, int idobject);
extern void connect_disconnect_robots(); 
extern void initialize_robot_position();
extern void initialize_world();
extern void update_motors(struct individual *pind, struct  iparameters *pipar, float *act);

// simulator.cpp
extern double RADtoDGR(double r);
extern double DGRtoRAD(double d);
extern float  SetX(float x,float d,double angle);
extern float  SetY(float y,float d,double angle);
extern double mang(int x, int y, int x1, int y1, double dist);
extern double mangrel(double absolute,double kepera);
extern double mangrel2(double absolute, double kepera);
extern int    read_ground(struct individual *pind);
extern float  read_ground_color(struct individual *pind);
extern int    check_crash_old(float p1,float p2, int team);
extern int    check_crash(float p1,float p2, int team, float *xi, float *yi, int *n, int dead, int ghost);
extern int    check_update_bar(struct  individual  *pind, long int T1, long int T2,float (*pffitness)(struct  individual  *pind));
extern void   reset_pos_from_crash(struct individual *pind);
extern void   reset_pos_from_crash_n_pos_before(struct individual *pind);
extern void   reset_pos_from_crash_around(struct individual *pind, int whereis);
extern void   update_infrared_s(struct individual *pind);
extern void   update_ginfrared_s(struct individual *pind, struct  iparameters *pipar);
extern void   update_lights_s(struct individual *pind);
extern void   update_signal_s(struct individual *pind, struct  iparameters *pipar);
extern void   update_camera(struct individual *pind, struct  iparameters *pipar);
extern void   update_simplecamera(struct individual *pind, struct  iparameters *pipar);
extern void   update_nxtcam(struct individual *pind, struct  iparameters *pipar); //simuler� la camera nxt
extern void   update_RGBColorRetina(struct individual *pind, struct  iparameters *pipar); //simuler� color retina

extern void   update_groundsensors(struct individual *pind, struct  iparameters *pipar);
//	Sensors relating to food
extern void   update_foodSensors(struct individual *pind, struct  iparameters *pipar);
extern void   update_hungerSensors(struct individual *pind, struct  iparameters *pipar);
//	Sensors relating to water
extern void   update_waterSensors(struct individual *pind, struct  iparameters *pipar);
extern void   update_thirstSensors(struct individual *pind, struct  iparameters *pipar);
//	Sensors relating to time of day (day / night)
extern void	update_dayNightSensors(struct individual *pind, struct  iparameters *pipar);
//	Sensors relating to the age of robots
extern void	update_ageSensors(struct individual *pind, struct  iparameters *pipar);

extern void   update_deltaground(struct individual *pind, struct  iparameters *pipar);
extern void   update_lastground(struct individual *pind, struct  iparameters *pipar);
extern void   move_robot(struct individual *pind);
extern void   move_lego_robot(struct individual *pind);
extern void   load_world(char *filename);
extern void   save_world(char *filename);
extern void   load_motor();
extern void   create_world_space();
extern void   load_obstacle(char *filename, int **object, int *objectcf);
extern int    intersect_line_circle(float x1,float y1, float x2,float y2, float cx,float cy, float r , float *xi,float *yi);


// epuck_io.cpp
void set_input_real(struct individual *pind, struct  iparameters *pipar);
void setpos_real(struct individual *pind,float o1, float o2);
void update_realsensors();
void perceptionSystem(struct individual *pind, struct  iparameters *pipar, int nsensor, const int GOAL_TYPE);

extern int   nworlds;              // number of different environments     
extern int   random_groundareas;   // randomize ground areas positions
extern int   random_sround;        // randomize sround positions
extern int   random_round;         // randomize round positions   
extern float **motor;              // motor sampling, 12 rows each with 12*2(ang and dist) columnsi
extern int   **wall;               // infrared wall sampling, (180 degrees and 38 distances)
extern int   **obst;               // infrared round obstacle sampling, (180 d. and 38 distances
extern int   **sobst;              // infrared small round obstacle sampling (180 d. and 20 distances
extern int   **light;              // light sampling (180 degrees and 27 distances)
extern int   *wallcf;              // wall sampling configuration                          
extern int   *obstcf;              // obst sampling configuration                          
extern int   *sobstcf;             // sobst sampling configuration                          
extern int   *lightcf;             // light sampling configuration                          


struct individual

{

  int   ninputs;                                /* n. input units                           */
  int   nhiddens;                               /* n. hidden units                          */
  int   noutputs;                               /* n. output units                          */
  float input[50];                              /* the sensors                              */

  float   pos[2];								/* kepera position (center of the body)     */
  double  direction;							/* kepera direction                         */
  float   oldpos[2];							/* kepera old position						*/
  float   noldpos[100][2];						/* kepera n old positions for bar_without_crash */
  double  oldirection;
  float   speed[2];								/* actual speed                             */
  int     crashed;                              // last step in which the robot crashed (-1 = no crash)
  int     jcrashed;                             // whether the robot crashed in the last time step
  int	  bumpsensors[2];						// Bump Sensors				
  float   cifsensors[8];                        /*  infrared sensors                        */
  int     cifang[8];                            //  orientation of each infrared
  int     cifdist[8];							//  distance of eache infrared sensors from the center //onofrio
  float   clightsensors[8];                     /*  light sensors                           */
  float   csignals[4];                          // signal sensors
  float   scamera[5];                           // simple camera
  float   camera[3][360];                       // camera up to 360 photoreceptors * 3 colors
  float   ground[10];                           /* ground sensors state                     */
	
	//Food zone sensor state
	float food[10];
	//Hunger sensors
	float hunger[10];
	//Hunger accumulator - keeps track of how hungry a robot is
	//Values: 0 (Empty) -> hungerThreshold (Full)
	double hungerAccumulator;

	//Water zone sensor state
	float water[10];
	//Thirst sensors
	float thirst[10];
	//Thirst accumulator - keeps track of how thirsty a robot is
	//Values: 0 (Empty) -> thirstThreshold (Full)
	double thirstAccumulator;

	//DayNightSensor
	float dayNight[10];
	
	//Age sensor
	float age[10];


  float   lastground[10];                       // last visited ground
  float   detected_signal[4][2];                //  signals detected from nearby robots and corresponding distance                           
  float   energylevel;                          //  the current energy level                          
  float   wheel_motor[3];                       //  desired speed of wheel motors                           
  float   signal_motor[4];                      //  the signals produced                           
  float   pseudo_motor[10];                     //  pseudo motor used in conjunction with efferent copies
  float   com_out[4];							//  communication output signals
  float   pushing_units[4];						//  pushing units signals
  float   categorization[4];					//  categorization signals
  int     camera_nr;                            //  camera number of photoreceptors
  int     camera_c;                             //  camera number of photoreceptors of different colors
  int     camera_ang;                           //  camera intitial angle
  int     camera_dang;                          //  camera view angle of each photoreceptors
  
  float   trackedBlob[8][3];					//  only for nxtcam, extract position and width of the colored blob
  int     touchedbar;							//  the number of the bar touched by the robot
  int     blocked_after_crash;					//  switch to 1 when the bar is blocked after crash with robot
	
  double  fitness;								/*  current fitness                         */
  double  virtualfitness;						/*  virtual fitness							*/
  double  oldfitness;							/*  fitness up to the last epoch            */
  double  oldcfitness;							/*  fitness up to the last cycle            */
  
  long int	  ncyclessawsomething;				/*  Numbero of cycles in which individual has seen something */ 
  long int    explorationability;				/*  Exploration ability of individual						*/
  long int    mobility;							/*  Mobility of individuals									*/

  int     population;                           /*  current population       */
  int	  race;									/*  Race wich individual belongs to */
  int     n_team;
  int     lifecycle;							/*  lifecycle (within the current epoch)    */
  int     totcycles;                            /*  total lifecycles                        */
  int     current_epoch;						/*  Current EPOCH of individual				*/				
  int     number;								/*  Current individual number				*/
  int     orig_number;							/*  Initial individual number				*/
  float   eatenfood[2];                         // type of food eaten
  int     visited[1];                           // indicate whether it previously visited area1 or 2

  int     dead;                                 // whether an individual is temporary dead
  int	  died_individual;						// whether an individual is eaten by predator and so dead
  int	  ghost_individual;						// whether an individual is ghosted
  int     controlmode;                          // 0=neural_controller 1=behavior_based_controller
  float   clk1;
  float   clk2;
  float   clk3;
  float   bnoise;						     	//implement brownian noise
  int     grid[100][100];						//grid to compute exploration fitness

  int	  *RColorPerceived;						// Red Color Perceived Array
  int	  *GColorPerceived;						// Green Color Perceived Array
  int	  *BColorPerceived;						// Blue Color Perceived Array

  int	  *GrayScaleColorPerceived;				// Gray Scale Color Perceived Array

  int	  RColor;								// componente rossa del colore dell'individuo	
  int	  GColor;								// componente verde del colore dell'individuo	
  int	  BColor;								// componente blu del colore dell'individuo	

  int	  ROrigColor;							// componente rossa di default del colore dell'individuo	
  int	  GOrigColor;							// componente verde di default del colore dell'individuo	
  int	  BOrigColor;							// componente blu di default del colore dell'individuo	

  int	  selected;								// 1 if selected	

  int	  can_see_fake_colors;						// 1 is can see fake color
};


extern struct individual *ind;					// pointer to individuals phenotype
extern int  drawtrace;							// draw robots' trajectory trace for n. cycles  

struct envobject
{
	int type;                   // type of the object (1=wall,2=cylinder,3=ground,4=light,5=landmark)
	int subtype;                // subclass (cylinder 1=r12.5,2=r25,10=on the robot; ground 1=circle,2=square)
	int id;                     // the id of the object
	float x,y;                  // x and y coordinate
	float X,Y;                  // x and y end coordinate  
	float r;                    // radius
	float h;                    // height
	float c[3];                 // rgb color 
	int nind;					// numero dell'individuo su cui � posizionato l'oggetto
};

extern struct envobject **envobjs;    // environment, pointer to object lists
extern int    envnobjs[];             // number of objects for each type
extern int    nobjects;               // max number of objects for each categorgy
extern int    envdx;                  // environment dx
extern int    envdy;                  // environment dy
#define NOBJS    12					  // number of object types
#define SWALL    0
#define SROUND   1
#define ROUND    2
#define GROUND   3
#define LIGHT    4
#define ROBOT    5
#define WALL     6
#define CYLINDER 7
#define BAR		 8					   // tipo di oggetto : sbarra
#define RANDRECTHALLWAY 9			   // tipo di oggetto : corridoio rettangolare per posizioni iniziali random dei robot
#define FOOD 10
#define WATER 11

struct iparameters

{
  // network structure
  int   nhiddens;                               /* number of hidden units                   */
  float wrange;                                 /* weights range                            */
  int   dynrechid;                              /* recurrent connections on the hiddens     */
  int   dynrecout;                              /* recurrent connections on the outputs     */
  int   dynioconn;                              /* input output connections                 */
  int   dyndeltai;                              /* delta on the input units                 */
  int   dyndeltah;                              /* delta on the hidden units                */
  int   dyndeltao;                              /* delta on the output units                */

  // sensors & motors
  int     bumpsensors;							/* bump sensors								*/
  int     bpsensors_startang_sect1;				/* start angle sector 1 of bump sensors		*/
  int     bpsensors_endang_sect1;				/* end angle sector 1 of bump sensors		*/
  int     bpsensors_startang_sect2;				/* start angle sector 2 of bump sensors		*/
  int     bpsensors_endang_sect2;				/* end angle sector 2 of bump sensors		*/
  int     bpsensors_startang_sect3;				/* start angle sector 3 of bump sensors		*/
  int     bpsensors_endang_sect3;				/* end angle sector 3 of bump sensors		*/
  int     bpsensors_startang_sect4;				/* start angle sector 4 of bump sensors		*/
  int     bpsensors_endang_sect4;				/* end angle sector 4 of bump sensors		*/
  int     bumpsensors_display;					/* bump sensors are displayed and width 	*/
  int     nifsensors;							/* number of infrared sensors               */
  int     ifsensorstype;                        /* 0=sampled 1=geometrical infrared sensors */
  int     ifang;                                /* infrared relative angle of the first     */
  int     ifdang;                               /* infrared, delta angle between sensors    */
  int     ifrange;                              /* infrared range in millimeters            */
  int     motmemory;							/* copy motor activation on sensors         */
  
	int     groundsensor;							/* ground sensors                           */
  
  int     a_groundsensor;						/* additional ground sensors (delta)        */
  int     a_groundsensor2;						/* additional ground sensors (prev-state)   */
  int     lightsensors;                         /* light sensors of the opponent's light    */
  int     oppsensor;                            /* sesor of the opponent dist               */
  int     camerareceptors;                      // camera n. of photoreceptors
  int     cameraviewangle;                      // camera angleof view (+- n. degrees)
  int     cameradistance;                       // max distance at which colour are perceived
  int     cameracolors;                         // n. of colors discriminated by camera photoreceptors
  int     input_outputs;                        /* additional output copied back into inputs*/
  int     signal_sensors;                       /* signal sensors                           */
  int     signalss;                             /* signals detected by nearby robots        */
  int     signalso;                             /* signals produced by robots                */
  int	  corvidsstart;							/* pre-arranged corvids start positions */
  int     communications;						/* number of communication outputs */
  int	  dist_com;								/* max communication distance     */
  int	  lfpercepsystemdist;					/* max leaders' and followers' perception system distance */
  int	  onlyfewcanperceive;					/* only few individuals can perceive something binded to this option */
  int	  nperceptualindividuals;				/* number of food zone perceptual individuals */
  int	  perceptualrace;						/* number of perceptual race, 0=all races */
  int	  motionlessrace;						/* number of race which is motion less, 0=no races */
  int	  motionlessgroup_but_nind;				/* number of mobile individual, montion less group */ 
  int	  motionlessgroup_but_nind2;			/* number of second mobile individual, montion less group */ 
  int	  motionlessind;						/* number of ind which is motion less, 0=no individuals */
  int	  Xmotionlessind;						/* X of motion less individual */
  int	  Ymotionlessind;						/* Y of motion less individual */
  int	  FDmotionlessind;						/* Face Direction of motion less individual */
  int	  lab_test_all_seed;					/* testing individual of all seed in laboratory */
  int	  lab_test_startseed;					/* testing individual of all seed in laboratory */
  int	  lab_test_number_seed;					/* Number of seed to test	*/
  int	  lab_test_all_gen;						/* testing individual of all generation in laboratory */
  int	  lab_test_gen_step;					/* testing step of generations in laboratory */
  int	  lab_test_startgen;					/* testing individual of all seed in laboratory */
  int	  lab_test_number_gen;					/* Number of generation to test	*/
  int	  lab_test_all_ind;						/* testing all individuals of all generation in laboratory */
  int	  lab_test_team1;						/* testing individual number 1 */
  int	  lab_test_team1_all_ind_race;			/* testing individuals of race n into team 1 */ 
  int	  lab_test_team2;						/* testing individual number 2 */
  int	  lab_test_team2_number_seed;			/* number of seed of individual 2 */  
  int	  lab_test_subst_startseed;				/* number of seed from which to start */
  int	  lab_test_team2_number_gen;			/* number of generation of individual 2 */ 
  int	  lab_test_subst_with_array;		    /* search in the proper array the number of individual in the team to be substituted */
  int	  lab_test_subst_ind_orig;			    /* number of source individual in the team to be substituted */
  int	  lab_test_subst_ind_dest;			    /* number of destination individual in the team to be substituted */
  int	  lab_test_subst_num;					/* number of individual in the population to insert in the team */						
  int	  lab_test_subst_number_seed;			/* number of seed of the individual to insert in the team */
  int	  lab_test_subst_number_gen;			/* number of generation of the individual to insert in the team */
  int	  lab_test_subst_color;					/* substitution of color with a fake color to be perceived by the substituted individual */
  int	  lab_test_team1_flag;					/* flag of ind team1 */
  int	  lab_test_team2_flag;					/* flag of ind team2 */
  int	  lab_test_team3_flag;					/* flag of ind team3 */
  int	  lab_test_team4_flag;					/* flag of ind team4 */
  int	  lab_test_alternate_team2;				/* alternate team2 visualization */	
  int	  lab_test_team2_all_ind_race;			/* testing individuals of race n into team 2 */ 
  int	  lab_test_all_pairs;					/* testing pairs of individuals */
  int	  lab_test_team3;						/* testing individual number 3 */
  int	  lab_test_team4;						/* testing individual number 4 */
  int	  lab_ntestingteam;						/* number of testing individuals in laboratory */ 
  int	  lab_test_best_team1;					/* testing best individuals for team 1*/
  int	  lab_test_best_team2;					/* testing best individuals for team 2*/
  int	  lab_test_best_team3;					/* testing best individuals for team 3*/
  int	  lab_test_best_team4;					/* testing best individuals for team 4*/
  int	  lab_test_ghost_team1;					/* individual team 1 ghost*/
  int	  lab_test_ghost_team2;					/* individual team 2 ghost*/
  int	  lab_test_ghost_team3;					/* individual team 3 ghost*/
  int	  lab_test_ghost_team4;					/* individual team 4 ghost*/
  int	  lab_show_number;						/* show number of ind into laboratory */
  int	  color_race0;							/* colore della specie 0 */
  int	  color_race1;							/* colore della specie 1 */
  int	  color_race2;							/* colore della specie 2 */
  int	  color_race3;							/* colore della specie 3 */
  int	  test_ind1_with_diff_color;			/* test individual 1 displaying it with a different color */
  int	  test_num_ind1;					    /* test from individual 1 to num individuals displaying them with a different color */
  int	  test_color_ind1;						/* color with which to display from individual 1 */
  int	  test_ind2_with_diff_color;			/* test individual 2 displaying it with a different color */
  int	  test_num_ind2;						/* test from individual 2 to num individuals displaying them with a different color */
  int	  test_color_ind2;						/* color with which to display from individual 2 */
  int	  percepindwithdiffcolor;				/* perceptual individuals with different colors */
  int	  percepindcannotseeother;				/* perceptual individuals cannot see other individuals */
  int	  percepindcannothavefitness;			/* perceptual individuals cannot obtain fitness */ 
  int     probpercepindinheritfromfather;		/* probability of perceptual individuals inheriting father's perceptual property */
  int	  save_leader_followers_percent;		/* save leaders followers percentades */
  int	  leader_cannot_become_follower;		/* leader cannot become follower everywhere */
  int	  pushingunits;							/* number of pushing units */
  int	  categorization;						/* number of categorization outputs */
  int	  binarycom;							/* binary communication and pushing units outputs */
  int	  binarycat;							/* binary categorizazion outputs */
  int	  visible_individual;					/* 1 if each individual is visible by other individuals */
  int	  visible_died_individual;				/* 1 if each dead individual is visible by researcher */
  int     resurrect_after_die;					/* 1 if each individual resurrect after die           */
  int     signalssf;                            /* signal segmented phonemes                */
  int     signalssb;                            /* binary signals                           */
  int     signaltype;                           /* signal type 0=sound 1=led                */
  int     wheelmotors;                          /* presence of wheel motors                 */
  int     compass;                              /* compass sensor                           */
  int     simplevision;                         /* return the angle of visually detected objects */
  int     nutrients;                            /* the eaten nutrients                      */
  int     energy;                               /* encode the time passed since the last visited target area */
  char    ffitness[64];							/* fitness function name                        */
  //NXTCam variables
  int     nxtcam;								/* nxt cam able to extract up to 8 blob, 43 degree of visual field */
  int     rgbcolorretina;						/* RGB Color Cam */
  int	  rgbcolorretinaviewangle;				/* RGB Color Cam View Angle */
  int     rgbcolorretinagrayscale;				/* RGB Color Cam Gray Scale */
  int     rgbcolorretinamanualgrayscale;		/* RGB Color Cam Manual Gray Scale */
  int	  rgbcolorretinamaxdistance;			/* RGB Color Cam Max Distance */
  int	  rgbcolorretinabluedisabledrace;			/* Number of species which has RGB Color Cam disabled */
  int	  orientationcam;						/* Orientation Cam */
  int	  orientationcammaxdist;				/* Orientation Cam Max Perception Distance */
  int	  eyes;									/* Eyes */
  int	  leadersfollowerspercepsystem;			/* perceptual system of leader and follower agents				   */
  
  //	Food
  int	  foodperceptionsystem;					/* perceptual system of food									   */
  int	  foodpercepdist;						/* max food perception system distance all races				   */
  int	  foodpercepdist0;						/* max food perception system distance race 0 (green)			   */
  int	  foodpercepdist1;						/* max food perception system distance race 1 (dark blue)		   */
  int	  foodpercepdist2;						/* max food perception system distance race 2 (cyan)			   */
  int	  foodpercepdist3;						/* max food perception system distance race 3 (yellow)			   */
  
  //int	hungerPerceptionSystem;					/* robots can perceive when they are hungry						   */
  int foodSensor;								/* food sensors								*/
  int hungerSensor;								/* hunger sensors							*/
	int hungerThreshold;

	float hungerIncreaseRate;
	float hungerDecreaseRate;
	
	float hungerIncreaseRate0;
	float hungerDecreaseRate0;

	float hungerIncreaseRate1;
	float hungerDecreaseRate1;

	float hungerIncreaseRate2;
	float hungerDecreaseRate2;

	float hungerIncreaseRate3;
	float hungerDecreaseRate3;
  
	//	Water
  int	  waterperceptionsystem;				/* perceptual system of water									   */
  int	  waterpercepdist;						/* max water perception system distance							   */
  int	waterpercepdist0;						/* max water perception system distance race 0 (green)			   */
  int	waterpercepdist1;						/* max water perception system distance race 1 (dark blue)		   */
  int	waterpercepdist2;						/* max water perception system distance race 2 (cyan)			   */
  int	waterpercepdist3;						/* max water perception system distance race 3 (yellow)			   */

  //int	thirstPerceptionSystem;					/* robots can perceive when they are thirsty					   */
  int waterSensor;								/* water sensors							*/
  
  int thirstSensor;								/* thirst sensors							*/
  int thirstThreshold;

	float thirstIncreaseRate;
	float thirstDecreaseRate;

	float thirstIncreaseRate0;
	float thirstDecreaseRate0;

	float thirstIncreaseRate1;
	float thirstDecreaseRate1;

	float thirstIncreaseRate2;
	float thirstDecreaseRate2;

	float thirstIncreaseRate3;
	float thirstDecreaseRate3;

  //	Day / Night cycle
  int	switchDayNightEvery;
  int dayNightSensor;

	//	Sense the age relative to lifetime
	int ageSensor;

  int     drawifsensorray;						/* draw infrared ray			            */
  int	  nxtcolors;							/* number of colors extracted, up to 8 */
  int     nxtcolorslist[8];						/* list of color to be extracte from the image */
  float   clock1;								/* maximum number of clocks=3 freely settable to qa value from 0 to 1 */
  float	  clock2;								/* each clock works as a sawtooth wave/|/| is settable */
  float	  clock3;								/* the value wil be added until clock>1 then clock wil be resetted to 0*/
  int	  brownoise;							/* brownian noise units */
  int	  whitenoise;							/* white noise units uniformly distribuited noise between 0 and 1*/
  int	  motorload;							/* motorload inputs increase with the activation of motr and decrease linaerly*/
  float   gatenoise;							/* probability to miss input data in a single time step*/
  int     sensorsDelay;							/* input delay to simulate control theory problems */
};

void switchDayNight();
