/****************************************************************************
** Meta object code from reading C++ file 'rend_controller.h'
**
** Created: Wed 15. May 18:06:19 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../header_files/rend_controller.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'rend_controller.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_RendNetwork[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      19,   13,   12,   12, 0x0a,
      39,   35,   12,   12, 0x0a,
      58,   52,   12,   12, 0x0a,
      87,   75,   12,   12, 0x0a,
     120,  108,   12,   12, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_RendNetwork[] = {
    "RendNetwork\0\0shape\0setShape(Shape)\0"
    "pen\0setPen(QPen)\0brush\0setBrush(QBrush)\0"
    "antialiased\0setAntialiased(bool)\0"
    "transformed\0setTransformed(bool)\0"
};

void RendNetwork::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        RendNetwork *_t = static_cast<RendNetwork *>(_o);
        switch (_id) {
        case 0: _t->setShape((*reinterpret_cast< Shape(*)>(_a[1]))); break;
        case 1: _t->setPen((*reinterpret_cast< const QPen(*)>(_a[1]))); break;
        case 2: _t->setBrush((*reinterpret_cast< const QBrush(*)>(_a[1]))); break;
        case 3: _t->setAntialiased((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 4: _t->setTransformed((*reinterpret_cast< bool(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData RendNetwork::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject RendNetwork::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_RendNetwork,
      qt_meta_data_RendNetwork, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &RendNetwork::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *RendNetwork::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *RendNetwork::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_RendNetwork))
        return static_cast<void*>(const_cast< RendNetwork*>(this));
    return QWidget::qt_metacast(_clname);
}

int RendNetwork::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 5)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 5;
    }
    return _id;
}
static const uint qt_meta_data_NetworkDialog[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       1,       // signalCount

 // signals: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x05,

 // slots: signature, parameters, type, tag, flags
      30,   14,   14,   14, 0x08,
      50,   14,   14,   14, 0x08,
      67,   14,   14,   14, 0x08,
      85,   14,   14,   14, 0x08,
      98,   14,   14,   14, 0x08,
     115,   14,   14,   14, 0x08,
     131,   14,   14,   14, 0x08,
     146,   14,   14,   14, 0x08,
     163,   14,   14,   14, 0x08,
     175,   14,   14,   14, 0x08,
     188,   14,   14,   14, 0x08,
     201,   14,   14,   14, 0x08,
     208,   14,   14,   14, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_NetworkDialog[] = {
    "NetworkDialog\0\0dialogClosed()\0"
    "set_neurondisplay()\0set_neuronbias()\0"
    "set_neurondelta()\0set_lesion()\0"
    "display_weight()\0display_delta()\0"
    "display_bias()\0display_blocks()\0"
    "erase_net()\0add_ublock()\0add_cblock()\0"
    "open()\0save()\0"
};

void NetworkDialog::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        NetworkDialog *_t = static_cast<NetworkDialog *>(_o);
        switch (_id) {
        case 0: _t->dialogClosed(); break;
        case 1: _t->set_neurondisplay(); break;
        case 2: _t->set_neuronbias(); break;
        case 3: _t->set_neurondelta(); break;
        case 4: _t->set_lesion(); break;
        case 5: _t->display_weight(); break;
        case 6: _t->display_delta(); break;
        case 7: _t->display_bias(); break;
        case 8: _t->display_blocks(); break;
        case 9: _t->erase_net(); break;
        case 10: _t->add_ublock(); break;
        case 11: _t->add_cblock(); break;
        case 12: _t->open(); break;
        case 13: _t->save(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData NetworkDialog::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject NetworkDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_NetworkDialog,
      qt_meta_data_NetworkDialog, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &NetworkDialog::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *NetworkDialog::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *NetworkDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_NetworkDialog))
        return static_cast<void*>(const_cast< NetworkDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int NetworkDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    }
    return _id;
}

// SIGNAL 0
void NetworkDialog::dialogClosed()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
QT_END_MOC_NAMESPACE
