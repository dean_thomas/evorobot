/****************************************************************************
** Meta object code from reading C++ file 'rend_network.h'
**
** Created: gio 16. apr 22:47:48 2009
**      by: The Qt Meta Object Compiler version 59 (Qt 4.1.0)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../header_files/rend_network.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'rend_network.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 59
#error "This file was generated using the moc from 4.1.0. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

static const uint qt_meta_data_RendNetwork[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
       5,   10, // methods
       0,    0, // properties
       0,    0, // enums/sets

 // slots: signature, parameters, type, tag, flags
      19,   13,   12,   12, 0x0a,
      39,   35,   12,   12, 0x0a,
      58,   52,   12,   12, 0x0a,
      87,   75,   12,   12, 0x0a,
     120,  108,   12,   12, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_RendNetwork[] = {
    "RendNetwork\0\0shape\0setShape(Shape)\0pen\0setPen(QPen)\0brush\0"
    "setBrush(QBrush)\0antialiased\0setAntialiased(bool)\0transformed\0"
    "setTransformed(bool)\0"
};

const QMetaObject RendNetwork::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_RendNetwork,
      qt_meta_data_RendNetwork, 0 }
};

const QMetaObject *RendNetwork::metaObject() const
{
    return &staticMetaObject;
}

void *RendNetwork::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_RendNetwork))
	return static_cast<void*>(const_cast<RendNetwork*>(this));
    return QWidget::qt_metacast(_clname);
}

int RendNetwork::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: setShape(*reinterpret_cast< Shape*>(_a[1])); break;
        case 1: setPen(*reinterpret_cast< QPen*>(_a[1])); break;
        case 2: setBrush(*reinterpret_cast< QBrush*>(_a[1])); break;
        case 3: setAntialiased(*reinterpret_cast< bool*>(_a[1])); break;
        case 4: setTransformed(*reinterpret_cast< bool*>(_a[1])); break;
        }
        _id -= 5;
    }
    return _id;
}
static const uint qt_meta_data_NetworkDialog[] = {

 // content:
       1,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   10, // methods
       0,    0, // properties
       0,    0, // enums/sets

 // signals: signature, parameters, type, tag, flags
      15,   14,   14,   14, 0x05,

 // slots: signature, parameters, type, tag, flags
      30,   14,   14,   14, 0x08,
      50,   14,   14,   14, 0x08,
      67,   14,   14,   14, 0x08,
      85,   14,   14,   14, 0x08,
      98,   14,   14,   14, 0x08,
     115,   14,   14,   14, 0x08,
     131,   14,   14,   14, 0x08,
     146,   14,   14,   14, 0x08,
     163,   14,   14,   14, 0x08,
     175,   14,   14,   14, 0x08,
     188,   14,   14,   14, 0x08,
     201,   14,   14,   14, 0x08,
     208,   14,   14,   14, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_NetworkDialog[] = {
    "NetworkDialog\0\0dialogClosed()\0set_neurondisplay()\0set_neuronbias()\0"
    "set_neurondelta()\0set_lesion()\0display_weight()\0display_delta()\0"
    "display_bias()\0display_blocks()\0erase_net()\0add_ublock()\0"
    "add_cblock()\0open()\0save()\0"
};

const QMetaObject NetworkDialog::staticMetaObject = {
    { &QDialog::staticMetaObject, qt_meta_stringdata_NetworkDialog,
      qt_meta_data_NetworkDialog, 0 }
};

const QMetaObject *NetworkDialog::metaObject() const
{
    return &staticMetaObject;
}

void *NetworkDialog::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_NetworkDialog))
	return static_cast<void*>(const_cast<NetworkDialog*>(this));
    return QDialog::qt_metacast(_clname);
}

int NetworkDialog::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QDialog::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        switch (_id) {
        case 0: dialogClosed(); break;
        case 1: set_neurondisplay(); break;
        case 2: set_neuronbias(); break;
        case 3: set_neurondelta(); break;
        case 4: set_lesion(); break;
        case 5: display_weight(); break;
        case 6: display_delta(); break;
        case 7: display_bias(); break;
        case 8: display_blocks(); break;
        case 9: erase_net(); break;
        case 10: add_ublock(); break;
        case 11: add_cblock(); break;
        case 12: open(); break;
        case 13: save(); break;
        }
        _id -= 14;
    }
    return _id;
}

// SIGNAL 0
void NetworkDialog::dialogClosed()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}
