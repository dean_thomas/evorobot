/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created: Wed 15. May 18:06:19 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.4)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../header_files/mainwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.4. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MainWindow[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      21,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      12,   11,   11,   11, 0x08,
      20,   11,   11,   11, 0x08,
      43,   11,   11,   11, 0x08,
      73,   11,   11,   11, 0x08,
      99,   11,   11,   11, 0x08,
     120,   11,   11,   11, 0x08,
     145,   11,   11,   11, 0x08,
     171,   11,   11,   11, 0x08,
     192,   11,   11,   11, 0x08,
     218,   11,   11,   11, 0x08,
     244,   11,   11,   11, 0x08,
     268,   11,   11,   11, 0x08,
     287,   11,   11,   11, 0x08,
     303,   11,   11,   11, 0x08,
     320,   11,   11,   11, 0x08,
     335,   11,   11,   11, 0x08,
     350,   11,   11,   11, 0x08,
     365,   11,   11,   11, 0x08,
     378,   11,   11,   11, 0x08,
     393,   11,   11,   11, 0x08,
     406,   11,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_MainWindow[] = {
    "MainWindow\0\0about()\0qt_n_fixed_evolution()\0"
    "qt_n_fixed_evolution_social()\0"
    "qt_n_variable_evolution()\0"
    "qt_test_individual()\0qt_test_one_individual()\0"
    "qt_test_some_individual()\0"
    "qt_test_population()\0qt_test_best_individual()\0"
    "qt_test_all_individuals()\0"
    "qt_test_all_seed_best()\0qt_test_all_seed()\0"
    "qt_test_alone()\0qt_test_double()\0"
    "qt_test_half()\0qt_userbreak()\0"
    "show_network()\0show_robot()\0show_robot3d()\0"
    "show_param()\0show_evolution()\0"
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MainWindow *_t = static_cast<MainWindow *>(_o);
        switch (_id) {
        case 0: _t->about(); break;
        case 1: _t->qt_n_fixed_evolution(); break;
        case 2: _t->qt_n_fixed_evolution_social(); break;
        case 3: _t->qt_n_variable_evolution(); break;
        case 4: _t->qt_test_individual(); break;
        case 5: _t->qt_test_one_individual(); break;
        case 6: _t->qt_test_some_individual(); break;
        case 7: _t->qt_test_population(); break;
        case 8: _t->qt_test_best_individual(); break;
        case 9: _t->qt_test_all_individuals(); break;
        case 10: _t->qt_test_all_seed_best(); break;
        case 11: _t->qt_test_all_seed(); break;
        case 12: _t->qt_test_alone(); break;
        case 13: _t->qt_test_double(); break;
        case 14: _t->qt_test_half(); break;
        case 15: _t->qt_userbreak(); break;
        case 16: _t->show_network(); break;
        case 17: _t->show_robot(); break;
        case 18: _t->show_robot3d(); break;
        case 19: _t->show_param(); break;
        case 20: _t->show_evolution(); break;
        default: ;
        }
    }
    Q_UNUSED(_a);
}

const QMetaObjectExtraData MainWindow::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow,
      qt_meta_data_MainWindow, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MainWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 21)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 21;
    }
    return _id;
}
static const uint qt_meta_data_Window[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       0,    0, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

       0        // eod
};

static const char qt_meta_stringdata_Window[] = {
    "Window\0"
};

void Window::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    Q_UNUSED(_o);
    Q_UNUSED(_id);
    Q_UNUSED(_c);
    Q_UNUSED(_a);
}

const QMetaObjectExtraData Window::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Window::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_Window,
      qt_meta_data_Window, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Window::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Window::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Window::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Window))
        return static_cast<void*>(const_cast< Window*>(this));
    return QWidget::qt_metacast(_clname);
}

int Window::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    return _id;
}
QT_END_MOC_NAMESPACE
