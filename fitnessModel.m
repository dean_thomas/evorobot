%%  Set up the graph

MAXIMUM_TIME_STEPS = 3000;

fitnessArray = zeros(1,MAXIMUM_TIME_STEPS);

%%  Calculate the values for each possible combination of food and water
%   level
for Food=1:MAXIMUM_TIME_STEPS
   Water = MAXIMUM_TIME_STEPS - Food;
   
   %    Calculate the fintess for the food / water values
   %    Old fitness formula
   %fitness = ((1/4500)*(Food + Water - abs(Food - Water)))*(3/2);
   %    Revised fitness formula (normalised)
   fitness = (Food + Water - abs(Food - Water))/3000;
   
   fitnessArray(Food) = fitness;
end

%%  Plot the fitness graph
plot(fitnessArray);
xlabel('Fitness');
ylabel('Food (Water = 3000 - Food)');
title('Fitness function');